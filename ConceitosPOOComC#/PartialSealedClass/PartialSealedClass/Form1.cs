﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PartialSealedClass
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        //Parcial podemos dividir entre atributos e regras de negocio para não ficar muito extenso o sistema de arquivos
        //e facilitar a manutenção
        public partial class Employee
        {
            public int EmployeeID { get; set; }
            public string Name { get; set; }
            public double Salary { get; set; }
        }

        public partial class Employee
        {
            public void SetValues()
            {
                Name = "Charles";
                EmployeeID = 1;
                Salary = 5000;
            }
        }
        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void btnShow_Click(object sender, EventArgs e)
        {
            Employee emp = new Employee();
            emp.SetValues();
            txtShow.Text += emp.EmployeeID + Environment.NewLine;
            txtShow.Text += emp.Name + Environment.NewLine;
            txtShow.Text += emp.Salary + Environment.NewLine;
        }
    }
}
