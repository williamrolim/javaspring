﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polymorphism
{
    public class Teacher: Employee
    {
        public string EmployeePosition { get; set; }

        public void SetValues(int ID, string EmployeeName, double salary, string position)
        {
            EmployeeId = ID;
            Name = EmployeeName;
            EmployeeSalary = salary;
            EmployeePosition = position;
        }
        public string GetValues()
        {
            string text = "Employee ID: " + EmployeeId + Environment.NewLine;
            text += "Employee Name:" + Name + Environment.NewLine;
            text += "Employee Salary: " + EmployeeSalary + Environment.NewLine;
            text += "Employee Position: " + EmployeePosition + Environment.NewLine;
            return text;
        }
    }
}
