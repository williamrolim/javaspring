﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polymorphism
{
    public class Employee
    {
        public int EmployeeId { get; set; }
        public string Name { get; set; }
        public double EmployeeSalary { get; set; }

        public void SetValues(int ID)
        {
            EmployeeId = ID;
        }

        public void SetValues(int ID, string EmployeeName) //Sobrecarga De Metodos
        {
            EmployeeId = ID;
            Name = EmployeeName;
        }

        public void SetValues(int ID, string EmployeeName,double salary)//Sobrecarga De Metodos
        {
            EmployeeId = ID;
            Name = EmployeeName;
            EmployeeSalary = salary;
        }

    }
}
