﻿
namespace Polymorphism
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnParameter = new System.Windows.Forms.Button();
            this.txtParameters = new System.Windows.Forms.TextBox();
            this.btn2Parameter = new System.Windows.Forms.Button();
            this.btn3Parameter = new System.Windows.Forms.Button();
            this.btn4Parameter = new System.Windows.Forms.Button();
            this.btnVirtual = new System.Windows.Forms.Button();
            this.btnOverride = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnParameter
            // 
            this.btnParameter.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnParameter.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnParameter.Location = new System.Drawing.Point(244, 12);
            this.btnParameter.Name = "btnParameter";
            this.btnParameter.Size = new System.Drawing.Size(108, 36);
            this.btnParameter.TabIndex = 0;
            this.btnParameter.Text = "1Parameter";
            this.btnParameter.UseVisualStyleBackColor = true;
            this.btnParameter.Click += new System.EventHandler(this.btnParameter_Click);
            // 
            // txtParameters
            // 
            this.txtParameters.Location = new System.Drawing.Point(12, 12);
            this.txtParameters.Multiline = true;
            this.txtParameters.Name = "txtParameters";
            this.txtParameters.Size = new System.Drawing.Size(213, 287);
            this.txtParameters.TabIndex = 1;
            // 
            // btn2Parameter
            // 
            this.btn2Parameter.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btn2Parameter.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btn2Parameter.Location = new System.Drawing.Point(244, 54);
            this.btn2Parameter.Name = "btn2Parameter";
            this.btn2Parameter.Size = new System.Drawing.Size(108, 36);
            this.btn2Parameter.TabIndex = 2;
            this.btn2Parameter.Text = "2Parameter";
            this.btn2Parameter.UseVisualStyleBackColor = true;
            this.btn2Parameter.Click += new System.EventHandler(this.btn2Parameter_Click);
            // 
            // btn3Parameter
            // 
            this.btn3Parameter.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btn3Parameter.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btn3Parameter.Location = new System.Drawing.Point(244, 96);
            this.btn3Parameter.Name = "btn3Parameter";
            this.btn3Parameter.Size = new System.Drawing.Size(108, 36);
            this.btn3Parameter.TabIndex = 3;
            this.btn3Parameter.Text = "3Parameter";
            this.btn3Parameter.UseVisualStyleBackColor = true;
            this.btn3Parameter.Click += new System.EventHandler(this.btn3Parameter_Click);
            // 
            // btn4Parameter
            // 
            this.btn4Parameter.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btn4Parameter.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btn4Parameter.Location = new System.Drawing.Point(244, 138);
            this.btn4Parameter.Name = "btn4Parameter";
            this.btn4Parameter.Size = new System.Drawing.Size(108, 36);
            this.btn4Parameter.TabIndex = 4;
            this.btn4Parameter.Text = "4Parameter";
            this.btn4Parameter.UseVisualStyleBackColor = true;
            this.btn4Parameter.Click += new System.EventHandler(this.btn4Parameter_Click);
            // 
            // btnVirtual
            // 
            this.btnVirtual.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnVirtual.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnVirtual.Location = new System.Drawing.Point(244, 180);
            this.btnVirtual.Name = "btnVirtual";
            this.btnVirtual.Size = new System.Drawing.Size(108, 36);
            this.btnVirtual.TabIndex = 5;
            this.btnVirtual.Text = "Virtual";
            this.btnVirtual.UseVisualStyleBackColor = true;
            this.btnVirtual.Click += new System.EventHandler(this.btnVirtual_Click);
            // 
            // btnOverride
            // 
            this.btnOverride.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnOverride.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnOverride.Location = new System.Drawing.Point(244, 221);
            this.btnOverride.Name = "btnOverride";
            this.btnOverride.Size = new System.Drawing.Size(108, 36);
            this.btnOverride.TabIndex = 6;
            this.btnOverride.Text = "Override";
            this.btnOverride.UseVisualStyleBackColor = true;
            this.btnOverride.Click += new System.EventHandler(this.btnOverride_Click);
            // 
            // btnClear
            // 
            this.btnClear.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnClear.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnClear.Location = new System.Drawing.Point(244, 263);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(108, 36);
            this.btnClear.TabIndex = 7;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(364, 308);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnOverride);
            this.Controls.Add(this.btnVirtual);
            this.Controls.Add(this.btn4Parameter);
            this.Controls.Add(this.btn3Parameter);
            this.Controls.Add(this.btn2Parameter);
            this.Controls.Add(this.txtParameters);
            this.Controls.Add(this.btnParameter);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnParameter;
        private System.Windows.Forms.TextBox txtParameters;
        private System.Windows.Forms.Button btn2Parameter;
        private System.Windows.Forms.Button btn3Parameter;
        private System.Windows.Forms.Button btn4Parameter;
        private System.Windows.Forms.Button btnVirtual;
        private System.Windows.Forms.Button btnOverride;
        private System.Windows.Forms.Button btnClear;
    }
}

