﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polymorphism
{
    public class Employee2
    {
        public int EmployeeId { get; set; }
        public string Name { get; set; }
        public double EmployeeSalary { get; set; }
        public string EmployeePosition { get; set; }

        //determinamos que na classe derivada, um membro VIRTUAL da classe base pode ser sobrescrito.
        public virtual string SetValues(int ID, string EmployeeName, double salary)
        {
            EmployeeId = ID;
            Name = EmployeeName;
            EmployeeSalary = salary;

            string text = "Employee ID: " + EmployeeId + Environment.NewLine;
            text += "Employee Name:" + Name + Environment.NewLine;
            text += "Employee Salary: " + EmployeeSalary + Environment.NewLine;

            return text;
        }
    }
}
