﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polymorphism
{
    public class Teacher2 : Employee2
    {

        override public string SetValues(int ID, string EmployeeName, double salary)
        {
            EmployeeId = ID;
            Name = EmployeeName;
            EmployeeSalary = salary;
            EmployeePosition = "Teacher";

            string text = "Employee ID: " + EmployeeId + Environment.NewLine;
            text += "Employee Name:" + Name + Environment.NewLine;
            text += "Employee Salary:" + EmployeeSalary + Environment.NewLine;
            text += "Employee Position: " + EmployeePosition + Environment.NewLine;

            return text;
        }

    }
}
