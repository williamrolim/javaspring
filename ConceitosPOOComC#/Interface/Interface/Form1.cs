﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Interface
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }

        interface Computer
        {
            void GetDesktops();
            void GetLaptops();
        }

        class DellComputers : Computer, DellComputer
        {
            public string Name { get; set; }

            public string Mark { get; set; }

            public double Price { get; set; }
            public void GetDesktops()
            {
                Name = "Desktop1";
                Price = 2000;

                }

            public void GetLaptops()
            {
                Name = "Laptop1";
                Price = 1500;
            }

            public void GetMark()
            {
                Mark = "Dell";
            }

            interface DellComputer {
                void GetMark();
            }
        }

        private void btnShow_Click(object sender, EventArgs e)
        {
            DellComputers dell = new DellComputers();
            dell.GetLaptops();
            string textvalue = "Name : " + dell.Name + Environment.NewLine;
            textvalue += "Price: " + dell.Price + Environment.NewLine;

            dell.GetDesktops();

           string textvalue2 = "Name : " + dell.Name + Environment.NewLine;
            textvalue2 += "Price: " + dell.Price + Environment.NewLine;
            dell.GetMark();
            textvalue += "Mark3: " + dell.Mark + Environment.NewLine;

            textBox1.Text = textvalue + textvalue2;

        }
    }
}
