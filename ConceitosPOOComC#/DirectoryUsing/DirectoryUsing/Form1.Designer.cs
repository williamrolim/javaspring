﻿
namespace DirectoryUsing
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Create = new System.Windows.Forms.Button();
            this.btnMove = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnTempoDeAcesso = new System.Windows.Forms.Button();
            this.btnTempoDeCriacao = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Create
            // 
            this.Create.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.Create.Location = new System.Drawing.Point(21, 12);
            this.Create.Name = "Create";
            this.Create.Size = new System.Drawing.Size(109, 42);
            this.Create.TabIndex = 0;
            this.Create.Text = "Create";
            this.Create.UseVisualStyleBackColor = true;
            this.Create.Click += new System.EventHandler(this.Create_Click);
            // 
            // btnMove
            // 
            this.btnMove.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnMove.Location = new System.Drawing.Point(21, 60);
            this.btnMove.Name = "btnMove";
            this.btnMove.Size = new System.Drawing.Size(109, 42);
            this.btnMove.TabIndex = 1;
            this.btnMove.Text = "Move";
            this.btnMove.UseVisualStyleBackColor = true;
            this.btnMove.Click += new System.EventHandler(this.btnMove_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnDelete.Location = new System.Drawing.Point(21, 108);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(109, 42);
            this.btnDelete.TabIndex = 2;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnTempoDeAcesso
            // 
            this.btnTempoDeAcesso.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnTempoDeAcesso.Location = new System.Drawing.Point(21, 156);
            this.btnTempoDeAcesso.Name = "btnTempoDeAcesso";
            this.btnTempoDeAcesso.Size = new System.Drawing.Size(109, 42);
            this.btnTempoDeAcesso.TabIndex = 3;
            this.btnTempoDeAcesso.Text = "T.A";
            this.btnTempoDeAcesso.UseVisualStyleBackColor = true;
            this.btnTempoDeAcesso.Click += new System.EventHandler(this.btnTempoDeAcesso_Click);
            // 
            // btnTempoDeCriacao
            // 
            this.btnTempoDeCriacao.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnTempoDeCriacao.Location = new System.Drawing.Point(21, 204);
            this.btnTempoDeCriacao.Name = "btnTempoDeCriacao";
            this.btnTempoDeCriacao.Size = new System.Drawing.Size(109, 42);
            this.btnTempoDeCriacao.TabIndex = 4;
            this.btnTempoDeCriacao.Text = "T.C";
            this.btnTempoDeCriacao.UseVisualStyleBackColor = true;
            this.btnTempoDeCriacao.Click += new System.EventHandler(this.btnTempoDeCriacao_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(151, 260);
            this.Controls.Add(this.btnTempoDeCriacao);
            this.Controls.Add(this.btnTempoDeAcesso);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnMove);
            this.Controls.Add(this.Create);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button Create;
        private System.Windows.Forms.Button btnMove;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnTempoDeAcesso;
        private System.Windows.Forms.Button btnTempoDeCriacao;
    }
}

