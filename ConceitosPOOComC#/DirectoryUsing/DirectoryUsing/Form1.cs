﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace DirectoryUsing
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Create_Click(object sender, EventArgs e)
        {
            //Directory.CreateDirectory("temp");
            //DirectoryInfo dr = new DirectoryInfo("temp2");

            //DirectoryInfo dr = new DirectoryInfo(@"C:\temp2");
            //dr.Create();
            if (!Directory.Exists("folder2"))
            {
                Directory.CreateDirectory("folder2");
            }else
            {
                MessageBox.Show("A pasta já existe");

            }

        }

        private void btnMove_Click(object sender, EventArgs e)
        {
            Directory.Move("temp2", @"A:\new folder\temp2");
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (Directory.Exists("folder2"))
            {
                Directory.Delete("folder2");
            }
            else
                MessageBox.Show("Pasta não existe");
            
        }

        private void btnTempoDeAcesso_Click(object sender, EventArgs e)
        {
            DirectoryInfo dr = new DirectoryInfo("folder2");
            string acesstime = dr.LastAccessTime.ToString();
            MessageBox.Show("A ultima vez que a pasta teve acesso foi>> ", acesstime);

        }

        private void btnTempoDeCriacao_Click(object sender, EventArgs e)
        {
            DirectoryInfo dr = new DirectoryInfo("folder2");
            string acesstime = dr.CreationTime.ToString();
            MessageBox.Show("A pasta foi criada>>>> ", acesstime);
        }
    }
}
