﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PathUsing
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnShow_Click(object sender, EventArgs e)
        {
            string orfile = @"A:\Imagens\WallPapers\Windows10\Animais\Tigre.jpg";
            string directpatch = Path.GetDirectoryName(orfile);
            txtInfo.Text += Path.GetFileNameWithoutExtension(orfile) + Environment.NewLine;
            txtInfo.Text += Path.GetExtension(orfile) + Environment.NewLine;
            txtInfo.Text += Path.GetFileName(orfile) + Environment.NewLine;
            txtInfo.Text += Path.GetPathRoot(orfile) + Environment.NewLine;
            txtInfo.Text += Path.GetFullPath(orfile) + Environment.NewLine;
        }
    }
}
