﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExceptionHandling
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnDivide_Click(object sender, EventArgs e)
        {
            try
            {
                int result = Convert.ToInt32(txtFirstNumber.Text) / Convert.ToInt32(txtSecondNumber.Text);
                MessageBox.Show(Convert.ToString(result));
            }
            catch (DivideByZeroException ex)
            {
                MessageBox.Show("Você Não pode dividir um número por Zero");

                throw new Exception("Você Não pode dividir um número por Zero");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Você não pode utilizar letras na operação");

                throw new Exception("Você Não pode usar letras");
            }
            finally
            {
                MessageBox.Show("Programa Finalizado");
            }
        }
    }
}
