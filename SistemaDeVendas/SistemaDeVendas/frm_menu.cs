﻿using System;
using System.Windows.Forms;

namespace SistemaDeVendas
{
    public partial class frm_menu : Form
    {
        public frm_menu()
        {
            InitializeComponent();
        }

        private void btn_cad_produtos_Click(object sender, EventArgs e)
        {
            frm_produtos frm = new frm_produtos();
            frm.Show();
        }

        private void btn_cad_categoria_Click(object sender, EventArgs e)
        {
            frm_categoria frm = new frm_categoria();
            frm.Show();
        }

        private void produtosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frm_produtos frm = new frm_produtos();
            frm.Show();
        }

        private void categoriaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frm_categoria frm = new frm_categoria();
            frm.Show();
        }

        private void btnConnection_Click(object sender, EventArgs e)
        {
            var svr = new Server();
            var isConnected = svr.Connected();

            if (isConnected)
            {
                MessageBox.Show("Connection Succed" , "SQL Connection");
            }
        }
    }
}
