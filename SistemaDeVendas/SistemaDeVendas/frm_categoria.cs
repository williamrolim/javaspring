﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SistemaDeVendas
{
    public partial class frm_categoria : Form
    {
        //SqlConnection conexao;
        SqlCommand comando;
        SqlDataAdapter da;
        SqlDataReader dr;

        string strsql;

        Server s = new Server();




        public frm_categoria()
        {
            InitializeComponent();
            DataGrid();

        }

        public void DataGrid()
        {
           // string conexao = string.Format("Server={0}; Database={1}; UID={2}; PWD={3}; Persist Security Info=true;", s.MyServer, s.Database, s.UserId, s.Password);

            strsql = "SELECT *  FROM TB_categoria";

            DataSet ds = new DataSet();

            da = new SqlDataAdapter(strsql, s.ToString());

            s.Connected();
            da.Fill(ds);

            dataGridView1.DataSource = ds.Tables[0];
        }

        private void frm_categoria_Load(object sender, EventArgs e)
        {
           
        }
    }
}
