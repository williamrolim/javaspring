﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace SistemaDeVendas
{
    public class Server
    {
        public string MyServer { get; set; } = @"DESKTOP-U7D910C\SQLEXPRESS";
        public string Database { get; set; } = "db_sistema_vendas";

        public string UserId { get; set; } = "sa";

        public string Password { get; set; } = "schema";

        public bool Connected()
        {
            try
            {
                var ConString = string.Format("Server={0}; Database={1}; UID={2}; PWD={3}; Persist Security Info=true;", MyServer,Database, UserId, Password);

                var Con = new SqlConnection(ConString);
                Con.Open();
                Con.Close();

                MessageBox.Show("Sucess", "Sucess-Messenge", MessageBoxButtons.OK, MessageBoxIcon.None);

                return true;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error-Messenge", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }
        
        public string ToString()
        {
            string conexao = string.Format("Server={0}; Database={1}; UID={2}; PWD={3}; Persist Security Info=true;", MyServer, Database, UserId, Password);

            return conexao;
        }

    }
}
