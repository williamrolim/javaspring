

var botaoAdicionar = document.querySelector("#adicionar-paciente");

botaoAdicionar.addEventListener("click",function(event) {
    event.preventDefault();

    var form = document.querySelector("#form-adiciona"); /* adicionando a id do formulario  */
    //Extraindo informações do paciente do form
    var paciente = obtemPacientesDoFormulario(form);
    //criar a tr e a td do paciente
    var pacienteTr = montaTr(paciente);

    var erros = validaPaciente(paciente);
    
    console.log(erros);

    if (erros.length > 0){
        exibeMensagensDeErro(erros);
        return;
    }

    adicionaPacienteNaTabela(paciente);
    form.reset();

    var mensagemErro = document.querySelector("#mensagens-erro");
    mensagemErro.innerHTML = "";
});

function obtemPacientesDoFormulario(form){

    //quando declaramos um objeto no Javascript utilizamos o sinal de : para separar propriedades e seus valores.
    var paciente = {
        nome:form.nome.value,
        peso:form.peso.value,
        altura:form.altura.value,
        gordura:form.gordura.value,
        imc:calculaIMC(form.peso.value, form.altura.value)
    }
    return paciente;
}

function montaTr(paciente){
    var pacienteTr = document.createElement("tr");//adiciona "os filhos TDs"
    pacienteTr.classList.add("paciente");

   /*
    var nomeTd = document.createElement("td");
    nomeTd.classList.add("info-nome");
    nomeTd.textContent = paciente.nome;
*/
    pacienteTr.appendChild(montaTd(paciente.nome, "info-nome")); /* todos filhos de tr*/
    pacienteTr.appendChild(montaTd(paciente.peso, "info-peso"));
    pacienteTr.appendChild(montaTd(paciente.altura, "info-altura"));
    pacienteTr.appendChild(montaTd (paciente.gordura, "info-gordura"));
    pacienteTr.appendChild(montaTd(paciente.imc, "info-imc"));

    return pacienteTr;
}

function montaTd(dado,classe){
    var td = document.createElement("td");
    td.classList.add(classe);
    td.textContent = dado;

    return td;
}

    function validaPaciente(paciente){
        var erros = [];
    
        if (paciente.nome.length ==0 ){
            erros.push("O nome não pode ser em branco")
    
        }
        if(!validaPeso(paciente.peso)){
           erros.push("Peso Invalido");
        }
        if(!validaAltura(paciente.altura)){
            erros.push("Altura invalida");
        }
        if(paciente.gordura.length == 0){
            erros.push("A gordura não pode ser em branco");
        }
    
        if (paciente.peso.length == 0){
            erros.push("O peso não pode ser vazio");
        }
    
        if (paciente.altura.length == 0){
            erros.push("A altura não pode ser vazia");
    
        }
        return erros;
    }
    
function exibeMensagensDeErro(erros){
    var ul = document.querySelector("#mensagens-erro");
    ul.innerHTML = ""; // inner html permite controlar o html * toda vez que vou exibir a msg de erro vou apagar as anteriores e colocar as novas
    erros.forEach(function(erro){
        var li = document.createElement("li");
        li.textContent = erro;
        ul.appendChild(li);
    });
}

function adicionaPacienteNaTabela(paciente) {
    var pacienteTr = montaTr(paciente);
    var tabela = document.querySelector("#tabela-pacientes");
    tabela.appendChild(pacienteTr);
}
