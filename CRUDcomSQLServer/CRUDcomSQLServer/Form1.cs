﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace CRUDcomSQLServer
{
    public partial class CRUD : Form
    {
        SqlConnection conexao;
        SqlCommand comando;
        SqlDataAdapter da;
        SqlDataReader dr;

        string strsql;

        public CRUD()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void btn_Novo_Click(object sender, EventArgs e)
        {
            try
            {
                conexao = new SqlConnection("Server=DESKTOP-U7D910C\\SQLEXPRESS;Database=db_Cliente_Teste;User Id=sa;Password=schema;");

                strsql = "INSERT INTO TB_CLIENTE_TESTES (NOME,NUMERO) VALUES (@NOME,@NUMERO)";

                comando = new SqlCommand(strsql, conexao);

                comando.Parameters.AddWithValue("@NOME", txt_Nome.Text);
                comando.Parameters.AddWithValue("@NUMERO", txt_Numero.Text);

                conexao.Open();
                comando.ExecuteNonQuery();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message ,"TESTE");
            }
            finally
            {
                conexao.Close();
                conexao = null;
                comando = null;
            }
           

            

        }

        private void btn_Exibir_Click(object sender, EventArgs e)
        {
            try
            {
                conexao = new SqlConnection("Server=DESKTOP-U7D910C\\SQLEXPRESS;Database=db_Cliente_Teste;User Id=sa;Password=schema;");

                strsql = "SELECT *  FROM TB_CLIENTE_TESTES";

                DataSet ds = new DataSet();

                da = new SqlDataAdapter(strsql, conexao);

                conexao.Open();

                da.Fill(ds);

                dataGridView1.DataSource = ds.Tables[0];
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "TESTE");
            }
            finally
            {
                conexao.Close();
                conexao = null;
                comando = null;
            }
        }


        private void btn_Editar_Click(object sender, EventArgs e)
        {
            try
            {
                conexao = new SqlConnection("Server=DESKTOP-U7D910C\\SQLEXPRESS;Database=db_Cliente_Teste;User Id=sa;Password=schema;");

                strsql = "UPDATE  TB_CLIENTE_TESTES SET NOME = @NOME, NUMERO = @NUMERO WHERE ID = @ID";
                comando = new SqlCommand(strsql, conexao);

                comando.Parameters.AddWithValue("@ID", txt_ID.Text);
                comando.Parameters.AddWithValue("@NOME", txt_Nome.Text);
                comando.Parameters.AddWithValue("@NUMERO", txt_Numero.Text);

                conexao.Open();
                comando.ExecuteNonQuery();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "TESTE");
            }
            finally
            {
                conexao.Close();
                conexao = null;
                comando = null;
            }

        }


        private void btn_Consultar_Click_1(object sender, EventArgs e)
        {
            try
            {
                conexao = new SqlConnection("Server=DESKTOP-U7D910C\\SQLEXPRESS;Database=db_Cliente_Teste;User Id=sa;Password=schema;");

                strsql = "SELECT * FROM TB_CLIENTE_TESTES WHERE ID = @ID";

                comando = new SqlCommand(strsql, conexao);

                comando.Parameters.AddWithValue("@ID", txt_ID.Text);

                conexao.Open();
                dr = comando.ExecuteReader();

                while (dr.Read())
                {
                    txt_Nome.Text = (string)dr["nome"];
                    txt_Numero.Text = Convert.ToString(dr["numero"]);
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "TESTE");
            }
            finally
            {
                conexao.Close();
                conexao = null;
                comando = null;
            }
        }

        private void btn_Excluir_Click(object sender, EventArgs e)
        {
            try
            {
                conexao = new SqlConnection("Server=DESKTOP-U7D910C\\SQLEXPRESS;Database=db_Cliente_Teste;User Id=sa;Password=schema;");

                strsql = " DELETE FROM TB_CLIENTE_TESTES WHERE ID = @ID";

                comando = new SqlCommand(strsql, conexao);

                comando.Parameters.AddWithValue("@ID", txt_ID.Text);
      
                conexao.Open();
                comando.ExecuteNonQuery();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "TESTE");
            }
            finally
            {
                conexao.Close();
                conexao = null;
                comando = null;
            }
        }
    }
}
