class Pontuacao:
    def __init__(self, chute, pontos):
        self.chute = chute
        self.pontos = pontos

    def pontuacao(self):
        return abs(self.chute - self.pontos)