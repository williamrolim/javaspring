numero = 40;
numero2 = 40
print("R$ {:7.2f}".format(numero))
print("R$ {:2d}".format(numero2))

print("Data {:-2d}/{:2d}/{:4d}".format(23,11,2021))
#inverter as datas padrões americanas
print("Ola Sr.{1} {0}".format("Cordeiro","Leonardo"))

#No exemplo abaixo, queremos um número com 7 casas inteiras, sendo uma delas decimal, resultado é:
print("R$ {:6.1f}".format(1000.12))

#No exemplo abaixo, queremos 7 casas inteiras, sendo duas delas decimais
print("R$ {:07.2f}".format(4.11))

nome = 'Matheus'
print(f'Meu nome é {nome.lower()}')

pontos_perdidos = 11 / 3 #3.6666666666666665
#formatando
print("{:7.2f}".format(pontos_perdidos))

print(3 / 2)
print(3 // 2)