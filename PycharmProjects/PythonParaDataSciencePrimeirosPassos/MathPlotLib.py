import matplotlib.pyplot as plt
import random


notas_matematica = []

for notas in range(8):
    notas_matematica.append(random.randrange(0,10))

x = list(range(1, 9)) #gerar oito números na sequencia
y = notas_matematica
plt.title("Notas de matematica")
plt.plot(x,y, marker = 'o')
plt.xlabel("Provas")
plt.ylabel("Notas")
plt.show()
print (notas_matematica)


