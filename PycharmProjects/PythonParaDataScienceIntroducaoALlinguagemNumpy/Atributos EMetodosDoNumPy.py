import numpy as np

dados = np.array([[44410.,  5712., 37123.,     0., 25757.], [ 2003.,  1991.,  1990.,  2019.,  2006.]])


#retorna tupla com dimensões do array
print (dados.shape)

#retorna o número de dimensões do array
print (dados.ndim)

#retorna o número de elemnetos que contem o array
print (dados.size)

#retorna os tipos de elmentos que contem o array
print (dados.dtype)

#retorna o array transposto isto é converte linhas em colunas e vice e versa
print(dados.T)
print( '\n')
print (dados.transpose())

#retorna o array com uma lista python
print(dados.tolist())

arrange = np.arange(12)
print (arrange)

#retorna um array que contem os mesmos dados em uma nova forma
print(arrange.reshape((6,2)))

