import numpy as np

km = [44410., 5712., 37123., 0., 25757.]
anos = [2003, 1991, 1990, 2019, 2006]

km = np.array([44410., 5712., 37123., 0., 25757.])
anos = np.array([2003, 1991, 1990, 2019, 2006])

#possivel com a bliblioteca numpy
idade = 2019 - anos;
print(idade)

#Operaçoces com arrays em duas dimensoes

dados = np.array([km, anos])

print(dados)

kmdados = dados[0] / (2019 - dados[1])
print (kmdados)

#imc = peso / altura ** 2

peso = np.array([70,69,68])
altura = np.array([1.70,1.69,1.68])
imc = peso / (altura ** 2)
print(np.round(imc, 2))

peso = np.array([70,69,68])
altura = np.array([1.70,1.69,1.68])
imc = peso / altura ** 2
print(np.round(imc, 2))