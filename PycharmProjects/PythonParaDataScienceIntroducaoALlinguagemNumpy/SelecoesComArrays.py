import numpy as np

contador = np.arange(10)
#0,1,2,3,4,5,6,7,8,9
item = 6
index = item - 1
#print(contador[index])

#print(contador[-1])

contador = np.arange(10)

print(contador)

print(contador[1:8:2])

km = np.array([44410., 5712., 37123., 0., 25757.])
anos=np.array([2003, 1991, 1990, 2019, 2006])

#possivel com a bliblioteca numpy


#Operaçoces com arrays em duas dimensoes

dados = np.array([km, anos])

print(dados[:, 1:3])
print(dados[:, 1:3][0]/ (2019 - dados [:, 1:3][1])) #: quero as duas linhas

dados = np.array(
    [
        ['Roberto', 'casado', 'masculino'],
        ['Sheila', 'solteiro', 'feminino'],
        ['Bruno', 'solteiro', 'masculino'],
        ['Rita', 'casado', 'feminino']
    ]
)

print(dados[0::2,:2])


