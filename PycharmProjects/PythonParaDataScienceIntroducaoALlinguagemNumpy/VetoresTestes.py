carro = [
    'Jetta Variant',
    'Motor 4.0 Turbo',
    2003,
    44410.0,
    False,
    ['Rodas de liga', 'Travas elétricas', 'Piloto automático'],
    88078.64
]

print ('2003' in carro)
print ('Rodas de liga' in carro)
print ('False' not in carro)
print("***********************************")

Acessorios = ['Rodas de liga', 'Travas elétricas', 'Piloto automático', 'Bancos de couro', 'Ar condicionado', 'Sensor de estacionamento', 'Sensor crepuscular', 'Sensor de chuva']
print(Acessorios[-1])#-1 acesso o ultimo item da lista
print(Acessorios[-2])#-2 acesso o penultimo item da lista

print("***********************************")

Carro_1 = ['Jetta Variant', 'Motor 4.0 Turbo', 2003, 44410.0, False, ['Rodas de liga', 'Travas elétricas', 'Piloto automático'], 88078.64]
Carro_2 = ['Passat', 'Motor Diesel', 1991, 5712.0, False, ['Central multimídia', 'Teto panorâmico', 'Freios ABS'], 106161.94]
Carros = [Carro_1, Carro_2]
print(Carros)

print("***********************************")
'''1) ['A', 'B']'''
'''2) ['C', 'D', 'E']'''
'''3) ['F', 'G', 'H']'''
letras = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H']

print(letras [:2])
print(letras[2:5])
print(letras[2:5])
print(letras[-3:])

print("***********************************")
#1) True
#2) 'Teto panorâmico'
#3) ['Rodas de liga', 'Travas elétricas', 'Piloto automático']
carros = [
    [
        'Jetta Variant',
        'Motor 4.0 Turbo',
        2003,
        False,
        ['Rodas de liga', 'Travas elétricas', 'Piloto automático']
    ],
    [
        'Passat',
        'Motor Diesel',
        1991,
        True,
        ['Central multimídia', 'Teto panorâmico', 'Freios ABS']
    ]
]
print(carros[1][3])
print(carros[1][-1][-2])
print(carros[0][-1])

Acessorios = [
    'Rodas de liga',
    'Travas elétricas',
    'Piloto automático',
    'Bancos de couro',
    'Ar condicionado'
]

#['Airbag',
#'Ar condicionado',
#'Bancos de couro',
#'Piloto automático',
#'Rodas de liga',
#'Vidros elétricos']

Acessorios.append('Airbag')
Acessorios.sort()
Acessorios.pop()
Acessorios.append('Vidros Elétricos')
print(Acessorios)

Acessorios = ['Rodas de liga', 'Travas elétricas', 'Piloto automático', 'Bancos de couro', 'Ar condicionado', 'Sensor de estacionamento', 'Sensor crepuscular', 'Sensor de chuva']
for item in Acessorios:
  print(item)
