from datetime import time

import numpy as np

km = np.arange(11)
print(km)

print(type(km))
print(km.dtype)

kms = np.loadtxt('A:\\Sistemas\\PycharmProjects\\Arquivos\\Python_Data_Science\\Numpy\\data\\carros-km.txt', dtype= int)
print(kms)
print(kms.dtype)

dados = [
    ['Rodas de liga', 'Travas elétricas', 'Piloto automático', 'Bancos de couro', 'Ar condicionado', 'Sensor de estacionamento', 'Sensor crepuscular', 'Sensor de chuva'],
    ['Central multimídia', 'Teto panorâmico', 'Freios ABS', '4 X 4', 'Painel digital', 'Piloto automático', 'Bancos de couro', 'Câmera de estacionamento'],
    ['Piloto automático', 'Controle de estabilidade', 'Sensor crepuscular', 'Freios ABS', 'Câmbio automático', 'Bancos de couro', 'Central multimídia', 'Vidros elétricos']
]
acessorios = np.array(dados)

for list in acessorios:
    for itens in list:
        print(itens)

print(kms.shape) #258 linhas
print(acessorios.shape) #3 linhas e 8 colunas

#np_array = np.arange(1000000)

#py_list = list(range(1000000))

