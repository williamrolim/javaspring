import numpy

dados = [
    ['Rodas de liga', 'Travas elétricas', 'Piloto automático', 'Bancos de couro', 'Ar condicionado', 'Sensor de estacionamento', 'Sensor crepuscular', 'Sensor de chuva'],
    ['Central multimídia', 'Teto panorâmico', 'Freios ABS', '4 X 4', 'Painel digital', 'Piloto automático', 'Bancos de couro', 'Câmera de estacionamento'],
    ['Piloto automático', 'Controle de estabilidade', 'Sensor crepuscular', 'Freios ABS', 'Câmbio automático', 'Bancos de couro', 'Central multimídia', 'Vidros elétricos']
]
acessorios = []

for lista in dados:
    for itens in lista:
        acessorios.append(itens)

print ([itens for lista  in dados for itens in lista])
#print (list(set(acessorios)))
print (set([itens for lista  in dados for itens in lista])) #remover duplicado

dados = [
    ['A', 'B', 'C'],
    ['D', 'E', 'F'],
    ['G', 'H', 'I']
]

result = []
result_2 = []
for lista in dados:
    result_2 += lista
result_2
print(result_2)

# 1º item da lista - Nome do veículo
# 2º item da lista - Ano de fabricação
# 3º item da lista - Veículo é zero km?
dados = [
    ['Jetta Variant', 2003, False],
    ['Passat', 1991, False],
    ['Crossfox', 1990, False],
    ['DS5', 2019, True],
    ['Aston Martin DB4', 2006, False],
    ['Palio Weekend', 2012, False],
    ['A5', 2019, True],
    ['Série 3 Cabrio', 2009, False],
    ['Dodge Jorney', 2019, False],
    ['Carens', 2011, False]
]

zero_km_list = []

for lista in dados:
    if(lista[2] == True):
        zero_km_list.append(lista)

#print("Lista dos carros 0km" , zero_km_list)

#print ([lista for lista in dados if lista[2] == True])

A,B,C = [],[],[]

for lista in dados:
    if (lista[1] <= 2000):
        A.append(lista)
    elif(lista[1] > 2000 and lista[1] <= 2010):
        B.append(lista)
    else:
        C.append(lista)

print(" Lista com carros com anos menor ou igual a 20000 " , A)
print(" Lista com carros com anos maior que 20000 e menor ou igual 2010 " , B)
print(C)