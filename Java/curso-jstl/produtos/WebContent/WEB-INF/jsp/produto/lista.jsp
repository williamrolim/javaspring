<%@page import="br.com.caelum.produtos.modelo.Produto"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<head>
<script type="text/javascript" src="<c:url value="/js/jquery.js"/>"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>

<script type="text/javascript">

	function novoProduto(){
	  window.location.href = "../produto/formulario"
	}

</script>
</head>
<body>
	<script type="text/javascript">
		function removeProduto(id){
			$("#mensagem").load('<c:url value="/produto/remove"/>' + '?produto.id=' + id);
			$("#produto" + id).remove();
		}
	</script>

	<h1>Produtos</h1>
	<h2><fmt:message key = "mensagem.bemvindo"/></h2>
	<div id="mensagem"></div>
	<table width="100%">
		<tr>
		    <td>No.</td>		
			<td width="20%">Nome</td>
			<td>Preco</td>
			<td>Descricao</td>
			<td>Data de Inicio da Venda</td>
			<td>Usado?</td>
			<td width="20%">Remover?</td>
		</tr>
		<c:forEach var="produtos" items="${produtoList}" varStatus="st">
			<tr id="produto${produtos.id }">
				<td>${st.count}</td>
				<td>${produtos.nome.toUpperCase()}</td>
				<td>
				<fmt:formatNumber value="${produtos.preco}" type="currency" /> 
				</td>
				<td>${produtos.descricao}</td>
				<td>                                                               <!-- pattern="EEEE/MMMM/yyyy" dia e mes e ano atuais -->
				<fmt:formatDate value="${produtos.dataInicioVenda.time}" pattern="dd/MM/yyyy"/> 
				</td>
				<c:if test="${produtos.usado }">
				<td>Sim</td>
				</c:if>
				<c:if test="${not produtos.usado }">
				<td>Não</td>
				</c:if>
				<!-- 
				<c:choose>
				<c:when test="${produtos.usado }">
				<td>Sim</td>
				</c:when>
				<c:otherwise>
				<td>Não</td>
				</c:otherwise>
				</c:choose>
				 -->	
								 
				<td><a href="#" onclick="return removeProduto(${produtos.id})">Remover</a></td>

			</tr>
		</c:forEach>
	</table>	
<!--A tag "set" serve para definir uma variável. Ela é útil quando temos um valor que será repetido por todo o programa, e guardar numa variável pode ser importante.

A tag "out" imprime o conteúdo da variável. Seu comportamento é idêntico ao da EL: ${nome}.
<c:set var="nome" value="João da Silva" />
<c:out value="${nome}" /> -->
	<!--<c:url value="/produto/formulario" var="urlAdicionar"/>
		    <a href="<c:url value='/produto/formulario'></c:url>"><fmt:message key="mensagem.novoProduto" /></a>
	
	<a href="${urlAdicionar }">Adicionar um produto </a>-->

 <input type="button" class="btn btn-terciary"  name="enviar" id="enviar" value="Cadastrar novo produto" onclick="novoProduto()" >
<br>	
<c:import url="../_comum/rodape.jsp"></c:import>
</body>
</html>