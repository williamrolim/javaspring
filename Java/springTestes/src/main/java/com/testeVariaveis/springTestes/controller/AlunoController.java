package com.testeVariaveis.springTestes.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.testeVariaveis.springTestes.model.Aluno;
import com.testeVariaveis.springTestes.model.Escolaridade;
import com.testeVariaveis.springTestes.repository.AlunoRepository;

@Controller
public class AlunoController {

	@Autowired
	private AlunoRepository repository;
	
	@GetMapping("index")
	private String salvar(Aluno aluno,Model model) {

		repository.save(aluno);
		return "/index";
	}
	
}
