package com.testeVariaveis.springTestes.model;

public enum Escolaridade {
EnsinoMedio,
Tecnico,
Bacharelado,
Mestrado,
Doutorado;
	
	public Escolaridade[] getEscolaridade(){
		   return Escolaridade.values();
		 }
}
