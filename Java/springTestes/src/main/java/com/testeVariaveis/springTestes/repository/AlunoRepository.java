package com.testeVariaveis.springTestes.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.testeVariaveis.springTestes.model.Aluno;



public interface AlunoRepository extends JpaRepository<Aluno, Integer> {

	
}
