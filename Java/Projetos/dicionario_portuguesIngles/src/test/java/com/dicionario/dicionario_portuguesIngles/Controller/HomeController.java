package com.dicionario.dicionario_portuguesIngles.Controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {

	@GetMapping("/")
	public String paginaIncial() {
		return "main/home";
	}
}
