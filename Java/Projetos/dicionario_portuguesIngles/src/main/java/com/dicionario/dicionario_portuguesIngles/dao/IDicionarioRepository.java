package com.dicionario.dicionario_portuguesIngles.dao;

import org.springframework.data.repository.CrudRepository;

import com.dicionario.dicionario_portuguesIngles.model.Dicionario;

public interface IDicionarioRepository extends CrudRepository<Dicionario, Long> {

}
