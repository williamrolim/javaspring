package com.dicionario.dicionario_portuguesIngles.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {

 @GetMapping("/home")
 public String Inicio(Model model) {
	 return "main/home";
 }
}
