package com.dicionario.dicionario_portuguesIngles.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import com.dicionario.dicionario_portuguesIngles.dao.IDicionarioRepository;
import com.dicionario.dicionario_portuguesIngles.model.Dicionario;

@Controller
public class CadastroController {

	@Autowired
	IDicionarioRepository dicioRepository;
	
	@GetMapping("/cadastro")
	public String cadastro (Dicionario dicio, Model model) {
		model.addAttribute("dicionario",dicio);
		return "cadastro/cadastros";
	}
	
	@PostMapping("/cadastros/save")
	public String cadastrandoPalavra(Dicionario dicio, Model model) {
		dicioRepository.save(dicio);
		return "redirect:/home";
	}
}
