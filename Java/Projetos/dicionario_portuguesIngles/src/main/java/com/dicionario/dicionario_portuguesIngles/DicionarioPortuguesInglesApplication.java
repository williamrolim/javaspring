package com.dicionario.dicionario_portuguesIngles;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DicionarioPortuguesInglesApplication {

	public static void main(String[] args) {
		SpringApplication.run(DicionarioPortuguesInglesApplication.class, args);
	}

}
