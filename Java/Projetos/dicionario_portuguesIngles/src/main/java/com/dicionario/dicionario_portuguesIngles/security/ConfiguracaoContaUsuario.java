package com.dicionario.dicionario_portuguesIngles.security;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
@Configuration
@EnableWebSecurity
public class ConfiguracaoContaUsuario extends WebSecurityConfigurerAdapter {

	@Override
	protected void configure(HttpSecurity http)throws Exception{
		http.authorizeRequests() //a ordem da hierarquia nesse metodo é importante, se colocassemos .permitAll() acima, o spring iria achar que todos teriam acesso e não leria os demais 
		.antMatchers("/", "/**")// tudo apos a barra "/" , será acessada por outros usuarios
		.permitAll()
		.and()
		.formLogin();//suporte a autenticação baseada em formulario
		 http.csrf().disable();
		 http.headers().frameOptions().disable();
	}
}
