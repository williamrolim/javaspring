package com.dicionario.dicionario_portuguesIngles.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class DicionarioIngles {
	
	@Id
	private long idName;
	
	private String name;
	private String meaning;
	private String synonyms;
	
	public DicionarioIngles() {

	}

	public DicionarioIngles(String name, String meaning, String synonyms) {
		this.name = name;
		this.meaning = meaning;
		this.synonyms = synonyms;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMeaning() {
		return meaning;
	}

	public void setMeaning(String meaning) {
		this.meaning = meaning;
	}

	public String getSynonyms() {
		return synonyms;
	}

	public void setSynonyms(String synonyms) {
		this.synonyms = synonyms;
	}
	
}
