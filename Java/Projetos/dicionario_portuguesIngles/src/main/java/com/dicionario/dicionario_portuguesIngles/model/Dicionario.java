package com.dicionario.dicionario_portuguesIngles.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Dicionario {
	
	@Id
	private long idPalavra;
	
	private String palavra;
	private String descricao;
	private String sinonimos;
	
	public Dicionario() {
		// TODO Auto-generated constructor stub
	}
	
	
	
	public Dicionario(String palavra, String descricao, String sinonimos) {
		this.palavra = palavra;
		this.descricao = descricao;
		this.sinonimos = sinonimos;
	}



	public long getIdPalavra() {
		return idPalavra;
	}
	public void setIdPalavra(long idPalavra) {
		this.idPalavra = idPalavra;
	}
	public String getPalavra() {
		return palavra;
	}
	public void setPalavra(String palavra) {
		this.palavra = palavra;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getSinonimos() {
		return sinonimos;
	}
	public void setSinonimos(String sinonimos) {
		this.sinonimos = sinonimos;
	}
	
}


