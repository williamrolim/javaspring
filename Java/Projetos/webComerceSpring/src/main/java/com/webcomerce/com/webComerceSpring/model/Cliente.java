package com.webcomerce.com.webComerceSpring.model;

public class Cliente extends Usuario {
	
	private Integer idUsuario;
	private String nomeCliente;
	private String endereco;
	private int numero;
	private String bairro;
	private String cep;
	private String infoEnvio;
	
	public Integer getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getNomeCliente() {
		return nomeCliente;
	}
	public void setNomeCliente(String nomeCliente) {
		this.nomeCliente = nomeCliente;
	}
	public String getEndereco() {
		return endereco;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	public int getNumero() {
		return numero;
	}
	public void setNumero(int numero) {
		this.numero = numero;
	}
	public String getBairro() {
		return bairro;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public String getCep() {
		return cep;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	public String getInfoEnvio() {
		return infoEnvio;
	}
	public void setInfoEnvio(String infoEnvio) {
		this.infoEnvio = infoEnvio;
	}
	
	
}
