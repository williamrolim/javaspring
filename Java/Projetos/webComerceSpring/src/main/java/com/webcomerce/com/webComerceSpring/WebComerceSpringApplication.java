package com.webcomerce.com.webComerceSpring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebComerceSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebComerceSpringApplication.class, args);
	}

}
