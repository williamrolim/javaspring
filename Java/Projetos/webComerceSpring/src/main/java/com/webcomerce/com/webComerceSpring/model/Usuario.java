package com.webcomerce.com.webComerceSpring.model;

import java.util.Date;

public class Usuario {
	
	private String numeroUsuario;
	private String senha;
	private String statusLogin;//conectado OU desconectado
	private Date data;
	
	public String getNumeroUsuario() {
		return numeroUsuario;
	}
	public void setNumeroUsuario(String numeroUsuario) {
		this.numeroUsuario = numeroUsuario;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public String getStatusLogin() {
		return statusLogin;
	}
	public void setStatusLogin(String statusLogin) {
		this.statusLogin = statusLogin;
	}
	public Date getData() {
		return data;
	}
	public void setData(Date data) {
		this.data = data;
	}
	
	
	
}
