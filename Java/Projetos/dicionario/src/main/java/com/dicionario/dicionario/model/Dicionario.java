package com.dicionario.dicionario.model;

public class Dicionario {

	private long idPalavra;
	private String palavra;
	private String descricao;
	private String sinonimos;
	
	public long getIdPalavra() {
		return idPalavra;
	}
	public void setIdPalavra(long idPalavra) {
		this.idPalavra = idPalavra;
	}
	public String getPalavra() {
		return palavra;
	}
	public void setPalavra(String palavra) {
		this.palavra = palavra;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getSinonimos() {
		return sinonimos;
	}
	public void setSinonimos(String sinonimos) {
		this.sinonimos = sinonimos;
	}
	
	
}
