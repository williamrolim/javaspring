package com.dicionario.dicionario.controller;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {

	@GetMapping("/")
	public String PaginaInicial (Model model) {
		return "main/home";
		
	}
	
}
