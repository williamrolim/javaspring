package com.dicionario.dicionario;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class DicionarioApplicationTests {

public static void main(String[] args) {
	SpringApplication.run(DicionarioApplicationTests.class, args);

}
}
