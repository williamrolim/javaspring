package arrays;

import java.util.ArrayList;

public class ExemplosArrays {
	
	
public static void main(String[] args) {
	

	
	Object[] generico = new Object[5];
    generico[0] = "casa";
    generico[1] = false;
    generico[2] = 2.0;
    generico[3] = 'a';
    generico[4] = 45454554;
    
    for (int i = 0; i < 5;i++) {
    //System.out.println(generico[i]);
    }
    ArrayList lista = new ArrayList();
    lista.add(generico);
    lista.add(generico[0]);
    lista.add(generico[1]);
    lista.add(generico[2]);
    lista.add(generico[3]);
    
    
    System.out.println("******************************************");
    for (Object oRef : lista) {
    System.out.println(oRef);
    }
    
    System.out.println("******************************************");


    
    ArrayList<Carro> listas = new ArrayList<>();
   
    Carro atributos0 = new Carro("azul", "ma�a", "pera", 1995);
    Carro atributos1 = new Carro("azul1", "ma�a1", "pera1", 2000);
    Carro atributos2 = new Carro("azul2", "ma�a2", "pera2", 3000);
    
    listas.add(atributos0);
    listas.add(atributos1);
    listas.add(atributos2);
    
    System.out.println(listas.toString()+ "\n");
    System.out.println("******************************************");

    ArrayList listao = new ArrayList(26); //capacidade inicial
    //LinkedList listao2 = new LinkedList();
    
    listao.add("RJ");
    listao.add("SP");
    listao.add("MG");
    //outros estados
    System.out.println(listao);
    
    boolean existe = listao.contains("RJ");
    System.out.println(existe);
  
    }
}
