package arrays;

public class Carro {

	public String marca;
	public String cor;
	public String modelo;
	public int ano;
	
	
	public Carro(String marca, String cor, String modelo, int ano) {
		super();
		this.marca = marca;
		this.cor = cor;
		this.modelo = modelo;
		this.ano = ano;
	}
	
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public String getCor() {
		return cor;
	}
	public void setCor(String cor) {
		this.cor = cor;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public int getAno() {
		return ano;
	}
	public void setAno(int ano) {
		this.ano = ano;
	}
	
	@Override
	public boolean equals(Object ref) {
		if (this.marca.equals(ref))
		return true;
		else
		return false;
	}

	@Override
	public String toString() {
		return "Carro [marca=" + marca + ", cor=" + cor + ", modelo=" + modelo + ", ano=" + ano + "]";
	}
	
	
	
}
