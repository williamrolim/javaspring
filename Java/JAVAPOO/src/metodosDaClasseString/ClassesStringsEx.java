package metodosDaClasseString;

import java.util.Arrays;

public class ClassesStringsEx {
	public static void main(String[] args) {
		
		//String name = "William";
		
		//String name = "      William    ";
		
		String name = "William;Marcela;Joana";

		//String palavra = name.toUpperCase();
		
		//String palavra = name.toLowerCase();
		
		//char palavra = name.charAt(5); //charAt = pegar caracter especifico
		
		//int palavra = name.indexOf("ll");
		
		// String sub = name.substring(3);
		
		//boolean palavra = name.isEmpty();
		
		//int palavra = name.length();
		
		//String palavra = name.trim();
		
		//boolean palavra = name.contains("ia");
		
		//String palavra = name.replace('m', 'M').trim();
		
		String[] palavra = name.split(";");
		
		StringBuilder builder = new StringBuilder("Socorram");
		builder.append("-");
		builder.append("me");
		builder.append(", ");
		builder.append("subi ");
		builder.append("no ");
		builder.append("�nibus ");
		builder.append("em ");
		builder.append("Marrocos");
		String texto = builder.toString();
		System.out.println(texto);
		
		//System.out.println(palavra);
		//System.out.println(sub);
		System.out.println(Arrays.toString(palavra));
		
		
	}
}
