package colecoesWrappersELambdaExpressoesEX;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ListasTeste {
	
	public static void main(String[] args) {
		  Conta cc1 = new ContaCorrente(22, 33);
	      cc1.deposita(333.0);

	      Conta cc2 = new ContaPoupanca(22, 44);
	      cc2.deposita(444.0);

	      Conta cc3 = new ContaCorrente(22, 11);
	      cc3.deposita(111.0);

	      Conta cc4 = new ContaPoupanca(22, 22);
	      cc4.deposita(222.0);

	      List<Conta> lista = new ArrayList<>();
	      lista.add(cc1);
	      lista.add(cc2);
	      lista.add(cc3);
	      lista.add(cc4);	
	      
	      for (Conta conta : lista) {
			System.out.println(conta);
		}
	      
	      System.out.println("*************************");
	      
	      lista.sort(new ComparandoNumeroDasContas());
	      
	      //Collections.sort(lista, new ComparandoNumeroDasContas());
	      //Collections.shuffle(lista);
	      //lista.sort(null);//ordem natural
	      
	      for (Conta conta : lista) {
			System.out.println(conta);
		}
	    
	      System.out.println("*************************");

	      Collections.reverse(lista);//inverte a lista
	     	      
	      for (Conta conta : lista) {
				System.out.println(conta);
			}
	      System.out.println("*************************");
	      
	      Collections.shuffle(lista);
	      Collections.rotate(lista, 5); //rotacionar 5 posicoes
	}
}

class ComparandoNumeroDasContas implements Comparator<Conta>{

	@Override
	public int compare(Conta c1, Conta c2) {
		return Integer.compare(c1.getNumero(), c2.getNumero());
		//return c1.getNumero() - c2.getNumero(); //se for positivo
		//if (c1.getNumero() < c2.getNumero())
		//return -1;
		//else
		//return +1;
	}
	
}
