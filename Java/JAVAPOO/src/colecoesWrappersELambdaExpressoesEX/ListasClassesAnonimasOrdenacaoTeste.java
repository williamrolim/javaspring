package colecoesWrappersELambdaExpressoesEX;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ListasClassesAnonimasOrdenacaoTeste {
	
	public static void main(String[] args) {
		 Conta cc1 = new ContaCorrente(22, 33);
	      cc1.deposita(333.0);

	      Conta cc2 = new ContaPoupanca(22, 44);
	      cc2.deposita(444.0);

	      Conta cc3 = new ContaCorrente(22, 11);
	      cc3.deposita(111.0);

	      Conta cc4 = new ContaPoupanca(22, 22);
	      cc4.deposita(222.0);

	      List<Conta> lista = new ArrayList<>();
	      lista.add(cc1);
	      lista.add(cc2);
	      lista.add(cc3);
	      lista.add(cc4);	
	      
	      //Function Object

	      lista.sort(new Comparator<Conta>(){ //Classe anononima pasta Bin (volte para lista.sort(null) para sumir

	    	  @Override
	    	  public int compare(Conta c1, Conta c2) {
	    		  return Integer.compare(c1.getNumero(), c2.getNumero());

	    	  			}
	      					}
	    		  				);
//Function Object
	      //Perfeito, um objeto que criamos para encapsular uma fun��o ou m�todo. As classes an�nimas facilitam um pouco a cria��o desses objetos.
	      
	      Comparator<Conta>comp = new Comparator<Conta>() {//Classe anononima pasta Bin visualiza��o navigator

		    	  @Override
		    	  public int compare(Conta c1, Conta c2) {
		    		  return Integer.compare(c1.getNumero(), c2.getNumero());

		    	  			}
		    		  				
		};
	      
	
	      for (Conta conta : lista) {
				System.out.println(conta);
			}
	      System.out.println("*************************");
	}
	}
	
	 
