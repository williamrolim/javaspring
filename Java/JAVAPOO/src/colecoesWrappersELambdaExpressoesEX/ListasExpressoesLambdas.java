package colecoesWrappersELambdaExpressoesEX;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


public class ListasExpressoesLambdas {
	public static void main(String[] args) {
		   Conta cc1 = new ContaCorrente(22, 33);
	        Cliente clienteCC1 = new Cliente();
	        clienteCC1.setNome("Nico");
	        cc1.setTitular(clienteCC1);
	        cc1.deposita(333.0);

	        Conta cc2 = new ContaPoupanca(22, 44);
	        Cliente clienteCC2 = new Cliente();
	        clienteCC2.setNome("Bismarck");
	        cc2.setTitular(clienteCC2);
	        cc2.deposita(444.0);

	        Conta cc3 = new ContaCorrente(22, 11);
	        Cliente clienteCC3 = new Cliente();
	        clienteCC3.setNome("Juliana");
	        cc3.setTitular(clienteCC3);
	        cc3.deposita(111.0);

	        Conta cc4 = new ContaPoupanca(22, 22);
	        Cliente clienteCC4 = new Cliente();
	        clienteCC4.setNome("Biharck");
	        cc4.setTitular(clienteCC4);
	        cc4.deposita(222.0);

	        List<Conta> lista = new ArrayList<>();
	        lista.add(cc1);
	        lista.add(cc2);
	        lista.add(cc3);
	        lista.add(cc4);

	      lista.sort((c1, c2) -> Integer.compare(c1.getNumero(), c2.getNumero()) );//Expressoes Lambdas

	      System.out.println("*****************************************************************************");
	      Comparator<Conta> comp =  (Conta c1, Conta c2) -> {
	    		String compare01 = c1.getTitular().getNome();
				String compare02 = c2.getTitular().getNome();
				return compare01.compareTo(compare02);
		    		  				
		};
	      
		lista.forEach((conta) -> System.out.println(conta + " , " + conta.getTitular().getNome()));
		
		List<String> nomes = new ArrayList<>();
		nomes.add("Super Mario");
		nomes.add("Yoshi"); 
		nomes.add("Donkey Kong"); 
		
	      System.out.println("*****************************************************************************");

		Collections.sort(nomes, new Comparator<String>() {
//Tamb�m � poss�vel usar o m�todo sort da lista. Repare tamb�m que o return � opcional (desde que tiremos tamb�m o {} ):
		    @Override
		    public int compare(String s1, String s2) {
		        return s1.length() - s2.length();
		    }
		});
		
		//Collections.sort(nomes, (s1, s2) ->  s1.length() - s2.length());

		System.out.println(nomes);
	
	}
	}

