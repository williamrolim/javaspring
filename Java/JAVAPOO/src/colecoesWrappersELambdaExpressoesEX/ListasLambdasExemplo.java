package colecoesWrappersELambdaExpressoesEX;

import java.util.ArrayList;
import java.util.List;

public class ListasLambdasExemplo {
public static void main(String[] args) {
	
	List<String> nomes = new ArrayList<>();
	nomes.add("Super Mario");
	nomes.add("Yoshi"); 
	nomes.add("Donkey Kong"); 

	for(int i = 0; i < nomes.size(); i++) {
	    System.out.println(nomes.get(i));
	}
	
	System.out.println("**************************");
	
	nomes.forEach((nome) -> System.out.println(nome));
}
}
