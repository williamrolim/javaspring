import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;


public class Main {


		 static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
		 static final String BD_URL = "jdbc:mysql://localhost:3306/cadastrocultural?useTimezone=true&serverTimezone=UTC";



		    //  Database credentials
		    static final String USER = "WilliamRolim";
		    static final String PASS = "sq3l@nd1A54Ever";
		    
		    public static void main(String[] args) {
			Connection conn = null;
			Statement stmt = null;
			try {
				// STEP 2: Register JDBC driver
				Class.forName("com.mysql.jdbc.Driver");

				//erro do fuso horario m
				// STEP 3: Open a connection
				System.out.println("Connecting to a selected database...");
				
				                                      //erro do fuso horario mysql resolvendo
				conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/cadastrocultural?useTimezone=true&serverTimezone=UTC", "WilliamRolim", "sq3l@nd1A54Ever");
				System.out.println("Connected database successfully...");

				// STEP 4: Execute a query
				System.out.println("Creating table in given database...");
				stmt = conn.createStatement();

				String sql = "CREATE TABLE REGISTRATION " + "(id INTEGER not NULL, " + " first VARCHAR(255), "
						+ " last VARCHAR(255), " + " age INTEGER, " + " PRIMARY KEY ( id ))";

				stmt.executeUpdate(sql);
				System.out.println("Created table in given database...");
			} catch (SQLException se) {
				// Handle errors for JDBC
				se.printStackTrace();
			} catch (Exception e) {
				// Handle errors for Class.forName
				e.printStackTrace();
			} finally {
				// finally block used to close resources
				try {
					if (stmt != null) {
						conn.close();
					}
				} catch (SQLException se) {
		            }// do nothing
		            try {
		                if (conn != null) {
		                    conn.close();
		                }
		            } catch (SQLException se) {
		                se.printStackTrace();
		            }//end finally try
		        }//end try
		        System.out.println("Goodbye!");
		    }//end main
		}//end JDBCExample