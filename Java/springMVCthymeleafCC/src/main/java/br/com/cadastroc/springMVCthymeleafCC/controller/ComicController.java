package br.com.cadastroc.springMVCthymeleafCC.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.cadastroc.springMVCthymeleafCC.dto.ComicDTO;
import br.com.cadastroc.springMVCthymeleafCC.model.Comic;
import br.com.cadastroc.springMVCthymeleafCC.repository.ComicRepository;

@Controller
@RequestMapping("/comics")
public class ComicController {

	@Autowired
	ComicRepository repository;
	
	
	@GetMapping("/cadastrarcomics")
	public String redirecionarComicsDTO(ComicDTO comic, Model model) {
		return"comics/cadastrarcomics";
	}
	
	@PostMapping("/salvarcomics")
	public String salvarComics(@Valid ComicDTO comicDto,Model model, BindingResult result) {
		if(result.hasErrors()) {
			return"comics/cadastrarcomics";

		}
		Comic comic = comicDto.toComic();	
		repository.save(comic);
		return "redirect:cadastrarcomics";
	}
}
