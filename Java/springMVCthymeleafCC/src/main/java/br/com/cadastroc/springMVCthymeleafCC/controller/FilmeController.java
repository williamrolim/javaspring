package br.com.cadastroc.springMVCthymeleafCC.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.cadastroc.springMVCthymeleafCC.dto.FilmeDTO;
import br.com.cadastroc.springMVCthymeleafCC.model.Filme;
import br.com.cadastroc.springMVCthymeleafCC.repository.FilmeRepository;
@Controller
@RequestMapping("/filmes")
public class FilmeController {
	
	@Autowired
	private FilmeRepository repository;
	
	@GetMapping("/cadastrarfilmes") 
	public String redirecionarfilmesDTO(FilmeDTO filmes, Model model) {
		return "filmes/cadastrarfilmes"; 
	}
	
	@PostMapping("salvarfilmes")
	public String salvarfilmes(@Valid FilmeDTO filmes, BindingResult result) {
		
		if(result.hasErrors()) {
			return "filmes/cadastrarfilmes"; 
		}
		Filme f = filmes.toFilmes();
		repository.save(f);
		System.out.println(f);
		return "redirect:cadastrarfilmes";
	}
	
}
