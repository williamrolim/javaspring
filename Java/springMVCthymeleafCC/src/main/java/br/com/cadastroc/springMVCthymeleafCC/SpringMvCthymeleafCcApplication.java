package br.com.cadastroc.springMVCthymeleafCC;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringMvCthymeleafCcApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringMvCthymeleafCcApplication.class, args);
	}

}
