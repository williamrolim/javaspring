package br.com.cadastroc.springMVCthymeleafCC.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;

@Entity
public class Comic {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int idComics;
	
	private String nomeComics;
	private int nota;
	private String genero;
	private String autor;
	private String desenhista;
	private String editora;
	private String lancamento;
	private String dataLeitura;
	private String descricao;
	
	
	public Comic() {
	
	}


	public Comic(String nomeComics, int nota, String genero, String autor, String desenhista, String editora,
			String lancamento, String dataLeitura, String descricao) {
		this.nomeComics = nomeComics;
		this.nota = nota;
		this.genero = genero;
		this.autor = autor;
		this.desenhista = desenhista;
		this.editora = editora;
		this.lancamento = lancamento;
		this.dataLeitura = dataLeitura;
		this.descricao = descricao;
	}




	public int getIdComics() {
		return idComics;
	}


	public void setIdComics(int idComics) {
		this.idComics = idComics;
	}


	public String getNomeComics() {
		return nomeComics;
	}


	public void setNomeComics(String nomeComics) {
		this.nomeComics = nomeComics;
	}


	public int getNota() {
		return nota;
	}


	public void setNota(int nota) {
		this.nota = nota;
	}


	public String getGenero() {
		return genero;
	}


	public void setGenero(String genero) {
		this.genero = genero;
	}



	public String getAutor() {
		return autor;
	}


	public void setAutor(String autor) {
		this.autor = autor;
	}


	public String getDesenhista() {
		return desenhista;
	}


	public void setDesenhista(String desenhista) {
		this.desenhista = desenhista;
	}


	public String getEditora() {
		return editora;
	}


	public void setEditora(String editora) {
		this.editora = editora;
	}


	public String getLancamento() {
		return lancamento;
	}


	public void setLancamento(String lancamento) {
		this.lancamento = lancamento;
	}


	public String getDataLeitura() {
		return dataLeitura;
	}


	public void setDataLeitura(String dataLeitura) {
		this.dataLeitura = dataLeitura;
	}


	public String getDescricao() {
		return descricao;
	}


	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	
	
	
}
