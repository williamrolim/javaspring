package br.com.cadastroc.springMVCthymeleafCC.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.cadastroc.springMVCthymeleafCC.model.Comic;

public interface ComicRepository extends JpaRepository<Comic, Integer>{

	@Override
	List<Comic>findAll();
}
