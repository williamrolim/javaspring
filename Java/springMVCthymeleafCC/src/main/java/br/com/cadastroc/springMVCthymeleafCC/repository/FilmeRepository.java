package br.com.cadastroc.springMVCthymeleafCC.repository;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.cadastroc.springMVCthymeleafCC.model.Filme;

public interface FilmeRepository extends JpaRepository<Filme, Integer> {
	@Override
	List<Filme>findAll();
}
