package br.com.cadastroc.springMVCthymeleafCC.dto;


import javax.validation.constraints.NotBlank;

import org.springframework.format.annotation.DateTimeFormat;

import br.com.cadastroc.springMVCthymeleafCC.model.Filme;

public class FilmeDTO {

	private int idFilme;
	@NotBlank
	private String nomeFilme;
	private int notaFilme;
	private String genero;
	private String diretor;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private String dataLancamento;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private String dataAssistiu;
	private String descricao;
	
	public int getIdFilme() {
		return idFilme;
	}
	public void setIdFilme(int idFilme) {
		this.idFilme = idFilme;
	}
	public String getNomeFilme() {
		return nomeFilme;
	}
	public void setNomeFilme(String nomeFilme) {
		this.nomeFilme = nomeFilme;
	}
	public int getNotaFilme() {
		return notaFilme;
	}
	public void setNotaFilme(int notaFilme) {
		this.notaFilme = notaFilme;
	}
	public String getGenero() {
		return genero;
	}
	public void setGenero(String genero) {
		this.genero = genero;
	}
	public String getDiretor() {
		return diretor;
	}
	public void setDiretor(String diretor) {
		this.diretor = diretor;
	}
	public String getDataLancamento() {
		return dataLancamento;
	}
	public void setDataLancamento(String dataLancamento) {
		this.dataLancamento = dataLancamento;
	}
	public String getDataAssistiu() {
		return dataAssistiu;
	}
	public void setDataAssistiu(String dataAssistiu) {
		this.dataAssistiu = dataAssistiu;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	public Filme toFilmes() {
		Filme filmes = new Filme();
		filmes.setNomeFilme(nomeFilme);
		filmes.setNotaFilme(notaFilme);
		filmes.setGenero(genero);
		filmes.setDiretor(diretor);
		filmes.setDataLancamento(dataLancamento);
		filmes.setDataAssistiu(dataAssistiu);
		filmes.setDescricao(descricao);
		return filmes;
	}
	
	
}
