package br.com.cadastroc.springMVCthymeleafCC.controller;


import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.cadastroc.springMVCthymeleafCC.dto.FilmeDTO;
import br.com.cadastroc.springMVCthymeleafCC.model.Comic;
import br.com.cadastroc.springMVCthymeleafCC.model.Filme;
import br.com.cadastroc.springMVCthymeleafCC.repository.ComicRepository;
import br.com.cadastroc.springMVCthymeleafCC.repository.FilmeRepository;

@Controller
@RequestMapping("/")
public class MenuController {

	
	@Autowired
	private FilmeRepository repository;
	
	@Autowired
	private ComicRepository repo;
	
	@GetMapping("filmes")
	public String listarFilmesTable(Model model) {
		List<Filme> filmes = repository.findAll();
		model.addAttribute("filmeslist", filmes);
		return "filmes/list-filmes";
	}
	
	@GetMapping("comics")
	public String listarComicsTable(Model model) {
		List<Comic> comics =  repo.findAll();
		model.addAttribute("comicslist", comics);
		return "comics/list-comics";
	}

}
