package br.com.cadastrocultural.springMVCthymeleaf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringMvCthymeleafCadastroCulturalApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringMvCthymeleafCadastroCulturalApplication.class, args);
	}

}
