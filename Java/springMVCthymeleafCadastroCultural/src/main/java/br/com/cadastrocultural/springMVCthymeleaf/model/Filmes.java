package br.com.cadastrocultural.springMVCthymeleaf.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


import org.springframework.format.annotation.DateTimeFormat;

@Entity
public class Filmes {
	
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	private Long idFilme;
	private String nomeFilme;
	private int notaFilme;
	
	private String genero;
	private String diretor;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private String dataLancamento;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private String dataAssistiu;
	private String descricao;
	
	public Long getIdFilme() {
		return idFilme;
	}
	public void setIdFilme(Long idFilme) {
		this.idFilme = idFilme;
	}
	public String getNomeFilme() {
		return nomeFilme;
	}
	public void setNomeFilme(String nomeFilme) {
		this.nomeFilme = nomeFilme;
	}
	public int getNotaFilme() {
		return notaFilme;
	}
	public void setNotaFilme(int notaFilme) {
		this.notaFilme = notaFilme;
	}

	public String getDiretor() {
		return diretor;
	}
	public void setDiretor(String diretor) {
		this.diretor = diretor;
	}

	public String getDataLancamento() {
		return dataLancamento;
	}
	public void setDataLancamento(String dataLancamento) {
		this.dataLancamento = dataLancamento;
	}
	public String getDataAssistiu() {
		return dataAssistiu;
	}
	public void setDataAssistiu(String dataAssistiu) {
		this.dataAssistiu = dataAssistiu;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getGenero() {
		return genero;
	}
	public void setGenero(String genero) {
		this.genero = genero;
	}
	@Override
	public String toString() {
		return "Filmes [idFilme=" + idFilme + ", nomeFilme=" + nomeFilme + ", notaFilme=" + notaFilme + ", genero="
				+ genero + ", diretor=" + diretor + ", dataLancamento=" + dataLancamento + ", dataAssistiu="
				+ dataAssistiu + ", descricao=" + descricao + "]";
	}

	
}