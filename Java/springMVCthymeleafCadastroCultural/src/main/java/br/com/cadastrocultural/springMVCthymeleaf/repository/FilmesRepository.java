package br.com.cadastrocultural.springMVCthymeleaf.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.cadastrocultural.springMVCthymeleaf.model.Filmes;

@Repository
public interface FilmesRepository extends JpaRepository<Filmes, Long> {

	
}
