package br.com.cadastrocultural.springMVCthymeleaf.controller;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.cadastrocultural.springMVCthymeleaf.dto.Filmesdto;
import br.com.cadastrocultural.springMVCthymeleaf.model.Filmes;
import br.com.cadastrocultural.springMVCthymeleaf.repository.FilmesRepository;




@Controller
@RequestMapping("cadastro")//Mapeei a pasta e os componenentes dentro dela como filme
public class MenuController {
	@Autowired
	private FilmesRepository filmeRepository;
	
	
	@GetMapping("filme")
	public String filme(Filmes f) {
		filmeRepository.save(f);
		System.out.println(f + "filmes");
		return "cadastro/filme";
	}
	
}
