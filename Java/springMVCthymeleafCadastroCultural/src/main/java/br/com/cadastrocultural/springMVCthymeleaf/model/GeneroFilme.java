package br.com.cadastrocultural.springMVCthymeleaf.model;

public enum GeneroFilme {
	Ação,
	Comedia,
	Drama,
	Fantasia,
	Terror,
	Misterio,
	Romance,
	Thriller,
	Western
}
