package gerenciadorDeCursos;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;

public class Curso {

	private String nome;
	private String instrutor;
	private List<Aula> aulas = new LinkedList<>();// Relacionamento entre as classes (quais aulas contem nno curso)
	private Set<Aluno> alunos = new HashSet<>();
	private Map<Integer, Aluno>matriculaParaAluno = new HashMap<>();
	
	
	public Curso(String nome, String instrutor) {
		this.nome = nome;
		this.instrutor = instrutor;
	}

	public String getNome() {
		return nome;
	}

	public String getInstrutor() {
		return instrutor;
	}

	public List<Aula> getAulas() {
		// atraves do unmodifiableList n�o sera mais poss�vel alterar o valor dessa
		// lista por fora da pr�pria classe Curso
		return Collections.unmodifiableList(aulas);
	}

	public void Adiciona(Aula aula) {
		this.aulas.add(aula);
	}

	public int getTempoTotal() {
		return this.aulas.stream().mapToInt(Aula::getTempo).sum();
		/*
		int tempoTotal = 0;
		for (Aula aula : aulas) {
			tempoTotal += aula.getTempo();
		}
		return tempoTotal;
		*/

	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "[ Titulo do curso: " + nome + " tempo total : " + getTempoTotal() + " Aulas: " + this.aulas + " ]";
	}

	public void matricula(Aluno aluno) {
		this.alunos.add(aluno);
		this.matriculaParaAluno.put(aluno.getNumeroMatricula(), aluno);
	}

	public Set<Aluno> getAlunos() {
		return Collections.unmodifiableSet(alunos);//Criar um outro conjunto (n�o vai duplicar os objetos) vai apontar para o mesmo objeto (n�o pode add ou remover objetos)
	}

	public boolean estaMatriculado(Aluno aluno) {

		return this.alunos.contains(aluno); 
	}

	public Aluno buscaMatriculado(int numero) {
		if(!matriculaParaAluno.containsKey(numero))
		throw new NoSuchElementException("Matricula n�o encontrada - numero.: " + numero);
		return matriculaParaAluno.get(numero);

//		for (Aluno aluno : alunos) {
//			if (aluno.getNumeroMatricula() == numero)
//				return aluno;
//		}
//		throw new NoSuchElementException("Matricula n�o encontrada" + numero);
	}
	

}
