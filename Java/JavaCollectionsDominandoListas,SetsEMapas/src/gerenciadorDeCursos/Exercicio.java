package gerenciadorDeCursos;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Exercicio {

    public static void main(String[] args) {

        Map<Integer, String> pessoas = new HashMap<>();

        pessoas.put(21, "Leonardo Cordeiro");
        pessoas.put(27, "Fabio Pimentel");
        pessoas.put(19, "Silvio Mattos");
        pessoas.put(23, "Romulo Henrique");
        
        // Standard classic way, recommend!
        for (Map.Entry<Integer, String> entrada : pessoas.entrySet()) {
        
        	System.out.println(entrada);
        }
        
        // Java 8, forEach and Lambda. recommend!
        System.out.println("\nExample 2...");
        pessoas.forEach((k, v) -> System.out.println("Key : " + k + " Value : " + v));

        // Map -> Set -> Iterator -> Map.Entry -> troublesome, don't use, just for fun
        System.out.println("\nExample 3...");
        Iterator<Map.Entry<Integer, String>> iterator = pessoas.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<Integer, String> entry = iterator.next();
            System.out.println("Key : " + entry.getKey() + " Value :" + entry.getValue());
        }

        // weired, but works anyway, don't use, just for fun
        System.out.println("\nExample 4...");
        for (Object key : pessoas.keySet()) {
            System.out.println("Key : " + key.toString() + " Value : " + pessoas.get(key));
        }
        
        pessoas.keySet().forEach(idade -> {
            System.out.println(pessoas.get(idade));
        });
        }
    }