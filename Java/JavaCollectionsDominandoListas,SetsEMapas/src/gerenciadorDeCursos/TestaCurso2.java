package gerenciadorDeCursos;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class TestaCurso2 {
	public static void main(String[] args) {
		Curso javaColecoes = new Curso("Dominando as cole��es Java" , "Paulo Silveira");
		

		javaColecoes.Adiciona(new Aula ("Trabalhando Com ArrayList,",21));
		javaColecoes.Adiciona(new Aula ("Criando uma Aula,",20));
		javaColecoes.Adiciona(new Aula ("Modelando Cole��es,",22));

		List<Aula>aulasImutaveis = javaColecoes.getAulas();//List especie de cole��o que sabe dizer a ordem dos elementos
		System.out.println(aulasImutaveis);
		
		//Collections.sort(aulasImutaveis);
		
		//System.out.println(aulasImutaveis);
		System.out.println("\n*********************************************");
		System.out.println("*********************************************");
		
		List<Aula>aulasMutaveis =new ArrayList<>(aulasImutaveis);
		Collections.sort(aulasMutaveis);
		System.out.println(aulasMutaveis);
		System.out.println("\n*********************************************");
		System.out.println("*********************************************");
		System.out.println(javaColecoes.getTempoTotal());
		System.out.println("\n*********************************************");
		System.out.println("*********************************************");

		System.out.println(javaColecoes);
		
		
	}
}
