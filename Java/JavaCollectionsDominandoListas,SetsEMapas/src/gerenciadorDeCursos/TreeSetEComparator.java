package gerenciadorDeCursos;

import java.util.Comparator;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

public class TreeSetEComparator implements Comparator<Funcionario>{

	
	
public static void main(String[] args) {
	Funcionario f1 = new Funcionario("Barney", 12);
    Funcionario f2 = new Funcionario("Jonatan", 9);
    Funcionario f3 = new Funcionario("Guaraciara", 13);

    Set<Funcionario> funcionarios = new TreeSet<>(new TreeSetEComparator());
    funcionarios.add(f1);
    funcionarios.add(f2);
    funcionarios.add(f3);

    Iterator<Funcionario> iterador = funcionarios.iterator();

    System.out.println("mais novo para o mais velho");
    while (iterador.hasNext()) {
        System.out.println(iterador.next().getNome());
    } 
}

@Override
public int compare(Funcionario funcionario, Funcionario outroFuncionario) {
	// TODO Auto-generated method stub
	return funcionario.getIdade() - outroFuncionario.getIdade();

}

}
