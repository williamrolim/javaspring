package gerenciadorDeCursos;

import java.util.*;
import java.util.Map.Entry;

public class TesteMap {

    public static void main(String[] args) {

        Map<String, Integer> nomesParaIdade = new HashMap<>();
        nomesParaIdade.put("Paulo", 31);
        nomesParaIdade.put("Adriano", 25);
        nomesParaIdade.put("Alberto", 33);
        nomesParaIdade.put("Guilherme", 26);
        // ...
        
        System.out.println(nomesParaIdade.containsValue(31));
        System.out.println(nomesParaIdade.containsKey("Adriano"));
        System.out.println();
        System.out.println(nomesParaIdade.toString());
        
        
        //Para acessar as chaves devemos executar:
        System.out.println("\n****************************SET************************************");
        Set<String> chaves = nomesParaIdade.keySet();    
        for (String nome : chaves) {
            System.out.println(nome);
        }
        
        //E para pegar os valores usamos:
        System.out.println("\n****************************Collection************************************");
        Collection<Integer> valores = nomesParaIdade.values();
        for (Integer idade : valores) {
            System.out.println(idade);
        }
        

        //Agora s� falta a terceira cole��o que devolve a associa��o. Para tal, existe o m�todo entrySet()
         //e cada associa��o � representado atrav�s da classe Entry
        System.out.println("\n****************************Set<Entry<String, Integer>>************************************");
        Set<Entry<String, Integer>> associacoes = nomesParaIdade.entrySet();    
        for (Entry<String, Integer> associacao : associacoes) {
            System.out.println(associacao.getKey() + " - " + associacao.getValue());
        }

    }
}