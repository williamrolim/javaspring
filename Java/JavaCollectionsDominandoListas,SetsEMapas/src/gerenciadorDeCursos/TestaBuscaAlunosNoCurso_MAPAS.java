package gerenciadorDeCursos;

public class TestaBuscaAlunosNoCurso_MAPAS {
	public static void main(String[] args) {
		
		Curso javaColecoes = new Curso("Dominando as cole��es Java" , "Paulo Silveira");
		javaColecoes.Adiciona(new Aula ("Trabalhando Com ArrayList,",21));
		javaColecoes.Adiciona(new Aula ("Criando uma Aula,",20));
		javaColecoes.Adiciona(new Aula ("Modelando Cole��es,",22));

		Aluno a1 = new Aluno("Rodrigo Turini", 34672);
		Aluno a2 = new Aluno("Guilherme Silveira", 5617);
		Aluno a3 = new Aluno("Mauricio Aniche", 17645); 
		javaColecoes.matricula(a1);
		javaColecoes.matricula(a2);
		javaColecoes.matricula(a3);
		
		Aluno aluno = javaColecoes.buscaMatriculado(34672);
		System.out.println(aluno);
	}
}
