package gerenciadorDeCursos;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class TestaAlunos {
	public static void main(String[] args) {
		Set<String> alunos = new HashSet<>();//Set  = 1 n�o tem caractersitica da ordem que ficaram esses obejtos
		alunos.add("Rodrigo Turini");        //Set =  2 n�o aceitam elementos repetidos, garante que s� tem um objeto unico (com esse nome) dentro do conjunto
		alunos.add("Alberto Souza");         //Set =  3 vantagem e perfomance
		alunos.add("Nico Steppat");
		alunos.add("Sergio Lopes");
		alunos.add("Mauricio Aniche");
		alunos.add("Alberto Souza");
		
		System.out.println(alunos.size());
				
		System.out.println("*********************************************");
		boolean estaMatriculado = alunos.contains("Paulo Silveira");
		System.out.println(estaMatriculado);
		System.out.println("*********************************************");
		System.out.println(alunos);
		System.out.println("*********************************************");
		
		for (String aluno : alunos) { //para cada aluno dentro dessa cole��o alunos
			System.out.println(aluno);
		}
		System.out.println("*********************************************");
		
		alunos.forEach(aluno -> System.out.println(aluno)); //lambda para cada aluno
		
		List<String>alunosEmLista = new ArrayList<>(alunos);
		
		System.out.println("*********************************************");
		Collections.sort(alunosEmLista);
		
		System.out.println(alunosEmLista);
	}
}
