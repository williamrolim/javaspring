package gerenciadorDeCursos;

import java.util.Collections;
import java.util.Iterator;
import java.util.Set;
import java.util.Vector;

public class TestaCursoComAluno {
	public static void main(String[] args) {
		
		Curso javaColecoes = new Curso("Dominando as cole��es Java" , "Paulo Silveira");
		javaColecoes.Adiciona(new Aula ("Trabalhando Com ArrayList,",21));
		javaColecoes.Adiciona(new Aula ("Criando uma Aula,",20));
		javaColecoes.Adiciona(new Aula ("Modelando Cole��es,",22));

		Aluno a1 = new Aluno("Rodrigo Turini", 34672);
		Aluno a2 = new Aluno("Guilherme Silveira", 5617);
		Aluno a3 = new Aluno("Mauricio Aniche", 17645); 
		javaColecoes.matricula(a1);
		javaColecoes.matricula(a2);
		javaColecoes.matricula(a3);
		
		System.out.println("Todos os Alunos modificados nesse curso");
		
		javaColecoes.getAlunos().forEach(aluno -> {
			System.out.println(aluno);
		});
		
		System.out.println("\n*******************Collections synchronizedSet********************");
		Set<Aluno> alunosSincronizados = Collections.synchronizedSet(javaColecoes.getAlunos());
		
		System.out.println(alunosSincronizados);
		
		System.out.println("\n*******************Collections Vazio********************");

		 Set<String> nomes = Collections.emptySet(); // Faz sentido se criarmo empty para cursos que foram cancelados
		 System.out.println(nomes);
		 
		 System.out.println(javaColecoes.estaMatriculado(a1));
		 
			System.out.println("******************************");
			
			Aluno turini = new Aluno("Rodrigo Turini", 34672);
			Aluno roberta = new Aluno("Roberta Tauros", 34672);


			
			System.out.println(a1.hashCode() == turini.hashCode());
			System.out.println(a1.hashCode() == roberta.hashCode());
			
			System.out.println("\n*******************ITERATOR (METODO ANTIGO)********************");

			Set<Aluno> alunos = javaColecoes.getAlunos();
			Iterator<Aluno> iterador = alunos.iterator();
			
			while(iterador.hasNext()) {
				Aluno proximo = iterador.next();
				System.out.println(proximo);
			}
			
			System.out.println("\n*******************JAVA 5********************");

			for(Aluno aluno : alunos) {
				System.out.println(aluno);
			}
			
			System.out.println("\n*******************Vector (antiga- desatualizada usada para multithreads********************");
			Vector<Aluno> vector = new Vector<Aluno>();
			System.out.println(vector);

	}
}
