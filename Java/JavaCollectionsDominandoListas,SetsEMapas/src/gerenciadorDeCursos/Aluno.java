package gerenciadorDeCursos;

import java.util.Objects;

public class Aluno {
	private String nome;
	private int numeroMatricula;
	
	public Aluno(String nome, int numeroMatricula) {
		if ((nome == null) || (numeroMatricula == 0)) {
			System.out.println("Campo nulo");
			throw new NullPointerException();
		}
		this.nome = nome;
		this.numeroMatricula = numeroMatricula;
	}

	public String getNome() {
		return nome;
	}

	public int getNumeroMatricula() {
		return numeroMatricula;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
        Aluno other = (Aluno) obj;
        if (nome == null) {
            if (other.nome != null)
                return false;
        } else if (!nome.equals(other.nome))
            return false;
        if (numeroMatricula != other.numeroMatricula)
            return false;
        return true;
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(nome, numeroMatricula);
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "[ " + this.nome + " ," + " matricula: " + this.numeroMatricula + " ]";
	}
	
	

}
