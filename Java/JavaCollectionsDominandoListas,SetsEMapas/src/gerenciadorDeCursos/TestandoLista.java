package gerenciadorDeCursos;

import java.util.ArrayList;
import java.util.Collections;

public class TestandoLista {
	public static void main(String[] args) {
		String aula1 = "Conhecendo mais de listas";
		String aula2 = "Modelando a classe Aula";
		String aula3 = "Trabalhando com cursos e Sets";
		
		ArrayList<String> aulas = new ArrayList <>();
		aulas.add(aula1);
		aulas.add(aula2);
		aulas.add(aula3);
		
		System.out.println(aulas);
		
		aulas.remove(aula1);
		
		System.out.println(aulas);
		
		
		//atalho for each
		 //para cada string aula dentro de aulas
		for (String aula : aulas) {
			System.out.println("Aula..: " + aula);
		}
		
		System.out.println("\n*********************************************");
		String primeiraAula = aulas.get(0);
		System.out.println(primeiraAula);
		
		System.out.println("\n*********************************************");

		for (int i=0; i < aulas.size(); i++) {
			System.out.println("aula :" + aulas.get(i));
		}
		System.out.println("\n*********************************************");
		//Expressoes Lambda
		aulas.forEach(aula -> {System.out.println("percorrendo..: " + aula);});
		
		aulas.add("Aumentando nosso conhecimento");
		System.out.println("\n*********************************************");
		System.out.println(aulas);
		
		System.out.println("\n*********************************************");

		Collections.sort(aulas);
		System.out.println("Depois de ordenados..: " + aulas );
		
	}
}
