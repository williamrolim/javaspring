package com.spring.projetogerenciamento.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.projetogerenciamento.dao.IFuncionarioRepository;
import com.spring.projetogerenciamento.dto.FuncionarioProjeto;
import com.spring.projetogerenciamento.entidades.Funcionario;

@Service
public class FuncionarioService {

	@Autowired
	IFuncionarioRepository funcionarioREPO;
	
	public Funcionario save(Funcionario funcionario){
		return funcionarioREPO.save(funcionario);
	}
	
	public List<Funcionario>pegueTodos(){
		return funcionarioREPO.findAll();
	}
	
	public List<FuncionarioProjeto> funcionarioProjetos(){
		return funcionarioREPO.funcionarioProjetos();
	}
}
