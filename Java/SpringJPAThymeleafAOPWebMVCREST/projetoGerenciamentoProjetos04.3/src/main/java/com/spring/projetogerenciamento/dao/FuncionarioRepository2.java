package com.spring.projetogerenciamento.dao;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import com.spring.projetogerenciamento.dto.FuncionarioProjeto;
import com.spring.projetogerenciamento.entidades.Funcionario;

@Repository
@Profile("dev")
public class FuncionarioRepository2 implements IFuncionarioRepository {
	
	@Override
	public List<Funcionario>findAll(){
		Funcionario fun1 = new Funcionario("Morena", "Marilian", "javana@gmail.com");
		Funcionario fun2 = new Funcionario("Washington", "Donovan", "wash@hotmail.com");
		Funcionario fun3 = new Funcionario("Florinda", "Fofis", "flora@gmail.com");
		return Arrays.asList(fun1,fun2,fun3);
	}
	@Override
	public <S extends Funcionario> S save(S entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends Funcionario> Iterable<S> saveAll(Iterable<S> entities) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Optional<Funcionario> findById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean existsById(Long id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Iterable<Funcionario> findAllById(Iterable<Long> ids) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long count() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void deleteById(Long id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(Funcionario entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteAllById(Iterable<? extends Long> ids) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteAll(Iterable<? extends Funcionario> entities) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteAll() {
		// TODO Auto-generated method stub
		
	}


	@Override
	public List<FuncionarioProjeto> funcionarioProjetos() {
		
		FuncionarioProjeto funcProj = new FuncionarioProjeto() {
			
			@Override
			public String getPrimeiroNome() {
				// TODO Auto-generated method stub
				return "Morena";
			}
			
			
			@Override
			public String getSobrenome() {
				// TODO Auto-generated method stub
				return "Marilian";
			}
			
		
			@Override
			public int getContagemProjetos() {
				return 10;
			}
		};
		
		return Arrays.asList(funcProj);
	}

}
