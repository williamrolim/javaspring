package com.spring.projetogerenciamento.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.spring.projetogerenciamento.entidades.Funcionario;

public interface IFuncionarioRepository extends CrudRepository<Funcionario, Long> {
	//O metodo padrão do findAll traz um Iterable sobrescrevesmo o mesmo para trazer lista

	@Override
	List<Funcionario>findAll();
	
}
