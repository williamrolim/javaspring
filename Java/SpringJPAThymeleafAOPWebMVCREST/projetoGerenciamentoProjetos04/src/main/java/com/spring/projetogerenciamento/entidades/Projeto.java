package com.spring.projetogerenciamento.entidades;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Projeto {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long projetoId; //indica cada instancia do projeto -- responsabilidade de gerenciar autoincremente e id unica banco de dados
	private String name;
	private String estagio; //incompleto, concluido ou não iniciado
	private String descricao;
							//"projeto" mapear a propriedade com o mesmo nome da func
	@OneToMany(mappedBy = "projeto")                           //esse projeto pode ter muitos funcionarios atribuidos a ele
	private List<Funcionario> funcionarios;//um projeto para muitos funcionarions one to many	
	
	public Projeto() {
	}
	
	public Projeto(String name, String estagio, String descricao) {
		this.name = name;
		this.estagio = estagio;
		this.descricao = descricao;
	}
	
	public long getProjetoId() {
		return projetoId;
	}
	public void setProjetoId(long projetoId) {
		this.projetoId = projetoId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEstagio() {
		return estagio;
	}
	public void setEstagio(String estagio) {
		this.estagio = estagio;
	}
	
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public List<Funcionario> getFuncionarios() {
		return funcionarios;
	}

	public void setFuncionarios(List<Funcionario> funcionarios) {
		this.funcionarios = funcionarios;
	}
	
	

}
