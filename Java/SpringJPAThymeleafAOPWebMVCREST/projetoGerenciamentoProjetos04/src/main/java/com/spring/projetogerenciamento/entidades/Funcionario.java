package com.spring.projetogerenciamento.entidades;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Funcionario {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long funcionarioId;
	
	private String primeiroNome;
	private String sobrenome;
	private String email;
	
	//muitos para um  Cascade se algo acontecer com a propriedade pai queremos que os filhos passem pela mesma etapa
	@ManyToOne(cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH,CascadeType.PERSIST}, fetch = FetchType.LAZY)                     //muitos funcionarios podem ser atribuidos ao mesmo projeto
	@JoinColumn(name="projeto_id") //projeto_id representara a relação de chave estrangeira 
	private Projeto projeto;//o projeto especifico ao qual o funcionaiorio será destinado
					//mapear a propriedade theProjeto
	
	//merge se fundir o projeto A com o projeto B os filhos dos projetos seriam fundidos tbém
	public Funcionario() {

	}
	
	public Funcionario(String primeiroNome, String sobrenome, String email) {
	
		this.primeiroNome = primeiroNome;
		this.sobrenome = sobrenome;
		this.email = email;
	}

	
	public long getFuncionarioId() {
		return funcionarioId;
	}
	public void setFuncionarioId(long funcionarioId) {
		this.funcionarioId = funcionarioId;
	}
	public String getPrimeiroNome() {
		return primeiroNome;
	}
	public void setPrimeiroNome(String primeiroNome) {
		this.primeiroNome = primeiroNome;
	}

	
	public String getSobrenome() {
		return sobrenome;
	}

	public void setSobrenome(String sobrenome) {
		this.sobrenome = sobrenome;
	}

	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

	public Projeto getProjeto() {
		return projeto;
	}

	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}

	
}
