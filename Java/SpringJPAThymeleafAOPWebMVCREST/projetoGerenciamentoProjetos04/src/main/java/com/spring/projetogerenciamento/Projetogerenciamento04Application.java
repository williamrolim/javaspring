package com.spring.projetogerenciamento;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Projetogerenciamento04Application {

	public static void main(String[] args) {
		SpringApplication.run(Projetogerenciamento04Application.class, args);
	}

}
