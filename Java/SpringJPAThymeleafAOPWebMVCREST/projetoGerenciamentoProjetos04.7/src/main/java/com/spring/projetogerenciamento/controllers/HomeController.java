package com.spring.projetogerenciamento.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.spring.projetogerenciamento.dao.IProjetoRepository;
import com.spring.projetogerenciamento.dto.FuncionarioProjeto;
import com.spring.projetogerenciamento.dto.ProjetoStatusGrafico;
import com.spring.projetogerenciamento.entidades.Projeto;
import com.spring.projetogerenciamento.service.FuncionarioService;

@Controller //sem o controller autowired não funcionaria 
public class HomeController {

	@Autowired
	FuncionarioService repositoryF;
	@Autowired
	IProjetoRepository repositoryP;
	
	//injeção de construtor não precisaria usar o Autowired (Exemplo)
//	public HomeController(IFuncionarioRepository repositoryF) {
//		this.repositoryF = repositoryF;
//	}
	
	//	@Autowired  //outro exemplo de injeção de depencia
//	public void setRepositoryF(IFuncionarioRepository repositoryF) {
//		this.repositoryF = repositoryF;
//	}
	
	@GetMapping("/")
	public String listandoFuncionariosHome(Model model) throws JsonProcessingException {
		//nome da coluna
		Map<String, Object> map = new HashMap<>();

		//Estamos consultando o bando de dados para projetos
		List<Projeto> projeto = repositoryP.findAll();
		model.addAttribute("projetoslist", projeto);
		
//		List<ProjetoStatusGrafico> projetoCount2 = repositoryP.getContagemProjetoStatus();
//		model.addAttribute("contagemProjetos", projetoCount2);
			
		List<ProjetoStatusGrafico> projetoCount = repositoryP.getContagemProjetoStatus();
		//vamos converter o objeto de dados do projeto em uma estrutura Jason para uso em Javascript
		ObjectMapper objectMapper = new ObjectMapper();//Mapeador de Objetos
		String jsonString = objectMapper.writeValueAsString(projetoCount);//convertendo os dados do projeto em Json
		//[["NOTSTARTED", 1],["INPROGRESS",2],["COMPLETED",1]]  
		model.addAttribute("statusGrafico", jsonString);
		
		//Estamos consultando o bando de dados para funcionarios 
		List<FuncionarioProjeto> funcionarioProjetoCount = repositoryF.funcionarioProjetos();
		model.addAttribute("funcionariosListProjetoCount", funcionarioProjetoCount);
		
		return "main/home";

	}


}
