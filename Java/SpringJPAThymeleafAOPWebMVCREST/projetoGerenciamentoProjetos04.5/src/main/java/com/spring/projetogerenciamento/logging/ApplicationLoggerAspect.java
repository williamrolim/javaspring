package com.spring.projetogerenciamento.logging;

import java.util.Arrays;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;


@Aspect //aspect é um codigo que é executado antes ou depois em torno de cortes pontuais definidos pelo dev
@Component
public class ApplicationLoggerAspect {
	
	private final Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Pointcut("within(com.spring.projetogerenciamento.controllers..*)") //..as classes , os metodos e tudo mais
	public void definindoCortesPontuaisPacote() {
		//metodo vazio apenas local especifico do ponto de corte
	}
	

	//after é exutado apos todo os controllers que foram definidos na anotação pointcut
//	@After("definindoCortesPontuaisPacote()")//@After significa que esse metodo será executado apos tudo dos controllers
//	public void logBefore(JoinPoint jp) {
//		log.debug("---------------------------------After---------------------------------\n\n\n");
//		log.debug("*****\n {}.{} ()	com argumentos[s] = {}",
//				jp.getSignature().getDeclaringTypeName().toString(),
//				jp.getSignature().getName().toString(), Arrays.toString(jp.getArgs()));
//		log.debug("------------------------------------------------------------------\n\n\n");
//
//
//	}
	
	@Around("definindoCortesPontuaisPacote()")//@After significa que esse metodo será executado apos tudo dos controllers
	public Object logAround(ProceedingJoinPoint jp) {//ProceedingJoinPoint podemos fazer o que seja executado ANTES seja separado do que é Executado depois
		log.debug("---------------------------------BEFORE METHOD EXECUTION---------------------------------\n\n\n");
		log.debug("*****\n {}.{} ()	com argumentos[s] = {}",
				jp.getSignature().getDeclaringTypeName().toString(),
				jp.getSignature().getName().toString(), Arrays.toString(jp.getArgs()));
		log.debug("------------------------------------------------------------------\n\n\n");
		Object o = null;
		try {
			 o = jp.proceed();//isso permitira que passe para proxima etapa da articulação
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		log.debug("---------------------------------After---------------------------------\n\n\n");
		log.debug("*****\n {}.{} ()	com argumentos[s] = {}",
				jp.getSignature().getDeclaringTypeName().toString(),
				jp.getSignature().getName().toString(), Arrays.toString(jp.getArgs()));
		log.debug("------------------------------------------------------------------\n\n\n");
		
		
		return o;

	}
}
