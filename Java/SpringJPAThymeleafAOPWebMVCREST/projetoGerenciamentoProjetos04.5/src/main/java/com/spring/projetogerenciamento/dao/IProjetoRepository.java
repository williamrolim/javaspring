package com.spring.projetogerenciamento.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.spring.projetogerenciamento.dto.ProjetoStatusGrafico;
import com.spring.projetogerenciamento.entidades.Projeto;

//Obs:por convenção em projetos profissionais devemos adicionaro  
//I na frente quando se trata de interfaces
public interface IProjetoRepository extends CrudRepository<Projeto, Long> {

	//O metodo padrão do findAll traz um Iterable sobrescrevesmo o mesmo para trazer lista
	@Override
	List<Projeto> findAll();
	
	@Query(nativeQuery = true, value="SELECT estagio as EstagioProjeto , COUNT(*) as Contagem "
			+ "FROM projeto "
			+ "GROUP BY estagio;")
	public List<ProjetoStatusGrafico> getContagemProjetoStatus();
}
