package com.spring.projetogerenciamento.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.spring.projetogerenciamento.dto.FuncionarioProjeto;
import com.spring.projetogerenciamento.entidades.Funcionario;

public interface IFuncionarioRepository extends CrudRepository<Funcionario, Long> {
	//O metodo padrão do findAll traz um Iterable sobrescrevesmo o mesmo para trazer lista

	@Override
	List<Funcionario>findAll();
	//descobrir cada projeto que cada funcionario está atribuido
		@Query(nativeQuery = true, value="SELECT f.primeiro_nome as PrimeiroNome, f.sobrenome as Sobrenome, COUNT(pf.funcionario_id) as ContagemProjetos "
				+ "FROM funcionario f LEFT JOIN projeto_funcionario pf ON pf.funcionario_id = f.funcionario_id "
				+ "GROUP BY f.primeiro_nome, f.sobrenome ORDER BY 3 DESC")
		public List<FuncionarioProjeto>funcionarioProjetos();
	
}
