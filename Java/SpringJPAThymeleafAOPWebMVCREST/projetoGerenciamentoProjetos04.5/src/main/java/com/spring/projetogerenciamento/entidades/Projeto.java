package com.spring.projetogerenciamento.entidades;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;

@Entity
public class Projeto {
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "projeto_seq")//Mmais rapido que o IDENTITY é mais rapido pq o spring tem atualização em lote
	@SequenceGenerator(name = "projeto_seq", allocationSize = 1)
	private long projetoId; //indica cada instancia do projeto -- responsabilidade de gerenciar autoincremente e id unica banco de dados
	private String name;
	private String estagio; //incompleto, concluido ou não iniciado
	private String descricao;
							//"projeto" mapear a propriedade com o mesmo nome da func
	//relação muito para muitos em ambos os lados (Projeto & Funcionario)
	@ManyToMany(cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH,CascadeType.PERSIST}, fetch = FetchType.LAZY)                     //muitos funcionarios podem ser atribuidos ao mesmo projeto
	@JoinTable(name="projeto_funcionario", // representa a tabela de junção, funcionario e projeto (tabela)
				joinColumns = @JoinColumn(name="projeto_id"),//atributo de coluna de junção
				inverseJoinColumns = @JoinColumn(name="funcionario_id") )
	private List<Funcionario> funcionarios;
	
	public Projeto() {
	}
	
	public Projeto(String name, String estagio, String descricao) {
		this.name = name;
		this.estagio = estagio;
		this.descricao = descricao;
	}
	
	public long getProjetoId() {
		return projetoId;
	}
	public void setProjetoId(long projetoId) {
		this.projetoId = projetoId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEstagio() {
		return estagio;
	}
	public void setEstagio(String estagio) {
		this.estagio = estagio;
	}
	
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public List<Funcionario> getFuncionarios() {
		return funcionarios;
	}

	public void setFuncionarios(List<Funcionario> funcionarios) {
		this.funcionarios = funcionarios;
	}
	
	
	//Metodo de conveniencia apenas para fazer o banco de dados temporados
	public void addFuncionario(Funcionario func) {
		if (funcionarios == null) {
			funcionarios = new ArrayList<>();
		}
		funcionarios.add(func);
	}


}
