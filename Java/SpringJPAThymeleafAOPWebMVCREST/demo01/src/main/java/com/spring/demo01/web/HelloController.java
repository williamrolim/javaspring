package com.spring.demo01.web;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("menu")
public class HelloController {

@RequestMapping(value = "/filmes", method = RequestMethod.GET)
public String DigaAlo() {
	return "<h1>Ola Mundeko 1</h1>";
}

@RequestMapping(value = "/animes", method = RequestMethod.GET)
public String DigaContinue() {
	return "<br><h1>Ola Mundeko 2</h1>";
}

@RequestMapping(value = "/jogos", method = RequestMethod.GET)
public String DigaContinueparasempre() {
	return "<br><h1>Ola Mundeko 3</h1>";
}
	
}
