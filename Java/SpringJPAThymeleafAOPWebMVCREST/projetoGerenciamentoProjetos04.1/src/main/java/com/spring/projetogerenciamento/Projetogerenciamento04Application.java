package com.spring.projetogerenciamento;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.spring.projetogerenciamento.dao.IFuncionarioRepository;
import com.spring.projetogerenciamento.dao.IProjetoRepository;

@SpringBootApplication
public class Projetogerenciamento04Application {
	
	
	public static void main(String[] args) {
		SpringApplication.run(Projetogerenciamento04Application.class, args);
	}
	
//	@Bean
//	CommandLineRunner runner() {
//		
//		return args -> {
//			
//			Funcionario func1 = new Funcionario("John", "Warton", "warton@gmail.com");
//			Funcionario func2 = new Funcionario("Mike", "Lanister", "lanister@gmail.com");
//			Funcionario func3 = new Funcionario("Steve", "Reeves", "Reeves@gmail.com");
//
//			Funcionario func4 = new Funcionario("Ronald", "Connor", "connor@gmail.com");
//			Funcionario func5 = new Funcionario("Jim", "Salvator", "Sal@gmail.com");
//			Funcionario func6 = new Funcionario("Peter", "Henley", "henley@gmail.com");
//
//			Funcionario func7 = new Funcionario("Richard", "Carson", "carson@gmail.com");
//			Funcionario func8 = new Funcionario("Honor", "Miles", "miles@gmail.com");
//			Funcionario func9 = new Funcionario("Tony", "Roggers", "roggers@gmail.com");
//
//			
//			Projeto pro1 = new Projeto("Implantação de grande produção", "NOTSTARTED", "Isso requer todas as mãos no deck para"
//					+ "implantação final do software em produção");
//			Projeto pro2 = new Projeto("Orçamento do novo funcionário",  "COMPLETED", "Decidir sobre um novo orçamento de bônus do funcionário"
//					+ "para o ano e definir quem será promovido");
//			Projeto pro3 = new Projeto("Reconstrução de escritórios", "INPROGRESS", "O prédio de escritórios em Cingapura tem"
//					+ "foi danificado devido ao furacão na região. Isso precisa ser reconstruído\"");
//			Projeto pro4 = new Projeto("Melhorar a segurança da intranet", "INPROGRESS", "Com o hack de dados recente, o escritório"
//					+ "a segurança precisa ser melhorada e uma equipe de segurança adequada precisa ser contratada para "
//					+ "implementação");
//			
//			
//			// need to set both sides of the relationship manually
//
//			pro1.addFuncionario(func1);
//			pro1.addFuncionario(func2);
//			pro2.addFuncionario(func3);
//			pro3.addFuncionario(func1);
//			pro4.addFuncionario(func1);
//			pro4.addFuncionario(func3);
//
//			
//			// need to set both sides of the relationship manually
//
//			func1.setProjetos(Arrays.asList(pro1, pro3, pro4));
//			func2.setProjetos(Arrays.asList(pro1));
//			func3.setProjetos(Arrays.asList(pro2, pro4));
//			
//			// save Funcionarios in database
//
//			funcRepo.save(func1);
//			funcRepo.save(func2); 
//			funcRepo.save(func3); 
//			funcRepo.save(func4);
//			funcRepo.save(func5); 
//			funcRepo.save(func6); 
//			funcRepo.save(func7); 
//			funcRepo.save(func8); 
//			funcRepo.save(func9);
//
//			
//			// save Projetos in database
//
//			projRepo.save(pro1);
//			projRepo.save(pro2); 
//			projRepo.save(pro3); 
//			projRepo.save(pro4);
//			
//			
//		};
//		
//	}
	
}


