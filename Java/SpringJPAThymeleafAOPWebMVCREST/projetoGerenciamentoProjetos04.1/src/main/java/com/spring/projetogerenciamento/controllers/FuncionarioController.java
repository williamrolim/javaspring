package com.spring.projetogerenciamento.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.spring.projetogerenciamento.dao.IFuncionarioRepository;
import com.spring.projetogerenciamento.entidades.Funcionario;

@Controller
@RequestMapping("/funcionarios")
public class FuncionarioController {

	@Autowired
	IFuncionarioRepository repository;
	
	@GetMapping
	public String exibicaoListaFuncionarios(Funcionario func, Model model){
		List<Funcionario> funcionario = repository.findAll();
		model.addAttribute("funcionarioslist", funcionario);
		return "/funcionarios/list-funcionarios";
	}
	@GetMapping("/novo")
	public String exibicaoFormularioFuncionarios(Funcionario func, Model model) {
		model.addAttribute("funcionario", func);
		return "funcionarios/novo-funcionario";//funcionarios é a pasta
	}
	
	@PostMapping("/save")
	public String criandoFuncionario(Funcionario func, Model model) {
		//Salvando o funcionario na base de dados "H2" usando o crud repository	
		repository.save(func);
		return "redirect:/funcionarios/novo";//redireciona direto do navegador funcionarios não é a pasta
	}
	

}
