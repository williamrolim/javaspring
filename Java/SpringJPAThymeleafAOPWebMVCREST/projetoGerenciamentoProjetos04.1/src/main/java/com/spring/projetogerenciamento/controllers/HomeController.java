package com.spring.projetogerenciamento.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.spring.projetogerenciamento.dao.IFuncionarioRepository;
import com.spring.projetogerenciamento.dao.IProjetoRepository;
import com.spring.projetogerenciamento.entidades.Funcionario;
import com.spring.projetogerenciamento.entidades.Projeto;

@Controller
public class HomeController {

	@Autowired
	IFuncionarioRepository repositoryF;
	@Autowired
	IProjetoRepository repositoryP;
	
	@GetMapping("/")
	public String listandoFuncionariosHome(Model model) {
		//Estamos consultando o bando de dados para funcionarios 
		List<Funcionario> funcionario = repositoryF.findAll();
		model.addAttribute("funcionarioslist", funcionario);
		
		//Estamos consultando o bando de dados para projetos
		List<Projeto> projeto = repositoryP.findAll();
		model.addAttribute("projetoslist", projeto);
		return "main/home";
	}
	
}
