new Chart (document.getElementById("myPieChart"), {
	type: 'pie',
	data:{
		labels:['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho'],
		datasets: [{
			label: 'Meu primeiro dataset',
			backgroundColor: ["#3e95cd","#8e5ea2","#EE82EE","#FFFF00","#778899","#20B2AA","#FF6347"],
			borderColor: 'rgb(255, 99, 132)',
			data: [10, 10, 5, 2, 20, 30, 45]
		}]
	},
	
	//Configurando as opções auqi
	options:{}
});