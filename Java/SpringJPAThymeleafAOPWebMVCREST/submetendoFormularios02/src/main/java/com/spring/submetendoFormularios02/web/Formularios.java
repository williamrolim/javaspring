package com.spring.submetendoFormularios02.web;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/menu")
public class Formularios {

	@RequestMapping()
	public String menu() {
		
		return "<form action=\"/menu/filmes\" method=\"POST\">\r\n"
				+ "  <input type=\"submit\" value=\"Filmes\">\r\n"
				+ "</form> "
				+ "<form action=\"/menu/comics\" method=\"POST\">\r\n"
				+ "	<input type=\"submit\" value=\"Comics\">\r\n"
				+ "</form>";
	}
	
	@PostMapping("/filmes")
	public String filmes() {
		return "<h1>Filmes</h1>"
				+ "<form action=\"/menu/userfilmessubmit\" method=\"POST\">\r\n"
				+ "  <label for=\"fname\">First name:</label><br>\r\n"
				+ "  <input type=\"text\" id=\"fname\" name=\"firstname\"><br>\r\n"
				+ "  <label for=\"lname\">Last name:</label><br>\r\n"
				+ "  <input type=\"text\" id=\"lname\" name=\"lastname\"><br><br>\r\n"
				+ "  <input type=\"submit\" value=\"Submit\">\r\n"
				+ "</form> ";
	}
	
	@PostMapping("/comics")
	public String comics() {
		return "<h1>Comics</h1>"
				+ "<form action=\"/menu/usercomicssubmit\"method=\"POST\">\r\n"
				+ "  <label for=\"fname\">First name:</label><br>\r\n"
				+ "  <input type=\"text\" id=\"fname\" name=\"firstname\"><br>\r\n"
				+ "  <label for=\"lname\">Last name:</label><br>\r\n"
				+ "  <input type=\"text\" id=\"lname\" name=\"lastname\"><br><br>\r\n"
				+ "  <input type=\"submit\" value=\"Submit\">\r\n"
				+ "</form> ";
	}
	
	@PostMapping("/userfilmessubmit") //SE FOSSE GET SÓ MUDAR PARA GET MAPPGIN
	public String userfilmesubmit(@RequestParam String firstname, String lastname){
		
		return "form_movie_submitted --- \n Bem vindo : " + firstname + " " + lastname;
	}
	
	@PostMapping("/usercomicssubmit")
	public String usercomicssubmit(@RequestParam String firstname, String lastname){
		
		return "form_comics_submitted \\n Bem vindo : " + firstname + " " + lastname;
	}
	
	
	//funcionaria assim como geth mapping
	/**
	 * 
	 * @param {id}
	 * isso não é u parametro é a variavel do caminho que faz parte do endereço do browser
	 * @PathVariable compativo quando quisermos pegar determinada URI do browser
	 * @return
	 */
	//funcionaria assim como geth mapping
	@RequestMapping(value="/controledeid/{id}",method=RequestMethod.GET )
	public String controleDeId(@PathVariable String id) {
		return "Ordem da id: " + id;
	}

}
