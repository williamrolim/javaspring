package com.spring.submetendoFormularios02;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SubmetendoFormularios02Application {

	public static void main(String[] args) {
		SpringApplication.run(SubmetendoFormularios02Application.class, args);
	}

}
