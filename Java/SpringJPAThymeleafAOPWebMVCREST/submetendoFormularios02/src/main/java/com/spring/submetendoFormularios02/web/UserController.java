package com.spring.submetendoFormularios02.web;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.spring.submetendoFormularios02.dominio.Produto;

@RestController
@RequestMapping("/user")   //essa é a rota que estamos usando para esse controlador
public class UserController {

	@RequestMapping ("/{userid}")
	public String idUsuario(@PathVariable int userid) {
		return "Id capturada : " + userid;
	}
	
	@RequestMapping ("/{userid}/faturas")  //data é o parametro de consulta ? date =
	public String faturasUsuario(@PathVariable int userid, @RequestParam Date date) {
		return "Id capturada : " + userid + "  Data da fatura: " + date;
	}
	
	//Deixando a data opicional (SIM OU NÃO)
	@RequestMapping ("/{userid}/faturasdatas")  //data é o parametro de consulta ? date =
	public String faturasUsuario1(@PathVariable int userid, @RequestParam (value = "date",  required=false)Date date) {
		return "Id capturada : " + userid + "  Data da fatura: " + date;
	}
	
	//exemplo pathvariabel com o mesmo nome "id" do quest map 
	@RequestMapping ("/{id}/faturasdatasteste")  //data é o parametro de consulta ? date =
	public String faturasUsuario2(@PathVariable("id") int userid, @RequestParam (value = "d",  required=false)Date date) {
		return "Id capturada : " + userid + "  Data da fatura: " + date;
	}
	
	//Entregando array de string como se o usuario realizace a compra com seu id
	@RequestMapping ("/{id}/items")  
	public List<String> displayStringJson(){
		return Arrays.asList("bolsa","notebook","comic") ;
	}
	
	@RequestMapping ("/{id}/produtos_com_json")  
	public List<Produto> displayProdutosJson(){
		return Arrays.asList(new Produto(1, "macbook pro i7", 7900.00),
							 new Produto(2,"guitarra explorer", 6000.00),
							 new Produto(3, "mouse", 40.00)) ;
	}
}
