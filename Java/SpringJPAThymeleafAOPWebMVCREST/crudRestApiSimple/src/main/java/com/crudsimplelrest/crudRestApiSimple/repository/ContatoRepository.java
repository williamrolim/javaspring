package com.crudsimplelrest.crudRestApiSimple.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.crudsimplelrest.crudRestApiSimple.entidades.Contato;

public interface ContatoRepository extends JpaRepository<Contato, Long> {

}
