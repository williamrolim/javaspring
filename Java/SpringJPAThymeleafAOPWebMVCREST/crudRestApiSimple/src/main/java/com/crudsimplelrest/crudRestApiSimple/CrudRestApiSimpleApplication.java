package com.crudsimplelrest.crudRestApiSimple;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrudRestApiSimpleApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrudRestApiSimpleApplication.class, args);
	}

}
