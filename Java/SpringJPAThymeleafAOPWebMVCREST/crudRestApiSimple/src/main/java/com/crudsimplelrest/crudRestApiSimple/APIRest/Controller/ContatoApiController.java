package com.crudsimplelrest.crudRestApiSimple.APIRest.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.crudsimplelrest.crudRestApiSimple.entidades.Contato;
import com.crudsimplelrest.crudRestApiSimple.repository.ContatoRepository;

@RestController
@RequestMapping("/contato")
public class ContatoApiController {
	
	@Autowired
	ContatoRepository repository;
	
	@GetMapping
	public List<Contato> listar(){
		return repository.findAll();
	}
	
	@PostMapping
	public void salvar(@RequestBody Contato c) {//@RequestBody o spring converte os dados que estão vindo em json para objeto java
		repository.save(c);
	}
	
	@PutMapping
	public void alterar(@RequestBody Contato c) {
		if(c.getId() > 0)
		repository.save(c);//Tanto salva quanto altera
	}
	
	@DeleteMapping
	public void delete(@RequestBody Contato c) {
		repository.delete(c);
	}
}
