package com.spring.envioformularios.envioFormularioVisualizacoes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EnvioFormularioVisualizacoesApplication {

	public static void main(String[] args) {
		SpringApplication.run(EnvioFormularioVisualizacoesApplication.class, args);
	}

}
