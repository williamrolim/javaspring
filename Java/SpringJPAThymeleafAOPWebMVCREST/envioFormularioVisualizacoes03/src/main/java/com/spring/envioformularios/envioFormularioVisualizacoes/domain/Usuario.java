package com.spring.envioformularios.envioFormularioVisualizacoes.domain;

public class Usuario {
	private String nomeCompleto;
	private int idade;
	private boolean condicaoEmpregado;
	private String genero;
	
	public Usuario() {
	
	}
	
	public Usuario(String nomeCompleto, int idade, boolean condicaoEmpregado, String genero) {
		super();
		this.nomeCompleto = nomeCompleto;
		this.idade = idade;
		this.condicaoEmpregado = condicaoEmpregado;
		this.genero = genero;
	}
	public String getNomeCompleto() {
		return nomeCompleto;
	}
	public void setNomeCompleto(String nomeCompleto) {
		this.nomeCompleto = nomeCompleto;
	}
	public int getIdade() {
		return idade;
	}
	public void setIdade(int idade) {
		this.idade = idade;
	}
	public boolean isCondicaoEmpregado() {
		return condicaoEmpregado;
	}
	public void setCondicaoEmpregado(boolean condicaoEmpregado) {
		this.condicaoEmpregado = condicaoEmpregado;
	}
	public String getGenero() {
		return genero;
	}
	public void setGenero(String genero) {
		this.genero = genero;
	}
	
	
	
}
