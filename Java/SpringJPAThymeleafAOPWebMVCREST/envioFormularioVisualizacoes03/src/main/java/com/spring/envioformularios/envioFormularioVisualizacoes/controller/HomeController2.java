package com.spring.envioformularios.envioFormularioVisualizacoes.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.spring.envioformularios.envioFormularioVisualizacoes.domain.Usuario;

@Controller
public class HomeController2 {
	@RequestMapping("/")//esse controlador lida com todas ações que vão para raiz
	public String index(Model model) {
		model.addAttribute("dadosForm", new Usuario());
		return "index";
	}
	
	//apenas postMapping já resolveria sem value ou method por criterios ditaticos deixarei assim
	@RequestMapping(value="/create", method = RequestMethod.POST)//action=create e o tipo de requisição é Post
	public String dadosFormulario(Usuario user, RedirectAttributes attr) {
		//supondo que persistimos esses dados no banco de dados
		attr.addFlashAttribute("usuario", user);
		return "redirect:/display";
	}
	
	@RequestMapping(value="/display", method = RequestMethod.GET)
	public String exicicaoDadosForm(Usuario user) {
		return "formresult";
	}
}
