package com.spring.envioformularios.envioFormularioVisualizacoes.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.spring.envioformularios.envioFormularioVisualizacoes.domain.Usuario;
//está comentando pois estou usando o controle 2 para testes
//@Controller
public class HomeController {

//	@RequestMapping("/")//esse controlador lida com todas ações que vão para raiz
//	public String index(Model model) {//model a moldura que irá preencher o objeto com os dados preenchidos no formulario
//		model.addAttribute("dadosForm", new Usuario());//Apelido para o objeto no th:object, passando como referencia o proprio objeto
//		//(Emulando)Fazendo a suposição que esses dados foram enviados para o banco de dados para ir ao metodo seguinte
//		
//		return "index";
//	}
//	
//	//apenas postMapping já resolveria sem value ou method por criterios ditaticos deixarei assim
//	@RequestMapping(value="/create", method = RequestMethod.POST)//action=create e o tipo de requisição é Post
//	public String dadosFormulario(Usuario usuario) {//Atravez do parametro trago o objeto já populado e com as anotações do thymelief ele é preenchido
//		
//		return "formresult";
//	}
}
