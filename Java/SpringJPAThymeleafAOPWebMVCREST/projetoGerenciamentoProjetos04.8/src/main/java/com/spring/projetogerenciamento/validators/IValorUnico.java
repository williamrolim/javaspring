package com.spring.projetogerenciamento.validators;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = ValidacaoUnica.class)
public @interface IValorUnico {
	String message() default "Unique Contraint violeted";
	
	Class<?>[] groups() default{};
	
	Class<? extends Payload>[] payload() default{};
}
