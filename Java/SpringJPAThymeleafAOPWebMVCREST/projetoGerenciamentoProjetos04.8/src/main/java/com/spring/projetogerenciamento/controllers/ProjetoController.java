package com.spring.projetogerenciamento.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.spring.projetogerenciamento.entidades.Funcionario;
import com.spring.projetogerenciamento.entidades.Projeto;
import com.spring.projetogerenciamento.service.FuncionarioService;
import com.spring.projetogerenciamento.service.ProjetoService;

@Controller
@RequestMapping("/projetos")
public class ProjetoController {
	
	@Autowired
	ProjetoService repository;
	
	@Autowired
	FuncionarioService repositoryF ;
	
	@GetMapping
	public String exibicaoListaProjetos(Projeto proj, Model model) {
		List<Projeto> projeto = repository.pegueTodosProjetos();
		model.addAttribute("projetoslist", projeto);
		return "/projetos/list-projetos";
	}
	
	@GetMapping("/novo")
	public String exibicaoFormularioProjeto(Model model) {
		Projeto projeto = new Projeto();//Vinculamos um objeto vazio ao projeto para o usuario preenchelo no formulario
		Iterable<Funcionario> funcionario = repositoryF.pegueTodos();
		model.addAttribute("todosFuncionarios", funcionario);
		model.addAttribute("projeto", projeto);
		return "projetos/novo-projeto";
	}
	
	@PostMapping("/save")                        
	public String criandoProjeto(Projeto projeto, Model model) {
		repository.save(projeto);		
		return "redirect:/projetos";//não esta se referindo a nenhuma pasta especifica esta apenas redirecionando para o metodo controlador
		//redirect para evitarmos envios duplicados
		//sempre use o redirect ao salvar uma pagina
	}
	

}
