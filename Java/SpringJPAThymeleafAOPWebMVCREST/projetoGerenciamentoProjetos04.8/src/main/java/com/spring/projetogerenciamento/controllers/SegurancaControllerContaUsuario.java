package com.spring.projetogerenciamento.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import com.spring.projetogerenciamento.dao.ISegurancaContaUsuario;
import com.spring.projetogerenciamento.entidades.ContaUsuario;

@Controller
public class SegurancaControllerContaUsuario {
	@Autowired // quando o usuario registrar no formulario vai criptografar sua senha 
	BCryptPasswordEncoder bCryptEncoder; //decifrar codificador 
	@Autowired
	ISegurancaContaUsuario repositorySecurityCU;
	
	@GetMapping("/registro")
	public String registro(Model model) {
		//Preenchendo o formulario com todas as propriedades corretas
		ContaUsuario contaUsuario = new ContaUsuario();
		model.addAttribute("criandoConta" , contaUsuario);
		return "seguranca/registro";
	}
	
	@PostMapping("/registro/save")
	public String salvandoRegistroUsuario(ContaUsuario cu, Model model) {
		cu.setPassword(bCryptEncoder.encode(cu.getPassword()));//codificando a senha e entregando ela codificada
		repositorySecurityCU.save(cu);
		return "redirect:/";
	}
}
