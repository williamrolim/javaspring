package com.spring.projetogerenciamento.controllers;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ErrosPagesController implements ErrorController {

	@GetMapping("/error")
	public String manipulandoErros(HttpServletRequest request) {// esse objeto tem informações a erros de status
		Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);

		if (status != null) {
			Integer codigoStatus = Integer.valueOf(status.toString());

			if (codigoStatus == HttpStatus.NOT_FOUND.value()) {
				return "pagina_erros/error-404";
			} else if (codigoStatus == HttpStatus.INTERNAL_SERVER_ERROR.value()) {
				return "pagina_erros/error-500";
			} else if (codigoStatus == HttpStatus.FORBIDDEN.value()) {
				return "pagina_erros/error-403";
			}
		}
		return "pagina_erros/error";
	}

	public String getErrorPath() {
		return "/error";
	}
}
