package com.spring.projetogerenciamento.dao;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.spring.projetogerenciamento.entidades.ContaUsuario;

@Repository                                      //PagingAndSortingRepository Extendendo o repositorio de paginação e classificação
public interface ISegurancaContaUsuario extends PagingAndSortingRepository<ContaUsuario, Long> {

}
