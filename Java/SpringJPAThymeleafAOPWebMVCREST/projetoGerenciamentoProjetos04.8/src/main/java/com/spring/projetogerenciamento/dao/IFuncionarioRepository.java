package com.spring.projetogerenciamento.dao;

import java.util.List;

import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

import com.spring.projetogerenciamento.dto.FuncionarioProjeto;
import com.spring.projetogerenciamento.entidades.Funcionario;

@RepositoryRestResource(collectionResourceRel = "apifuncionarios", path = "apifuncionarios")
@Profile("dev")
public interface IFuncionarioRepository extends PagingAndSortingRepository<Funcionario, Long> {
	//descobrir cada projeto que cada funcionario está atribuido
		@Query(nativeQuery = true, value="SELECT f.primeiro_nome as PrimeiroNome, f.sobrenome as Sobrenome, COUNT(pf.funcionario_id) as ContagemProjetos "
				+ "FROM funcionario f LEFT JOIN projeto_funcionario pf ON pf.funcionario_id = f.funcionario_id "
				+ "GROUP BY f.primeiro_nome, f.sobrenome ORDER BY 3 DESC")
		public List<FuncionarioProjeto>funcionarioProjetos();
		
		public Funcionario findByEmail(String value);
		
		//public Funcionario findById(long id);
}
