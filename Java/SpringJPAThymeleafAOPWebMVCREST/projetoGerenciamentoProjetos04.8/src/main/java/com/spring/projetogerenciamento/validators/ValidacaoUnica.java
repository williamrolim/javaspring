package com.spring.projetogerenciamento.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import com.spring.projetogerenciamento.dao.IFuncionarioRepository;
import com.spring.projetogerenciamento.entidades.Funcionario;

public class ValidacaoUnica implements ConstraintValidator<IValorUnico, String> {

	@Autowired
	IFuncionarioRepository repositoryFunc;
	
	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		System.out.println("Metodo de validação atuando");
		Funcionario funcionario = repositoryFunc.findByEmail(value);
		
		if (funcionario != null) {
			return false;
		}else
			return true;
	}

}
