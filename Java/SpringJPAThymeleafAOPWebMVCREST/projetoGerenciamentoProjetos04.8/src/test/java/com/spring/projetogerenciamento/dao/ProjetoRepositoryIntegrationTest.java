package com.spring.projetogerenciamento.dao;


import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.context.junit4.SpringRunner;

import com.spring.projetogerenciamento.Projetogerenciamento04Application;
							//Estamos dizendo ao teste de integração o ponto de partida da aplicação (será capaz de trazer todos os filhos relacionados)
import com.spring.projetogerenciamento.dao.IProjetoRepository;
import com.spring.projetogerenciamento.entidades.Projeto;

//@ContextConfiguration(classes=Projetogerenciamento04Application.class)
//@RunWith(SpringRunner.class)
//@DataJpaTest //Teste de Integração relacionado a dados (para sabermos que estamos lidando com bd temporarios)
//@SqlGroup({@Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = {"classpath:schema.sql", "classpath:data.sql"}),  //Grupo sequencial ,,, arquivos que serão executantes antes do metodo de testes temos que garantir que os dados são iguais scripts = {"classpath:schema.sql", "classpath:data.sql"}
//		   @Sql(executionPhase = ExecutionPhase.AFTER_TEST_METHOD, scripts = "classpath:drop.sql")}) //AFTER SERÁ EXECUTADO APOST O METODO DE TESTE,TODAS AS TABELAS SERAO ELIMINADAS APOS O METODO TESTE - temos controle total sobre a excecução de scripts, o teste irá rodar da mesma forma de nos livramos desse script - apenas exemplo
@SpringBootTest
@RunWith(SpringRunner.class)
@SqlGroup({@Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = {"classpath:schema.sql", "classpath:data.sql"}),  //Grupo sequencial ,,, arquivos que serão executantes antes do metodo de testes temos que garantir que os dados são iguais scripts = {"classpath:schema.sql", "classpath:data.sql"}
	   @Sql(executionPhase = ExecutionPhase.AFTER_TEST_METHOD, scripts = "classpath:drop.sql")}) //AFTER SERÁ EXECUTADO APOST O METODO DE TESTE,TODAS AS TABELAS SERAO ELIMINADAS APOS O METODO TESTE - temos controle total sobre a excecução de scripts, o teste irá rodar da mesma forma de nos livramos desse script - apenas exemplo
public class ProjetoRepositoryIntegrationTest {
//Teste de Integração
//Classe criada exclusivamente para executarmos nossos testes no db H2
	
	@Autowired
	IProjetoRepository projetoRepo;
	
	@Test
	public void seProjetoForSalvo_EntaoSucesso() {//nomear os testes mais explicitamente possivel para saber exatamente o que esta acontecendo
		Projeto newProjeto = new Projeto("Novo Teste Projeto", "COMPLETE", "Teste Descrição");
		projetoRepo.save(newProjeto);
		
		assertEquals(5, projetoRepo.findAll().size());//confirma que duas variaves são iguais
	}
}
