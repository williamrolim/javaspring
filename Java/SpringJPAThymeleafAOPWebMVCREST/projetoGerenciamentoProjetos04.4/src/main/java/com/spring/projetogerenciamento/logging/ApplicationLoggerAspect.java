package com.spring.projetogerenciamento.logging;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;


@Aspect //aspect é um codigo que é executado antes ou depois em torno de cortes pontuais definidos pelo dev
@Component
public class ApplicationLoggerAspect {
	
	private final Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Pointcut("within(com.spring.projetogerenciamento.controllers..*)") //..as classes , os metodos e tudo mais
	public void definindoCortesPontuaisPacote() {
		//metodo vazio apenas local especifico do ponto de corte
	}
	
	//after é exutado apos todo os controllers que foram definidos na anotação pointcut
	@After("definindoCortesPontuaisPacote()")//@After significa que esse metodo será executado apos tudo dos controllers
	public void log() {
		log.debug("---------------------------------AFTER---------------------------------");
	}
	
	//after é exutado apos todo os controllers que foram definidos na anotação pointcut
	@Before("definindoCortesPontuaisPacote()")//@After significa que esse metodo será executado apos tudo dos controllers
	public void logBefore() {
		log.debug("---------------------------------BEFORE---------------------------------");
	}
}
