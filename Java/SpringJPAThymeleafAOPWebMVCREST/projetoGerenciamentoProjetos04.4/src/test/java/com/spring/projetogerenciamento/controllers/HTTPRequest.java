package com.spring.projetogerenciamento.controllers;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class HTTPRequest {
//Vamos ver se podemos obter uma solicitação get na pagina inicial quando o app for carregado
	
	@LocalServerPort
	private int porta; //vamos pegar o número da porta e alocar
	
	@Autowired
	private TestRestTemplate restTemplate;//adequados para testes de integração, pode carregar cabeçalhos de autenticação basicos
	//TestRestTemplate: obter e recuperar recursos de paginas de web assim como objetos, recuperar JSON head cabeçalhos
	
	@Test
	public void homePageRetornarNumeroVersaoCorretamente() {
		String renderedHtml = this.restTemplate.getForObject("http://localhost:" + porta + "/", String.class);
		assertEquals(renderedHtml.contains("3.3.3"), true);
	}
}
