package com.spring.projetogerenciamento.dto;

import java.util.Date;
//OS campos devem corresponder aos respectivos nomes (html: projeto_linhadotempo) e (classe: IProjetoRepository)

public interface GraficoDataTempo {
	public String getProjetoNome();
	public Date getProjetoDataInicial();
	public Date getProjetoDataFinal();
}
