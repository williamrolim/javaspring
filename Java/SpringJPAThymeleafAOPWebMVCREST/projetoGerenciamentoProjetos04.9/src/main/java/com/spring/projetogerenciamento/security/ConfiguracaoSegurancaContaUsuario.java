package com.spring.projetogerenciamento.security;


import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
@Configuration
@EnableWebSecurity
public class ConfiguracaoSegurancaContaUsuario extends WebSecurityConfigurerAdapter {
	
	@Autowired
	DataSource dataSource;
	
	@Autowired // quando o usuario registrar no formulario vai criptografar sua senha 
	BCryptPasswordEncoder bCryptEncoder; //decifrar codificador 
	
	public void configure(AuthenticationManagerBuilder auth) throws Exception {
		//estabelecimento na memoria de nome de usuario e de senhas
		auth.jdbcAuthentication()
		.usersByUsernameQuery("select usuario,password,enable " +
		"from contas_usuarios where usuario = ?")
		.authoritiesByUsernameQuery("select usuario, role " +
		"from contas_usuarios where usuario = ?")
		.dataSource(dataSource)  //especificar a fonte de dados;
		.passwordEncoder(bCryptEncoder);//usamos esse codificador de senha para descriptografar a senha
	}
	
	@Override
	protected void configure(HttpSecurity http)throws Exception{
		http.authorizeRequests() //a ordem da hierarquia nesse metodo é importante, se colocassemos .permitAll() acima, o spring iria achar que todos teriam acesso e não leria os demais 
//		.antMatchers("/projetos/novo").hasRole("ADMIN") //hasRole espera ROLE_ADMIN
//		.antMatchers("/projetos/save").hasRole("ADMIN")//hasRole espera ROLE_ADMIN
//		.antMatchers("/funcionarios/novo").hasRole("ADMIN")//.antMatchers("/funcionarios/novo").hasAuthority("ADMIN")hasAuthority espera Somente ADMIN
//		.antMatchers("/funcionarios/save").hasRole("ADMIN")	
		.antMatchers("/", "/**")// tudo apos a barra "/" , será acessada por outros usuarios
		.permitAll()
		.and()
		.formLogin();//suporte a autenticação baseada em formulario
		http.csrf().disable();
	}
}
