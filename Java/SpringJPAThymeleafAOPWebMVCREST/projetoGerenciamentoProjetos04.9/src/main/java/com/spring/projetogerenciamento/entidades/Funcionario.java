package com.spring.projetogerenciamento.entidades;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.spring.projetogerenciamento.validators.IValorUnico;

@Entity
public class Funcionario {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "funcionario_seq")
	@SequenceGenerator(name = "funcionario_seq", allocationSize = 1)
	private long funcionarioId;
	
	@NotBlank(message = "*Devemos fornecer um nome")
	@Size (min=2,max=20)
	private String primeiroNome;
	@NotBlank(message = "*Devemos fornecer um sobrenome")
	@Size (min=1,max=50)
	private String sobrenome;
	@NotBlank
	@Email(message = "*Devemos fornecer um endereço de email corretor")
	@IValorUnico         //@Column(unique = true)//nullable = false
	private String email;
	
	//relação muito para muitos em ambos os lados (Funcionario & Projeto)
	@ManyToMany(cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH,CascadeType.PERSIST}, fetch = FetchType.LAZY)                     //muitos funcionarios podem ser atribuidos ao mesmo projeto
	//@JoinColumn(name="projeto_id") //projeto_id representara a relação de chave estrangeira 
	@JoinTable(name="projeto_funcionario",// representa a tabela de junção, funcionario e projeto (tabela)
	joinColumns = @JoinColumn(name="funcionario_id"),//atributo de coluna de junção  private long funcionarioId;
	inverseJoinColumns = @JoinColumn(name="projeto_id") )	
	
	@JsonIgnore //JSON IGNORA ESSES CAMPOS ESPECIFICOS PARA SERELIZACAO
	private List<Projeto> projetos;//o projeto especifico ao qual o funcionaiorio será destinado
					//mapear a propriedade theProjeto
	
	//merge se fundir o projeto A com o projeto B os filhos dos projetos seriam fundidos tbém
	public Funcionario() {

	}
	
	public Funcionario(String primeiroNome, String sobrenome, String email) {
	
		this.primeiroNome = primeiroNome;
		this.sobrenome = sobrenome;
		this.email = email;
	}

	
	public long getFuncionarioId() {
		return funcionarioId;
	}
	public void setFuncionarioId(long funcionarioId) {
		this.funcionarioId = funcionarioId;
	}
	public String getPrimeiroNome() {
		return primeiroNome;
	}
	public void setPrimeiroNome(String primeiroNome) {
		this.primeiroNome = primeiroNome;
	}

	
	public String getSobrenome() {
		return sobrenome;
	}

	public void setSobrenome(String sobrenome) {
		this.sobrenome = sobrenome;
	}

	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

	public List<Projeto> getProjetos() {
		return projetos;
	}

	public void setProjetos(List<Projeto> projetos) {
		this.projetos = projetos;
	}

	
}
