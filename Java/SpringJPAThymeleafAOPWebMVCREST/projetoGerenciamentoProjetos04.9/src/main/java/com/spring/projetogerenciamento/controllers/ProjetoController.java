package com.spring.projetogerenciamento.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.spring.projetogerenciamento.dto.GraficoDataTempo;
import com.spring.projetogerenciamento.entidades.Funcionario;
import com.spring.projetogerenciamento.entidades.Projeto;
import com.spring.projetogerenciamento.service.FuncionarioService;
import com.spring.projetogerenciamento.service.ProjetoService;

@Controller
@RequestMapping("/projetos")
public class ProjetoController {
	
	@Autowired
	ProjetoService repository;
	
	@Autowired
	FuncionarioService repositoryF ;
	
	@GetMapping
	public String exibicaoListaProjetos(Projeto proj, Model model) {
		List<Projeto> projeto = repository.pegueTodosProjetos();
		model.addAttribute("projetoslist", projeto);
		return "/projetos/list-projetos";
	}
	
	@GetMapping("/novo")
	public String exibicaoFormularioProjeto(Model model) {
		Projeto projeto = new Projeto();//Vinculamos um objeto vazio ao projeto para o usuario preenchelo no formulario
		Iterable<Funcionario> funcionario = repositoryF.pegueTodos();
		model.addAttribute("todosFuncionarios", funcionario);
		model.addAttribute("projeto", projeto);
		return "projetos/novo-projeto";
	}
	
	@PostMapping("/save")                        
	public String criandoProjeto(Model model,@Valid Projeto projeto, Errors erro) {
		if(erro.hasErrors()) {
			Iterable<Funcionario> funcionario = repositoryF.pegueTodos();
			model.addAttribute("todosFuncionarios", funcionario);
			return "projetos/novo-projeto";
		}
		Iterable<Funcionario> funcionario = repositoryF.pegueTodos();
		model.addAttribute("todosFuncionarios", funcionario);
		//model.addAttribute("projeto",projeto);
		repository.save(projeto);		
		return "redirect:/projetos";//não esta se referindo a nenhuma pasta especifica esta apenas redirecionando para o metodo controlador
		//redirect para evitarmos envios duplicados 
		//sempre use o redirect ao salvar uma pagina
	}
	
	@GetMapping("/atualizar")
	public String exibirFormularioAtualizacaoProjeto(@RequestParam("id")long id,Projeto projeto, Model model, Errors erros) {
		if(erros.hasErrors()) {
			return "projetos/novo-projeto";
		}
		projeto = repository.findById(id);
		model.addAttribute("projeto", projeto);
		Iterable<Funcionario> funcionario = repositoryF.pegueTodos();
		model.addAttribute("todosFuncionarios", funcionario);

		return "projetos/novo-projeto";
	}
	
	@GetMapping("/deletar")
	public String deletarProjeto(@RequestParam("id") long id) {
		try {
			repository.deleteById(id);
		}catch (EmptyResultDataAccessException e) {
			// TODO: handle exception
		}
		return "redirect:/projetos";
	}
	
	@GetMapping("/linhadotempo")
	public String exibicaoLinhaDoTempoProjeto(Model model) throws JsonProcessingException {
		List<GraficoDataTempo> dataLinhaTempo = repository.getTimeData();
		ObjectMapper objectMapper = new ObjectMapper();
		String linhaDoTempoJSON = objectMapper.writeValueAsString(dataLinhaTempo);
		System.out.println("----------------*Projeto Linha do Tempo*----------------");
		System.out.println(linhaDoTempoJSON);
		
		model.addAttribute("projetoLinhaTempoJSON",linhaDoTempoJSON);
		return "projetos/projeto-linhadotempo";
	}
	
}
