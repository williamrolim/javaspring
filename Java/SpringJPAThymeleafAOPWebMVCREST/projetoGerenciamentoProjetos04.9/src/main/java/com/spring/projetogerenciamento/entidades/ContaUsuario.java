package com.spring.projetogerenciamento.entidades;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "contas_usuarios")
public class ContaUsuario {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="contas_usuarios_seq")
	@SequenceGenerator(name = "contas_usuarios_seq", allocationSize = 1)
	@Column(name = "usuario_id")
	private long usuarioID;
	
	private String usuario;
	
	private String email;
	private String password;
	private boolean enable = true;
	
	public ContaUsuario() {
	}
	
//	public ContaUsuario(String usuario, String email, String password, boolean enabled) {
//		this.usuario = usuario;
//		this.email = email;
//		this.password = password;
//		this.enabled = enabled;
//	}

	public long getUsuarioID() {
		return usuarioID;
	}

	public void setUsuarioID(long usuarioID) {
		this.usuarioID = usuarioID;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isEnable() {
		return enable;
	}

	public void setEnable(boolean enable) {
		this.enable = enable;
	}
	
}
