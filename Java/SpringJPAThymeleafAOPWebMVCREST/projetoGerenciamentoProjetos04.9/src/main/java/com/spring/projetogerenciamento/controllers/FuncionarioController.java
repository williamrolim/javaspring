package com.spring.projetogerenciamento.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.spring.projetogerenciamento.entidades.Funcionario;
import com.spring.projetogerenciamento.service.FuncionarioService;

@Controller
@RequestMapping("/funcionarios")
public class FuncionarioController {

	@Autowired
	FuncionarioService repository;
	
	@GetMapping
	public String exibicaoListaFuncionarios(Funcionario func, Model model){
		Iterable<Funcionario> funcionario = repository.pegueTodos();
		model.addAttribute("funcionarioslist", funcionario);
		return "/funcionarios/list-funcionarios";
	}
	@GetMapping("/novo")
	public String exibicaoFormularioFuncionarios(Funcionario func, Model model) {
		model.addAttribute("funcionario", func);
		return "funcionarios/novo-funcionario";//funcionarios é a pasta
	}
	
	@PostMapping("/save")
	public String criandoFuncionario(Model model,@Valid Funcionario func, Errors erros) {
		if(erros.hasErrors()) {
			return "funcionarios/novo-funcionario";
		}
		//Salvando o funcionario na base de dados "H2" usando o crud repository	
		repository.save(func);
		return "redirect:/funcionarios";//redireciona direto do navegador funcionarios não é a pasta
	}
	
	@GetMapping("/atualizar")
	public String exibirFormularioAtualizacaoFuncionario(Model model,@RequestParam("id") long id, Funcionario funcionario, Errors erro) {
		if(erro.hasErrors()) {
			return "funcionarios/novo-funcionario";
		}
		funcionario = repository.findByFuncionarioId(id);
		
		model.addAttribute("funcionario", funcionario);
		return "funcionarios/novo-funcionario";
	}
	
	@GetMapping("/deletar")
	public String deletarFuncionario(@RequestParam("id") long id, Model model) {		
		try {
			repository.deleteById(id);
		} catch (EmptyResultDataAccessException e) {
			// TODO: handle exception
		}
		return "redirect:/funcionarios";
	}

}
