package com.spring.projetogerenciamento.dao;

import java.util.List;

import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.spring.projetogerenciamento.dto.GraficoDataTempo;
import com.spring.projetogerenciamento.dto.GraficoStatusProjeto;
import com.spring.projetogerenciamento.entidades.Projeto;

//Obs:por convenção em projetos profissionais devemos adicionaro  
//I na frente quando se trata de interfaces
@Repository
@Profile("dev")
public interface IProjetoRepository extends PagingAndSortingRepository<Projeto, Long> {

	//O metodo padrão do findAll traz um Iterable sobrescrevesmo o mesmo para trazer lista
	@Override
	List<Projeto> findAll();
	
	@Query(nativeQuery = true, value="SELECT estagio as EstagioProjeto , COUNT(*) as Contagem "
			+ "FROM projeto "
			+ "GROUP BY estagio;")
	public List<GraficoStatusProjeto> getContagemProjetoStatus();
									//OS ALIAS devem corresponder aos respectivos nomes do array javascript (classe: projeto_linhadotempo) e (classe IProjetoRepository)
	@Query(nativeQuery = true, value="SELECT name as projetoNome ,data_inicial as projetoDataInicial, data_final as projetoDataFinal from projeto "
			+ " where data_inicial IS NOT NULL")
	public List<GraficoDataTempo> getTimeData();//query acima preenche o objeto GraficoDataTempo
}
