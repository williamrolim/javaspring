package com.spring.projetogerenciamento.api.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.spring.projetogerenciamento.dao.IFuncionarioRepository;
import com.spring.projetogerenciamento.entidades.Funcionario;

@RestController
@RequestMapping("/app-api/funcionario")
public class FuncionarioAPIController {

	@Autowired
	IFuncionarioRepository funcRepo;
	
	@GetMapping
	public Iterable<Funcionario> getFuncionarios(){
		return funcRepo.findAll();
	}
	
	@GetMapping("/{id}")//http://localhost:8090/app-api/funcionarios/
	public Funcionario getById(@PathVariable("id") Long id) {		
		return funcRepo.findById(id).get();	
	}
	
	@PostMapping(consumes = "application/json")
	@ResponseStatus(HttpStatus.CREATED)
	public void create(@RequestBody @Valid Funcionario funcionario) {
		funcRepo.save(funcionario);
	}
	
	@PutMapping(consumes = "application/json")
	@ResponseStatus(HttpStatus.OK)
	public Funcionario update(@RequestBody @Valid Funcionario funcionario) {
		return funcRepo.save(funcionario);
	}
	

	@PatchMapping(path="/{id}", consumes = "application/json")
	public Funcionario partialUpdate(@PathVariable("id") long id, @RequestBody @Valid Funcionario patchFuncionario) {
		Funcionario func = funcRepo.findById(id).get();
		
		if(func.getEmail() != null) {
			func.setEmail(patchFuncionario.getEmail());
		}
		if(func.getPrimeiroNome() != null) {
			func.setPrimeiroNome(patchFuncionario.getPrimeiroNome());
		}
		if(func.getSobrenome() != null) {
			func.setSobrenome(patchFuncionario.getSobrenome());
		}
		
		return funcRepo.save(func);		
	}
	
	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void delete(@PathVariable("id") Long id) {
		try {
			funcRepo.deleteById(id);
		} catch (EmptyResultDataAccessException e) {
			// TODO: handle exception
		}
	}
	
	///app-api/funcionario?page=0&size=1app-api/ proejto?page=0&size=1 no rest
	@GetMapping(params = {"page", "size"})
	@ResponseStatus(HttpStatus.OK)
	public Iterable<Funcionario> paginacaoPageProjetos(@RequestParam("page") int page,
			@RequestParam("size") int size){
		Pageable paginacaoETamanho = PageRequest.of(page, size);
		return funcRepo.findAll(paginacaoETamanho);
	}
}
