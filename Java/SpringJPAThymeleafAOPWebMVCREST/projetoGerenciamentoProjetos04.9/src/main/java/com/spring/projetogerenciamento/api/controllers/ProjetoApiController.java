package com.spring.projetogerenciamento.api.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.spring.projetogerenciamento.dao.IProjetoRepository;
import com.spring.projetogerenciamento.entidades.Projeto;

@RestController
@RequestMapping("/app-api/projeto")
public class ProjetoApiController {

	@Autowired
	IProjetoRepository repository;
	
	@GetMapping
	public List<Projeto>encontreTodos(){
		return repository.findAll();
	}
	
	@GetMapping("/{id}")
	public Projeto achePeloId(@PathVariable("id") Long id) {
		return repository.findById(id).get();
	}
	
	@PostMapping(consumes = "application/json")
	@ResponseStatus(HttpStatus.CREATED)
	public Projeto save (@RequestBody Projeto projeto) {
		return repository.save(projeto);
	}
	
	@PutMapping(consumes = "application/json")
	@ResponseStatus(HttpStatus.OK)
	public Projeto atualizar(@RequestBody @Valid Projeto projeto) {
		return repository.save(projeto);
	}
	
	@PatchMapping(path = "/{id}" , consumes = "application/json")
	public Projeto atualizacaoParcial(@PathVariable("id") long id, @RequestBody @Valid Projeto patchProjeto) {
		Projeto existeProjetos = repository.findById(id).get();
		
		if (patchProjeto.getName() != null) {
			existeProjetos.setName(patchProjeto.getName());
		}
		if (patchProjeto.getEstagio() != null) {
			existeProjetos.setEstagio(patchProjeto.getEstagio());
		}
		if (patchProjeto.getDescricao() != null) {
			existeProjetos.setDescricao(patchProjeto.getDescricao());
		}
		return repository.save(existeProjetos);
	}
	
	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void delete(@PathVariable("id") long id) {
		try {
			repository.deleteById(id);
		} catch (EmptyResultDataAccessException e) {
			// TODO: handle exception
		}
	}
	
	///app-api/proejto?page=0&size=1 no rest
	@GetMapping(params = {"page", "size"})
	@ResponseStatus(HttpStatus.OK)
	public Iterable<Projeto> paginacaoPageProjetos(@RequestParam("page") int page,
			@RequestParam("size") int size){
		Pageable paginacaoETamanho = PageRequest.of(page, size);
		return repository.findAll(paginacaoETamanho);
	}
}
