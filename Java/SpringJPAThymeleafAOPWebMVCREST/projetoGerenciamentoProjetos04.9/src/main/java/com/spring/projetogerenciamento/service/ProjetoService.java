package com.spring.projetogerenciamento.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.projetogerenciamento.dao.IProjetoRepository;
import com.spring.projetogerenciamento.dto.GraficoDataTempo;
import com.spring.projetogerenciamento.dto.GraficoStatusProjeto;
import com.spring.projetogerenciamento.entidades.Projeto;

@Service
public class ProjetoService {

	@Autowired
	public IProjetoRepository projetoRepo;
	
	public Projeto save(Projeto projeto) {
		return projetoRepo.save(projeto);
	}
	
	public List<Projeto>pegueTodosProjetos(){
		return projetoRepo.findAll();
	}
	
	public List<GraficoStatusProjeto>projetoStatusGrafico(){
		return projetoRepo.getContagemProjetoStatus();
	}

	public Projeto findById(long id) {
		// TODO Auto-generated method stub
		return projetoRepo.findById(id).get();
	}

	public void deleteById(long id) {
		projetoRepo.deleteById(id);
	}

	public List<GraficoDataTempo> getTimeData() {
		
		return projetoRepo.getTimeData();
	}
	
}
