package com.spring.projetogerenciamento.dto;
//os dados serão armazenados nesses objetos de tranferencia de dados
public interface GraficoStatusProjeto {
   public String getEstagioProjeto();
   public String getContagem();
}
