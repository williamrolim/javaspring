CREATE SEQUENCE IF NOT EXISTS funcionario_seq;

CREATE TABLE IF NOT EXISTS funcionario (

funcionario_id BIGINT NOT NULL DEFAULT nextval('funcionario_seq') PRIMARY KEY,
email VARCHAR(100) NOT NULL,
primeiro_nome VARCHAR(100) NOT NULL,
sobrenome VARCHAR(100) NOT NULL

);

CREATE SEQUENCE IF NOT EXISTS projeto_seq;

CREATE TABLE IF NOT EXISTS projeto (

projeto_id BIGINT NOT NULL DEFAULT nextval('projeto_seq') PRIMARY KEY,
name VARCHAR(100) NOT NULL,
estagio VARCHAR(100) NOT NULL,
descricao VARCHAR(500) NOT NULL

);


CREATE TABLE IF NOT EXISTS projeto_funcionario (

projeto_id BIGINT REFERENCES projeto, 
funcionario_id BIGINT REFERENCES funcionario

);