package com.spring.conceitosBasicosSpring.exemplos;



public class Carro {

	 Motor m;
	 Portas p;
	 Pneus ps;
	public Carro(Motor m, Portas p, Pneus ps) {
		super();
		this.m = m;
		this.p = p;
		this.ps = ps;
	}

	public String imprimaDetalhesDoCarro(){
	 return("Motor" + this.m +  this.p + "  Rodas : " + this.ps);
	}
	 
	 
	}
