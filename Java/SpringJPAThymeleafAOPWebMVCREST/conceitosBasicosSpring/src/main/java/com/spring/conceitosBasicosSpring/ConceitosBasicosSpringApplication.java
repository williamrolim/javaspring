package com.spring.conceitosBasicosSpring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
											//pacote Default (padrão)            pacote trazendo filho diferente do pacote
@SpringBootApplication(scanBasePackages = {"com.spring.conceitosBasicosSpring","com.spring.TrazerPacoteDiferenteProjeto"})//agora vai escanear todos os pacotes	
public class ConceitosBasicosSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConceitosBasicosSpringApplication.class, args);
	}

}
