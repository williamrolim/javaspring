package com.spring.conceitosBasicosSpring.exemplos;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ExemploClasseBeanMaster {

	@Bean
	public Carro newCar() {
		Motor m = new Motor();
		Portas p = new Portas();
		Pneus ps = new Pneus();
		return new Carro(m,p,ps);
		
	}

}
