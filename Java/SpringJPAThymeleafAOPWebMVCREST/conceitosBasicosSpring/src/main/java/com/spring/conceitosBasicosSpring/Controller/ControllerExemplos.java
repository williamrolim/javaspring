package com.spring.conceitosBasicosSpring.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ControllerExemplos {

//Exemplo Lendo dado do application.properties
	@Value("${version}")
	private String versaoDoApplicationProperties;
	
	@Autowired
	public void teste() {
		System.out.println(" VERSAO " + versaoDoApplicationProperties);

		
	}
}
