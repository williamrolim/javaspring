package com.spring.conceitosBasicosSpring.PrimaryQualifierAnnotation;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

@Primary //quando temos 2 beans desputando o mesmo recurso temos que colocar a anotação para definir qual é o primerio
@Repository
public class IEquipeRepositoryImpl1 implements IEquipeRepository{

}
