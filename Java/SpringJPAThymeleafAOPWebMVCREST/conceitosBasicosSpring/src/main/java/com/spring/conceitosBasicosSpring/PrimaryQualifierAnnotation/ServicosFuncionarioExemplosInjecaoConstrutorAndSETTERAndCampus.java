package com.spring.conceitosBasicosSpring.PrimaryQualifierAnnotation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
@Service// @Component @Repository @Service essa classe não seria registrada no conceito spring
public class ServicosFuncionarioExemplosInjecaoConstrutorAndSETTERAndCampus {

	//agora temos 2 implementações em IMPL1 E IMPL2 o spring reclamara dizendo que tem 2 beans o que queremos fazer aqui?

	//O que acontece quando se tem diversos @Bean disputando a mesma injeção

	//IEquipeRepository equipeRepo;
	
	//Exemplo dos beans disputando recursos
	//Com primary Declarado em Impl1
	
//	public ServicosFuncionarioExemplosInjecaoConstrutorAndSETTERAndCampus(IEquipeRepository equipeRepo) {
//		this.equipeRepo = equipeRepo;
//	}
//	
	//_________________________________________________________________________________________

// Selecionado o Bean especifico com a anotação @Qualifier
//	public ServicosFuncionarioExemplosInjecaoConstrutorAndSETTERAndCampus(@Qualifier("iEquipeRepositoryImpl1") IEquipeRepository equipeRepo) {
//	this.equipeRepo = equipeRepo;
//	}
	//_________________________________________________________________________________________

	//Exemplo quando se trata de injeção de campo
	
	//Injeção de campo
	@Autowired
	IEquipeRepository equipeRepo;

}
