package com.spring.projetogerenciamento.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
@Configuration
@EnableWebSecurity
public class ConfiguracaoSeguranca extends WebSecurityConfigurerAdapter {
	@Override
	public void configure(AuthenticationManagerBuilder auth) throws Exception {
		//estabelecimento na memoria de nome de usuario e de senhas
		auth.inMemoryAuthentication()
		.withUser("myUser")
		.password("pass")
		.roles("USER")
			.and()
				.withUser("William")
				.password("wil")
				.roles("USER")
					.and()
					.withUser
					("managerUser")
					.password("pass123")
					.roles("ADMIN");
	}
	
	//esse bean será registrado conforme o aplicativo carrega
	@Bean
	public PasswordEncoder getPasswordEncoder() {
		return NoOpPasswordEncoder.getInstance();
	}
	
	@Override
	protected void configure(HttpSecurity http)throws Exception{
		http.authorizeRequests()
		.antMatchers("/projetos/novo")
		.hasRole("ADMIN")
		.antMatchers("/funcionarios.novo")
		.hasRole("ADMIN")
		.antMatchers("/")// qualquer pessoa que estiver autenticada será capaz de acessar a barra de ponto
		.authenticated()
		.and()
		.formLogin();//suporte a autenticação baseada em formulario
	}
}
