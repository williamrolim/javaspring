var graficoDataStr = decodeHtml(chartData);//charData está vindo da variavel do javascript	var charData = "[[${projetoStatusContagem}]]";
//AFINAL VEM ASSIM DO console DOM html [{&quot;estagioProjeto&quot;:&quot;NOTSTARTED&quot;,&
var graficoJsonArray = JSON.parse(graficoDataStr);

var arrayLength = graficoJsonArray.length;

var numericData = [];
var labelData = [];

for(var i = 0; i < arrayLength; i++){ //poderemos popular o grafico com os respectivos status do projeto e valores
	numericData[i] = graficoJsonArray[i].contagem;//os respectivos campus contagem e estagio projeto se encontram tanto na query quanto na inferface dto ProjetoStatusGrafico
	labelData[i] = graficoJsonArray[i].estagioProjeto;
}


new Chart(document.getElementById("myPieChart"), { //myPieChart id do javascript
	type: 'pie',
	data:{
		labels: labelData,
		datasets: [{
			label: 'Meu primeiro dataset',
			backgroundColor: ["#3e95cd","#8e5ea2","#3cba9f"],
			//borderColor: 'rgb(255, 99, 132)',
			data: numericData
		}]
	},
	
	//Configurando as opções auqi
	options:{ //isso é um objeto json
		title: {
			display: true,
			text: 'Status Do Projeto'
		}
	}
});

//[{"value: 1, "label":"COMPLETED"},{"value": 2, "label":"INPROGRESS"},{"value": 3, "label":"NOTSTARTED}"}]
function decodeHtml(html){
	var txt = document.createElement("textarea");
	txt.innerHTML = html;
	return txt.value;
}