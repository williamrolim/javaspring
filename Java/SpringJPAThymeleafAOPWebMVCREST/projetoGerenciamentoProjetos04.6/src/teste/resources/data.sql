-- INSERT EMPLOYEES			
insert into funcionario (funcionario_id, primeiro_nome, sobrenome, email) values (nextval('funcionario_seq'), 'John', 'Warton', 'warton@gmail.com');
insert into funcionario (funcionario_id, primeiro_nome, sobrenome, email) values (nextval('funcionario_seq'), 'Mike', 'Lanister', 'lanister@gmail.com');
insert into funcionario (funcionario_id, primeiro_nome, sobrenome, email) values (nextval('funcionario_seq'), 'Steve', 'Reeves', 'Reeves@gmail.com');
insert into funcionario (funcionario_id, primeiro_nome, sobrenome, email) values (nextval('funcionario_seq'), 'Ronald', 'Connor', 'connor@gmail.com');
insert into funcionario (funcionario_id, primeiro_nome, sobrenome, email) values (nextval('funcionario_seq'), 'Jim', 'Salvator', 'Sal@gmail.com');
insert into funcionario (funcionario_id, primeiro_nome, sobrenome, email) values (nextval('funcionario_seq'), 'Peter', 'Henley', 'henley@gmail.com');
insert into funcionario (funcionario_id, primeiro_nome, sobrenome, email) values (nextval('funcionario_seq'), 'Richard', 'Carson', 'carson@gmail.com');
insert into funcionario (funcionario_id, primeiro_nome, sobrenome, email) values (nextval('funcionario_seq'), 'Honor', 'Miles', 'miles@gmail.com');
insert into funcionario (funcionario_id, primeiro_nome, sobrenome, email) values (nextval('funcionario_seq'), 'Tony', 'Roggers', 'roggers@gmail.com');

-- INSERT PROJECTS			
-- INSERT PROJECTS			
insert into projeto (projeto_id, name, estagio, descricao) values (nextval('projeto_seq'), 'Implementação de grande produção', 'NOTSTARTED', 'Isso requer todas as mãos para a implementação final do software na produção');
insert into projeto (projeto_id, name, estagio, descricao) values (nextval('projeto_seq'), 'Novo orçamento funcional',  'COMPLETED', 'Deliberar sobre o novo orçamento funcionariobonus do ano e definir quem será promovido');
insert into projeto (projeto_id, name, estagio, descricao) values (nextval('projeto_seq'), 'Reconstrução de escritórios', 'INPROGRESS','O prédio de escritórios em Brasilandia foi danificado devido ao tornado na região. Este precisa ser reconstruído');
insert into projeto (projeto_id, name, estagio, descricao) values (nextval('projeto_seq'), 'Melhorar a Segurança da Intranet', 'INPROGRESS', 'Com o recente hack de dados, a segurança do escritório precisa ser melhorada e uma equipe de segurança adequada precisa ser contratada para a implementação');

-- INSERT PROJECT_EMPLOYEE_RELATION
insert into projeto_funcionario (funcionario_id, projeto_id) (select f.funcionario_id, p.projeto_id from funcionario f, projeto p where f.sobrenome ='Warton' AND p.name = 'Implementação de grande produção');
insert into projeto_funcionario (funcionario_id, projeto_id) (select f.funcionario_id, p.projeto_id from funcionario f, projeto p where f.sobrenome ='Warton' AND p.name = 'Novo orçamento funcional');
insert into projeto_funcionario (funcionario_id, projeto_id) (select f.funcionario_id, p.projeto_id from funcionario f, projeto p where f.sobrenome ='Warton' AND p.name = 'Reconstrução de escritórios');
insert into projeto_funcionario (funcionario_id, projeto_id) (select f.funcionario_id, p.projeto_id from funcionario f, projeto p where f.sobrenome ='Reeves' AND p.name = 'Implementação de grande produção');
insert into projeto_funcionario (funcionario_id, projeto_id) (select f.funcionario_id, p.projeto_id from funcionario f, projeto p where f.sobrenome ='Warton' AND p.name = 'Novo orçamento funcional');
insert into projeto_funcionario (funcionario_id, projeto_id) (select f.funcionario_id, p.projeto_id from funcionario f, projeto p where f.sobrenome ='Warton' AND p.name = 'Melhorar a Segurança da Intranet');
insert into projeto_funcionario (funcionario_id, projeto_id) (select f.funcionario_id, p.projeto_id from funcionario f, projeto p where f.sobrenome ='Henley' AND p.name = 'Reconstrução de escritórios');
insert into projeto_funcionario (funcionario_id, projeto_id) (select f.funcionario_id, p.projeto_id from funcionario f, projeto p where f.sobrenome ='Henley' AND p.name = 'Melhorar a Segurança da Intranet');	

													