-- INSERT funcionario
insert into funcionario (funcionario_id, primeiro_nome, sobrenome, email) values (1, 'John', 'Warton', 'warton@gmail.com');
insert into funcionario (funcionario_id, primeiro_nome, sobrenome, email) values (2, 'Mike', 'Lanister', 'lanister@gmail.com');
insert into funcionario (funcionario_id, primeiro_nome, sobrenome, email) values (3, 'Steve', 'Reeves', 'Reeves@gmail.com');
insert into funcionario (funcionario_id, primeiro_nome, sobrenome, email) values (4, 'Ronald', 'Connor', 'connor@gmail.com');
insert into funcionario (funcionario_id, primeiro_nome, sobrenome, email) values (5, 'Jim', 'Salvator', 'Sal@gmail.com');
insert into funcionario (funcionario_id, primeiro_nome, sobrenome, email) values (6, 'Peter', 'Henley', 'henley@gmail.com');
insert into funcionario (funcionario_id, primeiro_nome, sobrenome, email) values (7, 'Richard', 'Carson', 'carson@gmail.com');
insert into funcionario (funcionario_id, primeiro_nome, sobrenome, email) values (8, 'Honor', 'Miles', 'miles@gmail.com');
insert into funcionario (funcionario_id, primeiro_nome, sobrenome, email) values (9, 'Tony', 'Roggers', 'roggers@gmail.com');

-- INSERT projeto			
insert into projeto (projeto_id, name, estagio, descricao) values (1000, 'Implementação de grande produção', 'NOTSTARTED', 'Isso requer todas as mãos para a implementação final do software na produção');
insert into projeto (projeto_id, name, estagio, descricao) values (1001, 'Novo orçamento funcional',  'COMPLETED', 'Deliberar sobre o novo orçamento funcionariobonus do ano e definir quem será promovido');
insert into projeto (projeto_id, name, estagio, descricao) values (1002, 'Reconstrução de escritórios', 'INPROGRESS','O prédio de escritórios em Brasilandia foi danificado devido ao tornado na região. Este precisa ser reconstruído');
insert into projeto (projeto_id, name, estagio, descricao) values (1003, 'Melhorar a Segurança da Intranet', 'INPROGRESS', 'Com o recente hack de dados, a segurança do escritório precisa ser melhorada e uma equipe de segurança adequada precisa ser contratada para a implementação');

-- INSERT projeto_EMPLOYEE_RELATION (Removed duplicates from video)
insert into projeto_funcionario(funcionario_id, projeto_id) values (1,1000);
insert into projeto_funcionario(funcionario_id, projeto_id) values (1,1001);
insert into projeto_funcionario(funcionario_id, projeto_id) values (1,1002);
insert into projeto_funcionario(funcionario_id, projeto_id) values (3,1000);
insert into projeto_funcionario(funcionario_id, projeto_id) values (6,1002);
insert into projeto_funcionario(funcionario_id, projeto_id) values (6,1003);

