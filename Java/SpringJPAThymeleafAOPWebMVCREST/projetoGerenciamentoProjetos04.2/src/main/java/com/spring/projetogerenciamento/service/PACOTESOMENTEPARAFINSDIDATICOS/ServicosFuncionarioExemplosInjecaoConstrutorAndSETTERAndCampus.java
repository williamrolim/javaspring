package com.spring.projetogerenciamento.service.PACOTESOMENTEPARAFINSDIDATICOS;

import org.springframework.stereotype.Service;
@Service// @Component @Repository essa classe não seria registrada no conceito spring
public class ServicosFuncionarioExemplosInjecaoConstrutorAndSETTERAndCampus {

	//Injeção de Campo Calibragem automatica exemplo
//	@Autowired
//	IFuncionarioRepository repositoryF;
//	@Autowired
//	IProjetoRepository repositoryP;
//_________________________________________________________________________________________	
	
//Exemplo Injeção Construtor
//	IFuncionarioRepository repositoryF;
//	public ServicosFuncionario(IFuncionarioRepository repositoryF){
//		this.repositoryF = repositoryF;
//	}
	//_________________________________________________________________________________________
	
//Exemplo Injeção Setter necessita do Autowired para funcionar
//IFuncionarioRepository repositoryF;

//	@Autowired
//	public void setRepositoryF(IFuncionarioRepository repositoryF) {
//		this.repositoryF = repositoryF;
//	}
//	
	//_________________________________________________________________________________________

}
