package com.spring.projetogerenciamento.dto;

//Tem que começar com Get pois o spring sabe que esses objetos de tranferecia de dados precisam
//ser populados com os dados que vem da tabela
public interface FuncionarioProjeto {
	public String getPrimeiroNome();
	public String getSobrenome();
	public String getContagemProjetos();
}
