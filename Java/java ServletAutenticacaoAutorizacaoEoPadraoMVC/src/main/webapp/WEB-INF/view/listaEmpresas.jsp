<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.List,javaServletGerenciador.modelo.Empresa"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%><!-- uri = nome da tag lib da WEB-INF -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Java Standard Taglib</title>
</head>
<body>
<c:import url="logout-parcial.jsp"/>
Usuario Logado : ${usuarioLogado.login}
<br>
<br>

	<c:if test="${not empty empresa}">
Empresa  ${empresa} Cadastrada com Sucesso
	</c:if>
			lista empresas <br/>
	<ul>
		<c:forEach items="${empresas}" var="empresa">
		<li>${empresa.nome} - <fmt:formatDate value= "${empresa.dataAbertura}" pattern ="dd/MM/yyyy"/>
				<a	href="/gerenciador/entrada?acao=MostraEmpresa&id=${empresa.id }">Editar</a>		
				<a	href="/gerenciador/entrada?acao=RemoveEmpresa&id=${empresa.id }">Remove</a>
		</li>         
		</c:forEach>
	</ul>

</body>
</html>

 	