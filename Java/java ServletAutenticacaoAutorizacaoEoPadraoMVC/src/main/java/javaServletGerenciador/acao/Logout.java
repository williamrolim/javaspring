package javaServletGerenciador.acao;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class Logout implements Acao{

	@Override
	public String executa(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		HttpSession sessao = request.getSession();
		//sessao.removeAttribute("usuarioLogado");//remove o atributo usuario
		//remover toda sess�o, carrinho de compras, quantidade de itens, login
		sessao.invalidate(); //invalidar o usuario e destroe o cookie
		
		return "redirect:entrada?acao=LoginForm";
		
	}

}
