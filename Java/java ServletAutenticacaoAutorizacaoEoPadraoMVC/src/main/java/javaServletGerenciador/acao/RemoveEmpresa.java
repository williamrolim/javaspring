package javaServletGerenciador.acao;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javaServletGerenciador.modelo.BancoDados;

public class RemoveEmpresa implements Acao {
	public String executa(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("Removendo Empresa");
		String paramId = request.getParameter("id");
		Integer id = Integer.parseInt(paramId);
		System.out.println(id);
		
		BancoDados banco = new BancoDados();
		banco.removeEmpresa(id);
		
		return "redirect:entrada?acao=ListaEmpresas";
	}
}
