package javaServletGerenciador.acao;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javaServletGerenciador.modelo.BancoDados;
import javaServletGerenciador.modelo.Empresa;

public class ListaEmpresas implements Acao{
	public String executa(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		System.out.println("Listando Empresa");

		BancoDados banco = new BancoDados();
		List<Empresa> lista = banco.getEmpresas();
		request.setAttribute("empresas", lista);
	
		return "forward:listaEmpresas.jsp";
	}
}
