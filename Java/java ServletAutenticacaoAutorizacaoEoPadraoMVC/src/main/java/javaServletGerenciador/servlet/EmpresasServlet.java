package javaServletGerenciador.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javaServletGerenciador.modelo.*;

import com.google.gson.Gson;
import com.thoughtworks.xstream.XStream;

/**
 * Servlet implementation class EmpresasServlet
 */
@WebServlet("/empresas")
public class EmpresasServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<Empresa> empresas = new BancoDados().getEmpresas();
		
		String valor = request.getHeader("Accept");
		
		System.out.println(valor);
		if (valor.endsWith("xml")) {
		XStream xtream = new XStream();
		String xml = xtream.toXML(empresas);
		xtream.alias("empresa", Empresa.class);
		response.setContentType("application/xml");
		response.getWriter().print(xml);
		}else if (valor.endsWith("json")) {
		Gson gson = new Gson();
		String json = gson.toJson(empresas);
		
		response.setContentType("application/json");
		response.getWriter().print(json);
	}else {
		response.setContentType("appication/json");
		response.getWriter().print("{'message':'no contend'}");
	}
	}
}
