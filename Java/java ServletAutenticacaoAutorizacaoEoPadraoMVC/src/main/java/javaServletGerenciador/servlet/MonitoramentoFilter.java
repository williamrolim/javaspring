package javaServletGerenciador.servlet;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;


//As requisi��es entram primeiro no filter para continuar no servlet
//@WebFilter("/entrada")
public class MonitoramentoFilter implements Filter {
	
	//A partir do java 8 n�o precisa dessas implementa��es init destroy
	//Entretando � adequado utilizar para rodar em vers�es mais antigas do tom cat
	//E em outros servidores como por Exemplo o Jetty
	
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {}
	
	@Override
	public void destroy() {}
	
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		System.out.println("MonitoramentoFilter");
		//Medindo o tempo de execu��o
		long antes = System.currentTimeMillis();//Devolte os milissegundos desde 1970 que � considerado na computa��o o ano zero
		
		String acao = request.getParameter("acao");
		//executa acao
		chain.doFilter(request, response);//se n�o chama o chain.dofilter a excecu��o fica parada
		
		long depois = System.currentTimeMillis();
		
		System.out.println("Tempo de execu��o da a��o " + acao + " -> " +(depois - antes));
	}

}
