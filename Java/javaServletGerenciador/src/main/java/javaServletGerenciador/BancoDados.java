package javaServletGerenciador;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class BancoDados {
	
	private static List<Empresa> lista = new ArrayList<>();
	private static Integer chaveSequencial = 1;
	
	static {
		  Empresa empresa = new Empresa();
		  empresa.setId(chaveSequencial++);
		  empresa.setNome("Alura");
		  Empresa empresa2 = new Empresa();
		  empresa2.setId(chaveSequencial++);
		  empresa2.setNome("Caelum");
		  BancoDados.lista.add(empresa);
		  BancoDados.lista.add(empresa2);
		}
	
	public void adiciona(Empresa empresa) {
		empresa.setId(BancoDados.chaveSequencial++);
		BancoDados.lista.add(empresa);
	}
	
	public List<Empresa> getEmpresas(){
		return BancoDados.lista;
	}

	public void removeEmpresa(Integer id) {
		Iterator<Empresa> it  = lista.iterator();
		
		while (it.hasNext()) {
			Empresa emp  = it.next();
			if(emp.getId() == id)
				it.remove();
		}

	}

	public Empresa buscaEmpresaPelaId(Integer id) {
		for (Empresa empresa : lista) {
			if(empresa.getId() == id) {
				return empresa;
			}
		}
		
		return null;
	}
	}


