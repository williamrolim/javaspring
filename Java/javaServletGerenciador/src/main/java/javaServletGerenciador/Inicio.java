package javaServletGerenciador;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//@WebServlet marca a classe como uma servlet e al�m disso a registra a URL.
@WebServlet(urlPatterns = "/inicio") 
public class Inicio extends HttpServlet {
	
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws IOException  {
		PrintWriter out = resp.getWriter();
		out.println("<html>");
		out.println("<body>");
		out.println("TESTANDO");
		out.println("</body>");
		out.println("</html>");

	}
}
