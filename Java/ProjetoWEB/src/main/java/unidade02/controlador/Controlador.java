package unidade02.controlador;

import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Controlador
 */
@WebServlet("/Controlador")
public class Controlador extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Controlador() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		//criar as variaveis
		int idformulario;//identificar se a pagina � 1-cliente 2 - curso 3- pagamentos
		int tipoformulario;//identificar .....
		
		//pegar o parametro que ser� passado pelo usuario
		idformulario = Integer.parseInt(request.getParameter("idformulario"));
		tipoformulario = Integer.parseInt(request.getParameter("tipoformulario"));
		
		String cpfmascara,nome,nomecurso,email,valorcurso,site,datainscricao;
		long cpf,cdcurso;
		
		if (idformulario == 1) {//clientes
			switch (tipoformulario) {
			case 11: {
				
				break;
			}

			case 12: { // Consultar
				cpfmascara = request.getParameter("cpf");
				cpfmascara = cpfmascara.replaceAll("[.-]", "");
				cpf = Long.parseLong(cpfmascara);
				out.println("<h2> Clientes ==> Consultar ==> " + cpf);
				break;
			}
			case 13: {// Cadastrar
				cpfmascara = request.getParameter("cpf");
				cpfmascara = cpfmascara.replaceAll("[.-]", "");
				cpf = Long.parseLong(cpfmascara);
				nome = request.getParameter("nome");
				email = request.getParameter("email");
				out.println("<h2> Clientes ==> Cadastrar ==> " + cpf + " - " + nome + " email " + email);
				break;
			}
			case 14: {// Altera��o

				cpfmascara = request.getParameter("cpf");
				cpfmascara = cpfmascara.replaceAll("[.-]", "");
				cpf = Long.parseLong(cpfmascara);
				nome = request.getParameter("nome");
				email = request.getParameter("email");
				out.println("<h2> Clientes ==> Alterar	 ==> " + cpf + " - " + nome + " email " + email);
				break;
			}
			case 15: {// Excluir
				cpfmascara = request.getParameter("cpf");
				cpfmascara = cpfmascara.replaceAll("[.-]", "");
				cpf = Long.parseLong(cpfmascara);
				out.println("<h2> Clientes ==> Excluir ==> " + cpf);

				System.out.println("Cliente Exluido : ");
				break;
			}

			case 6: {
				System.out.println("Encerrando o Sistema");
				break;
			}
			default:
				throw new IllegalArgumentException("N�mero errado, tente novamente " );
			}
		 
		}else if (idformulario == 2) {//cursos
			switch (tipoformulario) {
			case 21: {
				
				break;
			}

			case 22: { // Consultar
				cdcurso = Long.parseLong(request.getParameter("cdcurso"));
				out.println("<h2> Cursos ==> Consultar ==> " + cdcurso);
				break;
			}
			case 23: {// Cadastrar
				cdcurso = Long.parseLong(request.getParameter("cdcurso"));
				nomecurso = request.getParameter("nome");
				valorcurso = request.getParameter("valor");
				site = request.getParameter("site");
				out.println("<h2> Cursos ==> Cadastrar ==> " + cdcurso + " - " + nomecurso + " - " + valorcurso + " - " + site);
				break;
			}
			case 24: {// Altera��o

				cdcurso = Long.parseLong(request.getParameter("cdcurso"));
				nomecurso = request.getParameter("nome");
				valorcurso = request.getParameter("valor");
				site = request.getParameter("site");
				out.println("<h2> Cursos ==> Cadastrar ==> " + cdcurso + " - " + nomecurso + " - " + valorcurso + " - " + site);
				break;
			}
			case 25: {// Excluir
				cdcurso = Long.parseLong(request.getParameter("cdcurso"));
				out.println("<h2> Cursos ==> Excluir ==> " + cdcurso);

				System.out.println("CursosExluido : ");
				break;
			}

			case 26: {
				System.out.println("Encerrando o Sistema");
				break;
			}
			default:
				throw new IllegalArgumentException("N�mero errado, tente novamente " );
			}
		}else if (idformulario == 3) {//pagamentos
			switch (tipoformulario) {
			case 31: {
				
				break;
			}

			case 32: { // Consultar
				cpfmascara = request.getParameter("cpf");
				cpfmascara = cpfmascara.replaceAll("[.-]", "");
				cpf = Long.parseLong(cpfmascara);
				cdcurso = Long.parseLong(request.getParameter("cdcurso"));
				out.println("<h2> Pagamentos  ==> Consultar ==> " + cpf + " - " + cdcurso);
				break;
			}
			case 33: {// Cadastrar
				cpfmascara = request.getParameter("cpf");
				cpfmascara = cpfmascara.replaceAll("[.-]", "");
				cpf = Long.parseLong(cpfmascara);
				cdcurso = Long.parseLong(request.getParameter("cdcurso"));
				datainscricao = request.getParameter("datainscricao");
				
				//out.println("<h2> Pagamentos ==> Data sem estar formatada ==> " + datainscricao);

				DateTimeFormatter formater = DateTimeFormatter.ofPattern("yyyy-MM-dd");
				LocalDate date = LocalDate.parse(datainscricao, formater);
				DateTimeFormatter fmt  = DateTimeFormatter.ofPattern("dd/MM/yyyy");
				out.println("<h2> Pagamentos ==> Cadastrar ==> " + cpf + " - " + cdcurso +  " - " +  fmt.format(date));
				break;
			}
			case 34: {// Altera��o
				cpfmascara = request.getParameter("cpf");
				cpfmascara = cpfmascara.replaceAll("[.-]", "");
				cpf = Long.parseLong(cpfmascara);
				cdcurso = Long.parseLong(request.getParameter("cdcurso"));
				datainscricao = request.getParameter("datainscricao");
				DateTimeFormatter formater = DateTimeFormatter.ofPattern("yyyy-MM-dd");
				LocalDate date = LocalDate.parse(datainscricao, formater);
				DateTimeFormatter fmt  = DateTimeFormatter.ofPattern("dd/MM/yyyy");
				out.println("<h2> Pagamentos ==> Cadastrar ==> " + cpf + " - " + cdcurso +  " - " +  fmt.format(date));
				break;
			}
			case 35: {// Excluir
				cpfmascara = request.getParameter("cpf");
				cpfmascara = cpfmascara.replaceAll("[.-]", "");
				cpf = Long.parseLong(cpfmascara);
				cdcurso = Long.parseLong(request.getParameter("cdcurso"));
				out.println("<h2> Pagamentos  ==> Excluir ==> " + cpf + " - " + cdcurso);
				break;
			}

			case 6: {
				System.out.println("Encerrando o Sistema");
				break;
			}
			default:
				throw new IllegalArgumentException("N�mero errado, tente novamente " );
			}
		}
	
	}

}
