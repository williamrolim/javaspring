package br.com.springMVC.SpringMVCCrieWebAppComThymeleafBootstrap.dto;

import javax.validation.constraints.NotBlank; 
import br.com.springMVC.SpringMVCCrieWebAppComThymeleafBootstrap.model.Pedido;
import br.com.springMVC.SpringMVCCrieWebAppComThymeleafBootstrap.model.StatusPedido;

public class RequisicaoNovoPedido {
	//como recebo as informações de um formulario?
	//só criar uma nova classe e deve ter o nome dos imputs do formulario
	//Ele bate o name do input no HTML com o nome do atributo na classe.
	//Usar o padrão DTO (Data Transfer Object) para receber dados da requisição HTTP
	
	@NotBlank //NotBlank.requisicaoNovoPedido.nomeProduto=não deve estar em branco
	private String nomeProduto;
	
	@NotBlank
	private String urlProduto;
	
	@NotBlank
	private String urlIMagem;
	private String descricao;
	
	public String getNomeProduto() {
		return nomeProduto;
	}
	public void setNomeProduto(String nomeProduto) {
		this.nomeProduto = nomeProduto;
	}
	public String getUrlProduto() {
		return urlProduto;
	}
	public void setUrlProduto(String urlProduto) {
		this.urlProduto = urlProduto;
	}
	public String getUrlIMagem() {
		return urlIMagem;
	}
	public void setUrlIMagem(String urlIMagem) {
		this.urlIMagem = urlIMagem;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public Pedido toPedido() {
		Pedido pedido = new Pedido();
		pedido.setNomeProduto(nomeProduto);
		pedido.setUrlProduto(urlProduto);
		pedido.setUrlImagem(urlIMagem);
		pedido.setDescricao(descricao);
		pedido.setStatuspedido(StatusPedido.AGUARDANDO);
		return pedido;
	}
	
	
}
