package br.com.springMVC.SpringMVCCrieWebAppComThymeleafBootstrap.model;

import org.springframework.beans.factory.annotation.Autowired;


public enum StatusPedido {
	AGUARDANDO, 
	APROVADO, 
	ENTREGUE;
}
