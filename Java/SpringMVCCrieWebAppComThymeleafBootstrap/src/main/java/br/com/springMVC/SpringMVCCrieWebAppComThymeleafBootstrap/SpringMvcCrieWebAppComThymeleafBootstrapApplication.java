package br.com.springMVC.SpringMVCCrieWebAppComThymeleafBootstrap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringMvcCrieWebAppComThymeleafBootstrapApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringMvcCrieWebAppComThymeleafBootstrapApplication.class, args);
	}

}
