package br.com.springMVC.SpringMVCCrieWebAppComThymeleafBootstrap.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.springMVC.SpringMVCCrieWebAppComThymeleafBootstrap.model.Pedido;
import br.com.springMVC.SpringMVCCrieWebAppComThymeleafBootstrap.model.StatusPedido;
import br.com.springMVC.SpringMVCCrieWebAppComThymeleafBootstrap.repository.PedidoRepository;

@Controller
@RequestMapping("/home")
public class HomeController {
	//CADA METODO É UMA ACTION

	@Autowired   
	private PedidoRepository pedidoRepository;
	
	@GetMapping()
	public String home(Model model) {	//model seria o request do servlet substitui na view com thymeleaf
		List<Pedido> pedidos = pedidoRepository.findAll();
		model.addAttribute("pedidos", pedidos);
		return "home";
	}
	
	@GetMapping("/{statuspedido}")
	public String porStatus(@PathVariable("statuspedido") String statuspedido, Model model) {	
		List<Pedido> pedidos = pedidoRepository.findByStatus(StatusPedido.valueOf(statuspedido.toUpperCase()));
		model.addAttribute("statuspedido", statuspedido);
		model.addAttribute("pedidos",pedidos);
		return "home";
	}
	
	@ExceptionHandler(IllegalArgumentException.class)
	public String onError() {
		return "redirect:/home";
	}
}
/*
@Controller
public class HomeController {
	@Autowired   //quero uma instancia de pedido repository
	private PedidoRepository repository;
	
	@GetMapping("/home")
	public String home(Model model) {	
		List<Pedido> pedidos = repository.recuperaTodosOsPedidos();
		model.addAttribute("pedidos", pedidos);
		return "home";
	}
}
*/