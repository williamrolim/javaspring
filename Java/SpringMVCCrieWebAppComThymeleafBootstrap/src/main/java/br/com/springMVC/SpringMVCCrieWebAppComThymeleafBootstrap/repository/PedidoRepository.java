package br.com.springMVC.SpringMVCCrieWebAppComThymeleafBootstrap.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;

import br.com.springMVC.SpringMVCCrieWebAppComThymeleafBootstrap.model.Pedido;
import br.com.springMVC.SpringMVCCrieWebAppComThymeleafBootstrap.model.StatusPedido;

//é um repositorio e quero quer vc gerencie essa classe , poderia ser o controller

@Repository
public interface PedidoRepository extends JpaRepository<Pedido, Long> {
	List<Pedido>findByStatus(StatusPedido statuspedido);
     //Já herda alguns metodos que o jpaRepository ele já tem inclusive o find all
	}

/*
@Repository //é um repositorio e quero quer vc gerencie essa classe , poderia ser o controller
public class PedidoRepository {
	@PersistenceContext
	private EntityManager entitiManager;
	public List<Pedido>recuperaTodosOsPedidos(){
		Query query = entitiManager.createQuery("select p from Pedido p", Pedido.class);		
		return query.getResultList();
	}
}
*/