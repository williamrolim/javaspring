package br.springthymeleaf.springthymeleaf.controller;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import br.springthymeleaf.springthymeleaf.model.Pedido;
import br.springthymeleaf.springthymeleaf.model.StatusPedido;
import br.springthymeleaf.springthymeleaf.repository.PedidoRepository;

//Controller ela é gerenciada pelo proprio spring
@Controller
@RequestMapping("/home") //todas as requisições /home vão bater aqui
public class HomeController {
	//INJEÇÃO DE DEPENDENCIA
	@Autowired //serve para pedir para o spring quero uma instancia de pedidoreposity
	private PedidoRepository repository;
	
	
	@GetMapping //mapiei a minha action
	public String home(Model model) {//Model seria o request do servlet
		List<Pedido> pedido = repository.findAll();
		model.addAttribute("pedidos",pedido);
		return "home";
	}
	//{status} declarei na patch da url , pega o status
	@GetMapping("/{status}") //NAV status do pedido AGUARDANDO/APROVADO/ENTREGUE
	public String porStatus(@PathVariable("status")String status, Model model) {//Model seria o request do servlet
		List<Pedido> pedido = repository.findByStatus(StatusPedido.valueOf(status.toUpperCase()));
		model.addAttribute("pedidos",pedido);
		model.addAttribute("status",status);
		return "home";
	}
	@ExceptionHandler(IllegalArgumentException.class)
	public String onError() {//se 
		return "redirect:/home";
	}
	
	
	
//	@GetMapping("/home") //mapiei a minha action
//	public String home(Model model) {//Model seria o request do servlet
//		Query query = entityManager.createQuery("select p from Pedido as p", Pedido.class);
//		query.getResultList();
//		List <Pedido> pedido = query.getResultList();
//		model.addAttribute("pedidos",pedido);
//		return "home";
//	}
}
