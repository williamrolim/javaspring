package br.springthymeleaf.springthymeleaf.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.springthymeleaf.springthymeleaf.model.Pedido;
import br.springthymeleaf.springthymeleaf.model.StatusPedido;

@Repository //@Repository essa classe aqui é um repositorio quero que gerencie essa classe e quero que crie instancia toda vez que alguem pedir
public interface PedidoRepository extends JpaRepository<Pedido, Long> {
							//vai fazer um filtro por status
	List<Pedido> findByStatus(StatusPedido status); // Spring Data JPA usa interfaces para fazer comunicação e ele pelo nome vai interpretar todos os metodos
// extends jparepository = para ficar integrado ao springdata  precisa de dois tipos o nome do objeto e id long
//já herdamos alguns metodos que ele já tem. já herdamos o find all								
	
	//	@PersistenceContext //PersistenceContext pedimos para o hibernate configurar
//	private EntityManager entityManager;//EntityManager classe responsavel para comunicar com o banco de dados
	//CLasse com a responsabilidade de acessar repositorios de aplicação de dados
//	public List<Pedido> recuperaTodosOsPedidos(){
//		Query query = entityManager.createQuery("select p from Pedido as p", Pedido.class);
//		query.getResultList();
//		List <Pedido> pedido = query.getResultList();	
//		return pedido;
//		
//	}
}
