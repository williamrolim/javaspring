package concessionaria.view;

import java.util.Date;

public class Automovel {
	
	int id;
	Date anoFabricacao;
	Date anoModelo;
	String observacoes;
	float preco;
	int kilometragem;
	
	public Automovel () {}
	
	public Automovel(Date anoFabricacao, Date anoModelo, String observacoes, float preco, int kilometragem) {
		this.anoFabricacao = anoFabricacao;
		this.anoModelo = anoModelo;
		this.observacoes = observacoes;
		this.preco = preco;
		this.kilometragem = kilometragem;
	}
	
	public Automovel(int id, Date anoFabricacao, Date anoModelo, String observacoes, float preco, int kilometragem) {
		this.id = id;
		this.anoFabricacao = anoFabricacao;
		this.anoModelo = anoModelo;
		this.observacoes = observacoes;
		this.preco = preco;
		this.kilometragem = kilometragem;
	}
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Date getAnoFabricacao() {
		return anoFabricacao;
	}
	public void setAnoFabricacao(Date anoFabricacao) {
		this.anoFabricacao = anoFabricacao;
	}
	public Date getAnoModelo() {
		return anoModelo;
	}
	public void setAnoModelo(Date anoModelo) {
		this.anoModelo = anoModelo;
	}
	public String getObservacoes() {
		return observacoes;
	}
	public void setObservacoes(String observacoes) {
		this.observacoes = observacoes;
	}
	public float getPreco() {
		return preco;
	}
	public void setPreco(float preco) {
		this.preco = preco;
	}
	public int getKilometragem() {
		return kilometragem;
	}
	public void setKilometragem(int kilometragem) {
		this.kilometragem = kilometragem;
	}
	
	
	
}
