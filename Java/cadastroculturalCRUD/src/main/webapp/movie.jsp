<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<form action="MovieController.do" method="post">
		<fieldset>	
		<div>
				<label for="movieID"></label> <input type="hidden"
					name="movieID" value="<c:out value="${movie.id}" />"
					readonly="readonly" />
			</div>
			<div>
				<label for="nome">Nome:</label> <input type="text"
					name="nome" value="<c:out value="${movie.nome}" />"
					placeholder="Nome do filme" />
			</div>
			<div>
				<label for="genero">Genero:</label> <input type="text"
					name="genero" value="<c:out value="${movie.genero}" />"
					placeholder="genero" />
			</div>
			<div>
				<label for="nota">Nota:</label> <input type="text" name="nota"
					value="<c:out value="${movie.nota}" />" placeholder="Nota" />
			</div>
			<div>
				<label for="diretor">Diretor</label> <input type="text" name="diretor"
					value="<c:out value="${movie.diretor}" />" placeholder="Diretor" />
			</div>
			<div>
				<label for="dataLancamento">Data Lan�amento:</label> <input type="text"
					name="dataLancamento" value="<c:out value="${movie.dataLancamento}" />"
					placeholder="Data do Lan�amento" />
			</div>
			<div>
				<label for="dataAssistiu">Data que Assistiu:</label> <input type="text"
					name="dataAssistiu" value="<c:out value="${movie.dataAssistiu}" />"
					placeholder="Data que assistiu" />
			</div>
			<div>
				<label for="descricao">Descri��o:</label> <textarea type="text" name="descricao"
					value="<c:out value="${movie.descricao}" />" placeholder="Descri��o" /></textarea>
			</div>			
			<div>
				<input type="submit" value="Submit" />
			</div>
		</fieldset>
	</form>
</body>
</html>