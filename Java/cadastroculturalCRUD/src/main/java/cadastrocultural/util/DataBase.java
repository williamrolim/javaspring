package cadastrocultural.util;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;


public class DataBase {
private static Connection conn;
	
	//public static Connection getConnection() {
//		if( conn != null )
//			return conn;
//		
//		InputStream inputStream = DataBase.class.getClassLoader().getResourceAsStream( "/database.properties" );
//		Properties properties = new Properties();
//		try {
//			properties.load( inputStream );
//			String driver = properties.getProperty("driver");
//			String url = properties.getProperty("url");
//			String user = properties.getProperty("user");
//			String password = properties.getProperty("password");
//			Class.forName( driver );
//			conn = DriverManager.getConnection(url,user,password);
//		} catch (IOException e) {
//			e.printStackTrace();
//		} catch (ClassNotFoundException e) {
//			e.printStackTrace();
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//		
//		return conn;
		
		private static final long serialVersionUID = 1L;
		 static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
		 static final String BD_URL = "jdbc:mysql://localhost:3306/cadastrocultural?useTimezone=true&serverTimezone=UTC";



		    //  Database credentials
		    static final String USER = "WilliamRolim";
		    static final String PASS = "sq3l@nd1A54Ever";
		
		    public static Connection getConnection() throws ClassNotFoundException, SQLException 	    {
		        //				Class.forName("com.mysql.jdbc.Driver");
				Class.forName("com.mysql.cj.jdbc.Driver");
		  
				System.out.println("Connecting to a selected database...");
				
				                                      //erro do fuso horario mysql resolvendo
				Connection connection = DriverManager.getConnection(
						"jdbc:mysql://localhost:3306/cadastrocultural?useTimezone=true&serverTimezone=UTC",
						"WilliamRolim", "sq3l@nd1A54Ever");
				System.out.println("Connected database successfully...");     
		        System.out.println("Conexao Sucesso");
		        
		        return connection;
	}
	
	public static void closeConnection( Connection toBeClosed ) {
		if( toBeClosed == null )
			return;
		try {
			toBeClosed.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
