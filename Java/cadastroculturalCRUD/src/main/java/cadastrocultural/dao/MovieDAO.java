package cadastrocultural.dao;

import java.util.List;

import cadastrocultural.model.Movies;

public interface MovieDAO {
	public List<Movies> getAllMovies();
	public void adicionarMovie(Movies movies);
	public void deleteMovie( int movieId );
	public void updateMovie(Movies movies);
	public Movies getStudentById( int studentId );
}
