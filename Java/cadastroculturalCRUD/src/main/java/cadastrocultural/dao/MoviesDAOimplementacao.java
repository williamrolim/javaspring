package cadastrocultural.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import cadastrocultural.util.DataBase;
import cadastrocultural.model.Movies;

public class MoviesDAOimplementacao implements MovieDAO {
	private Connection con;

	public MoviesDAOimplementacao() throws ClassNotFoundException, SQLException {
		con = DataBase.getConnection();
	}
	
	@Override
	public List<Movies> getAllMovies() {
		List<Movies> list = new ArrayList<Movies>();
		try {
			Statement statement = con.createStatement();
			ResultSet resultSet = statement.executeQuery("select * from movies_cadastro");
			while (resultSet.next()) {
				Movies movie = new Movies();
				movie.setId(resultSet.getInt("id_movie_cad"));
				movie.setNome(resultSet.getString("nome_filme"));
				movie.setGenero(resultSet.getString("genero_filme"));
				movie.setNota(resultSet.getInt("nota"));
				movie.setDiretor(resultSet.getString("diretor_cad"));
				movie.setDataLancamento(resultSet.getString("data_lancamento"));
				movie.setDataAssistiu(resultSet.getString("data_assistiu"));
				movie.setDescricao(resultSet.getString("descricao"));

				list.add(movie);
			}
			resultSet.close();
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public void deleteMovie(int movieId) {
		try {
			String query = "delete from movies_cadastro where id_movie_cad=?";
			PreparedStatement preparedStatement = con.prepareStatement(query);
			preparedStatement.setInt(1, movieId);
			preparedStatement.executeUpdate();
			preparedStatement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public void updateMovie(Movies movies) {
		try {
			String query = "update movies_cadastro set nome_filme=?, genero_filme=?, nota=?, diretor_cad=?,data_lancamento=?,data_assistiu=?,descricao=? where id_movie_cad=?";
			PreparedStatement preparedStatement = con.prepareStatement( query );
			preparedStatement.setString(1, movies.getNome());
			preparedStatement.setString(2, movies.getGenero());
			preparedStatement.setInt(3, movies.getNota());
			preparedStatement.setString(4, movies.getDiretor());
			preparedStatement.setString(5, movies.getDataLancamento());
			preparedStatement.setString(6, movies.getDataAssistiu());
			preparedStatement.setString(7, movies.getDescricao());
			preparedStatement.setInt(8, movies.getId() );

			preparedStatement.executeUpdate();
			preparedStatement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public void adicionarMovie(Movies movies) {

		try {
			String query = "insert into movies_cadastro values (DEFAULT,?,?,?,?,?,?,?)";
			PreparedStatement preparedStatement = con.prepareStatement( query );
			preparedStatement.setString(1, movies.getNome());
			preparedStatement.setString(2, movies.getGenero());
			preparedStatement.setInt(3, movies.getNota());
			preparedStatement.setString(4, movies.getDiretor());
			preparedStatement.setString(5, movies.getDataLancamento());
			preparedStatement.setString(6, movies.getDataAssistiu());
			preparedStatement.setString(7, movies.getDescricao());
			preparedStatement.executeUpdate();
			preparedStatement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public Movies getStudentById(int movieId) {
		Movies movie = new Movies();
		try {
			String query = "select * from movies_cadastro where id_movie_cad=?";
			PreparedStatement preparedStatement = con.prepareStatement( query );
			preparedStatement.setInt(1, movieId);
			ResultSet resultSet = preparedStatement.executeQuery();
			while(resultSet.next() ) {
				movie.setId(resultSet.getInt("id_movie_cad"));
				movie.setNome(resultSet.getString("nome_filme"));
				movie.setGenero(resultSet.getString("genero_filme"));
				movie.setNota(resultSet.getInt("nota"));
				movie.setDiretor(resultSet.getString("diretor_cad"));
				movie.setDataLancamento(resultSet.getString("data_lancamento"));
				movie.setDataAssistiu(resultSet.getString("data_assistiu"));
				movie.setDescricao(resultSet.getString("descricao"));
			}
			resultSet.close();
			preparedStatement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return movie;
	}
}
