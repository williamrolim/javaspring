package cadastrocultural.controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cadastrocultural.dao.MovieDAO;
import cadastrocultural.dao.MoviesDAOimplementacao;
import cadastrocultural.model.Movies;

/**
 * Servlet implementation class MovieController
 */
@WebServlet("/MovieController")
public class MovieController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private MovieDAO dao;
	public static final String lIST_MOVIE = "/listmovies.jsp";
	public static final String INSERT_OR_EDIT = "/movie.jsp";

	/**
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 * @see HttpServlet#HttpServlet()
	 */
	public MovieController() throws ClassNotFoundException, SQLException {
		dao = new MoviesDAOimplementacao();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String forward = "";
		String action = request.getParameter("action");

		if (action.equalsIgnoreCase("delete")) {
			forward = lIST_MOVIE;
			int movieId = Integer.parseInt(request.getParameter("movieID"));
			dao.deleteMovie(movieId);
			request.setAttribute("movies", dao.getAllMovies());
		} else if (action.equalsIgnoreCase("edit")) {
			forward = INSERT_OR_EDIT;
			int movieId = Integer.parseInt(request.getParameter("movieID"));
			Movies movie = dao.getStudentById(movieId);
			request.setAttribute("movie", movie);
		} else if (action.equalsIgnoreCase("insert")) {
			forward = INSERT_OR_EDIT;
		} else {
			forward = lIST_MOVIE;
			request.setAttribute("movies", dao.getAllMovies());
		}
		RequestDispatcher view = request.getRequestDispatcher(forward);

		view.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Movies movie = new Movies();
		movie.setNome(request.getParameter("nome"));
		movie.setGenero(request.getParameter("genero"));
		movie.setNota(Integer.parseInt(request.getParameter("nota")));
		movie.setDiretor(request.getParameter("diretor"));
		movie.setDataLancamento(request.getParameter("dataLancamento"));
		movie.setDataAssistiu(request.getParameter("dataAssistiu"));
		movie.setDescricao(request.getParameter("descricao"));

		String movieId = request.getParameter("movieID");

		if (movieId == null || movieId.isEmpty())
			dao.adicionarMovie(movie);
		else {
			movie.setId(Integer.parseInt(request.getParameter("movieID")));
			dao.updateMovie(movie);
		}

		RequestDispatcher view = request.getRequestDispatcher(lIST_MOVIE);
		request.setAttribute("movies", dao.getAllMovies());
		view.forward(request, response);
	}

}
