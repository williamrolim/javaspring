package teste_javaIO;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;

public class TesteEscrita {

	public static void main(String[] args) throws IOException {
		
		//FileOutPutStream = serve para escrita de dados bin�rios
		OutputStream fos = new FileOutputStream("A:\\Sistemas\\Java\\Files\\Examples\\lorem2.txt");
		Writer osw = new OutputStreamWriter(fos);
		BufferedWriter bw = new BufferedWriter(osw);

		bw.write("William Analise Desenvolvimento de Sistemas");
		bw.newLine();
		bw.newLine();
		bw.write("Universidade Rapadura");
		
		bw.close();		

	}

}
