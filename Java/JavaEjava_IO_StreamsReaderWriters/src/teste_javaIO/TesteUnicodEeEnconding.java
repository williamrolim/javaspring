package teste_javaIO;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

public class TesteUnicodEeEnconding {

	public static void main(String[] args) throws UnsupportedEncodingException, FileNotFoundException {

		String s = "c";
		//Unicode � o mapeamento do caractere o simbolo para um codigo decimal
		//java usa unicode por debaixo dos pontos
		//codepoint � o n�mero associado a tabela do unicode
		System.out.println(s.codePointAt(0));
		
		//Qual encoding o java utiliza por padr�o
		Charset charset = Charset.defaultCharset();//retorna a referencia default
		System.out.println(charset.displayName());
		
		byte[] bytes = s.getBytes("windows-1252"); //metodo que tranforma caracteres em bytes
		System.out.println(bytes.length + ", windows-1252");//1 BYTE
		System.out.println(new String(bytes));
		
		System.out.println("******************2**********************");
		byte[] bytes2 = s.getBytes("UTF-16");
		System.out.println(bytes2.length + ",UTF-16");//4 BYTES
		System.out.println(new String(bytes2, "UTF-16"));

		
		System.out.println("*******2*********************************");
		byte[] bytes3 = s.getBytes(StandardCharsets.US_ASCII);
		System.out.println(bytes3.length + ", US_ASCII");//1 BYTES
		System.out.println(new String(bytes3 , "windows-1252"));
		
		System.out.println("****************************************");

		String s1 = "13� �rg�o Oficial";
		byte[] bytes4 = s1.getBytes("UTF-8");
		System.out.println(new String(bytes4, "UTF-8"));
		
		Scanner scanner = new Scanner(new File("A:\\Sistemas\\Java\\Files\\Examples\\lorem2.txt"), 
				  StandardCharsets.UTF_8.name());

	}

}
