package teste_javaIO;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

public class TesteLeitura {

	public static void main(String[] args) throws IOException {
		//Fluxo de Entrada com arquivo
		//FileInputStream somente l� os bytes
//		FileInputStream fis = new FileInputStream("A:\\Sistemas\\Java\\Files\\Examples\\lorem.txt");
//		InputStreamReader para transformar esses bytes em caracteres
	//	InputStreamReader isr = new InputStreamReader(fis);
//		BufferedReader br = new BufferedReader(isr);
//		
//		String linha = br.readLine();
//		System.out.println(linha);
//		
//		br.close();
		
		InputStream fis = new FileInputStream("A:\\Sistemas\\Java\\Files\\Examples\\lorem.txt");
		Reader isr = new InputStreamReader(fis);
		BufferedReader br = new BufferedReader(isr);

		String linha = br.readLine();
		
		while (linha != null) {			
		System.out.println(linha);
		linha = br.readLine();
		
		}
		br.close();		
	}

}
