package teste_javaIO;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;

public class TesteEscrita3 {
public static void main(String[] args) throws IOException{	
	//PrintStream Consegue imprimir para um fluxo binario
	//PrintStream ps = new PrintStream(("A:\\Sistemas\\Java\\Files\\Examples\\lorem2.txt"));
	PrintWriter ps = new PrintWriter("A:\\Sistemas\\Java\\Files\\Examples\\lorem2.txt" , "UTF-8");
	ps.println("Imprimindo para fluxo");
	ps.println("Na mesma linha");
	ps.println();
	ps.println("Pulando linha - PRINTLN ACIMA");
	ps.println("FIM");

	ps.close();	
	
	
}
}
