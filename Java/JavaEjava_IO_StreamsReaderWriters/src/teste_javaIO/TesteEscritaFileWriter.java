package teste_javaIO;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class TesteEscritaFileWriter {

    public static void main(String[] args) throws IOException {
/* recebemos os milissegundos que passaram desde 1 de janeiro de 1970. 
 * Essa data � considerada o in�cio da Era Unix ou Unix Epoch, que o Java
 *  tamb�m usa. Ou seja, essa data � o marco zero no sistema de calend�rio 
 *  usado nos sistemas operacionais UNIX.
 */
        long ini = System.currentTimeMillis();
       
        BufferedWriter bw = new BufferedWriter(new FileWriter(("A:\\Sistemas\\Java\\Files\\Examples\\lorem2.txt")));

        bw.write("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod");
        bw.newLine();
        bw.write("tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam");

        bw.close();

        long fim = System.currentTimeMillis();

        System.out.println("Passaram " + (fim - ini) + " milissegundos");

    }
}