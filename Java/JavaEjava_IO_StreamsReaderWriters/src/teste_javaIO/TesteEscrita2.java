package teste_javaIO;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;

public class TesteEscrita2 {

	public static void main(String[] args) throws IOException {
		
		//PARTE 1 : EXEMPLO 1
//		FileWriter fw = new FileWriter("A:\\Sistemas\\Java\\Files\\Examples\\lorem2.txt");
//		fw.write("William Analise Desenvolvimento de Sistemas");
//		fw.write(System.lineSeparator());//fw.write("\n\r"); /
//		fw.write(System.lineSeparator());//fw.write("\n\r");
//		fw.write("Universidade Rapadura");
//		fw.write("William Analise Desenvolvimento de Sistemas");
//		fw.write(System.lineSeparator());//fw.write("\n\r");
//		fw.write("Universidade Tupamaru");
//
//		
//		fw.close();		
		
		//Exemplo2
		BufferedWriter bw = new BufferedWriter(new FileWriter("A:\\Sistemas\\Java\\Files\\Examples\\lorem2.txt"));
		bw.write("William Analise Desenvolvimento de Sistemas");
		bw.newLine();
		bw.newLine();
		bw.write("Universidade Rapadura");
		bw.newLine();
		bw.write("William Analise Desenvolvimento de Sistemas");
		bw.newLine();
		bw.write("Universidade Tupamaru");

		
		bw.close();		
	}}


