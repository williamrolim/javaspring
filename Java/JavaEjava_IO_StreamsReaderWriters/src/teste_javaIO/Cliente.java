package teste_javaIO;

import java.io.Serializable;

public class Cliente implements Serializable{

	/**
	 * 
	 */                        //Administrar a vers�o da classe : 
	private static final long serialVersionUID = -5006556096979822337L;
	private String nome;
	private String cpf;
	//private transient Cliente2 cliente; n�o faz parte da serializa��o
	private String profissao;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getProfissao() {
		return profissao;
	}

	public void setProfissao(String profissao) {
		this.profissao = profissao;
	}

	@Override
	public String toString() {
		return "Cliente [nome=" + nome + ", cpf=" + cpf + ", profissao=" + profissao + "]";
	}

		
}
