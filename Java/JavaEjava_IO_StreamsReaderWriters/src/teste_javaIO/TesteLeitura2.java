package teste_javaIO;

import java.io.File;
import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

public class TesteLeitura2 {

	public static void main(String[] args) throws Exception {
		Scanner scanner = new Scanner(new File("A:\\Sistemas\\Java\\Files\\Examples\\contas.csv"));
		
		while (scanner.hasNextLine()) {
		String linha = scanner.nextLine();
		System.out.println(linha);
		}
		
		scanner.close();
		System.out.println("**************************************************8");
		Scanner scanner2 = new Scanner(new File("A:\\Sistemas\\Java\\Files\\Examples\\contas.csv"));
		while (scanner2.hasNextLine()) {
		String linha = scanner2.nextLine();
		System.out.println(linha);
		String [] valores = linha.split(",");
		System.out.println(Arrays.toString(valores));
		System.out.println(valores[3]);
		}
		
		scanner2.close();
		System.out.println("**************************************************8");
		
		Scanner scanner3 = new Scanner(new File("A:\\Sistemas\\Java\\Files\\Examples\\contas.csv"));
		while (scanner3.hasNextLine()) {
		String linha = scanner3.nextLine();
		
		Scanner linhaScanner = new Scanner(linha);
		
		linhaScanner.useLocale(Locale.US);//Usar  o scanner para aceitar as regras locais da regi�o
		                                  //No caso o ultimo numero do arquivo csv estava 350.40 ou seja
		                                   //separa��o da casa decimal do padr�o americano
										   //No brasil se separa por virgula, sendo assim locale.Us resolve
		linhaScanner.useDelimiter(",");//m�todo de Scanner respons�vel por separar o conte�do por um delimitador
		
		String valor1 = linhaScanner.next();
		int valor2 = linhaScanner.nextInt();
		int valor3 = linhaScanner.nextInt();
		String valor4 = linhaScanner.next();
		double valor5 = linhaScanner.nextDouble();
		
		System.out.println(valor1 + valor2 + valor3 + valor4 + valor5);
		linhaScanner.close();
		}
		
	}

}
