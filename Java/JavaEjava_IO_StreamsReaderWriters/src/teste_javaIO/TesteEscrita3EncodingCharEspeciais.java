package teste_javaIO;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.util.Scanner;

public class TesteEscrita3EncodingCharEspeciais {

	public static void main(String[] args) throws IOException {

		//Scanner scanner = new Scanner(new File("A:\\Sistemas\\Java\\Files\\Examples\\contas2.csv"), "UTF-8");
		
		InputStream fis = new FileInputStream("A:\\Sistemas\\Java\\Files\\Examples\\contas2.csv");
		Reader isr = new InputStreamReader(fis, "UTF-8");//Sem o charset adequado a leitura � incorreta
		
		//PrintWriter pw = new PrintWriter("lorem2.txt", "UTF-8");
		
		BufferedReader br = new BufferedReader(isr);
		
		String linha = br.readLine();
		
		while (linha != null) {			
		System.out.println(linha);
		linha = br.readLine();
		
		}
		br.close();	
	}

}
