package teste_javaIO;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Locale;
import java.util.Scanner;

public class TesteLeitura3FormatacaoDeValores  {
	
	public static void main(String[] args) throws FileNotFoundException {
		Scanner scanner3 = new Scanner(new File("A:\\Sistemas\\Java\\Files\\Examples\\contas.csv"));
		while (scanner3.hasNextLine()) {
		String linha = scanner3.nextLine();
		
		Scanner linhaScanner = new Scanner(linha);
		
		linhaScanner.useLocale(Locale.US);//Usar  o scanner para aceitar as regras locais da regi�o
		                                  //No caso o ultimo numero do arquivo csv estava 350.40 ou seja
		                                   //separa��o da casa decimal do padr�o americano
										   //No brasil se separa por virgula, sendo assim locale.Us resolve
		linhaScanner.useDelimiter(",");//m�todo de Scanner respons�vel por separar o conte�do por um delimitador
		
		String tipoConta = linhaScanner.next();
		int agencia = linhaScanner.nextInt();
		int numero = linhaScanner.nextInt();
		String titular = linhaScanner.next();
		double saldo = linhaScanner.nextDouble();
		
		//String valorFormato = String.format(Locale.JAPAN, "%s - %04d-%d, %s: %f" , tipoConta ,agencia , numero , titular , saldo);
		                                   //new Locale ("pt","BR") = FormatandoPortuguesDoBrasil
		System.out.format(new Locale ("pt","BR"), "%s - %04d-%08d, %20s: %08.2f %n" , tipoConta ,agencia , numero , titular , saldo);

		linhaScanner.close();
		}
		
	}
	}


