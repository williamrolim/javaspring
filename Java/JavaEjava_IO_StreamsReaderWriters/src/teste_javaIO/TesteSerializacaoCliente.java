package teste_javaIO;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class TesteSerializacaoCliente {
	public static void main(String[] args) throws FileNotFoundException, IOException, ClassNotFoundException {
		
//		Cliente cliente = new Cliente();
//		cliente.setNome("William");
//		cliente.setProfissao("Dev");
//		cliente.setCpf("12323232");
				
		//Tranformar esse objeto "nome" em um fluxo de bits e bytes
		// lembrando que String � uma classe
		
//		ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("A:\\Sistemas\\Java\\Files\\Examples\\cliente.bin"));
//		oos.writeObject(cliente);
//		oos.close();
		
		ObjectInputStream ois = new ObjectInputStream(new FileInputStream("A:\\Sistemas\\Java\\Files\\Examples\\cliente.bin"));
		Cliente clientes = (Cliente) ois.readObject();
		ois.close();
		System.out.println(clientes.toString());
	}
}
