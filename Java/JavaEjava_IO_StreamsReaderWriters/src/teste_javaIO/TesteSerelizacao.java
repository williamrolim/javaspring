package teste_javaIO;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;

public class TesteSerelizacao {

	public static void main(String[] args) throws FileNotFoundException, IOException, ClassNotFoundException {

		String nome = "William Rolim";
		
		//Tranformar esse objeto "nome" em um fluxo de bits e bytes
		// lembrando que String � uma classe
		
//		ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("A:\\Sistemas\\Java\\Files\\Examples\\objeto.bin"));
//		oos.writeObject(nome);
//		oos.close();
		
		ObjectInputStream ois = new ObjectInputStream(new FileInputStream("A:\\Sistemas\\Java\\Files\\Examples\\objeto.bin"));
		String nome2 =(String) ois.readObject();
		ois.close();
		System.out.println(nome2);
		
	}

}
