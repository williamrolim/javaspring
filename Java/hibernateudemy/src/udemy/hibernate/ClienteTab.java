package udemy.hibernate;

public class ClienteTab implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private long cpf;
	private String nome;
	private String email;

	public ClienteTab() {
	}

	public ClienteTab(long cpf, String nome, String email) {
		this.cpf = cpf;
		this.nome = nome;
		this.email = email;
	}

	public long getCpf() {
		return this.cpf;
	}

	public void setCpf(long cpf) {
		this.cpf = cpf;
	}

	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "ClienteTab [cpf=" + cpf + ", nome=" + nome + ", email=" + email + "]";
	}	
	
}
