package udemy.hibernate;

import java.util.List;
import java.util.Scanner;

public class ClienteApp {

	public static void main(String[] args) {
		try {
			Scanner entrada = new Scanner(System.in);
			int opcao = 0;
			long cpf = 0;
			String nome = "", email = "";
			
			ClienteTabDAO clientedao = new ClienteTabDAO();
			while (opcao != 6) {
				System.out.println("Sistema De Geranciamento de Clientes");
				System.out.println("============================================");
				System.out.println("Digite [1] para Consultar Todos os Clientes");
				System.out.println("Digite [2] para Consultar cliente especifico");
				System.out.println("Digite [3] para Cadastrar Novo cliente");
				System.out.println("Digite [4] para Alterar o Cliente");
				System.out.println("Digite [5] para Exluir um Cliente");
				System.out.println("Digite [6] para Sair");
				System.out.println("============================================");
				opcao = entrada.nextInt();

				switch (opcao) {
				case 1: {
					System.out.println("Opcao [1] Consultar Todos os clientes");
					List<ClienteTab> clientes = clientedao.findAll();
					clientes.forEach(System.out::println);
					break;
				}

				case 2: { // Consultar
					System.out.println("Opcao [2] Consultar Cliente Especifico");
					System.out.println("=======================================");
					System.out.println("Informar o CPF do cliente para consultar : ");
					cpf = entrada.nextLong();
					entrada.nextLine();
					System.out.println(clientedao.find(cpf));

					break;
				}
				case 3: {// Cadastrar
					System.out.println("Opcao [3] Cadastrar Cliente");
					System.out.println("=======================================");
					System.out.println("Digite o cpf do Cliente: ");
					cpf = entrada.nextLong();
					entrada.nextLine(); // Esvaziar o Buffer
					System.out.println("Digite o nome do Cliente: ");
					nome = entrada.nextLine();
					System.out.println("Digite o email do Cliente: ");
					email = entrada.nextLine();

					ClienteTab cliente = new ClienteTab(cpf,nome,email);
					clientedao.persist(cliente);
					break;
				}
				case 4: {// Altera��o

					System.out.println("Opcao [4] Alterar Cliente");
					System.out.println("=======================================");
					System.out.println("Digite o cpf do Cliente: ");
					cpf = entrada.nextLong();
					entrada.nextLine(); // Esvaziar o Buffer
					System.out.println("Digite o nome do Cliente: ");
					nome = entrada.nextLine();
					System.out.println("Digite o email do Cliente: ");
					email = entrada.nextLine();
					ClienteTab cliente = new ClienteTab(cpf,nome,email);
					clientedao.merge(cliente);

					break;
				}
				case 5: {// Excluir

					System.out.println("Opcao [5] Exluir Cliente Especifico");
					System.out.println("=======================================");
					System.out.println("Informar o CPF do cliente para exclus�o : ");
					cpf = entrada.nextLong();
					entrada.nextLine();
				
					ClienteTab cliente = clientedao.find(cpf);
					
					clientedao.delete(cliente);;
					//System.out.println("Cliente Exluido : ");
					break;
				}

				case 6: {

					System.out.println("Encerrando o Sistema");
					break;
				}
				default:
					throw new IllegalArgumentException("N�mero errado, tente novamente " + opcao);
				}
			}
			entrada.close();
			} catch (Exception e) {
			e.printStackTrace();
			}

	}

}
