package udemy.hibernate;
// Generated 8 de set. de 2021 20:01:41 by Hibernate Tools 5.5.3.Final

import java.io.File;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.InitialContext;
import org.hibernate.LockMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Example;
import org.hibernate.query.Query;

/**
 * Home object for domain model class CursoTab.
 * @see udemy.hibernate.CursoTab
 * @author Hibernate Tools
 */
public class CursoTabDAO {

	private static final Logger logger = Logger.getLogger(CursoTabDAO.class.getName());

	private final SessionFactory sessionFactory = getSessionFactory();
	
	protected SessionFactory getSessionFactory() {
		SessionFactory sessionFactory = new Configuration().configure(new File("src/META-INF/hibernate.cfg.xml"))
				.buildSessionFactory();
		return sessionFactory;
	}

	public void persist(CursoTab transientInstance) {
		logger.log(Level.INFO, "persisting Curso instance");
		try {
	
			Session session = sessionFactory.getCurrentSession();
			//n�o fa�o nada sem ter uma transa�o
			session.beginTransaction();
			session.persist(transientInstance);
			session.getTransaction().commit();
			
		} catch (RuntimeException re) {
			logger.log(Level.SEVERE, "persist failed", re);
			throw re;
		}
	}
	
	public void delete(CursoTab persistentInstance) {
		logger.log(Level.INFO, "delete Curso instance");
		try {
	
			Session session = sessionFactory.getCurrentSession();
			//n�o fa�o nada sem ter uma transa�o
			session.beginTransaction();
			session.delete(persistentInstance);
			session.getTransaction().commit();
			logger.log(Level.INFO, "delete sucessuful");
		} catch (RuntimeException re) {
			logger.log(Level.WARNING, "Persiste Failed " + re);
			throw re;
		}
	}
	
	public void merge(CursoTab detachedInstance) {
		logger.log(Level.INFO, "merge Curso instance");
		try {
	
			Session session = sessionFactory.getCurrentSession();
			//n�o fa�o nada sem ter uma transa�o
			session.beginTransaction();
			session.merge(detachedInstance);
			session.getTransaction().commit();
			logger.log(Level.INFO, "merge sucessuful");
		} catch (RuntimeException re) {
			logger.log(Level.WARNING, "merge Failed " + re);
			throw re;
		}
	}
	
	public List<CursoTab>findAll() {
		logger.log(Level.INFO, "getting All Curso instance");
		try {
	
			Session session = sessionFactory.getCurrentSession();
			//n�o fa�o nada sem ter uma transa�o
			session.beginTransaction();
			//HQL
			String hql = "from CursoTab";
			Query query = session.createQuery(hql);
			List<CursoTab> cursos = query.list();
			session.getTransaction().commit();
			return cursos;
		} catch (RuntimeException re) {
			logger.log(Level.WARNING, "merge Failed " + re);
			throw re;
		}
	}
	
	public CursoTab find(long codcurso) {
		logger.log(Level.INFO, "getting Clientes instance");
		try {
	
			Session session = sessionFactory.getCurrentSession();
			session.beginTransaction();

			//ClienteTab instance = (ClienteTab) sessionFactory.getCurrentSession().get("udemy.hibernate.CursoTab", cdcurso);
			CursoTab curso = (CursoTab)sessionFactory.getCurrentSession().get("udemy.hibernate.CursoTab" ,codcurso) ;            
			session.getTransaction().commit();
			if (curso == null) {
				logger.log(Level.INFO, "get successful, no instance found");
			} else {
				logger.log(Level.INFO, "get successful, instance found");
			}
			return curso;
		} catch (RuntimeException re) {
			logger.log(Level.WARNING, "merge Failed " + re);
			throw re;
		}
			
	}	
}