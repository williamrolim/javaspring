package udemy.hibernate;

import java.util.List;
import java.util.Scanner;

public class PagamentoApp {

	public static void main(String[] args) {
		try {
			Scanner entrada = new Scanner(System.in);
			int opcao = 0;
			long cdcurso, cpf;
			String datainscricao;

			PagamentoTabDAO dao = new PagamentoTabDAO();
			while (opcao != 6) {
				System.out.println("Sistema De Geranciamento de Pagamento");
				System.out.println("============================================");
				System.out.println("Digite [1] para Consultar Todos os Pagamentos");
				System.out.println("Digite [2] para Consultar Cursos especifico");
				System.out.println("Digite [3] para Cadastrar Novo Pagamentos");
				System.out.println("Digite [4] para Alterar o Pagamentos");
				System.out.println("Digite [5] para Exluir um Pagamentos");
				System.out.println("Digite [6] para Sair");
				System.out.println("============================================");
				opcao = entrada.nextInt();

				switch (opcao) {
				case 1: {
					System.out.println("Opcao [1] Consultar Todos os pagamentos");
					List<PagamentoTab> pags = dao.findAll();
					pags.forEach(System.out::println);
					break;
				}

				case 2: { // Consultar
					System.out.println("Opcao [2] Consultar pagamentos Especifico");
					System.out.println("=======================================");
					System.out.println("Informar o cpf Do curso para consultar : ");
					cpf = entrada.nextLong();
					System.out.println("Informar o codigo Do curso para consultar : ");
					cdcurso = entrada.nextLong();
					PagamentoTabId pgtoid = new PagamentoTabId(cpf, cdcurso);
					System.out.println(dao.find(pgtoid));
					break;
				}
				case 3: {// Cadastrar
					System.out.println("Opcao [3] Cadastrar Cursos");
					System.out.println("=======================================");
					System.out.println("Digite o codigo do Curso: ");
					cdcurso = entrada.nextLong();
					entrada.nextLine();
					System.out.println("Digite o cpf do curso: ");
					cpf = entrada.nextLong();
					entrada.nextLine();
					PagamentoTabId pagID = new PagamentoTabId(cpf,cdcurso);
					System.out.println("Informar a data de inscri��o : ");
					datainscricao = entrada.nextLine();
					PagamentoTab pag = new PagamentoTab(pagID,datainscricao);
					dao.persist(pag);
					break;
				}
				case 4: {// Altera��o

					System.out.println("Opcao [4] Alterar Pagamento");
					System.out.println("=======================================");
					System.out.println("Digite o codigo do pagamento: ");
					cdcurso = entrada.nextLong();
					entrada.nextLine();
					System.out.println("Digite o cpf do pagamento: ");
					cpf = entrada.nextLong();
					entrada.nextLine();
					PagamentoTabId pagID = new PagamentoTabId(cpf,cdcurso);
					System.out.println("Informar a data de inscri��o : ");
					datainscricao = entrada.nextLine();
					PagamentoTab pag = new PagamentoTab(pagID,datainscricao);
					dao.merge(pag);

					break;
				}
				case 5: {// Excluir

					System.out.println("Opcao [5] Exluir Cursos Especifico");
					System.out.println("=======================================");
					System.out.println("Digite o cpf do pagamento: ");
					cpf = entrada.nextLong();
					entrada.nextLine();
					System.out.println("Informar o cod do Curso para exclus�o : ");
					cdcurso = entrada.nextLong();
					entrada.nextLine();


					PagamentoTabId pagID = new PagamentoTabId(cpf,cdcurso);
					PagamentoTab pag = dao.find(pagID);

					dao.delete(pag);
					System.out.println("Cursos Exluido : ");
					break;
				}

				case 6: {

					System.out.println("Encerrando o Sistema");
					break;
				}
				default:
					throw new IllegalArgumentException("N�mero errado, tente novamente " + opcao);
				}
			}
			entrada.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
