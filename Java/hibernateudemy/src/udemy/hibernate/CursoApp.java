package udemy.hibernate;

import java.util.List;
import java.util.Scanner;

public class CursoApp {
	
public static void main(String[] args) {
	
	try {
		Scanner entrada = new Scanner(System.in);
		int opcao = 0;
		long codCurso,valor;
		String nome, url;
		
		CursoTabDAO cursodao = new CursoTabDAO();
		while (opcao != 6) {
			System.out.println("Sistema De Geranciamento de Cursos");
			System.out.println("============================================");
			System.out.println("Digite [1] para Consultar Todos os Cursos");
			System.out.println("Digite [2] para Consultar curso especifico");
			System.out.println("Digite [3] para Cadastrar Novo curso");
			System.out.println("Digite [4] para Alterar o curso");
			System.out.println("Digite [5] para Exluir um Curso");
			System.out.println("Digite [6] para Sair");
			System.out.println("============================================");
			opcao = entrada.nextInt();

			switch (opcao) {
			case 1: {
				System.out.println("Opcao [1] Consultar Todos");
				List<CursoTab> cursos = cursodao.findAll();
				cursos.forEach(System.out::println);
				break;
			}

			case 2: { // Consultar
				System.out.println("Opcao [2] Consultar Curso Especifico");
				System.out.println("=======================================");
				System.out.println("Informar o codigo do curso do  para consultar : ");
				codCurso = entrada.nextLong();
				entrada.nextLine();
				System.out.println(cursodao.find(codCurso));

				break;
			}
			case 3: {// Cadastrar
				System.out.println("Opcao [3] Cadastrar Curso");
				System.out.println("=======================================");
				System.out.println("Digite o codigo do Curso: ");
				codCurso = entrada.nextLong();
				entrada.nextLine(); // Esvaziar o Buffer
				System.out.println("Digite o nome do Curso: ");
				nome = entrada.nextLine();
				System.out.println("Digite o valor do Curso: ");
				valor = entrada.nextLong();
				entrada.nextLine();
				System.out.println("Digite a url do curso: ");
				url= entrada.nextLine();

				CursoTab curso = new CursoTab(codCurso,nome,valor,url);
				cursodao.persist(curso);
				break;
			}
			case 4: {// Altera��o

				System.out.println("Opcao [4] Alterar Curso");
				System.out.println("=======================================");
				System.out.println("Digite o codigo do Curso: ");
				codCurso = entrada.nextLong();
				entrada.nextLine(); // Esvaziar o Buffer
				System.out.println("Digite o nome do Curso: ");
				nome = entrada.nextLine();
				System.out.println("Digite o valor do Curso: ");
				valor = entrada.nextLong();
				entrada.nextLine();
				System.out.println("Digite a url do curso: ");
				url= entrada.nextLine();

				CursoTab curso = new CursoTab(codCurso,nome,valor,url);
				cursodao.merge(curso);
				break;
			}
			case 5: {// Excluir

				System.out.println("Opcao [5] Exluir Curso Especifico");
				System.out.println("=======================================");
				System.out.println("Informar o CPF do curso para exclus�o : ");
				codCurso = entrada.nextLong();
				entrada.nextLine();
			
				CursoTab curso = cursodao.find(codCurso);
				
				cursodao.delete(curso);;
				System.out.println("Cliente Exluido : ");
				break;
			}

			case 6: {

				System.out.println("Encerrando o Sistema");
				break;
			}
			default:
				throw new IllegalArgumentException("N�mero errado, tente novamente " + opcao);
			}
		}
		entrada.close();
		} catch (Exception e) {
		e.printStackTrace();
		}
}
}
