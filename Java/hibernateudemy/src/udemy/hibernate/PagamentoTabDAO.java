package udemy.hibernate;
// Generated 8 de set. de 2021 20:01:41 by Hibernate Tools 5.5.3.Final

import java.io.File;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

/**
 * Home object for domain model class PagamentoTab.
 * @see udemy.hibernate.PagamentoTab
 * @author Hibernate Tools
 */
public class PagamentoTabDAO {

	private static final Logger logger = Logger.getLogger(PagamentoTabDAO.class.getName());

	private final SessionFactory sessionFactory = getSessionFactory();

	protected SessionFactory getSessionFactory() {
		SessionFactory sessionFactory = new Configuration().configure(new File("src/META-INF/hibernate.cfg.xml"))
				.buildSessionFactory();
		return sessionFactory;
	}

	public void persist(PagamentoTab transientInstance) {
		logger.log(Level.INFO, "persisting Pagamento instance");
		try {
	
			Session session = sessionFactory.getCurrentSession();
			//n�o fa�o nada sem ter uma transa�o
			session.beginTransaction();
			session.persist(transientInstance);
			session.getTransaction().commit();
			
		} catch (RuntimeException re) {
			logger.log(Level.SEVERE, "persist failed", re);
			throw re;
		}
	}
	
	public void delete(PagamentoTab persistentInstance) {
		logger.log(Level.INFO, "delete Pagamento instance");
		try {
	
			Session session = sessionFactory.getCurrentSession();
			//n�o fa�o nada sem ter uma transa�o
			session.beginTransaction();
			session.delete(persistentInstance);
			session.getTransaction().commit();
			logger.log(Level.INFO, "delete sucessuful");
		} catch (RuntimeException re) {
			logger.log(Level.WARNING, "Persiste Failed " + re);
			throw re;
		}
	}
	
	public void merge(PagamentoTab detachedInstance) {
		logger.log(Level.INFO, "delete Pagamento instance");
		try {
	
			Session session = sessionFactory.getCurrentSession();
			//n�o fa�o nada sem ter uma transa�o
			session.beginTransaction();
			session.merge(detachedInstance);
			session.getTransaction().commit();
			logger.log(Level.INFO, "merge sucessuful");
		} catch (RuntimeException re) {
			logger.log(Level.WARNING, "merge Failed " + re);
			throw re;
		}
	}
	
	public List<PagamentoTab>findAll() {
		logger.log(Level.INFO, "getting All Pagamento instance");
		try {
	
			Session session = sessionFactory.getCurrentSession();
			//n�o fa�o nada sem ter uma transa�o
			session.beginTransaction();
			//HQL
			//HQL
			String hql = "from PagamentoTab";
			Query query = session.createQuery(hql);
			List<PagamentoTab> pags = query.list();
			session.getTransaction().commit();
			return pags;
		} catch (RuntimeException re) {
			logger.log(Level.WARNING, "merge Failed " + re);
			throw re;
		}
	}
	
	public PagamentoTab find(PagamentoTabId pagID) {
		logger.log(Level.INFO, "getting Pagamento instance");
		try {
	
			Session session = sessionFactory.getCurrentSession();
	
			PagamentoTab instance = (PagamentoTab) sessionFactory.getCurrentSession().get("udemy.hibernate.PagamentoTabId", pagID);
			session.getTransaction().commit();
			if (instance == null) {
				logger.log(Level.INFO, "get successful, no instance found");
			} else {
				logger.log(Level.INFO, "get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			logger.log(Level.WARNING, "merge Failed " + re);
			throw re;
		}
	}	

}
