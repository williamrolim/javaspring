package udemy.hibernate;
// Generated 8 de set. de 2021 20:01:41 by Hibernate Tools 5.5.3.Final

import java.io.File;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import sun.rmi.runtime.Log;

/**
 * Home object for domain model class ClienteTab.
 * @see udemy.hibernate.ClienteTab
 * @author Hibernate Tools
 */
public class ClienteTabDAO{

	private static final Logger logger = Logger.getLogger(ClienteTabDAO.class.getName());

	private final SessionFactory sessionFactory = getSessionFactory();

	protected SessionFactory getSessionFactory() {
		SessionFactory sessionFactory = new Configuration().configure(new File("src/META-INF/hibernate.cfg.xml"))
				.buildSessionFactory();
		return sessionFactory;
	}

	public void persist(ClienteTab transientInstance) {
		logger.log(Level.INFO, "persisting Cliente instance");
		try {
	
			Session session = sessionFactory.getCurrentSession();
			//n�o fa�o nada sem ter uma transa�o
			session.beginTransaction();
			session.persist(transientInstance);
			session.getTransaction().commit();
			logger.log(Level.INFO, "persist sucessuful");
		} catch (RuntimeException re) {
			logger.log(Level.SEVERE, "persist failed", re);
			throw re;
		}
	}
	
	public void delete(ClienteTab persistentInstance) {
		logger.log(Level.INFO, "delete Cliente instance");
		try {
	
			Session session = sessionFactory.getCurrentSession();
			//n�o fa�o nada sem ter uma transa�o
			session.beginTransaction();
			session.delete(persistentInstance);
			session.getTransaction().commit();
			logger.log(Level.INFO, "delete sucessuful");
		} catch (RuntimeException re) {
			logger.log(Level.WARNING, "Persiste Failed " + re);
			throw re;
		}
	}
	
	public void merge(ClienteTab detachedInstance) {
		logger.log(Level.INFO, "merge Cliente instance");
		try {
	
			Session session = sessionFactory.getCurrentSession();
			//n�o fa�o nada sem ter uma transa�o
			session.beginTransaction();
			session.merge(detachedInstance);
			session.getTransaction().commit();
			logger.log(Level.INFO, "merge sucessuful");
		} catch (RuntimeException re) {
			logger.log(Level.WARNING, "merge Failed " + re);
			throw re;
		}
	}
	
	public List<ClienteTab>findAll() {
		logger.log(Level.INFO, "getting All Clientes instance");
		try {
	
			Session session = sessionFactory.getCurrentSession();
			//n�o fa�o nada sem ter uma transa�o
			session.beginTransaction();
			//HQL
			String hql = "from ClienteTab";
			Query query = session.createQuery(hql);
			List<ClienteTab> clientes = query.list();
			session.getTransaction().commit();
			return clientes;
		} catch (RuntimeException re) {
			logger.log(Level.WARNING, "merge Failed " + re);
			throw re;
		}
	}
	
	public ClienteTab find(long cpf) {
		logger.log(Level.INFO, "getting Clientes instance");
		try {
	
			Session session = sessionFactory.getCurrentSession();
			session.beginTransaction();
			ClienteTab instance = (ClienteTab) sessionFactory.getCurrentSession().get("udemy.hibernate.ClienteTab", cpf);
			session.getTransaction().commit();
			if (instance == null) {
				logger.log(Level.INFO, "get successful, no instance found");
			} else {
				logger.log(Level.INFO, "get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			logger.log(Level.WARNING, "merge Failed " + re);
			throw re;
		}
	}	

}
