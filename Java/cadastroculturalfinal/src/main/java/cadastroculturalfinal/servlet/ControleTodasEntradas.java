package cadastroculturalfinal.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cadastroculturalfinal.action.ActionInterface;

/**
 * Servlet implementation class ControleTodasEntradas
 */	
@WebServlet("/controletodasentradas")
public class ControleTodasEntradas extends HttpServlet {
	private static final long serialVersionUID = 1L;
	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String parametro = request.getParameter("acao");
		if (parametro == null) {
		RequestDispatcher rd = request.getRequestDispatcher("WEB-INF/view/login.jsp");
		rd.forward(request, response);
		}
		
		
		String paramAcao = request.getParameter("acao");
		String nomeDaClasse = "cadastroculturalfinal.action." + paramAcao;
		String nome;
		try {
			Class classe = Class.forName(nomeDaClasse);//carrega a classe com o nome
			ActionInterface actioninterface = (ActionInterface ) classe.newInstance();
			nome = actioninterface.executa(request,response);
		} catch (Exception e) {
			throw new ServletException(e);
		}

		String []tipoEEndereco = nome.split(":");
		if(tipoEEndereco[0].equals("forward")) {
		RequestDispatcher rd = request.getRequestDispatcher("WEB-INF/view/" + tipoEEndereco[1]);
		rd.forward(request, response);
		} else {
			//quando o navegador recebe a resposta o navegador envia
			response.sendRedirect(tipoEEndereco[1]);
		}
	}

}
