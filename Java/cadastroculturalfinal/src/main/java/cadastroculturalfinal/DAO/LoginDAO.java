package cadastroculturalfinal.DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.text.StyledEditorKit.BoldAction;

import cadastroculturalfinal.util.DataBase;

public class LoginDAO {

	Connection con;
	DataBase db = new DataBase();
	boolean condicaoLogado;

	public Boolean existeUsuario(String login, String senha) {
		try {
			con = db.getConnection();
			String sql = "Select name, password from login where name = ? and password = ?";

			PreparedStatement preparestatement = con.prepareStatement(sql);

			preparestatement.setString(1, login);
			preparestatement.setString(2, senha);

			ResultSet rs = preparestatement.executeQuery();

			if (rs.next()) {
				
				condicaoLogado = true;
			}
			preparestatement.close();
			rs.close();
		} catch (SQLException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			condicaoLogado = false;
		}
		return condicaoLogado;
	}
}
