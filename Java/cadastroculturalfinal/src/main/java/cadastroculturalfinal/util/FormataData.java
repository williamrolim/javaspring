package cadastroculturalfinal.util;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class FormataData {

	public static LocalDate formataDataMysql(String data) {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy"); 
		LocalDate era =  LocalDate.parse(data, dtf);
		return era;	
	}
	
}
