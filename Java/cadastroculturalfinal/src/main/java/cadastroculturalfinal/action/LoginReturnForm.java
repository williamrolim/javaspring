package cadastroculturalfinal.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LoginReturnForm implements ActionInterface{

	@Override
	public String executa(HttpServletRequest request, HttpServletResponse response) {
		
		return "forward:menuescolha.jsp";

	}

	
}
