package cadastroculturalfinal.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import cadastroculturalfinal.DAO.LoginDAO;

public class Login implements ActionInterface {

	@Override
	public String executa(HttpServletRequest request, HttpServletResponse response) {

		String login = request.getParameter("login");
		String senha = request.getParameter("senha");

		System.out.println("Logando .: " + login);

		LoginDAO logindao = new LoginDAO();
		boolean usuario = logindao.existeUsuario(login, senha);

		if (usuario != false) {
			System.out.println("Usuario existe");
			// httpsession objeto que guarda informações do usuario (cookies)
			// carinhos de compra, permissões , credenciais
			// HttpSession sessao = request.getSession();
			// sessao.setAttribute("usuarioLogado", usuario);
			return "redirect:controletodasentradas?acao=LoginReturnForm";
			// return "forward:menucadastro.jsp";
		} else {
			return "forward:login.jsp";

		}
	}

}
