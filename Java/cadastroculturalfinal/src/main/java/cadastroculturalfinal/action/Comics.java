package cadastroculturalfinal.action;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Comics implements ActionInterface{

	@Override
	public String executa(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String parametro = request.getParameter("acao");

		RequestDispatcher rd;

		if(parametro.equalsIgnoreCase("Comics")) {
			rd = request.getRequestDispatcher("WEB-INF/view/comics.jsp");
			rd.forward(request, response);
		}
		return null;
	}

}
