package cadastroculturalfinal.action;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Movies implements ActionInterface {

	@Override
	public String executa(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String parametro = request.getParameter("acao");

		RequestDispatcher rd;

		if(parametro.equalsIgnoreCase("Movies")) {
			String nome = request.getParameter("nome");
			System.out.println(nome);
			rd = request.getRequestDispatcher("WEB-INF/view/movie.jsp");
			rd.forward(request, response);
	}

		return null;
}
}
