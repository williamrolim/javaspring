<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
   pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%><!-- fmt formatar datas -->

<!DOCTYPE html>
<html>
   <head>
      <meta charset="ISO-8859-1">
<link href="movie.css" rel="stylesheet" type="text/css"> <style><%@include file="movie.css"%></style>
	</head>
   <body>
      <h1>MOVIE</h1>
      <form action="">
         <div>
         	 <label>Nome:</label> 
         	 <input name="nome" type="text" />
         	 <br>
            <label>Genero:</label> 
             <div id = escolhasFilmes>
            <input type="checkbox" id="genero"
               name="genero" value="acao">
            <label for="acao">A��o</label>
            <input type="checkbox" id="genero" name="genero" value="acao">
            <label for="acao">Aventura</label>
            <input type="checkbox" id="genero"
               name="genero" value="acao"> 
            <label for="acao">Biogr�fico</label>
            <input type="checkbox" id="genero" name="genero" value="acao">
            <label for="acao">Com�dia</label> <input type="checkbox" id="genero"
               name="genero" value="acao"> <label for="acao">Documentario</label>
            <input type="checkbox" id="genero" name="genero" value="acao">
            <label for="acao">Hist�rico</label> <input type="checkbox"
               id="genero" name="genero" value="acao"> <label
               for="acao">Drama</label> <input type="checkbox" id="genero"
               name="genero" value="acao"> <label for="acao">Fantasia</label>
            <input type="checkbox" id="genero" name="genero" value="acao">
            <label for="acao">Fic��o Cientifica</label> <input type="checkbox"
               id="genero" name="genero" value="acao"> <label
               for="acao">Musical</label> <input type="checkbox" id="genero"
               name="genero" value="acao"> <label for="acao">Romance</label>
            <input type="checkbox" id="genero" name="genero" value="acao">
            <label for="acao">Terror</label> <input type="checkbox" id="genero"
               name="genero" value="acao"> <label for="acao">Suspense</label>
         </div>
         </div>
         <label>Nota:</label> <input name="nota" type="number" /> <label>Diretor:</label>
         <input name="diretor" type="text" /> <label>Data De
         Lan�amento:</label> <input name="dataLancamento" type="date" /> <label>Data
         Que Assistiu:</label> <input name="dataAssistiu" type="date" /> <label>Descricao:</label>
         <textarea name="descricao" rows="4" cols="50"> </textarea>
         <input name="EnviarMovie" type="button" value="Enviar"/>
      </form>
      <ul>
         <c:forEach items="${movies}" var="movie">
            <li>
               <fmt:formatDate value="${movie.dataLancamento}"
                  pattern="dd/MM/YYYY" />
               <fmt:formatDate
                  value="${movie.dataAssistiu}" />
            </li>
         </c:forEach>
      </ul>
      <!-- Atravez da Expression Language verificar que o filme est� ou n�o cadastrado -->
      <c:if test="${not empty msg }">
         Filme cadastrado com sucesso
      </c:if>
      <c:if test="${empty msg }">
         Filme n�o cadastrado
      </c:if>
   </body>
</html>