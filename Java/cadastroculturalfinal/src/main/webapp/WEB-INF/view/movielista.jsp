<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<table>
		<thead>
			<tr>
				<th>Codigo do Filme</th>
				<th>Nome</th>
				<th>Genero</th>
				<th>Nota</th>
				<th>Diretor</th>
				<th>Data Lancamento</th>
				<th>Data que Assistiu</th>
				<th>Descricao</th>
				 <th colspan="2">Action</th> 
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${movies}" var="movie">
				<tr>	
					<td><c:out value="${movie.id}" /></td>
					<td><c:out value="${movie.nome}" /></td>
                    <td><c:out value="${movie.genero}" /></td>
					<td><c:out value="${movie.nota}" /></td>
					<td><c:out value="${movie.diretor}" /></td>
					<td><c:out value="${movie.dataLancamento}" /></td>
					<td><c:out value="${movie.dataAssistiu}" /></td>
					<td><c:out value="${movie.descricao}" /></td>
					<td><a
						href="MovieController.do?action=edit&movieID=<c:out value="${movie.id}"/>">Update</a></td>
					<td><a
						href="MovieController.do?action=delete&movieID=<c:out value="${movie.id}"/>">Delete</a></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
		<p>
		<a href="MovieController.do?action=insert">Adicionar Filme</a>
	</p>
</body>
</html>