package br.springthymeleaf.springthymeleaf.controller;

import java.security.Principal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import br.springthymeleaf.springthymeleaf.model.Pedido;
import br.springthymeleaf.springthymeleaf.model.StatusPedido;
import br.springthymeleaf.springthymeleaf.repository.PedidoRepository;

@Controller
@RequestMapping("usuario")
public class UsuarioController {
	@Autowired //serve para pedir para o spring quero uma instancia de pedidoreposity
	private PedidoRepository repository;
		
	@GetMapping("pedido") //mapiei a minha action   //Principal vai enjetar os dados do usuario logado (pega dados do usuario logado)
	public String home(Model model, Principal principal) {//Model seria o request do servlet
		List<Pedido> pedido = repository.findAllByUsuario(principal.getName());
		model.addAttribute("pedidos",pedido);
		return "usuario/home";
	}
	
	@GetMapping("pedido/{status}")
	public String porStatus(@PathVariable("status")String status, Model model, Principal principal) {
		List<Pedido> pedido = repository.findByStatusEUsuario(StatusPedido.valueOf(status.toUpperCase()),principal.getName());
		model.addAttribute("pedidos",pedido);
		model.addAttribute("status",status);
		return "usuario/home";
	}
	@ExceptionHandler(IllegalArgumentException.class)
	public String onError() {
		return "redirect:/usuario/home";
	}
	
}
