package br.springthymeleaf.springthymeleaf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@EnableCaching
@SpringBootApplication
public class SpringthymeleafApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringthymeleafApplication.class, args);
	}

}
