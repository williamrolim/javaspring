package br.springthymeleaf.springthymeleaf.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.springthymeleaf.springthymeleaf.model.Pedido;
import br.springthymeleaf.springthymeleaf.model.StatusPedido;



@Repository //@Repository essa classe aqui é um repositorio quero que gerencie essa classe e quero que crie instancia toda vez que alguem pedir
public interface PedidoRepository extends JpaRepository<Pedido, Long> {

	List<Pedido> findByStatus(StatusPedido status, Pageable sort);
								
	@Query("SELECT p from Pedido p JOIN p.user u WHERE u.username = :username")
	List<Pedido> findAllByUsuario(@Param("username") String username);

	@Query("SELECT p from Pedido p JOIN p.user u WHERE u.username = :username and p.status = :status")//@Param mapear o status para colocar na query
	List<Pedido> findByStatusEUsuario(@Param("status")StatusPedido status, @Param("username")String username);

}
