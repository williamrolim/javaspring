package br.springthymeleaf.springthymeleaf.service;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.springthymeleaf.springthymeleaf.dto.RequisicaoNovaOferta;
import br.springthymeleaf.springthymeleaf.model.Oferta;
import br.springthymeleaf.springthymeleaf.model.Pedido;
import br.springthymeleaf.springthymeleaf.repository.PedidoRepository;

@RestController
@RequestMapping("/api/ofertas")
public class OfertasServiceRest {
	@Autowired
	private PedidoRepository pedidoRepository;
	
	@PostMapping 
	public Oferta enviarOferta(@Valid @RequestBody RequisicaoNovaOferta requisicao) {
		Optional<Pedido>pedidoBuscado = pedidoRepository.findById(requisicao.getPedidoId());
		if(!pedidoBuscado.isPresent()) {
			return null;
		}
		Pedido pedido = pedidoBuscado.get();
		Oferta nova = requisicao.toOferta();
		nova.setPedido(pedido);
		pedido.getOfertas().add(nova);//como está cascade já salva para oferta
		pedidoRepository.save(pedido);
		
		return nova;
		}
}
