package br.springthymeleaf.springthymeleaf.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import br.springthymeleaf.springthymeleaf.dto.RequisicaoNovoPedido;
import br.springthymeleaf.springthymeleaf.model.Pedido;
import br.springthymeleaf.springthymeleaf.model.User;
import br.springthymeleaf.springthymeleaf.repository.PedidoRepository;
import br.springthymeleaf.springthymeleaf.repository.UserRepository;
@Controller 
@RequestMapping("pedido") //todas as requisições para pedido vão bater aqui
public class PedidoController {
	
	@Autowired
	private PedidoRepository pedidoRepository;//PARA SALVAR NO BANCO DE DADOS
	
	@Autowired
	private UserRepository userRepository;
	//action /pedido/formulario
	@GetMapping("formulario") //mapeado no nivel da action
	public String formulario(RequisicaoNovoPedido requisicao) {
		return "pedido/formulario"; //pasta pedido/formulario
	}
	
	@PostMapping("novo")                                       //bindigg result resultado da mensagem de erro
	public String novo(@Valid RequisicaoNovoPedido requisicao, BindingResult result) {//Requisição pedido informações do input do formulario
		if(result.hasErrors()) {
			return "pedido/formulario";
		}
		
		//Descobrir o usuario que está logado
		String username = SecurityContextHolder.getContext().getAuthentication().getName();
		User user = userRepository.findByUsername(username);
		
		Pedido pedido = requisicao.toPedido();
		pedido.setUser(user);
		pedidoRepository.save(pedido);//salvando no banco de dados
		return "redirect:/home";//redirect pedindo para outra URL
		//return "pedido/formulario";
		
	}
}
