package br.springthymeleaf.springthymeleaf.controller;

import java.security.Principal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import br.springthymeleaf.springthymeleaf.model.Pedido;
import br.springthymeleaf.springthymeleaf.model.StatusPedido;
import br.springthymeleaf.springthymeleaf.repository.PedidoRepository;

//Controller ela é gerenciada pelo proprio spring
@Controller
@RequestMapping("/home") //todas as requisições /home vão bater aqui
public class HomeController {
	//INJEÇÃO DE DEPENDENCIA
	@Autowired //serve para pedir para o spring quero uma instancia de pedidoreposity
	private PedidoRepository repository;
	
	
	@GetMapping //mapiei a minha action   //Principal vai enjetar os dados do usuario logado (pega dados do usuario logado)
	public String home(Model model, Principal principal) {//Model seria o request do servlet
		
		Sort sort = Sort.by("id").descending();// data de entrega por ordenação
		PageRequest paginacao = PageRequest.of(0,1,sort); //paginação me de apenas X itens por pagina
		
		List<Pedido> pedido = repository.findByStatus(StatusPedido.ENTREGUE, paginacao);
		model.addAttribute("pedidos",pedido);
		return "home";
	}
}
