package br.springthymeleaf.springthymeleaf.service;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.springthymeleaf.springthymeleaf.interceptor.InterceptadorDeAcessos;
import br.springthymeleaf.springthymeleaf.interceptor.InterceptadorDeAcessos.Acesso;

@RequestMapping("acessos")
@RestController
public class AcessosRest {
	
	@GetMapping
	public List<Acesso>getAcessos(){
		return InterceptadorDeAcessos.acessos;
	}
}
