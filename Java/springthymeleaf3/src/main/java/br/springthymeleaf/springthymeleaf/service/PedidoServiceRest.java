package br.springthymeleaf.springthymeleaf.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import br.springthymeleaf.springthymeleaf.model.Pedido;
import br.springthymeleaf.springthymeleaf.model.StatusPedido;
import br.springthymeleaf.springthymeleaf.repository.PedidoRepository;

@RestController
@RequestMapping("/api/pedidos")
public class PedidoServiceRest {

	@Autowired
	private PedidoRepository pedidoRepository;

	@GetMapping("aguardando")
	public List<Pedido> getPedidosAguardandoOfertas() {
		Sort sort = Sort.by("id").descending();// data de entrega por ordenação
		PageRequest paginacao = PageRequest.of(0, 1, sort); // paginação me de apenas X itens por pagina
		return pedidoRepository.findByStatus(StatusPedido.AGUARDANDO, paginacao);
	}
	
	
}
