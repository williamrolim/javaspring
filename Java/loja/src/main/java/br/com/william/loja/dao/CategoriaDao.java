package br.com.william.loja.dao;

import javax.persistence.EntityManager;

import br.com.william.loja.modelo.Categoria;

public class CategoriaDao {

	private EntityManager em;

	//EntityManager para n�o deixar a classe dao responsavel para gerenciar o empinity Manager
	public CategoriaDao(EntityManager em) {
		super();
		this.em = em;
	}
	
	public void Cadastrar(Categoria categoria) {
		this.em.persist(categoria);
	}
	
	public void Atualizar(Categoria categoria) {
		this.em.merge(categoria);//volta para o estado gerenciado pela aplica��o (managed)
	}
	
	public void Remover(Categoria categoria) {
		categoria = this.em.merge(categoria);//se n�o atribuir ao objeto categoria ela na� est� em um estado maneged, esta na categoria dateched
		this.em.remove(categoria);
	}
	
}
