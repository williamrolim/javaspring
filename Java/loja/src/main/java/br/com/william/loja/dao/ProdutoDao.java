package br.com.william.loja.dao;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.EntityManager;

import br.com.william.loja.modelo.Categoria;
import br.com.william.loja.modelo.Produto;

public class ProdutoDao {

	private EntityManager em;

	// EntityManager para n�o deixar a classe dao responsavel para gerenciar o
	// empinity Manager
	public ProdutoDao(EntityManager em) {
		super();
		this.em = em;
	}

	public void Cadastrar(Produto produto) {
		this.em.persist(produto);
	}

	public void Atualizar(Categoria categoria) {
		this.em.merge(categoria);// volta para o estado gerenciado pela aplica��o (managed)
	}

	public void Remover(Categoria categoria) {
		categoria = this.em.merge(categoria);// se n�o atribuir ao objeto categoria ela na� est� em um estado maneged,
												// esta na categoria dateched
		this.em.remove(categoria);
	}

	public Produto buscarPorId(Long id) {
		return em.find(Produto.class, id);// busca a classe da entidade produto (pelo mapeamento ele sabe)
	}

	public List<Produto> buscarTodos() {
		// JPQL PRINCIPAL MANEIRA DE FAZER UMA BUSCA, JAVA PERSISTENCE QUERY LANGANGE,
		// SQL ORIENTADA OBJETOS
		String jpql = "SELECT p FROM Produto as p";// carrega o objeto p
		return em.createQuery(jpql, Produto.class).getResultList();// create query nao dispara a query no banco de dados
		// getResultlist(dispara no bando de dados
	}

	public List<Produto> buscarPorNome(String nome) {
		String jpql = "SELECT p FROM Produto as p WHERE p.nome = :nome";
		return em.createQuery(jpql, Produto.class)
				.setParameter("nome", nome)
				.getResultList();
	}
	//ou
//	public List<Produto> buscarPorNome(String nome) {
//		String jpql = "SELECT p FROM Produto as p WHERE p.nome = :?1";
//		return em.createQuery(jpql, Produto.class)
//				.setParameter("1", nome)
//				.getResultList();
//	}
	
	public List<Produto> buscarPorNomeDaCategoria(String nome) {
		String jpql = "SELECT p FROM Produto as p WHERE p.categoria.nome = :nome";
		return em.createQuery(jpql, Produto.class)
				.setParameter("nome", nome)
				.getResultList();
	}
	
	public BigDecimal buscarPrecoDoProdutoComNome(String nome) {
		String jpql = "SELECT p.preco FROM Produto as p WHERE p.nome = :nome";
		return  em.createQuery(jpql, BigDecimal.class)
				.setParameter("nome", nome)
				.getSingleResult();//Metodo que tras somente um unico resultado
	}
}