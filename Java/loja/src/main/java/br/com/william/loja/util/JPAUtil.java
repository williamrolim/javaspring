package br.com.william.loja.util;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JPAUtil {
	//criar uma unica vez static
	private static final EntityManagerFactory FACTORY =Persistence.createEntityManagerFactory("loja"); //VEM DA TAG persistence-unity do persistence.xml
	//EntityManagerFactory          //classe Persistence , tem o metodo statico
			//EntityManager interface responsavel por todos os inserts do sql
			
			public 	static EntityManager getEntityManager() {
				return FACTORY.createEntityManager();
			}
}
