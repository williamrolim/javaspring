package br.com.william.loja.testes;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.EntityManager;

import br.com.william.loja.dao.CategoriaDao;
import br.com.william.loja.dao.ProdutoDao;
import br.com.william.loja.modelo.Categoria;
import br.com.william.loja.modelo.Produto;
import br.com.william.loja.util.JPAUtil;

public class CadastrosDeProdutos {
	public static void main(String[] args) {
		cadastrarProduto();	

		EntityManager em = JPAUtil.getEntityManager();
		
		ProdutoDao produtod = new ProdutoDao(em);
		Produto p = produtod.buscarPorId(1l);
		System.out.println(p.getPreco());
		
//		List<Produto> todos = produtod.buscarTodos();
//		todos.forEach(t -> System.out.println(p.getNome()));
		
//		List<Produto> buscar = produtod.buscarPorNome("Samsung s10e");
//		buscar.forEach(b -> System.out.println(p.getNome()));
		
		List<Produto> buscar = produtod.buscarPorNomeDaCategoria("Celulares");
		buscar.forEach(b -> System.out.println(p.getNome()));
		
		BigDecimal preco = produtod.buscarPrecoDoProdutoComNome("Samsung s10e");
		System.out.println(preco);
	}

	private static void cadastrarProduto() {
		Categoria celulares = new Categoria("Celulares");
		Produto celular = new Produto("Samsung s10e", "Nova gera��o",new BigDecimal("800"), celulares);	
		
		EntityManager em = JPAUtil.getEntityManager();
		
		ProdutoDao produtod = new ProdutoDao(em);
		CategoriaDao categoriad = new CategoriaDao(em);
		
		em.getTransaction().begin();/// Iniciando a transa��o
		//insert
		categoriad.Cadastrar(celulares);
		produtod.Cadastrar(celular);
		em.getTransaction().commit();
		em.close();
	}
}
