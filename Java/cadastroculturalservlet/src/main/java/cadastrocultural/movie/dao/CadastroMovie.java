package cadastrocultural.movie.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cadastroculturalservlet.database.ConectorMySQL;

public class CadastroMovie {

	public void cadastromovie(HttpServletRequest request, HttpServletResponse response) {
		Boolean condicao;
		
		ConectorMySQL con = new ConectorMySQL();
			String msg ;
			
			String botaoGravar = request.getParameter("Gravar");

			try {
				if (botaoGravar.equals("Gravar")) {

//				if ((request.getParameter("nome").isEmpty() || request.getParameter("senha").isEmpty())
//						|| (request.getParameter("nome").isEmpty()) && (request.getParameter("senha").isEmpty())) {
//					RequestDispatcher dispatcher = request.getRequestDispatcher("erro/404.html");
//					dispatcher.forward(request, response);
//				} else {	
			    Connection cone = con.initializeDatabase();
				PreparedStatement st = cone.prepareStatement("insert into movies_cadastro values(DEFAULT,?,?,?,?,?,?,?)");

				String dataLancamento, dataAssistiu;
				dataLancamento = request.getParameter("dataLancamento");
				dataAssistiu = request.getParameter("dataAssistiu");	
				DateTimeFormatter formater = DateTimeFormatter.ofPattern("yyyy-MM-dd");
				LocalDate dataL = LocalDate.parse(dataLancamento, formater);
				LocalDate dataA = LocalDate.parse(dataAssistiu,formater);


					st.setString(1, request.getParameter("name"));

					// Same for second parameter
					st.setString(2, request.getParameter("genero"));				
					st.setString(3, request.getParameter("nota"));						
					st.setString(4, request.getParameter("diretor"));						
					st.setString(5, dataL.toString());						
					st.setString(6, dataA.toString());
					st.setString(7, request.getParameter("descricao"));

					st.executeUpdate();

					st.close();
					cone.close();
					condicao = true;
					msg = "Cadastrado com sucesso";

					RequestDispatcher dispatcher = request.getRequestDispatcher("/moviescadastro.jsp");

					if (condicao = true) {
						request.setAttribute("mensagem", msg);
						dispatcher.forward(request, response);
					}
				
				}//end if gravar
			} catch (Exception e) {
				e.printStackTrace();
				condicao = false;
				msg = null;
			}
	
	}
}
