package cadastrocultural.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cadastroculturalservlet.database.ConectorMySQL;

/**
 * Servlet implementation class LoginController
 */
@WebServlet("/CadastroController")
public class CadastroUserController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	ConectorMySQL conexao;

	public CadastroUserController() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Boolean condicao;


		{
			try {
				String msg ;

				Class.forName("com.mysql.cj.jdbc.Driver");

				// Initialize the database
				Connection con = DriverManager.getConnection(
						"jdbc:mysql://localhost:3306/cadastrocultural?useTimezone=true&serverTimezone=UTC",
						"WilliamRolim", "sq3l@nd1A54Ever");

				// Create a SQL query to insert data into demo table
				// demo table consists of two columns, so two '?' is used

				try {

					if ((request.getParameter("nome").isEmpty() || request.getParameter("senha").isEmpty())
							|| (request.getParameter("nome").isEmpty()) && (request.getParameter("senha").isEmpty())) {
						RequestDispatcher dispatcher = request.getRequestDispatcher("erro/404.html");
						dispatcher.forward(request, response);
					} else {	
					PreparedStatement st = con.prepareStatement("insert into login values(DEFAULT,?, ?)");

					// For the first parameter,
					// get the data using request object
					// sets the data to st pointer
		

				


						st.setString(1, request.getParameter("nome"));

						// Same for second parameter
						st.setString(2, request.getParameter("senha"));

						// Execute the insert command using executeUpdate()
						// to make changes in database
						st.executeUpdate();

						// Close all the connections
						st.close();
						con.close();
						condicao = true;
						msg = "Cadastrado com sucesso";

						RequestDispatcher dispatcher = request.getRequestDispatcher("/cadastrousuario.jsp");

						if (condicao = true) {
							request.setAttribute("mensagem", msg);
							dispatcher.forward(request, response);
						}

					}
				} catch (Exception e) {
					e.printStackTrace();
					condicao = false;
					msg = null;
				}

			} catch (Exception e) {
				request.setAttribute("mensagem", "N�o cadastrado ");

				e.printStackTrace();
			}

		}
	}
}
