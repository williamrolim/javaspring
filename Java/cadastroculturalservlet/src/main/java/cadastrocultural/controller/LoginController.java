package cadastrocultural.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * Servlet implementation class LoginController
 */
@WebServlet("/logincontroller")
public class LoginController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	Boolean status;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");

			// Initialize the database
			Connection con = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/cadastrocultural?useTimezone=true&serverTimezone=UTC",
					"WilliamRolim", "sq3l@nd1A54Ever");
			
			PreparedStatement ps = con.prepareStatement("SELECT * FROM login WHERE name = ? and password = ?");
			
			ps.setString(1, request.getParameter("nome"));
			ps.setString(2, request.getParameter("senha"));
			
			ResultSet rs =  ps.executeQuery();
			
			status = rs.next();
			
			if (status.booleanValue() == true) {
				RequestDispatcher rd = request.getRequestDispatcher("/paginainicial.html");
				rd.forward(request, response);
			}else if (status.booleanValue() == false) {
				RequestDispatcher rd = request.getRequestDispatcher("/login.jsp");
				String msg = " Usuario ou senha invalido, tente novamente ";
				request.setAttribute("mensagem", msg);
				rd.forward(request, response);
			}
			
		}catch (Exception e) {
			e.printStackTrace();
		}

}
}
