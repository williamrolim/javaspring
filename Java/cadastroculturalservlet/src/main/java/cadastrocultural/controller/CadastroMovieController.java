package cadastrocultural.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cadastrocultural.view.Movies;
import cadastroculturalservlet.database.ConectorMySQL;

/**
 * Servlet implementation class CadastroMovieController
 */
@WebServlet("/CadastroMovieController")
public class CadastroMovieController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	ConectorMySQL con = new ConectorMySQL();
	String msg;
	Boolean condicao;

	public CadastroMovieController() {
		super();
		// TODO Auto-generated constructor stub
	}

//	String botaoExcluir = request.getParameter("Excluir");
//	String botaoMostrarTodos = request.getParameter("MostrarTodos");

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		{

			try {

				try {

					String botaoGravar = request.getParameter("Gravar");

					if (botaoGravar.equals("Gravar")) {

//					if ((request.getParameter("nome").isEmpty() || request.getParameter("senha").isEmpty())
//							|| (request.getParameter("nome").isEmpty()) && (request.getParameter("senha").isEmpty())) {
//						RequestDispatcher dispatcher = request.getRequestDispatcher("erro/404.html");
//						dispatcher.forward(request, response);
//					} else {	
						Connection cone = con.initializeDatabase();
						PreparedStatement st = cone
								.prepareStatement("insert into movies_cadastro values(DEFAULT,?,?,?,?,?,?,?)");

						String dataLancamento, dataAssistiu;
						dataLancamento = request.getParameter("dataLancamento");
						dataAssistiu = request.getParameter("dataAssistiu");
						DateTimeFormatter formater = DateTimeFormatter.ofPattern("yyyy-MM-dd");
						LocalDate dataL = LocalDate.parse(dataLancamento, formater);
						LocalDate dataA = LocalDate.parse(dataAssistiu, formater);

						st.setString(1, request.getParameter("name"));

						// Same for second parameter
						st.setString(2, request.getParameter("genero"));
						st.setString(3, request.getParameter("nota"));
						st.setString(4, request.getParameter("diretor"));
						st.setString(5, dataL.toString());
						st.setString(6, dataA.toString());
						st.setString(7, request.getParameter("descricao"));

						st.executeUpdate();

						st.close();
						cone.close();
						condicao = true;
						msg = "Cadastrado com sucesso";

						RequestDispatcher dispatcher = request.getRequestDispatcher("/moviescadastro.jsp");

						if (condicao = true) {
							request.setAttribute("mensagem", msg);
							dispatcher.forward(request, response);
						}

					} // end if gravar
				} // end of try

				catch (Exception e) {
					e.printStackTrace();
					condicao = false;
					msg = null;
					request.setAttribute("mensagem", "N�o cadastrado ");

				}

			} catch (Exception e) {
				request.setAttribute("mensagem", "N�o cadastrado ");

				e.printStackTrace();
			}
		}

	}

	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		List<Movies> lista = new ArrayList<>();

		try {

			String botaoAlterar = request.getParameter("Atualizar");
			if (botaoAlterar.equals("Atualizar")) {
				Connection conexao = con.initializeDatabase();
				PreparedStatement ps = conexao.prepareStatement("SELECT * FROM movies_cadastro");

				ResultSet rs = ps.executeQuery();
			
				// Movies m = new Movies();
				while (rs.next()) {

					Movies filmes = new Movies(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getInt(4),
							rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8));

					lista.add(filmes);
//					m.setNome(rs.getString(1));
//					m.setGenero(rs.getString(2));
//					m.setNota(rs.getInt(3));
//					m.setDiretor(rs.getString(4));
//					m.setDataLancamento(rs.getString(5));
//					m.setDataAssistiu(rs.getString(6));
//					m.setDescricao(rs.getString(7));
					request.setAttribute("filmes", lista);

			}
				RequestDispatcher rd = request.getRequestDispatcher("/mostrafilmes.jsp");
				rd.forward(request, response);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {

			String botaoPesquisar = request.getParameter("Pesquisar");

		}


		//return "forward:listafilmes.jsp";

	}

}
