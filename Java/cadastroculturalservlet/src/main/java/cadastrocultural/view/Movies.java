package cadastrocultural.view;

public class Movies {
	
	private int id;
	private String nome;
	private String genero;
	private int nota;
	private String diretor;
	private String dataLancamento;
	private String dataAssistiu;
	private String descricao;
		
	public Movies(int id, String nome, String genero, int nota, String diretor, String dataLancamento,
			String dataAssistiu, String descricao) {
		super();
		this.id = id;
		this.nome = nome;
		this.genero = genero;
		this.nota = nota;
		this.diretor = diretor;
		this.dataLancamento = dataLancamento;
		this.dataAssistiu = dataAssistiu;
		this.descricao = descricao;
	}
	
	public Movies() {
		
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getGenero() {
		return genero;
	}
	public void setGenero(String genero) {
		this.genero = genero;
	}
	public int getNota() {
		return nota;
	}
	public void setNota(int nota) {
		this.nota = nota;
	}
	public String getDiretor() {
		return diretor;
	}
	public void setDiretor(String diretor) {
		this.diretor = diretor;
	}
	public String getDataLancamento() {
		return dataLancamento;
	}
	public void setDataLancamento(String dataLancamento) {
		this.dataLancamento = dataLancamento;
	}
	public String getDataAssistiu() {
		return dataAssistiu;
	}
	public void setDataAssistiu(String dataAssistiu) {
		this.dataAssistiu = dataAssistiu;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	@Override
	public String toString() {
		return "Movies [id=" + id + ", nome=" + nome + ", genero=" + genero + ", nota=" + nota + ", diretor=" + diretor
				+ ", dataLancamento=" + dataLancamento + ", dataAssistiu=" + dataAssistiu + ", descricao=" + descricao
				+ "]";
	}

	
}
