package jdbc03_MYSQL;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class TestaListagemDeCategoriasNMAIS1 {
 public static void main(String[] args) {
	try (Connection connection = new ConnectionFactory().recuperarConexao()) {
		CategoriaDAO categoriaDAO = new CategoriaDAO(connection);
		List<Categoria> listaCategorias = categoriaDAO.listar();
		
		listaCategorias.stream().forEach(ct -> {System.out.println(ct.getNome());
		try {
			for (Produto produto : new ProdutoDAO(connection).buscar(ct)) {
				System.out.println(ct.getNome() + " - " + produto.getNome());
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		});
	} catch (Exception e) {
		// TODO: handle exception
	}
}
}
/*N + 1 = Quando estamos falando de consultas simples, pode-se n�o ver o
 *  problema, mas conforme a complexidade vai aumentando e 
 *  precisa-se trazer informa��es de v�rias tabelas, o acesso
 *   aumenta exponencialmente, atrapalhando a performance do banco
 *    de dados.
 */