package jdbc03_MYSQL;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class TestaListagem {
	public static void main(String[] args) throws SQLException {
		
		ConnectionFactory connectionFactory = new ConnectionFactory();
		Connection connection = connectionFactory.recuperarConexao();
		
		Statement stm = connection.createStatement();
		stm.execute("SELECT * FROM PRODUTO");

		ResultSet rst = stm.getResultSet();

		while (rst.next()) {
			int id = rst.getInt("ID");
			String nome = rst.getString("NOME");
			String descricao = rst.getString("descricao");
			System.out.println("ID: " + id + ", Nome.: " + nome + ", Descricao.: " + descricao);
		}
		connection.close();
	}
}
