package jdbc03_MYSQL;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import com.mchange.v2.c3p0.ComboPooledDataSource;


public class ConnectionFactory {
	
    public DataSource dataSource;

	public ConnectionFactory() {
		ComboPooledDataSource comboPooledDataSource = new ComboPooledDataSource();
        comboPooledDataSource.setJdbcUrl("jdbc:mysql://localhost/lojavirtual?userTimezone=true&serverTimezone=UTC");
		comboPooledDataSource.setUser("root");
		comboPooledDataSource.setPassword("R@izesone17#");
	
		//Setar o n�mero maximo de conex�es que tem que ser abertas
		comboPooledDataSource.setMaxPoolSize(15);
		
        this.dataSource = comboPooledDataSource;

	}

	public Connection recuperarConexao(){
		
		try {
			return dataSource.getConnection();

		} catch (SQLException ex) {
			ex.printStackTrace();
			System.out.println("Erro de conex�o..: " + ex.toString() );
			return null;
		}
	}
}
