package jdbc03_MYSQL;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class ProdutoDAO {

	private Connection connection;

	public ProdutoDAO(Connection connection) {
		this.connection = connection;
	}

	public void SalvarProduto(Produto produto) throws SQLException {
		try (PreparedStatement ps = connection.prepareStatement(
				"INSERT INTO PRODUTO (NOME, DESCRICAO) VALUES (?,?)", Statement.RETURN_GENERATED_KEYS)) {
			ps.setString(1, produto.getNome());
			ps.setString(2, produto.getDescricao());
			
			ps.execute();
			
			try (ResultSet rs = ps.getGeneratedKeys()) {
				while (rs.next()) {
					produto.setId(rs.getInt(1));
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}
		public List<Produto>listar() throws SQLException {
			List<Produto> produtos = new ArrayList<Produto>();
			
			System.out.println("Executando a query de listar a categoria");
			
			String sql = "SELECT ID, NOME, DESCRICAO FROM PRODUTO";
			
			try(PreparedStatement ps = connection.prepareStatement(sql)){
				ps.execute();

			try (ResultSet rs = ps.getResultSet()){
				while(rs.next()) {
					Produto itens = new Produto(rs.getInt(1), rs.getString(2), rs.getString(3));
					produtos.add(itens);

				}
			} catch (SQLException ex) {
					ex.printStackTrace();
			}
			}
			return produtos;
		}

		public List<Produto> buscar(Categoria ct) throws SQLException {
			List<Produto> produtos = new ArrayList<Produto>();
			System.out.println("Executando a query de buscar produto por categoria");

			String sql = "SELECT ID, NOME, DESCRICAO FROM PRODUTO WHERE CATEGORIA_ID = ?";
			
			try(PreparedStatement ps = connection.prepareStatement(sql)){
				ps.setInt(1, ct.getId());
				ps.execute();

			try (ResultSet rs = ps.getResultSet()){
				while(rs.next()) {
					Produto itens = new Produto(rs.getInt(1), rs.getString(2), rs.getString(3));
					produtos.add(itens);

				}
			} catch (SQLException ex) {
					ex.printStackTrace();
			}
			}
			return produtos;
		}


}
