package jdbc03_MYSQL;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class TestaInsercao {
	public static void main(String[] args) throws SQLException  {
		ConnectionFactory factory = new ConnectionFactory();
		Connection connection = factory.recuperarConexao();
		
		
//		Statement stm = connection.createStatement();
//		boolean resultado = stm.execute("INSERT INTO PRODUTO (NOME, DESCRICAO) VALUES ('Mouse','Mouse sem Fio')");
		//retorna um boolean verdadeiro para lista falso para n�o lista, como n�o � uma lista retorna falso e entra no banco
//		System.out.println(resultado); 
		
		Statement stm = connection.createStatement();
		stm.execute("INSERT INTO PRODUTO (NOME, DESCRICAO) VALUES ('Teclado','Teclado sem fio')", Statement.RETURN_GENERATED_KEYS);
		ResultSet rst = stm.getGeneratedKeys();
		while (rst.next()) {
			Integer id = rst.getInt(1);
			System.out.println("O ID criado foi " + id);
		}
	}
}
