package jdbc03_MYSQL;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Scanner;
import java.util.List;

public class TestaInsercaoEListagemComProdutos {

	public static void main(String[] args) throws SQLException {

		@SuppressWarnings("resource")
		Scanner entrada = new Scanner(System.in);
		int number = 0;
		String nome = "", descricao = "";
		System.out.println("=======================================");
		System.out.println("Digite 1 para gravar produto \n Digite 2 para listar produtos ");
		number = entrada.nextInt();
		switch (number) {
		case 1: {
			System.out.println("=======================================");
			System.out.println("Informar o nome do produto : ");
			nome = entrada.nextLine();
			System.out.println("=======================================");
			System.out.println("Informar a descri��o do produto : ");
			descricao = entrada.nextLine();
			Produto produto = new Produto(nome, descricao);

			try (Connection connection = new ConnectionFactory().recuperarConexao()) {
				new ProdutoDAO(connection).SalvarProduto(produto);
			}

			System.out.println("=======================================");
			System.out.println(produto.toString());
			break;
		}
		case 2: {
			try (Connection connection = new ConnectionFactory().recuperarConexao()) {
				ProdutoDAO produtoSdao = new ProdutoDAO(connection);
				List<Produto>listaProdutos = produtoSdao.listar();
				listaProdutos.stream().forEach(lp -> System.out.println(lp));
				break;
			}
		}
		default:
			throw new IllegalArgumentException("Unexpected value: " + number);
		}

	}

}
