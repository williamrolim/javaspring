package jdbc03_MYSQL;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class TestaRemocao {
  public static void main(String[] args) throws SQLException {
	
	  ConnectionFactory factory = new ConnectionFactory();
	  Connection connection = factory.recuperarConexao();
	  
	  Statement statement = connection.createStatement();
	  
	  statement.execute("DELETE FROM PRODUTO WHERE ID > 3");
	  
	  //getupdatecont = quantas linhas foram executadas apos o statement ser executado

	  int linhasModificadas = statement.getUpdateCount();
	  System.out.println("Quantidade de Linhas  que foram Excluidas  " + linhasModificadas);
}
}
