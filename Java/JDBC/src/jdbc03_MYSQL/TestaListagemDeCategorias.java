package jdbc03_MYSQL;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class TestaListagemDeCategorias {

	public static void main(String[] args) {
		try (Connection connection = new ConnectionFactory().recuperarConexao()) {
			CategoriaDAO categoriaDAO = new CategoriaDAO(connection);
			List<Categoria> listaCategorias = categoriaDAO.listarComProdutos();

			listaCategorias.stream().forEach(ct -> {
				System.out.println(ct.getNome());
			try {
				for (Produto produto : ct.getProdutos()) {
					System.out.println(ct.getNome() + " - " + produto.getNome());
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			});
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

}
