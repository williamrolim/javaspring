package jdbc03_MYSQL;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class TestaInsercaoComParametro {

	public static void main(String[] args) throws SQLException {
		String nome = "Pilha'";
		String descricao = "Pilha palito); delete from Produto;";//usuario mal intencionado utilizando sql injection
		
		ConnectionFactory factory = new ConnectionFactory();
		Connection connection = factory.recuperarConexao();
		
		PreparedStatement ps = connection.prepareStatement("INSERT INTO PRODUTO (NOME, DESCRICAO) VALUES (?,?)", Statement.RETURN_GENERATED_KEYS);
		
		ps.setString(1, nome);
		ps.setString(2, descricao);
		
		ps.execute();
		
		ResultSet rs = ps.getGeneratedKeys();

		while (rs.next()) {
			Integer id = rs.getInt(1);
			System.out.println("O ID criado foi " + id);
	}

}
}
