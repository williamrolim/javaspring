package jdbc03_MYSQL;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class TestaInsercaoComParametro2 {

	public static void main(String[] args) throws SQLException {

		ConnectionFactory factory = new ConnectionFactory();
		try (Connection connection = factory.recuperarConexao()) {

			connection.setAutoCommit(false);

			try (PreparedStatement ps = connection.prepareStatement(
					"INSERT INTO PRODUTO (NOME, DESCRICAO) VALUES (?,?)", Statement.RETURN_GENERATED_KEYS)) {
				;

				adicionarVariavel("Smart Tv", "42 polegadas", ps);
				adicionarVariavel("Radio", "Radio Portatil", ps);

				connection.commit();

			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("ROLL BACK EXECUTADO");
				connection.rollback();
			}

		}

	}

	private static void adicionarVariavel(String nome, String descricao, PreparedStatement pm) throws SQLException {
		pm.setString(1, nome);
		pm.setString(2, descricao);

		
		pm.execute();
		try (ResultSet rs = pm.getGeneratedKeys()) {
			while (rs.next()) {
				int id = rs.getInt(0);
				System.out.println("O ID criado foi " + id);
			}
		} catch (SQLException e) {
			e.printStackTrace();

		}
	}
}
