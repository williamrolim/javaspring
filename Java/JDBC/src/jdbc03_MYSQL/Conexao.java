package jdbc03_MYSQL;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Conexao {
public static void main(String[] args) throws SQLException  {
//	Connection connection = DriverManager.getConnection(
//			"jdbc:mysql://localhost/lojavirtual?userTimezone=true&serverTimezone=UTC", "root", "R@izesone17#");
//	connection.close();
	
	ConnectionFactory connectionFactory = new ConnectionFactory();
	Connection connection =  connectionFactory.recuperarConexao();
	
	System.out.println("conex�o close");
	connection.close();
}
}
