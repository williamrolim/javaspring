package jdbc02.VO.DAO_ORACLE;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class ClienteDAO {
	static String url = "jdbc:oracle:thin:@localhost:1521:xe";
	static String usuario = "CursoJava";
	static String senha = "schema";
	static Connection conexao;
	
	public static void conectar() throws SQLException {
		conexao = DriverManager.getConnection(url,usuario,senha);
		DatabaseMetaData meta = conexao.getMetaData();
		conexao.setAutoCommit(false);
		System.out.println("Conectado ao Banco de Dados..: " + meta.getDatabaseProductVersion());
	}
	
	public static void desconectar() throws SQLException {
		conexao.close();
	}
	
	public static void inserirCliente(Cliente cliente) throws SQLException
	{
		String inserir = "INSERT INTO Cliente_tab VALUES (" + cliente.getCpf() + ",'" + cliente.getNome() + "','" + cliente.getEmail() + "')";
		Statement statement = conexao.createStatement();
		statement.execute(inserir);
		conexao.commit();
	}
	/**
	 * 
	 * @param cliente
	 * @throws SQLException
	 */
	
	public static void inserirClientePS(Cliente cliente) throws SQLException
	{
		String inserir = "INSERT INTO Cliente_tab VALUES (?,?,?)";
		PreparedStatement statement = conexao.prepareStatement(inserir);
		statement.setLong(1, cliente.getCpf());
		statement.setString(2, cliente.getNome());
		statement.setString(3, cliente.getEmail());
		statement.executeUpdate();
		conexao.commit();
	}
	
	/**
	 * 	
	 * @param cliente
	 * @throws SQLException
	 */
	public static void inserirClienteSP(Cliente cliente) throws SQLException
	{
		String inserir = "call SP_INSERIR_CLIENTE (?,?,?)";
		CallableStatement callable = conexao.prepareCall(inserir);
		callable.setLong(1, cliente.getCpf());
		callable.setString(2, cliente.getNome());
		callable.setString(3, cliente.getEmail());
		callable.executeUpdate();
		conexao.commit();
	}
	
	public static Cliente consultarCliente (long cpf) throws SQLException {
		String consultar = "SELECT * FROM CLIENTE_TAB WHERE cpf = " + cpf + "";
		Statement statement = conexao.createStatement();
		ResultSet resultadoP = statement.executeQuery(consultar);
		Cliente cliente = null;
		while (resultadoP.next()) {
			cliente = new Cliente(resultadoP.getLong(1), resultadoP.getString(2), resultadoP.getString(2));
		}
		return cliente;
	}
	public static List <Cliente> consultarTodos () throws SQLException {
		String consultar = "SELECT * FROM CLIENTE_TAB";
		Statement statement = conexao.createStatement();
		ResultSet resultadoConsulta = statement.executeQuery(consultar);
		List<Cliente> cliente = new LinkedList<>();
		//int contador = resultadoConsulta.getRow();
		while (resultadoConsulta.next()) {
			Cliente clientes = new Cliente(resultadoConsulta.getLong(1),resultadoConsulta.getString(2),resultadoConsulta.getString(3));
			cliente.add(clientes);
		}
		return cliente;
	}
	
	public static void alterarCliente(Cliente cliente) throws SQLException
	{
		String alterar = "UPDATE Cliente_tab SET nome = '" + cliente.getNome() + "',email='" + cliente.getEmail() + "' WHERE cpf = " + cliente.getCpf() + "";
		Statement statement = conexao.createStatement();
		statement.executeUpdate(alterar);
		conexao.commit();
	}
	
	public static void excluirCliente(long cpf) throws SQLException
	{
		String excluir = "DELETE FROM CLIENTE_TAB WHERE CPF = " + cpf ;
		Statement statement = conexao.createStatement();
		statement.execute(excluir);
		conexao.commit();
	}
		 
	public static void main(String[] args) {
		try {
			conectar();
			Scanner entrada = new Scanner(System.in);
			int opcao = 0;
			long cpf = 0;
			String nome = "", email = "";
			while (opcao != 6) {
				System.out.println("Sistema De Geranciamento de Clientes");
				System.out.println("============================================");
				System.out.println("Digite [1] para Consultar Todos os Clientes");
				System.out.println("Digite [2] para Consultar cliente especifico");
				System.out.println("Digite [3] para Cadastrar Novo cliente");
				System.out.println("Digite [4] para Alterar o Cliente");
				System.out.println("Digite [5] para Exluir um Cliente");
				System.out.println("Digite [6] para Sair");
				System.out.println("============================================");
				opcao = entrada.nextInt();

				switch (opcao) {
				case 1: {
					System.out.println("Opcao [1] Consultar Todos");
					List<Cliente> clientes = ClienteDAO.consultarTodos();
					clientes.forEach(System.out::println);
					System.out.println("N�mero Total de clientes..: " + clientes.size() + "\n");
					break;
				}

				case 2: { // Consultar
					System.out.println("Opcao [2] Consultar Cliente Especifico");
					System.out.println("=======================================");
					System.out.println("Informar o CPF do cliente para consultar : ");
					cpf = entrada.nextLong();
					entrada.nextLine();
					Cliente cliente = ClienteDAO.consultarCliente(cpf);
					System.out.println("Cliente consultado..: " + cliente);

					break;
				}
				case 3: {// Cadastrar
					System.out.println("Opcao [3] Cadastrar Cliente");
					System.out.println("=======================================");
					System.out.println("Digite o cpf do Cliente: ");
					cpf = entrada.nextLong();
					entrada.nextLine(); // Esvaziar o Buffer
					System.out.println("Digite o nome do Cliente: ");
					nome = entrada.nextLine();
					System.out.println("Digite o email do Cliente: ");
					email = entrada.nextLine();
					Cliente cliente = new Cliente(cpf,nome,email);
					//inserirCliente(cpf,nome,email);
					inserirClientePS(cliente);
					//inserirClienteSP(cliente);


					break;
				}
				case 4: {// Altera��o

					System.out.println("Opcao [4] Alterar Cliente");
					System.out.println("=======================================");
					System.out.println("Digite o cpf do Cliente: ");
					cpf = entrada.nextLong();
					entrada.nextLine(); // Esvaziar o Buffer
					System.out.println("Digite o nome do Cliente: ");
					nome = entrada.nextLine();
					System.out.println("Digite o email do Cliente: ");
					email = entrada.nextLine();
					Cliente cliente = new Cliente(cpf,nome,email);
					ClienteDAO.alterarCliente(cliente);
					System.out.println("Cliente Alterado : ");

					break;
				}
				case 5: {// Excluir

					System.out.println("Opcao [5] Exluir Cliente Especifico");
					System.out.println("=======================================");
					System.out.println("Informar o CPF do cliente para exclus�o : ");
					cpf = entrada.nextLong();
					entrada.nextLine();
					ClienteDAO.excluirCliente(cpf);
					System.out.println("Cliente Exluido : ");
					break;
				}

				case 6: {

					System.out.println("Encerrando o Sistema");
					break;
				}
				default:
					throw new IllegalArgumentException("N�mero errado, tente novamente " + opcao);
				}
			}
			entrada.close();
			desconectar();

			} catch (SQLException e) {
			e.printStackTrace();
			}

	}

}
