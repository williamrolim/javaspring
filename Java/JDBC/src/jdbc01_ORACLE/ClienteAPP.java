package jdbc01_ORACLE;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class ClienteAPP {
	static String url = "jdbc:oracle:thin:@localhost:1521:xe";
	static String usuario = "CursoJava";
	static String senha = "schema";
	static Connection conexao;
	
	public static void conectar() throws SQLException {
		conexao = DriverManager.getConnection(url,usuario,senha);
		conexao.setAutoCommit(false);
	}
	
	public static void desconectar() throws SQLException {
		conexao.close();
	}
	
	public static void inserirCliente(long cpf, String nome, String email ) throws SQLException
	{
		String inserir = "INSERT INTO Cliente_tab VALUES (" + cpf + ",'" + nome + "','" + email + "')";
		Statement statement = conexao.createStatement();
		statement.execute(inserir);
		conexao.commit();
	}
	
	public static void inserirClientePS(long cpf, String nome, String email ) throws SQLException
	{
		String inserir = "INSERT INTO Cliente_tab VALUES (?,?,?)";
		PreparedStatement statement = conexao.prepareStatement(inserir);
		statement.setLong(1, cpf);
		statement.setString(2, nome);
		statement.setString(3, email);
		statement.executeUpdate();
		conexao.commit();
	}
	
	public static void inserirClienteSP(long cpf, String nome, String email ) throws SQLException
	{
		String inserir = "call SP_INSERIR_CLIENTE (?,?,?)";
		CallableStatement callable = conexao.prepareCall(inserir);
		callable.setLong(1, cpf);
		callable.setString(2, nome);
		callable.setString(3, email);
		callable.executeUpdate();
		conexao.commit();
	}
	
	public static void consultarCliente (long cpf) throws SQLException {
		String consultar = "SELECT * FROM CLIENTE_TAB WHERE cpf = " + cpf + "";
		Statement statement = conexao.createStatement();
		ResultSet resultadoP = statement.executeQuery(consultar);
		while (resultadoP.next()) {
			System.out.println("CPF..: " + resultadoP.getLong(1) + "\n Nome..: " + resultadoP.getString(1) + "\n email ..:" + resultadoP.getString(2));
		}
	}
	
	public static void consultarTodos () throws SQLException {
		String consultar = "SELECT * FROM CLIENTE_TAB";
		Statement statement = conexao.createStatement();
		ResultSet resultadoConsulta = statement.executeQuery(consultar);
		int contador = resultadoConsulta.getRow();
		while (resultadoConsulta.next()) {
			System.out.println("CPF..: " + resultadoConsulta.getLong(1) + "\n Nome..: " + resultadoConsulta.getString(1) + "\n email ..:" + resultadoConsulta.getString(2));
		}
		System.out.println("\n **********************************************");
		System.out.println("\n" + contador + "N�mero de clientes listados");
	}
	
	public static void alterarCliente(long cpf, String nome, String email ) throws SQLException
	{
		String alterar = "UPDATE Cliente_tab SET nome = '" + nome + "',email='" + email + "' WHERE cpf = " + cpf + "";
		Statement statement = conexao.createStatement();
		statement.execute(alterar);
		conexao.commit();
	}
	
	public static void excluirCliente(long cpf) throws SQLException
	{
		String excluir = "DELETE FROM CLIENTE_TAB WHERE CPF = " + cpf ;
		Statement statement = conexao.createStatement();
		statement.execute(excluir);
		conexao.commit();
	}
		 
	public static void main(String[] args) {
		try {
			conectar();
			Scanner entrada = new Scanner(System.in);
			int opcao = 0;
			long cpf = 0;
			String nome = "", email = "";
			while (opcao != 6) {
				System.out.println("Sistema De Geranciamento de Clientes");
				System.out.println("============================================");
				System.out.println("Digite [1] para Consultar Todos os Clientes");
				System.out.println("Digite [2] para Consultar cliente especifico");
				System.out.println("Digite [3] para Cadastrar Novo cliente");
				System.out.println("Digite [4] para Alterar o Cliente");
				System.out.println("Digite [5] para Exluir um Cliente");
				System.out.println("Digite [6] para Sair");
				System.out.println("============================================");
				opcao = entrada.nextInt();

				switch (opcao) {
				case 1: {
					System.out.println("Opcao [1] Consultar Todos");
					consultarTodos();

					break;
				}

				case 2: { // Consultar
					System.out.println("Opcao [2] Consultar Cliente Especifico");
					System.out.println("=======================================");
					System.out.println("Informar o CPF do cliente para consultar : ");
					cpf = entrada.nextLong();
					entrada.nextLine();
					consultarCliente(cpf);

					break;
				}
				case 3: {// Cadastrar
					System.out.println("Opcao [3] Cadastrar Cliente");
					System.out.println("=======================================");
					System.out.println("Digite o cpf do Cliente: ");
					cpf = entrada.nextLong();
					entrada.nextLine(); // Esvaziar o Buffer
					System.out.println("Digite o nome do Cliente: ");
					nome = entrada.nextLine();
					System.out.println("Digite o email do Cliente: ");
					email = entrada.nextLine();
					//inserirCliente(cpf,nome,email);
					//inserirClientePS(cpf, nome, email);
					inserirClienteSP(cpf, nome, email);
					System.out.println("Cliente Cadastrado : ");

					break;
				}
				case 4: {// Altera��o

					System.out.println("Opcao [4] Alterar Cliente");
					System.out.println("=======================================");
					System.out.println("Digite o cpf do Cliente: ");
					cpf = entrada.nextLong();
					entrada.nextLine(); // Esvaziar o Buffer
					System.out.println("Digite o nome do Cliente: ");
					nome = entrada.nextLine();
					System.out.println("Digite o email do Cliente: ");
					email = entrada.nextLine();
					alterarCliente(cpf, nome, email);
					System.out.println("Cliente Alterado : ");

					break;
				}
				case 5: {// Excluir

					System.out.println("Opcao [5] Exluir Cliente Especifico");
					System.out.println("=======================================");
					System.out.println("Informar o CPF do cliente para exclus�o : ");
					cpf = entrada.nextLong();
					entrada.nextLine();
					excluirCliente(cpf);
					System.out.println("Cliente Exluido : ");
					break;
				}

				case 6: {

					System.out.println("Encerrando o Sistema");
					break;
				}
				default:
					throw new IllegalArgumentException("N�mero errado, tente novamente " + opcao);
				}
			}
			entrada.close();
			desconectar();
			} catch (SQLException e) {
			e.printStackTrace();
			}

	}

	}

