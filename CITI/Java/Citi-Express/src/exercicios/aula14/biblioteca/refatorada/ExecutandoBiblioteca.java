package exercicios.aula14.biblioteca.refatorada;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class ExecutandoBiblioteca {
	public static void main(String[] args) {
		RegraNegocio();
	}

	static void RegraNegocio() {
		List<Biblioteca> b = new LinkedList<Biblioteca>();

		String nome, autor, editora, pesquisaLivros, escolha2;
		int paginas, escolha;

		Scanner s = new Scanner(System.in);

		do {

			System.out.println("Bem vindo a sua Biblioteca," + "\n***************************************************"
					+ "\n Digite 1 para Cadastrar O Livro..: " + "\n Digite 2 para Consultar os Livros..: "
					+ "\n Digite 3 para Sair do Programa" + "\n***************************************************\n");

			escolha = s.nextInt();
			switch (escolha) {
			case 1:
				s.nextLine();

				System.out.print("Digite o nome do livro..: ");
				nome = s.nextLine();

				System.out.print("Digite o nome do autor..: ");
				autor = s.nextLine();

				System.out.print("Digite o nome da editora..: ");
				editora = s.nextLine();

				System.out.println("Digite o n�mero de paginas...: ");
				paginas = s.nextInt();

				Biblioteca biblioteca = new Biblioteca(nome, autor, editora, paginas);
				b.add(biblioteca);

				try {
					BufferedWriter escrever = new BufferedWriter(new FileWriter(
							"C:\\Users\\Administrador\\Downloads\\CITI-EXPRESS\\JAVAIO\\livrosCadastrados.txt"));

					for (Biblioteca livros : b) {
						escrever.write(
								"\n___________________________________________________________________________________________\n\n"
										+ livros
										+ "\n___________________________________________________________________________________________\n");
					}
					escrever.close();
				} catch (IOException e) {
					e.printStackTrace();
				}

				break;
			case 2:
				s.nextLine();
				System.out.println("Digite o nome do livro a ser pesquisado..: ");
				pesquisaLivros = s.nextLine();
				if (buscaPeloNome(b, pesquisaLivros) == null)
					System.out.println("Livro n�o encontrado no Banco de Dados da biblioteca\n");
				else
					System.out.println(buscaPeloNome(b, pesquisaLivros));
				break;
			case 3:
				System.out.println("At� logo!!! Volte sempre");
				break;
			default:
				break;
			}
			System.out.println("Deseja continuar com o programa s para SIM OU n para N�O ");
			escolha2 = s.next();
		} while (escolha2.equalsIgnoreCase("s"));
		System.out.println(b);

	}

	public static Biblioteca buscaPeloNome(List<Biblioteca> b, String nome) {

		for (Biblioteca livros : b) {
			if (livros.getLivro().equals(nome)) {
				return livros;
			}
		}
		return null;
	}

}
