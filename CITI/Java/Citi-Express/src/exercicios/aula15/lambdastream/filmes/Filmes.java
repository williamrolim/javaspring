package exercicios.aula15.lambdastream.filmes;

import java.util.ArrayList;
import java.util.List;

import estudo.aula15.stream.Pessoa;

public class Filmes {

	private String titulo, direcao, genero, uf, duracaoEmMinutos,anoLancamento,classificacao,totalPublico,sinopse;

	public Filmes() {
		
	}

	public Filmes(String titulo, String direcao, String genero, String uf, String duracaoEmMinutos,
			String anoLancamento, String classificacao, String totalPublico, String sinopse) {
		this.titulo = titulo;
		this.direcao = direcao;
		this.genero = genero;
		this.uf = uf;
		this.duracaoEmMinutos = duracaoEmMinutos;
		this.anoLancamento = anoLancamento;
		this.classificacao = classificacao;
		this.totalPublico = totalPublico;
		this.sinopse = sinopse;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getDirecao() {
		return direcao;
	}

	public void setDirecao(String direcao) {
		this.direcao = direcao;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

	public String getDuracaoEmMinutos() {
		return duracaoEmMinutos;
	}

	public void setDuracaoEmMinutos(String duracaoEmMinutos) {
		this.duracaoEmMinutos = duracaoEmMinutos;
	}

	public String getAnoLancamento() {
		return anoLancamento;
	}

	public void setAnoLancamento(String anoLancamento) {
		this.anoLancamento = anoLancamento;
	}

	public String getClassificacao() {
		return classificacao;
	}

	public void setClassificacao(String classificacao) {
		this.classificacao = classificacao;
	}

	public String getTotalPublico() {
		return totalPublico;
	}

	public void setTotalPublico(String totalPublico) {
		this.totalPublico = totalPublico;
	}

	public String getSinopse() {
		return sinopse;
	}

	public void setSinopse(String sinopse) {
		this.sinopse = sinopse;
	}
	


	public List<Filmes>popularFilmes(){
		
		Filmes f = new Filmes(titulo, direcao, genero, uf, duracaoEmMinutos, anoLancamento, classificacao, totalPublico, sinopse);
		List<Filmes> listFilmes = new ArrayList<>();
		listFilmes.add(f);
		
		return listFilmes;
	
	}

	@Override
	public String toString() {
		return "Filmes \n[titulo=" + titulo + ", direcao=" + direcao + ", genero=" + genero + ", uf=" + uf
				+ ", duracaoEmMinutos=" + duracaoEmMinutos + ", anoLancamento=" + anoLancamento + ", classificacao="
				+ classificacao + ", totalPublico=" + totalPublico + ", sinopse=" + sinopse + "]";
	}
	

}
