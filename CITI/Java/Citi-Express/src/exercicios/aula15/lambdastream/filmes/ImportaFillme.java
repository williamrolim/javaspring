package exercicios.aula15.lambdastream.filmes;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ImportaFillme {
	List<Filmes> filmesLista = new ArrayList<>();
	public void listandoTodosFilmes() {

		try {
			BufferedReader leitura = new BufferedReader(
					new FileReader("C:\\Users\\Administrador\\Downloads\\\\CITI-EXPRESS\\JAVAIO\\filmes.txt"));
			String linha;

			while ((linha = leitura.readLine()) != null) {

				List<String> dadosTxt = new ArrayList<>(Arrays.asList(linha.split(";")));
				Filmes filmes2 = new Filmes();

				filmes2.setTitulo(dadosTxt.get(0));
				filmes2.setDirecao(dadosTxt.get(1));
				filmes2.setGenero(dadosTxt.get(2));
				filmes2.setUf(dadosTxt.get(3));
				filmes2.setDuracaoEmMinutos(dadosTxt.get(4));
				filmes2.setAnoLancamento(dadosTxt.get(5));
				filmes2.setClassificacao(dadosTxt.get(6));
				filmes2.setTotalPublico(dadosTxt.get(7));
				filmes2.setSinopse(dadosTxt.get(8));
				
				filmesLista.add(filmes2);
			}
			System.out.println("Lista filmes\n");
			filmesLista.forEach(s -> System.out.println(s));
			//System.out.println(filmesLista.toString());

			leitura.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public void listFilmeAno() {
		System.out.println("\nListar todos os Filmes do ano de 2021\n");
		Stream<Filmes> filtrando = filmesLista.stream().filter(p -> p.getAnoLancamento().equals("2021"));
		filtrando.forEach(System.out::println);
	}

	public void listarFilmeMaiorBilheteria() {
	System.out.println("\nListar Filme de maior bilheteria\n");
	filmesLista.stream().filter(f -> Integer.parseInt(f.getTotalPublico()) >= 4532).findAny()
	.ifPresent(t -> System.out.println("Filme com maior publico: " + t.getTitulo() +  " com publico de " + t.getTotalPublico()));
//	List<Filmes> resultado = filmesLista.stream()
//			.filter(c -> Integer.parseInt(c.getTotalPublico()) >= 4532)
//			.collect(Collectors.toList());
//	
//	System.out.println(resultado.toString());
//	
	}
	
	public void agruparPorNomeDiretor() {
		System.out.println("\nAgrupar por nome do Diretor\n");
		filmesLista.stream().sorted((p1, p2) -> p1.getDirecao().compareTo(p2.getDirecao()))
				.forEach(p -> System.out.println(p));
	}

	public void agruparPorCategoria() {
		System.out.println("\nAgrupar por Categoria\n");
		filmesLista.stream().sorted((p1, p2) -> p1.getGenero().compareTo(p2.getGenero()))
				.forEach(p -> System.out.println(p));
	}

	public void calculoMediaClassificacaoTodosFilmes() {
		System.out.println("\nListar Filme de maior bilheteria");
		int quantidade = filmesLista.size();
		Double somaTotal = filmesLista.stream().mapToDouble(i -> Double.parseDouble(i.getClassificacao())).sum()
				/ quantidade;
		System.out.println("Calculo media da classificacao com todos os filmes: " + somaTotal);
	}

	public void calculoMediaPublicoDeTodosFilmes() {
		System.out.println("\nListar Filme de maior bilheteria");
		int quantidade = filmesLista.size();
		Integer somaTotal = filmesLista.stream().mapToInt(i -> Integer.parseInt(i.getTotalPublico())).sum()
				/ quantidade;
		System.out.println("Calculo media Público de todos os filmes: " + somaTotal);
		
	}
}
