package exercicios.aula11.collections;

import java.util.LinkedList;
import java.util.List;

public class Cliente {

	private String nome; 
	private int idade;
	private String endereco;
	public Carros carros;
		
	private List <Cliente> cliente = new LinkedList<>();
	
	public Cliente () {
		
	}
	
public Cliente (Cliente cliente) {
		this.cliente = (List<Cliente>) cliente;
	}

	public Cliente(String nome, int idade, String endereco) {
		this.nome = nome;
		this.idade = idade;
		this.endereco = endereco;

	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getIdade() {
		return idade;
	}

	public void setIdade(int idade) {
		this.idade = idade;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public List<Cliente> getCliente() {
		return cliente;
	}

	public void setCliente(List<Cliente> cliente) {
		this.cliente = cliente;
	}
	
	
	public void adiciona (Cliente cliente, Carros carros) {

		carros.setCarros((List<Carros>) carros);
		this.cliente.add(cliente);
		
	}
	
	public void adiciona2 (Cliente cliente) {

		this.cliente.add(cliente);
		
	}
	
	@Override
	public String toString() {
		return "Cliente [nome=" + nome + ", idade=" + idade + ", endereco=" + endereco + ", cliente=" + cliente + "]";
	}
	
	
   
	
	
}
