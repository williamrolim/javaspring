package exercicios.aula11.collections;

import java.util.LinkedList;
import java.util.List;

public class Carros {
	private String nome;
	private String combustivel;
	private int Lugares;
	private int portas;
	private double preco;
	private int ano;
	private TabelaDePrecos tp;
	private int numeroVeiculos;
	
	
	private List <Carros> carros = new LinkedList<>();
   	
	public Carros() {
		
	}
	
	public Carros(String nome, String combustivel, int lugares, int portas, double preco, int ano) {
		this.nome = nome;
		this.combustivel = combustivel;
		Lugares = lugares;
		this.portas = portas;
		this.preco = preco;
		this.ano = ano;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCombustivel() {
		return combustivel;
	}

	public void setCombustivel(String combustivel) {
		this.combustivel = combustivel;
	}

	public int getLugares() {
		return Lugares;
	}

	public void setLugares(int lugares) {
		Lugares = lugares;
	}

	public int getPortas() {
		return portas;
	}

	public void setPortas(int portas) {
		this.portas = portas;
	}

	public double getPreco() {
		return preco;
	}

	public void setPreco(double preco) {
		this.preco = preco;
	}

	public int getAno() {
		return ano;
	}

	public void setAno(int ano) {
		this.ano = ano;
	}

	public List<Carros> getCarros() {
		
		return carros;
	}

	public void setCarros(List<Carros> carros) {
		this.carros = carros;
	}
	
	
	
	public TabelaDePrecos getTp() {
		return tp;
	}

	public void setTp(TabelaDePrecos tp) {
		this.tp = tp;
	}

	@Override
	public String toString() {
		return "Carros [nome=" + nome + ", combustivel=" + combustivel + ", Lugares=" + Lugares + ", portas=" + portas
				+ ", preco=" + preco + ", ano=" + ano + ", tp=" + tp + ", numeroVeiculos=" + numeroVeiculos
				+ ", carros=" + carros + "]";
	}




}
