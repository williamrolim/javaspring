package exercicios.aula9.abstracao.EX2;

public abstract class Pessoa {
	public boolean andar, falar;

	public boolean Andar(int idade) {
		if (idade > 1)
			return true;
		else
			return false;
	}
	
	public boolean Falar(int idade) {
		if (idade > 3)
			return true;
		else
			return false;
	}

	public boolean isAndar() {
		return andar;
	}

	public void setAndar(boolean andar) {
		this.andar = andar;
	}

	public boolean isFalar() {
		return falar;
	}

	public void setFalar(boolean falar) {
		this.falar = falar;
	}

}
