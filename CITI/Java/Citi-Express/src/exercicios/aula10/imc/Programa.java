package exercicios.aula10.imc;

import java.util.Scanner;

public class Programa {

	public static void main(String[] args) {
		regraDeNegocio();
	}
	
	public static void regraDeNegocio() {
		Paciente p = new Paciente();
		Scanner s = new Scanner(System.in);
		
		System.out.println("Nome..: ");
		String nome = s.nextLine();
		p.setNome(nome.toString());
		System.out.println("Sobrenome..: ");
		String sobrenome = s.nextLine();
		p.setSobrenome(sobrenome.toString());
		
		System.out.println("Idade..: ");
		int idade = s.nextInt();
		p.setIdade(idade);
		
	   IMC i = new IMC();
	   
	   System.out.println("Digite seu peso..: ");
	    double peso = s.nextDouble();
	    i.setPeso(peso);
	   
	   System.out.println("Digite Sua Altura..: ");
	   double altura = s.nextDouble();
	   i.setAltura(altura);
	   
	   System.out.println(p.toString() +  i.toString() + "\nValor do IMC..: " + i.calculoIMC(i.getPeso(), i.getAltura()) + "\n "
	   		+ "Condi��o do Paciente ..: " 
			   + i.CondicaoPaciente());
	   //substring(0,6).toLowerCase().concat(i.CondicaoPaciente().substring(2))

	}
}
