package exercicios.aula10.imc;

import java.text.DecimalFormat;

public class IMC {
	double peso = 0, altura = 0, imc = 0;

	public double getPeso() {
		return peso;
	}

	public void setPeso(double peso) {
		this.peso = peso;
	}

	public double getAltura() {
		
		return altura;
	}

	public void setAltura(double altura) {
		this.altura = altura;
	}
	
	public double getImc() {
		return imc;
	}

	public void setImc(double imc) {
		this.imc = imc;
	}

	@Override
	public String toString() {
		return "\nDados Paciente = [peso=" + peso + ", altura=" + altura + "]";
	}
	
	public Double calculoIMC(double peso, double altura) {
		this.imc = Math.round(peso / (altura * altura)) ;
		return imc;
	}
	
	
	public String CondicaoPaciente() {
		if (imc > 0 && imc < 18.5) {
			return CondicaoPacienteENUM.MAGREZA.toString();
		}else if (imc >= 18.5 && imc < 24.9) {
			return CondicaoPacienteENUM.NORMAL.toString();
		}else if (imc >= 24.9 && imc < 30) {
			return CondicaoPacienteENUM.SOBREPESO.toString();
		}else if (imc >= 30) {
			return CondicaoPacienteENUM.OBESIDADE.toString();
		}
		return null;
	}
	
	
	
}
