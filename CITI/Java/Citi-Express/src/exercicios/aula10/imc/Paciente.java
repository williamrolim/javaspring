package exercicios.aula10.imc;

public class Paciente {

	/*do usuario, NOME, SOBRENOME, IDADE ?*/
	String nome, sobrenome;
	int idade;
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getSobrenome() {
		return sobrenome;
	}
	public void setSobrenome(String sobrenome) {
		this.sobrenome = sobrenome;
	}

	public int getIdade() {
		return idade;
	}
	public void setIdade(int idade) {
		this.idade = idade;
	}
	
	@Override
	public String toString() {
		return "Paciente [nome=" + nome + ", sobrenome=" + sobrenome + ", idade=" + idade + "]";
	}
	
}
