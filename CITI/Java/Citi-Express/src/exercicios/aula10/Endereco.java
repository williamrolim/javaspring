package exercicios.aula10;

public class Endereco {
 
	String nome, sobrenome, cpf;
	boolean ativo;
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getSobrenome() {
		return sobrenome;
	}
	public void setSobrenome(String sobrenome) {
		this.sobrenome = sobrenome;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public boolean isAtivo() {
		return ativo;
	}
	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public int getIdade() {
		return idade;
	}
	public void setIdade(int idade) {
		this.idade = idade;
	}
	int idade;
	
	@Override
	public String toString() {
		return "Endereco [nome=" + nome + ", sobrenome=" + sobrenome + ", cpf=" + cpf + ", ativo=" + ativo + ", idade="
				+ idade + "]";
	}
	
	
	
}
