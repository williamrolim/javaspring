package exercicios.aula10;

public class Empregado {
	
	String tipoLogradouro, cidade, nome;
	int numero;
	public String getTipoLogradouro() {
		return tipoLogradouro;
	}
	public void setTipoLogradouro(String tipoLogradouro) {
		this.tipoLogradouro = tipoLogradouro;
	}
	public String getCidade() {
		return cidade;
	}
	public void setCidade(String cidade) {
		this.cidade = cidade;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getNumero() {
		return numero;
	}
	public void setNumero(int numero) {
		this.numero = numero;
	}
	
	@Override
	public String toString() {
		return "Empregado [tipoLogradouro=" + tipoLogradouro + ", cidade=" + cidade + ", nome=" + nome + ", numero="
				+ numero + "]";
	}
	
}
