package exercicios.aula13.biblioteca;

import java.util.LinkedList;
import java.util.List;

public class Biblioteca {

	private String livro,autor,editora;
	private int paginas;
	List<Biblioteca> b = new LinkedList<Biblioteca>();

	public Biblioteca(String livro, String autor, String editora, int paginas) {
		this.livro = livro;
		this.autor = autor;
		this.editora = editora;
		this.paginas = paginas;
	}

	public String getLivro() {
		return livro;
	}

	public void setLivro(String livro) {
		this.livro = livro;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public String getEditora() {
		return editora;
	}

	public void setEditora(String editora) {
		this.editora = editora;
	}

	public int getPaginas() {
		return paginas;
	}

	public void setPaginas(int paginas) {
		this.paginas = paginas;
	}

	
	public List<Biblioteca> getB() {
		return b;
	}

	public void setB(List<Biblioteca> b) {
		this.b = b;
	}

	@Override
	public String toString() {
		return "Biblioteca [livro=" + livro + ", autor=" + autor + ", editora=" + editora + ", paginas=" + paginas
				+ "]";
	}
	
	
	
}
