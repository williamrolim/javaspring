package exercicios.aula13.biblioteca;

import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class ExecutandoBiblioteca {
	public static void main(String[] args) {
		RegraNegocio();
	}

	static void RegraNegocio() {
		List<Biblioteca> b = new LinkedList<Biblioteca>();

		String nome, autor, editora, pesquisaLivros, escolha2;
		int paginas, escolha;

		Scanner s = new Scanner(System.in);

		do {

			System.out.println("Bem vindo a sua biblioteca," + "\n Digite 1 para novo livro"
					+ "\n Digite 2 para Consultar os livros" + "\n Digite 3 para sair");

			escolha = s.nextInt();
			switch (escolha) {
			case 1:
				s.nextLine();

				System.out.print("Digite o nome do livro..: ");
				nome = s.nextLine();

				System.out.print("Digite o nome do autor..: ");
				autor = s.nextLine();

				System.out.print("Digite o nome da editora..: ");
				editora = s.nextLine();

				System.out.println("Digite o n�mero de paginas...: ");
				paginas = s.nextInt();

				Biblioteca biblioteca = new Biblioteca(nome, autor, editora, paginas);
				b.add(biblioteca);
				break;
			case 2:
				s.nextLine();
				System.out.println("Digite o nome do livro a ser pesquisado..: ");
				pesquisaLivros = s.nextLine();
				
				System.out.println(buscaPeloNome(b, pesquisaLivros));
			case 3:
				break;
			default:
				break;
			}
			System.out.println("Deseja continuar com o programa s para SIM OU n para N�O ");
			escolha2 = s.next();
		} while (escolha2.equalsIgnoreCase("s"));
		System.out.println(b);

	}
	
	public static Biblioteca buscaPeloNome(List<Biblioteca> b, String nome) {

        for(Biblioteca livros : b) {
            if(livros.getLivro().equals(nome)) {
                return livros;
            }
        }
        return null;
    }
}
