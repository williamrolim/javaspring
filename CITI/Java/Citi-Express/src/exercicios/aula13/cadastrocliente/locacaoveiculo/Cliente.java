package exercicios.aula13.cadastrocliente.locacaoveiculo;

import java.util.LinkedList;
import java.util.List;

public class Cliente {	
 private int id, idade;
 private String nome;
 private double total;
 private List<Cliente> cliente = new LinkedList<Cliente>();
// private List<TabelaAluguelPlanos> planos = new LinkedList<TabelaAluguelPlanos>();
// private List<Veiculo> veiculos = new LinkedList<Veiculo>();

public Cliente() {
	
}
public Cliente(int id, String nome, int idade) {
	this.id = id;
	this.idade = idade;
	this.nome = nome;
}

public int getId() {
	return id;
}

public void setId(int id) {
	this.id = id;
}

public int getIdade() {
	return idade;
}

public void setIdade(int idade) {
	this.idade = idade;
}

public String getNome() {
	return nome;
}

public void setNome(String nome) {
	this.nome = nome;
}


public double getTotal() {
	return total;
}
public void setTotal(double total) {
	this.total += total;
}

public List<Cliente> getCliente() {
	return cliente;
}
public void setCliente(List<Cliente> cliente) {
	this.cliente = cliente;
}


@Override
public String toString() {
	return "Cliente [id=" + id + ", idade=" + idade + ", nome=" + nome + ", total=" + total + "]";
}



 
}
