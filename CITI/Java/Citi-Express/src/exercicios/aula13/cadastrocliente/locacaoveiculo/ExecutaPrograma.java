package exercicios.aula13.cadastrocliente.locacaoveiculo;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

import exercicios.aula13.biblioteca.Biblioteca;

public class ExecutaPrograma {

	List<TabelaAluguelPlanos> planos = new LinkedList<TabelaAluguelPlanos>();

	static Veiculo v = new Veiculo();
	static Cliente c = new Cliente();
	static List<Cliente> cliente = new LinkedList<Cliente>();
	static List<Veiculo> veiculo = new LinkedList<Veiculo>();
	static LinkedHashMap<Veiculo, Cliente> idParaCliente = new LinkedHashMap<Veiculo, Cliente>();

	static Scanner s = new Scanner(System.in);

	public static void main(String[] args) {
		ExecutandoRegra();
	}

	public static void ExecutandoRegra() {

		String escolhaCliente = "", escolha = "", escolhaVeiculo = "", simNao = "", simNao2 = "s", choice = "";
		String sairDoBasico = "", sairDoIntermediario = "", sairDoExecutivo = "", sairDoPremium = "";

		double soma = 0;
		int indice = 0;

		while (simNao2.equalsIgnoreCase("s")) {

			do {
				System.out.println("Digite seu identificador");
				int id = s.nextInt();

				System.out.println("Digite seu nome");
				String nome = s.next();

				System.out.println("Digite sua idade");
				int idade = s.nextInt();

				Cliente c = new Cliente(id + indice, nome + indice, idade + indice);

				cliente.add(c);
				indice++;

				do {
					System.out.println("Digite o plano ao qual quer alugar em maiusculo..: BASICO, INTERMEDIARIO,EXECUTIVO , PREMIUM  ");
					escolha = s.next();
					switch (escolha) {
					case "BASICO":
						do {
							System.out.println("Selecione o tipo de veiculo f para Fiat Uno para vw  para gol");
							escolhaVeiculo = s.next();

							switch (escolhaVeiculo) {
							case "f":
								int numero = 1;
								Veiculo v1 = new Veiculo("Fiat Uno", "Modelo way", "motor 1.0", "cor-preto", 130,
										TabelaAluguelPlanos.BASICO);
								c.setTotal(v1.getValor());
								veiculo.add(v1);
								v.setVeiculos(veiculo);
								idParaCliente.put(v1, c);
								break;
							case "vw":
								Veiculo v2 = new Veiculo("VW Gol", "Modelo G3", "motor 1.0", "cor branco", 150,
										TabelaAluguelPlanos.BASICO);
								c.setTotal(v2.getValor());
								veiculo.add(v2);
								v.setVeiculos(veiculo);
								idParaCliente.put(v2, c);
								break;
							default:
								break;
							}

							System.out.println("Voce deseja continuar com esse plano S para sim N para n�o ");
							sairDoBasico = s.next();
						} while (sairDoBasico.equalsIgnoreCase("s"));
						break;
					case "INTERMEDIARIO":
						do {
							System.out.println(
									"Intermediario : Selecione o tipo de veiculo jr Para Jeep Renagade ou rd para Renault Dust");
							escolhaVeiculo = s.next();

							switch (escolhaVeiculo) {
							case "jr":
								Veiculo jr = new Veiculo("Jeep Renagade", "Modelo std AT", "motor 1.8 ", "cor vermelho",
										350, TabelaAluguelPlanos.INTERMEDIARIO);
								c.setTotal(jr.getValor());
								veiculo.add(jr);
								v.setVeiculos(veiculo);
								idParaCliente.put(jr, c);
								break;
							case "rd":
								Veiculo rd = new Veiculo("Renault Duster", "Modelo Dynamique", "motor 1.6", "cor azul",
										350, TabelaAluguelPlanos.INTERMEDIARIO);
								c.setTotal(rd.getValor());
								veiculo.add(rd);
								v.setVeiculos(veiculo);
								idParaCliente.put(rd, c);
								break;
							default:
								break;

							}
							System.out
									.println("Voce deseja continuar com o plano Intermediario S para sim N para n�o ");
							sairDoIntermediario = s.next();
						} while (sairDoIntermediario.equals("s"));
						break;
					case "EXECUTIVO":
						do {
							System.out.println(
									"Executivo: Selecione o tipo de veiculo th para Toyota Hilux - gmt para GM TrailBlazer");
							escolhaVeiculo = s.next();

							switch (escolhaVeiculo) {
							case "th":
								int numero = 1;
								Veiculo th = new Veiculo("Toyota Hilux", "Modelo SW4", "motor 3.0", "cor: laranja",
										1500, TabelaAluguelPlanos.EXECUTIVO);
								c.setTotal(th.getValor());
								veiculo.add(th);
								v.setVeiculos(veiculo);
								idParaCliente.put(th, c);
								break;
							case "gmt":
								Veiculo gmt = new Veiculo("GM TrailBlazer", "Modelo LTZ", "motor 3.0", "cor: vermelha",
										1800, TabelaAluguelPlanos.EXECUTIVO);
								c.setTotal(gmt.getValor());
								veiculo.add(gmt);
								v.setVeiculos(veiculo);
								idParaCliente.put(gmt, c);

								break;
							default:
								break;
							}
							System.out
									.println("Voce deseja continuar com esse plano (EXECUTIVO) S para sim N para n�o ");
							sairDoExecutivo = s.next();
						} while (sairDoExecutivo.equalsIgnoreCase("s"));
						break;
					case "PREMIUM":
						do {
							System.out.println(
									"PREMIUM : Selecione o tipo de veiculo pc Para Porche Cayenne ou fm para Ford Mustang");
							escolhaVeiculo = s.next();

							switch (escolhaVeiculo) {
							case "pc":
								Veiculo pc = new Veiculo("Porche Cayenne", "Modelo turbo GT", " motor 4.0", "cor: azul",
										3500, TabelaAluguelPlanos.PREMIUM);
								c.setTotal(pc.getValor());
								veiculo.add(pc);
								v.setVeiculos(veiculo);
								idParaCliente.put(pc, c);
								break;
							case "fm":
								Veiculo fm = new Veiculo("Ford Mustang", "Modelo Mach 1", "motor 5.0", "cor:preta",
										4000, TabelaAluguelPlanos.PREMIUM);
								c.setTotal(fm.getValor());
								veiculo.add(fm);
								v.setVeiculos(veiculo);
								idParaCliente.put(fm, c);
								break;
							default:
								break;

							}
							System.out.println("Voce deseja continuar com o plano PREMIUM S para sim N para n�o ");
							sairDoPremium = s.next();
						} while (sairDoPremium.equals("s"));
					}
					System.out.println("Voce deseja alugar outro veiculo..: s para Sim N para N�o");
					simNao = s.next();
				} while (simNao.equalsIgnoreCase("s"));
				System.out.println("Voce deseja cadastrar outro cliente..: s para Sim N para N�o");
				simNao2 = s.next();
				// idParaCliente.put(c, v2);
			} while (simNao2.equalsIgnoreCase("s"));
		}
		c.getId();
		c.setTotal(soma);
		// System.out.println(c.toString() + " " + v.getVeiculos().toString());
		System.out.println(soma);

//		System.out.println(" teste " + idParaCliente);
//		System.out.println(" Teste 2 " + cliente);
		System.out.println(" Clientes cadastrados ->  " + cliente + " veiculos cadastrados " + veiculo);

		System.out.println("idpara clientes");
		System.out.println(idParaCliente);

		System.out.println("Voc� quer realizar a busca pelo cliente..:");
		choice = s.next();

		if (choice.equalsIgnoreCase("s"))
			buscaId();
		else
			simNao2 = "s";
	}

	@SuppressWarnings("unlikely-arg-type")
	static void buscaId() {
		
		System.out.println(idParaCliente.entrySet() + "\n");

	
		System.out.println("Digite o id do cliente que quer verificar ...::");
		int id = s.nextInt();
		System.out.println(idParaCliente.containsKey(id));
		
		
	}
	

}