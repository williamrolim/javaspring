package exercicios.aula13.cadastrocliente.locacaoveiculo;

import java.util.LinkedList;
import java.util.List;

public class Veiculo {
	private String nome, modelo, motor,cor;
	private double valor;
	private List<Veiculo> veiculos = new LinkedList<Veiculo>();


//	
//	 private List<TabelaAluguelPlanos> planos = new LinkedList<TabelaAluguelPlanos>();
//	 private List<Cliente> cliente = new LinkedList<Cliente>();
		
	TabelaAluguelPlanos ap = null;

	public Veiculo() {
		
	}
	
	public Veiculo(String nome, String modelo, String motor, String cor, double valor, TabelaAluguelPlanos ap) {
	super();	this.nome = nome;
	this.modelo = modelo;
	this.motor = motor;
	this.cor = cor;
	this.valor = valor;
	this.ap = ap;
}

	public String getNome() {
		return nome;
	}


	public void setNome(String nome) {
		this.nome = nome;
	}


	public String getModelo() {
		return modelo;
	}


	public void setModelo(String modelo) {
		this.modelo = modelo;
	}


	public String getMotor() {
		return motor;
	}


	public void setMotor(String motor) {
		this.motor = motor;
	}

	public String getCor() {
		return cor;
	}


	public void setCor(String cor) {
		this.cor = cor;
	}


	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}

	public TabelaAluguelPlanos getAp() {
		return ap;
	}

	public void setAp(TabelaAluguelPlanos ap) {
		this.ap = ap;
	}	

	public List<Veiculo> getVeiculos() {
		return veiculos;
	}

	public void setVeiculos(List<Veiculo> veiculos) {
		this.veiculos = veiculos;
	}
	

	@Override
	public String toString() {
		return "Veiculo [nome=" + nome + ", modelo=" + modelo + ", cor=" + cor + ", valor=" + valor + ", Plano=" + this.ap +   "]";
	}

	
}
