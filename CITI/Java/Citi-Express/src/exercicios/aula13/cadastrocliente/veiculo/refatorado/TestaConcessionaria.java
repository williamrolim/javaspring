package exercicios.aula13.cadastrocliente.veiculo.refatorado;

import java.util.HashMap;
import java.util.Map;

public class TestaConcessionaria {
public static void main(String[] args) {
	Cliente cliente1 = new Cliente (1, "William Rolim", 30, 400);
	Cliente cliente2 = new Cliente (2, "Rodrigo Rolim", 20, 300);
	Cliente cliente3 = new Cliente (3, "Maite Proen�a", 40, 500);
	
	Veiculo veiculo1 = new Veiculo (1, "Fiat Uno", "Modelo way", "motor 1.0", "cor-preto", 130, TabelaDePlanos.BASICO);
	Veiculo veiculo2 = new Veiculo(2,"VW Gol", "Modelo G3", "motor 1.0", "cor branco", 150, TabelaDePlanos.BASICO);
	Veiculo veiculo3 = new Veiculo(3,"Jeep Renagade", "Modelo std AT", "motor 1.8 ", "cor vermelho",350, TabelaDePlanos.INTERMEDIARIO);
	Veiculo veiculo4 = new Veiculo(4,"Renault Duster", "Modelo Dynamique", "motor 1.6", "cor azul",350, TabelaDePlanos.INTERMEDIARIO);
	Veiculo veiculo5 = new Veiculo(5,"Toyota Hilux", "Modelo SW4", "motor 3.0", "cor: laranja",1500, TabelaDePlanos.EXECUTIVO);
	Veiculo veiculo6 = new Veiculo(6,"GM TrailBlazer", "Modelo LTZ", "motor 3.0", "cor: vermelha",1800, TabelaDePlanos.EXECUTIVO);
	Veiculo veiculo7 = new Veiculo(7,"Porche Cayenne", "Modelo turbo GT", " motor 4.0", "cor: azul",3500, TabelaDePlanos.PREMIUM);
	Veiculo veiculo8 = new Veiculo(8,"Ford Mustang", "Modelo Mach 1", "motor 5.0", "cor:preta",4000, TabelaDePlanos.PREMIUM);
	
	Map<Veiculo, Cliente> mapa = new HashMap<>();
	mapa.put(veiculo7, cliente1);
	mapa.put(veiculo8, cliente1);
	mapa.put(veiculo1, cliente2);
	mapa.put(veiculo2, cliente2);
	mapa.put(veiculo3, cliente2);
	mapa.put(veiculo4, cliente3);
	mapa.put(veiculo4, cliente1);

	
	for(Map.Entry<Veiculo,Cliente> entry : mapa.entrySet()) {
		System.out.println(entry.getKey() + " " + entry.getValue());
	}
	
	
	
}
}
