package exercicios.aula13.cadastrocliente.veiculo.refatorado;

public class Cliente {
	 private int id, idade;
	 private String nome;
	 private double total;
	 
	public Cliente(int id, String nome, int idade, double total) {
		this.id = id;
		this.nome = nome;
		this.idade = idade;
		this.total = total;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getIdade() {
		return idade;
	}

	public void setIdade(int idade) {
		this.idade = idade;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	@Override
	public String toString() {
		return "Cliente [id=" + id + ", idade=" + idade + ", nome=" + nome + ", total=" + total + "]";
	}
	 
	 
}
