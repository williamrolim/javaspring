package exercicios.aula13.cadastrocliente.veiculo.refatorado;

public class Veiculo {
	private String nome, modelo, motor,cor;
	private double valor;
	private int id;
	TabelaDePlanos tap = null;

	public Veiculo(int id,String nome, String modelo, String motor, String cor, double valor, TabelaDePlanos tap) {
		this.id = id;
		this.nome = nome;
		this.modelo = modelo;
		this.motor = motor;
		this.cor = cor;
		this.valor = valor;
		this.tap = tap;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getMotor() {
		return motor;
	}

	public void setMotor(String motor) {
		this.motor = motor;
	}

	public String getCor() {
		return cor;
	}

	public void setCor(String cor) {
		this.cor = cor;
	}

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Veiculo [nome=" + nome + ", modelo=" + modelo + ", motor=" + motor + ", cor=" + cor + ", valor=" + valor
				+ ", id=" + id + "]";
	}
	
	
	
	
}
