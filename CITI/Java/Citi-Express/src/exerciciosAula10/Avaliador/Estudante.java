package exerciciosAula10.Avaliador;

import java.util.Arrays;

public class Estudante {
 int id, idade;
 String nome;
 double media;
double notas[];
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public int getIdade() {
	return idade;
}
public void setIdade(int idade) {
	this.idade = idade;
}
public String getNome() {
	return nome;
}
public void setNome(String nome) {
	this.nome = nome;
}
public double getMedia() {
	return media;
}
public void setMedia(double media) {
	this.media = media;
}


public double[] getNotas() {
	return notas;
}
public void setNotas(double[] notas) {
	this.notas = notas;
}
@Override
public String toString() {
	return "Estudante [id=" + id + ", idade=" + idade + ", nome=" + nome + ", media=" + media + ", notas="
			+ Arrays.toString(notas) + "]";
}




 
 
}
