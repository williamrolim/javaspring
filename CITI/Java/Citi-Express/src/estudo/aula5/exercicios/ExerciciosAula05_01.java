package estudo.aula5.exercicios;

import java.util.Scanner;

public class ExerciciosAula05_01 {
	public static void main(String[] args) {
		/*
		 * CADASTRAR ALUNO ENQUANTO �S� ou �s� SOLICITAR O NOME DO ALUNO E CRIAR UM
		 * ARRAY DE REAL QUE O USUARIO INFORME 4 NOTAS E ARMAZENE NO ARRAY , AO FINAL
		 * DEVE DIVIDIR O VALOR TOTAL DO ARRAY POR 4 PARA ENCONTRAR A MEDIA E INFORMAR O
		 * NOME E MEDIA DO ALUNO
		 */

		String nome, opcao = "";
		float notas[] = new float[4], media = 0;

		Scanner s = new Scanner(System.in);
		System.out.println("Deseja realizar o calculo de Notas  S para S, N para n�o");
		opcao = s.next();
		s.nextLine();
		while (opcao.equalsIgnoreCase("S")) {
			opcao = "";
			System.out.println("Digite o nome do aluno..: ");
			nome = s.next();
			s.nextLine();

			for (int i = 0; i < notas.length; i++) {
				System.out.println("Digite " + i +  " das 4 notas ..: ");
				notas[i] = s.nextFloat();
				media += notas[i];

			}
			
			media = media / 4;
			
			System.out.println("\nNome ..: " +  nome + "\nMedia do aluno..: " + media + "\n");
			
			System.out.println("Deseja realizar o calculo de Notas  S para S, N para n�o");
			opcao = s.next();
			s.nextLine();
		}
		System.out.println("GoodBye");
	}
}
