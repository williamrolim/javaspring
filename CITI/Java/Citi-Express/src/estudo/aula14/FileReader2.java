package estudo.aula14;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FileReader2 {
	public static void main(String[] args) {
		String nomeArquivo = "simples.txt";

		List<Estudante> estudantes = new ArrayList<>();
		try {

			BufferedReader bufferedReader = new BufferedReader(new FileReader(nomeArquivo));

			String linha;

			while ((linha = bufferedReader.readLine()) != null) {
				// Esse linha quebra em dados separado por ;
				List<String> dados = new ArrayList<>(Arrays.asList(linha.split(";")));

				Estudante estudante = new Estudante(linha, 0);
				estudante.setNome(dados.get(0));
				double valorTotal = 0.0;
				// Pega as notas e faz o calculo para m�dia
				for (int i = 1; i < dados.size(); i++) {
					valorTotal += Double.parseDouble(dados.get(i));
				}
				Double media = valorTotal / 4;
				estudante.setMedia(media);

				estudantes.add(estudante);
			}

			bufferedReader.close();

			System.out.printf("NOME     \t MEDIA\n");
			for (Estudante estudante : estudantes) {
				System.out.printf("%s     \t %.2f \n", estudante.getNome(), estudante.getMedia());
			}

		} catch (Exception e) {
			e.getStackTrace();
		}

	}
}
