package estudo.aula3.exercicios;

import java.util.Scanner;

public class exercicosAula3_9 {
	
	/*- Uma empresa quer verificar se um empregado est� qualificado para a
	aposentadoria ou n�o. Para estar em condi��es, um dos seguintes
	requisitos deve ser satisfeito:
	� Ter no m�nimo 65 anos de idade (Para homens ou outros).
	� Ter trabalhado no m�nimo 30 anos (Para homens ou outros).
	� Ter no m�nimo 60 anos e ter trabalhado no m�nimo 25 anos. (Para
	mulheres ou outros).
	 */
public static void main(String[] args) {
	int idade, anosTrabalhado;
	String sexo;
	
	Scanner sc = new Scanner(System.in);
	
	System.out.println("Digite sexo (m para mulher, h para homem, o outros).: ");
	sexo = sc.next();
	
	System.out.println("Digite idade Usuario.: ");
	idade = sc.nextInt();
	
	System.out.println("Digite anos Trabalhado.: ");
	anosTrabalhado = sc.nextInt();
	
	
	if ((sexo.equals("h") || sexo.equals("o")) && idade >= 65 && anosTrabalhado >= 30){
		System.out.println("Qualificado para Aposentar - Homem , Outros");
	}else if ((sexo.equals("m") || sexo.equals("o")) && idade >= 60 && anosTrabalhado >= 25){
		System.out.println("Qualificado para Aposentar - Mulher , Outros");

	}else {
		System.out.println("N�o qualificado");
	}
	sc.close();
}
}
