package estudo.aula3.exercicios;

import java.util.Scanner;

public class exercicosAula3_6 {

	/*6 - Fa�a Um Program Para Ler: Quantidade Atual Em Estoque, Quantidade
	M�xima Em Estoque E Quantidade M�nima Em Estoque De Um Produto.
	Calcular E Escrever A Quantidade M�dia ((Quantidade M�dia =
	Quantidade M�xima + Quantidade M�nima)/2). Se A Quantidade Em
	Estoque For Maior Ou Igual A Quantidade M�dia Escrever A Mensagem
	'N�o Efetuar Compra', Sen�o Escrever A Mensagem 'Efetuar Compra'.*/
	
	public static void main(String[] args) {
		int quantidadeAtualEstoque, quantidadeMaximaEstoque, quantidadeMinimaEstoque;
		
		float quantidadeMedia;
	
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Quantidade atual Estoque.: ");
		quantidadeAtualEstoque = sc.nextInt();
		
		System.out.println("Quantidade maxima Estoque.: ");
		quantidadeMaximaEstoque = sc.nextInt();
		
		System.out.println("Quantidade minima Estoque.: ");
		quantidadeMinimaEstoque = sc.nextInt();
		
		quantidadeMedia = (quantidadeMaximaEstoque + quantidadeMinimaEstoque)/2;
		
		System.out.println("Quantidade Media em estoque..: " + quantidadeMedia);
		if (quantidadeAtualEstoque >= quantidadeMedia) {
			System.out.println("N�o efetuar Compra");
		}else {
			System.out.println("Efetuar Compra");
		}
	}
}
