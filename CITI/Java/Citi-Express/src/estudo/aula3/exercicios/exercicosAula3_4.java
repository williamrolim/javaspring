package estudo.aula3.exercicios;

import java.util.Scanner;

public class exercicosAula3_4 {

	/*4 - Escreva Um Programa De Calculadora Basica:
	- Entre Com O Numero1
	- Entre Com O Operador ( / * + - )
	- Entre Com O Segundo Numero
	Usar O SWITCH
	Case "+"
	RESULTADO = NUMERO 1 + NUMERO 2
	Escrever Na Tela ( Numero 1," + ", Numero2, " = ", Resultado) , Assim Por Diante,
	Outro Caso Escrever "Operador Invalido*/
	
	public static void main(String[] args) {
		float numberOne, numberTwo;
		
		String operador = "";
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Digite o primeiro valor..: ");
		numberOne = sc.nextFloat();
		
		System.out.println("Digite o segundo valor..: ");
		numberTwo = sc.nextFloat();
		
		System.out.println("Digite o operador..: ");
		operador = sc.next();
		
		switch (operador) {
		case "*":
			System.out.println(numberOne * numberTwo);
			break;
		case "/":
			System.out.println(numberOne / numberTwo);
			break;
		case "+":
			System.out.println(numberOne + numberTwo);
			break;
		case "-":
			System.out.println(numberOne - numberTwo);
			break;
			

		default:
			break;
		}
	}
}
