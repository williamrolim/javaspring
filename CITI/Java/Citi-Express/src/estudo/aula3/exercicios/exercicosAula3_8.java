package estudo.aula3.exercicios;

import java.util.Scanner;

public class exercicosAula3_8 {

	/*Fa�a um Programa para ler um n�mero que � um c�digo de usu�rio.
	Caso este c�digo seja diferente de um c�digo armazenado internamente
	no algoritmo (igual a 1234) deve ser apresentada a mensagem �Usu�rio
	inv�lido!�. Caso o C�digo seja correto, deve ser lido outro valor que � a
	senha. Se esta senha estiver incorreta (a certa � 9999) deve ser mostrada
	a mensagem �senha incorreta�. Caso a senha esteja correta, deve ser
	mostrada a mensagem �Acesso permitido�.*/
	
	public static void main(String[] args) {
	
		int codigoUsuario = 0, senhaUsuario = 0;
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Digite o codigo Usuario.: ");
		codigoUsuario = sc.nextInt();
		
		System.out.println("Digite a senha.: ");
		senhaUsuario = sc.nextInt();
		
		if (codigoUsuario == 1234 && senhaUsuario == 9999 ) {
			System.out.println("Acesso Permitido ao usuario " + codigoUsuario);
		}else
			System.out.println("AcessoNegado - verifique usuario e senha");
	
}
}
