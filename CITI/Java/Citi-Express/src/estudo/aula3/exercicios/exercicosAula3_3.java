package estudo.aula3.exercicios;

import java.util.Scanner;

public class exercicosAula3_3 {

	/*3 - Crie Um Programa Temperatura De Uma Regi�o:
		Voc� Deve Entrada Com Os Dados: Entre Com A Regiao� E Outro Com �Entre Com A
		Temperatura� Regiao(sul, Sudeste, Centro-oeste, Norte, Nordeste), Temperatura
		Se O Clima Estiver Entre Maior Igual Que 0 At� 15 Deve Informar �Muito Frio Na
		Regiao �, Regiao, Se O Clima Estiver Entre 16 At� 20 Deve Informar �Frio Moderado Na
		Regiao �, Regiao, Se O Clima Estiver Entre
		21 At� 25 Deve Informar �Clima Moderado Na Regiao �, Regiao
		26 At� 35 Deve Informar �Clima Quente Na Regiao �, Regiao
		Acima de 35 graus Deve Informar �Clima Muito Quente Na Regiao �, Regiao*/
	
	
	public static void main(String[] args) {
		String regiao; 
		Float temperatura;
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Regi�o..: ");
		regiao = sc.next();
		
		System.out.println("Temperatura..: ");
		temperatura = sc.nextFloat();
		
		if (temperatura > 0 && temperatura <= 15) {
			System.out.println("Muito Frio Na Regi�o: " + regiao);
		}else if ( temperatura > 15 && temperatura <= 20){
			System.out.println("Frio Moderado na regi�o: " + regiao);
		}else if (temperatura > 20 && temperatura <= 25) {
			System.out.println("Clima Moderado na regi�o: " + regiao);
		}else if (temperatura > 25 && temperatura <=35) {
			System.out.println("Clima muito quente na regi�o: " + regiao);
		}else if (temperatura > 35) {
			System.out.println("Clima Muito Quente Na Regiao: " + regiao);
		}
		
	}
}
