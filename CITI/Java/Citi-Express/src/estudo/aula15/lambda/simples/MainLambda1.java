package estudo.aula15.lambda.simples;

public class MainLambda1 {
	public static void main(String[] args) {
		Lambda1 valor = (nome) -> "Eu estou falando algo com " + nome;

		System.out.println(valor.falar("Huelton"));
	}

}
