package estudo.aula15.lambda.simples;

public class MainLambda2 {

	public static void main(String[] args) {
		
		FuncaoMatematica ad1 = (a, b) -> a + b;
		
		System.out.println(ad1.calculo(10, 20));
		
		FuncaoMatematica ad2 = (a, b) -> a - b;
		
		System.out.println(ad2.calculo(10, 20));
		
	    FuncaoMatematica ad3 = (a, b) -> a * b;
		
		System.out.println(ad3.calculo(10, 20));
		
		FuncaoMatematica ad4 = (a,b) -> a / b;
		
		System.out.println(ad4.calculo(100, 50));

	}

}
