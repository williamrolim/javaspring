package estudo.aula15.lambda.intermediario;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MainLambda4 {

	public static void main(String[] args) {
		
		List<Produto> produtos = new ArrayList<>();
		
		produtos.add(new Produto(3, "HP Laptop", 25000.0));
		produtos.add(new Produto(2, "Teclado", 250.0));
		produtos.add(new Produto(1, "Mouse", 100.0));
		produtos.add(new Produto(4, "Cadeira", 2000.0));
		
		Collections.sort(produtos, (p1, p2) -> p1.getId().compareTo(p2.getId()));
		Collections.sort(produtos, (p1, p2) -> p1.getPreco().compareTo(p2.getPreco()));

		for (Produto produto : produtos) {
			System.out.println(produto.getNome() + " - "+ produto.getPreco());
		}

	}

}
