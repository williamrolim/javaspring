package estudo.aula15.lambda.intermediario;

import java.util.HashMap;
import java.util.Map;

public class MainLambda3 {

	public static void main(String[] args) {
		Map<String, Integer> salarios = new HashMap<>();
		
		salarios.put("Joao", 40000);
		salarios.put("Maria", 20000);
		salarios.put("Claudio", 8000);
		salarios.put("Fernanda", 10000);
		
		salarios.replaceAll( (nome, salario) -> 
		         nome.equals("Claudio") ? salario + 10000 : salario);
		
		for (String key : salarios.keySet())
              System.out.println(key + " - "+ salarios.get(key));
		
		System.out.println();
	}
}