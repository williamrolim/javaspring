package estudo.aula15.stream;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class MainStreamPessoas {

	public static void main(String[] args) {

		List<Pessoa> pessoas1 = new Pessoa().populaPessoas();

		Map<String, Integer> salarios = new HashMap<>();

		salarios.put("Joao", 40000);
		salarios.put("Maria", 20000);
		salarios.put("Claudio", 8000);
		salarios.put("Fernanda", 10000);

		List<Pessoa> stream1 = pessoas1.stream().filter(p -> p.getNacionalidade().equals("Brasil"))
				.collect(Collectors.toList());
		System.out.println(stream1);

		List<Pessoa> pessoas2 = new Pessoa().populaPessoas();
		List<Integer> stream2 = pessoas2.stream().filter(p -> p.getNacionalidade().equals("Argentino"))
				.map(Pessoa::getIdade).collect(Collectors.toList());

		System.out.println(stream2);

		List<Pessoa> pessoas3 = new Pessoa().populaPessoas();
		List<Pessoa> stream3 = pessoas3.stream().filter(p -> p.getNacionalidade().equals("Brasil"))
				.sorted(Comparator.comparing(Pessoa::getIdade)).collect(Collectors.toList());

		System.out.println(stream3);

		List<Pessoa> pessoas4 = new Pessoa().populaPessoas();
		List<Pessoa> stream4 = pessoas4.stream().distinct().collect(Collectors.toList());

		System.out.println(stream4);

		List<Pessoa> pessoas5 = new Pessoa().populaPessoas();
		double stream5 = pessoas5.stream().filter(p -> p.getNacionalidade().equals("Brasil"))
				.mapToInt(p -> p.getIdade()).average().getAsDouble();

		System.out.println(stream5);

		List<Pessoa> pessoas6 = new Pessoa().populaPessoas();
		List<String> stream6 = pessoas6.stream().filter(p -> p.getNacionalidade().equals("Brasil"))
				.filter(p -> p.getIdade() > 23).distinct().map(p -> p.getNome().toUpperCase())
				.collect(Collectors.toList());

		System.out.println(stream6);

		//Agrupa por idade
		List<Pessoa> pessoas7 = new Pessoa().populaPessoas();
		Map<Integer, List<Pessoa>> pessoaPorIdade = pessoas7.stream()
				.collect(Collectors.groupingBy(p -> p.getIdade()));
		pessoaPorIdade.forEach((idade, p) -> System.out.format("idade %s: %s\n", idade, p));
		

		System.out.println(stream6);
		
		// MAP
		List<Integer> sal = salarios.entrySet().stream().filter(x -> x.getKey().equals("Maria"))
				.map(Map.Entry::getValue).collect(Collectors.toList());

		sal.forEach(x -> System.out.println(x));

		Map<String, Integer> salMap = salarios.entrySet().stream()
				.filter( x -> x.getKey().equals("Maria")) // filtro
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

		System.out.println(salMap);

	}

}