package estudo.aula15.stream;

import java.util.ArrayList;
import java.util.List;

public class Pessoa {

	private String id;
	private String nome;
	private String nacionalidade;
	private int idade;
	
	public Pessoa() {
		
	}
	
	public Pessoa(String id, String nome, String nacionalidade, int idade) {
		this.id = id;
		this.nome = nome;
		this.nacionalidade = nacionalidade;
		this.idade = idade;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getNacionalidade() {
		return nacionalidade;
	}

	public void setNacionalidade(String nacionalidade) {
		this.nacionalidade = nacionalidade;
	}

	public int getIdade() {
		return idade;
	}

	public void setIdade(int idade) {
		this.idade = idade;
	}
	
	
	public List<Pessoa> populaPessoas() {
		Pessoa pessoa1 = new Pessoa("p1","Jose Carlos", "Brasil", 30);
		Pessoa pessoa2 = new Pessoa("p2","Balak", "Alemanha", 25);
		Pessoa pessoa3 = new Pessoa("p3","Di Maria", "Argentino", 34);
		Pessoa pessoa4 = new Pessoa("p4","Leonel Messi", "Argentino", 30);
		Pessoa pessoa5 = new Pessoa("p5","Neymar Junior", "Brasil", 28);
		Pessoa pessoa6 = new Pessoa("p5","Neymar Junior", "Brasil", 28);
		Pessoa pessoa7 = new Pessoa("p6","Hernandes", "Brasil", 20);
		Pessoa pessoa8 = new Pessoa("p7","Gabi gol", "Brasil", 23);
		
		List<Pessoa> listPessoas = new ArrayList<>();
		
		listPessoas.add(pessoa1);
		listPessoas.add(pessoa2);
		listPessoas.add(pessoa3);
		listPessoas.add(pessoa4);
		listPessoas.add(pessoa5);
		listPessoas.add(pessoa6);	
		listPessoas.add(pessoa7);
		listPessoas.add(pessoa8);
		
		return listPessoas;
		
	}

	@Override
	public String toString() {
		return "Pessoa [id=" + id + ", nome=" + nome + ", nacionalidade=" + nacionalidade + ", idade=" + idade + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pessoa other = (Pessoa) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
