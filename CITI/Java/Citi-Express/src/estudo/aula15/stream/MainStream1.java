package estudo.aula15.stream;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import estudo.aula15.lambda.intermediario.Produto;

public class MainStream1 {

	public static void main(String[] args) {
		
		List<Produto> celulares = new ArrayList<>();
		
		celulares.add(new Produto(1, "Samsung A5", 1700.0));
		celulares.add(new Produto(2, "Iphone 11Pro", 6500.0));
		celulares.add(new Produto(3, "Samsung S22", 7000.0));
		celulares.add(new Produto(4, "Iphone 13 ProMax", 16500.0));
		celulares.add(new Produto(5, "Xioami L431", 3500.0));
		celulares.add(new Produto(6, "Lenovo Vibe", 1900.0));
		
		Stream<Produto> filtro = celulares.stream().filter(p -> p.getPreco() > 6000.0);
		
		//filtro.forEach(p -> System.out.println(p.getNome()+ " - "+ p.getPreco()));

		filtro.forEach(System.out::println);
	}

}
