package estudo.aula4.exercicios;

public class exerciciosAula4_4 {
	/*
	 * UM INTEIRO � UM N�MERO PRIMO SE ELE FOR DIVIS�VEL SOMENTE POR 1 E POR ELE
	 * MESMO. ASSIM, 2, 3, 5 E 7 S�O PRIMOS , ENQUANTO 4, 6, 8 E 9 N�O S�O ESCREVA
	 * UM PROGRAMA (ALGOR�TMO) JAVA QUE USA UM LA�O FOR, WHILE OU DO...WHILE PARA
	 * CALCULAR E EXIBIR OS 20 PRIMEIROS N�MEROS PRIMOS.
	 */

	public static void main(String[] args) {
		int numeroPrimo = 0;
		for (int i = 2; i <= 20; i++) {
			for (int j = 2; j <= 20; j--) {
				if ( j % i == 0) {
					System.out.println("Primo..:" + i);
					
				}else {
				 System.out.println(i);
				}

			}
		}
	}
}
