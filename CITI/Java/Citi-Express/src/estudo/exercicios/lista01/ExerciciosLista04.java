package estudo.exercicios.lista01;

import java.util.Scanner;

public class ExerciciosLista04 {
/*- 4) Escrever um Programa que leia o nome de um vendedor, o seu sal�rio fixo e o total de vendas efetuadas por ele no
m�s (em dinheiro). Sabendo que este vendedor ganha 15% de comiss�o sobre suas vendas efetuadas, informar o seu
nome, o sal�rio fixo e sal�rio no final do m�s*/
	
	public static void main(String[] args) {
		String nome = "";
		double salario = 0, vendasV = 0, comissao = 0.15, tVE = 0;
		
		Scanner s = new Scanner(System.in);
		System.out.println("Nome do vendedor..: ");
		nome = s.next();
		s.nextLine();
		
		System.out.println("Salario Fixo..: " );
		salario = s.nextDouble();
		
		System.out.println("Total de vendas efetuada..: ");
		vendasV = s.nextDouble();
		
		tVE = (salario + vendasV) * comissao;
		tVE = tVE + salario;
		System.out.println("Nome..: " + nome + "\n" + 
						   "Salario Fixo..: " + salario + "\n" +
							"Salario com a sua comiss�o " + tVE);
		
	}
}
