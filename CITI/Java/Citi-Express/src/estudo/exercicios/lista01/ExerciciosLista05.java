package estudo.exercicios.lista01;

import java.util.Scanner;

public class ExerciciosLista05 {
	public static void main(String[] args) {
		/*
		 * 05) - Escrever um Programa que leia 10 alunos nome e matricula do aluno e as
		 * notas das 4 provas que ele obteve no semestre. No final informar matricula,o
		 * nome do aluno da media (PASSOU, REPROVADO) mais alta e matricula,o nome do
		 * aluno da media (PASSOU, REPROVADO) mais baixa da lista cadastrada.Calculo da
		 * m�dia (aritm�tica) e informar se ele passou(media >=7), ficou de
		 * recupera��o(media >=4 ou < 7) ou reprovou (media < 4),
		 */

		String[] nomeAlunos = new String[10];
		int[] matricula = new int[10];
		float notas4P[][] = new float[10][4];

		Scanner s = new Scanner(System.in);
		for (int i = 0; i < notas4P.length; i++) {
			System.out.println("Digite seu nome..: ");
			nomeAlunos[i] = s.next();
			s.nextLine();

			System.out.println("Digite sua Matricula..: ");
			matricula[i] = s.nextInt();

			for (int j = 0; j < notas4P[i].length; j++) {
				System.out.println("Entre com o valor da primeira nota da pos [" + i + " , " + j + "]");
				notas4P[i][j] = s.nextFloat();
			}
		}

		for (int i = 0; i < notas4P.length; i++) {
			float somatoria = 0;

			for (int j = 0; j < notas4P[i].length; j++) {
				somatoria += notas4P[i][j];

			}
			if ((somatoria / 4) >= 7) {
				System.out.println("Nome aluno..: " + nomeAlunos[i]);
				System.out.println("Matricula aluno..: " + matricula[i]);
				System.out.println("media..: " + i + "------ " + (somatoria / 4));
				System.out.println("Aprovado!!!Congratulations!!!");
				System.out.println("****************************************\n");
			} else if (((somatoria / 4) >= 4) && ((somatoria / 4) < 7)) {
				System.out.println("Nome aluno..: " + nomeAlunos[i]);
				System.out.println("Matricula aluno..: " + matricula[i]);
				System.out.println("media..: " + i + "------ " + (somatoria / 4));
				System.out.println("Recupera��o!!!Estude Mais!!!\n");
				System.out.println("****************************************\n");
			} else if ((somatoria / 4) < 4) {
				System.out.println("Nome aluno..: " + nomeAlunos[i]);
				System.out.println("Matricula aluno..: " + matricula[i]);
				System.out.println("media..: " + i + "------ " + (somatoria / 4));
				System.out.println("Reprovado!!!At� a proxima!!!\n");
				System.out.println("****************************************\n");

			}
		}

	}
}

//	 for(int i = 0; i < notas4P.length; i++) {
//		 for(int j = 0; j < notas4P[i].length; j ++) {
//			 System.out.println("Notas dos alunos " + notas4P[i][j] + " -- "	);	
//			 
//		
//		 }
//	 }
