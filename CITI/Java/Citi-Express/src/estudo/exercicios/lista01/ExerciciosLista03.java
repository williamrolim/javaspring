package estudo.exercicios.lista01;

import java.util.Scanner;

public class ExerciciosLista03 {
/*- Escrever um Programa para determinar o consumo m�dio de um autom�vel sendo fornecida a dist�ncia total
percorrida pelo autom�vel e o total de combust�vel gasto.*/
	
	/*Consumo m�dio = dist�ncia percorrida / quantidade de litros usada*/
	public static void main(String[] args) {
		double v1, v2, cM = 0;
		 
		 Scanner s = new Scanner(System.in);
		 System.out.println("Digite a distancia percorrida..: ");
		 v1 = s.nextInt();
		 
		 System.out.println("Quantidade de litros usada..: ");
		 v2 = s.nextInt();
		 
		 cM = v1 / v2;
		 
		 System.out.println("Total de combustivel gasto..: " + cM + " km/l");
	}
}
