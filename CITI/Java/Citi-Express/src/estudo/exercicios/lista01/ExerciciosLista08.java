package estudo.exercicios.lista01;

import java.util.Scanner;

public class ExerciciosLista08 {
public static void main(String[] args) {
	/*Elaborar um Programa que efetue a apresenta��o do valor da convers�o em real (R$)
	 *  de um valor lido em d�lar(US$). O Programa dever� solicitar o valor da cota��o do
	 *  d�lare tamb�m a quantidade de d�lares dispon�veis com o
	usu�rio. Use o do-while. */
	
	double dolar = 5.53d , real = 0;
	String condicao = "";
	
	Scanner s = new Scanner(System.in);

	do {
		
		System.out.println("Digite o valor em reais..: ");
		real = s.nextDouble();
		if (real <=0) {
			System.out.println("Digite um valor valido!!! maior que ZERO");
		}
		System.out.println("Convers�o real para Dolar.: R$ " + dolar * real);
		
		System.out.println("Deseja realizar a convers�o Real para Dolar S para Sim, N para n�o");
		condicao = s.next();
		s.nextLine();
	} while (condicao.toLowerCase().equals("s"));
	
	System.out.println("good bye");

}
}
