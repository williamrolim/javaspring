package estudo.exercicios.lista01;

import java.util.Scanner;

public class ExerciciosLista02 {
/*2)Escrever um Programa que receba dois valores n�mericos e ao final mostre a soma, subtra��o, multiplica��o e a
divis�o dos valores lidos.
*/
	public static void main(String[] args) {
		double v1, v2;
		 
		 Scanner s = new Scanner(System.in);
		 System.out.println("Digite o Primeiro n�mero..: ");
		 v1 = s.nextInt();
		 
		 System.out.println("Digite o Segundo n�mero..: ");
		 v2 = s.nextInt();
		 
		 System.out.println("Soma :" + (v1 + v2) + "\n" + 
				 			"Subtra��o : " + (v1 - v2) + "\n" +
				 			"Multiplica��o : " + (v1 * v2) + "\n" +
				 			"Divis�o : " + (v1 / v2) );
		 
	}
}
