package estudo.exercicios.lista01;

import java.util.Scanner;

public class ExerciciosLista01 {
/*01 - Escrever um Programa que receba dois valores n�mericos e exiba o maior entre os 2 numeros, use Scanner, validar
caso os numeros forem iguais tamb�m.*/
	
	public static void main(String[] args) {
	 int v1, v2;
	 
	 Scanner s = new Scanner(System.in);
	 System.out.println("Digite o Primeiro n�mero Inteiro..: ");
	 v1 = s.nextInt();
	 
	 System.out.println("Digite o Segundo n�mero Inteiro..: ");
	 v2 = s.nextInt();
	 
	 if (v1 > v2) {
		 System.out.println("Primeiro valor digitado � maior " + v1);
	 }else if (v1 < v2) {
		 System.out.println("Segundo valor digitado � maior " + v2);
	 }else { //v1 == v2 -- n�o coloquei condi��o aqui
		 System.out.println("N�meros iguais " + v1 + " e " + v2);
	 }

	}
}
