package exercicio.aula09.sabado.inferface.forma.geometrica;

public class Triangulo implements FormaGeometrica {
	double area, base, altura;

	public double getArea() {
		return area;
	}

	public void setArea(double area) {
		this.area = area;
	}

	public double getBase() {
		return base;
	}

	public void setBase(double base) {
		this.base = base;
	}

	public double getAltura() {
		return altura;
	}

	public void setAltura(double altura) {
		this.altura = altura;
	}

	@Override
	public String toString() {
		return "Triangulo [area=" + area + ", base=" + base + ", altura=" + altura + "]";
	}

	@Override
	public  Double trianguloLosanguloRetanguloPi(double base, double altura) {
		System.out.println("Valor da area do triangulo...: " + (area = base * altura) / 2);
		return area;				
	}

	@Override
	public Double quadrado(double l) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Double trapezio(double B, double b2, double a) {
		// TODO Auto-generated method stub
		return null;
	}



}
