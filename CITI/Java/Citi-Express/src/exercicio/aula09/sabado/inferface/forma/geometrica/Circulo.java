package exercicio.aula09.sabado.inferface.forma.geometrica;

public class Circulo implements FormaGeometrica {

	double constante, raio, area;
	
	
	public double getConstante() {
		return constante;
	}

	public void setConstante(double constante) {
		this.constante = constante;
	}

	public double getRaio() {
		return raio;
	}

	public void setRaio(double raio) {
		this.raio = raio;
	}

	
	public double getArea() {
		return area;
	}

	public void setArea(double area) {
		this.area = area;
	}

	
	@Override
	public String toString() {
		return "Circulo [Constante PI=" + constante + ", Raio=" + raio + ", area=" + area + "]";
	}

	@Override
	public Double trianguloLosanguloRetanguloPi(double b, double h) {
		System.out.println("Area do Circulo..: " + (area = Math.round((Math.PI * (b)) * (Math.pow(h,2)))));
		return null;
	}

	@Override
	public Double trapezio(double B, double b2, double a) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Double quadrado(double l) {
		// TODO Auto-generated method stub
		return null;
	}

}
