package exercicio.aula09.sabado.inferface.forma.geometrica;

import java.util.Scanner;

public class ExecutarPrograma {

	public static void main(String[] args) {

		CalculoDasFigurasGeomatricas();
	}

	public static void CalculoDasFigurasGeomatricas() {
		
		Triangulo t = new Triangulo();
		Quadrado q = new Quadrado();
		Losangulo losa = new Losangulo();
		Retangulo r = new Retangulo();
		Trapezio tra = new Trapezio();
		Circulo c = new Circulo();

		FormaGeometrica fg = new Triangulo();
		FormaGeometrica fg1 = new Quadrado();
		FormaGeometrica fg2 = new Losangulo();
		FormaGeometrica fg3 = new Retangulo();
		FormaGeometrica fg4 = new Trapezio();
		FormaGeometrica fg5 = new Circulo();

		Scanner s = new Scanner(System.in);
		String escolha = "", choice = "";
		
		do {

			System.out.println("Digite a figura gemetrica que quer calcular..: ");
			escolha = s.nextLine();

			switch (escolha) {

			case "triangulo":
				System.out.println("Digite o valor da base..: ");
				double b = s.nextDouble();
				t.setBase(b);

				System.out.println("Digite o valor da altura..: ");
				double h = s.nextDouble();
				t.setAltura(h);
				fg.trianguloLosanguloRetanguloPi(b, h);
				break;
				
			case "quadrado":
				System.out.println("Digite o valor do lado..: ");
				double l = s.nextDouble();
				q.setLado(l);
				fg1.quadrado(l);
				break;
				
			case "losango":
				System.out.println("Digite o valor da diagonal Maior.: ");
				double D = s.nextDouble();
				losa.setDiagonalMaior(D);

				System.out.println("Digite o valor da diagonal Menor..: ");
				double d = s.nextDouble();
				losa.setDiagonalMenor(d);
				fg2.trianguloLosanguloRetanguloPi(D, d);
				break;

			case "retangulo":
				System.out.println("Digite o valor da base.: ");
				double ba = s.nextDouble();
				r.setBase(ba);

				System.out.println("Digite o valor da altura..: ");
				double al = s.nextDouble();
				r.setAltura(al);
				fg3.trianguloLosanguloRetanguloPi(ba, al);
				break;
				
			case "trapezio":
				System.out.println("Digite o valor da base Maior.: ");
				double bM = s.nextDouble();
				tra.setB(bM);

				System.out.println("Digite o valor da Base Menor..: ");
				double bm = s.nextDouble();
				tra.setB1(bm);

				System.out.println("Digite o valor da Altura..: ");
				double alt = s.nextDouble();
				tra.setA(alt);
				fg4.trapezio(bM, bm, alt);
				break;

			case "circulo":
				System.out.println("Digite o valor da constante PI.: ");
				double pi = s.nextDouble();
				c.setConstante(pi);
				
				System.out.println("Digite o valor do raio..: ");
				double raio = s.nextDouble();
				c.setRaio(raio);

				fg5.trianguloLosanguloRetanguloPi(pi, raio);
				break;
				default:
					System.out.println("Error: Op��o errada. \nDigite \ntriangulo, \nquadrado, \nlosango, \nretangulo,\ntrapezio,\ncirculo");
					break;
			}
			System.out.println("Voc� deseja calcular outra figura geometrica (S) para (Sim), (N) para (N�o)");
			choice = s.next();
			s.nextLine();
		}while (choice.equalsIgnoreCase("s"));
		
		if (choice.equals("n") || choice.equals ("")){
			System.out.println("Good Bye!!!! See you next time");
		}
	
	}

}
