package exercicio.aula09.sabado.inferface.forma.geometrica;

public class Retangulo implements FormaGeometrica {

	double area, base, altura;

	public double getArea() {
		return area;
	}

	public void setArea(double area) {
		this.area = area;
	}

	public double getBase() {
		return base;
	}

	public void setBase(double base) {
		this.base = base;
	}

	public double getAltura() {
		return altura;
	}

	public void setAltura(double altura) {
		this.altura = altura;
	}

	
	@Override
	public String toString() {
		return "Retangulo [Area=" + area + ", Base=" + base + ", Altura=" + altura + "]";
	}

	@Override
	public Double trianguloLosanguloRetanguloPi(double b, double h) {
	System.out.println("Valor da area do retangulo...: " + (area = b * h));
		return area;
	
	}

	@Override
	public Double quadrado(double l) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Double trapezio(double B, double b2, double a) {
		// TODO Auto-generated method stub
		return null;
	}
	
	
}
