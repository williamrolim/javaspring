package exercicio.aula09.sabado.inferface.forma.geometrica;

public interface FormaGeometrica {		
	public Double trianguloLosanguloRetanguloPi (double b, double h);
	public Double trapezio (double B, double b, double a);
	public Double quadrado (double l);
}
