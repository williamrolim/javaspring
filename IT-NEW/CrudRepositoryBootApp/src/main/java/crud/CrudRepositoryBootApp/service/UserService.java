package crud.CrudRepositoryBootApp.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import crud.CrudRepositoryBootApp.entity.User;
import crud.CrudRepositoryBootApp.repository.UserRepository;

@Service
public class UserService {
	@Autowired
	UserRepository userRepository;

	public Collection<? extends User> getAllUsers() {
		List<User> userList = new ArrayList<User>();
		userRepository.findAll().forEach(userList::add);
		return userList;
	}
	

	public Collection<? extends User> getUserById(String userId) {
		List<User> userList = new ArrayList<User>();
		userList.add(userRepository.findById(userId).get());
		return userList;
	}


	public Collection<? extends User> getAllUsersByIds(String ids) {
		List<User> userList = new ArrayList<User>();
		//split para separar ids com "," e formatar a lista dos ids
		List<String> idList = Arrays.asList(ids.split(","));
		userRepository.findAllById(idList).forEach(userList::add);
		return userList;
	}
	
	public String saveUser(User user) {
		User savedUser = userRepository.save(user);
		if(savedUser != null) {
			return "Saved : userId = " + savedUser.getUserid();
		}else {
			return "Failed : userId - " + user.getUserid();
		}
	}


	public String saveUserList(List<User> userList) {
		List<User> savedUserList = new ArrayList<User>();
		userRepository.saveAll(userList).forEach(savedUserList::add);
		return "Saved : users ids " + savedUserList.stream().map(u->u.getUserid()).collect(Collectors.toList());
	}


	public String deleteUser(User user) {
		userRepository.delete(user);
		return "Deletado com sucesso" ;
	}

	
	public String deleteUserById(String userId) {
		
		try {
			userRepository.deleteById(userId);
			return "Deletado pelo id: " + userId + " com sucesso!!!";
		} catch (Exception e) {
			return "Usuario com o id: " + userId + " não existe";
		}
	}


	public String deleteAllUsersList(List<User> userList) {
		userRepository.deleteAll(userList);
		return "Todos os usuarios Deletados com sucesso";
	}


	public String deleteAllUsers() {
		userRepository.deleteAll();		
		return "Todos os usuarios deletados";
	}







}
