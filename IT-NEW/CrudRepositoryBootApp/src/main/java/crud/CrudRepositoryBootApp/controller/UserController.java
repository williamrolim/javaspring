package crud.CrudRepositoryBootApp.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import crud.CrudRepositoryBootApp.entity.User;
import crud.CrudRepositoryBootApp.service.UserService;

@RestController
@RequestMapping(path="/user")
public class UserController {

	@Autowired
	UserService userService;
	/**
	 * Traz todos os usuarios cadastrados no banco de dados
	 * @return userList
	 */
	@GetMapping(path = "/getAllUsers")
	public List<User> getAllUser(){
		List <User> userList = new ArrayList<User>();
		userList.addAll(userService.getAllUsers());
		
		return userList;
	}
	
	/**
	 * Busca de usuario atravez do id fornecido
	 * @param userId
	 * @return userList
	 */
	@GetMapping(path="/getUserById/{userid}")
	public List<User> getUserById(@PathVariable(name = "userId")String userId){
		List <User> userList = new ArrayList<User>();
		userList.addAll(userService.getUserById(userId));
		
		return userList;
	}
	
	/**
	 * Buscar varios ids separando por virgula Ex..: 1234,1532,153
	 * @param ids
	 * @return userList;
	 */
	@GetMapping(path="/getAllUsersByIds/{userid}")
	public List<User> getAllUserByIds(@PathVariable(name = "userid")String ids){
		List <User> userList = new ArrayList<>();
		userList.addAll(userService.getAllUsersByIds(ids));
		
		return userList;
	}
	/**
	 * Salvar um Unico usuario no banco de dados
	 * @param user
	 * @return userService.saveUser(user); 
	 */
	@PostMapping(path="/saveUser")
	public String saveUser(@RequestBody User user) {
		return userService.saveUser(user); 
	}
	
	/**
	 * Salvar diveros usuarios de uma vez no banco de dados
	 * @param userList
	 * @return userService.saveUserList(userList);
	 */
	@PostMapping(path="/saveUserList")
	public String saveUserList(@RequestBody List<User> userList) {
		return userService.saveUserList(userList);
	}
	
	/**
	 * 	exclui a entidade fornecida.
		se a entidade estiver disponível, está excluindo
		se a entidade não estiver disponível ela está inserinda
		e, em seguida, excluir a mesma entidade;
	 * @param user
	 * @return userService.deleteUser(user);
	 */
	
	@DeleteMapping(path="/deleteUser")
	public String deleteUser(@RequestBody User user) {
		return userService.deleteUser(user);
	}
	
	/**
	 * exclui a entidade com o id fornecido.
	   primeiro irá verificar se a entidade está disponível ou não;
	   se disponível ele vai deletar
       se não estiver disponível lança exceção
	 * @param userId
	 * @return userService.deleteUserById(userId);
	 */

	@DeleteMapping(path="/deleteUserById/{userId}")
	public String deleteUserById (@PathVariable(name="userId") String userId) {
		return userService.deleteUserById(userId);
	}
	
	/**
	 * isso exclui a coleção da entidade.
	  verificará se as entidades disponíveis ou não
		se estiver disponível vai deletar
		se não estiver disponível ,exibira a mensagem
		de deletado mesmo assim
	 * @param user
	 * @return
	 */
	@DeleteMapping(path="/deleteUserList")
	public String deleteAllUsersList (@RequestBody List<User> userList) {
		return userService.deleteAllUsersList(userList);
	}
	
	/**
	 * Deleta todas as entidades gerenciadas pelo
	 * repositorio
	 *	 @return
	 */
	@DeleteMapping(path="/deleteAllUsers")
	public String deleteAllUsers() {
		return userService.deleteAllUsers();
	}
}
