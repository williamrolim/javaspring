package crud.CrudRepositoryBootApp.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import crud.CrudRepositoryBootApp.entity.User;

@Repository
public interface UserRepository extends CrudRepository<User, String>  {

}
