package crud.CrudRepositoryBootApp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrudRepositoryBootAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrudRepositoryBootAppApplication.class, args);
	}

}
