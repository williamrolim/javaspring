package locadoraJPA.teste;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import locadoraJPA.model.Marca;

public class MarcaTeste {
public static void main(String[] args) {
	EntityManagerFactory emf = Persistence.createEntityManagerFactory("locadora");
	
	EntityManager em = emf.createEntityManager();
	
	EntityTransaction et  = em.getTransaction();
//	EntityManager em = JpaUTIL.getEntityManagerFactory();

	Marca marca = new Marca();
	em.persist(marca);
	marca.setDescricao("Ford");
	et.commit();
}
}
