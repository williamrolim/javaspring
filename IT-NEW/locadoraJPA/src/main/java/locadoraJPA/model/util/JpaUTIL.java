package locadoraJPA.model.util;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JpaUTIL {
	private static final EntityManagerFactory FACTORY = Persistence
			.createEntityManagerFactory("locadora");
	
	public static EntityManager getEntityManagerFactory() {
		return FACTORY.createEntityManager();
	}
	
}
