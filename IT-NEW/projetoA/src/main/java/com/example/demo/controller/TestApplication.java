package com.example.demo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

public class TestApplication {

	@GetMapping
	public String hello() {
		return "Hello World";
	}
	
	@GetMapping("/{nome}")
	public String hello(@PathVariable String nome) {
		return "Ola Bem Vindo " + nome;
	}
}
