package com.example.demo.controller;

import java.net.URI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.example.demo.model.Cliente;
import com.example.demo.model.Endereco;
import com.example.demo.service.ClienteService;

@RestController
@RequestMapping("/api/v1/clientes")
public class ClienteController {
	
	@Autowired
	private ClienteService clienteService;
	/*UriComponentsBuilder com métodos de fábrica estáticos adicionais para criar links com base no HttpServletRequest atual.
	 * .fromCurrentRequest() Prepare um construtor copiando o esquema, host, porta, caminho e string de consulta de um HttpServletRequest.
	 * patch() Anexar ao caminho deste construtor.
		O valor fornecido é anexado como está aos path valores anteriores sem inserir nenhuma barra adicional
		path- o caminho do URI
		buildAndExpand Crie uma UriComponentsinstância e substitua as variáveis ​​de modelo de URI pelos 
		valores de um mapa. Este é um método de atalho que combina chamadas para build()e depois UriComponents.expand(Map).*/
		
	@PostMapping
	public ResponseEntity<Cliente>criaCliente(@RequestBody Cliente cliente){
		Cliente cli = clienteService.create(cliente);
		URI location = ServletUriComponentsBuilder
				.fromCurrentRequest()
				.path("/{id}")
				.buildAndExpand(cli.getId())
				.toUri();
		return ResponseEntity.created(location).build();
		
		/*ServletUriComponentsBuilder = UriComponentsBuilder com métodos de fábrica estáticos adicionais para criar links com base no HttpServletRequest atual*/
	}
	
	@GetMapping
	public ResponseEntity<Page<Cliente>> retornaTodosClientes(Pageable pageable){
		Page<Cliente> list = clienteService.getAll(pageable);
		return ResponseEntity.ok().body(list);
	}
	
//	@GetMapping("/filter")
//	public ResponseEntity<Cliente>retornaClientePorNomeESobrenome(@RequestParam String nome,@RequestParam String sobreNome){		
//		Cliente cli = clienteService.findByNomeAndSobreNome(nome, sobreNome);
//		return ResponseEntity.ok().body(cli);		
//	}
	
	@GetMapping("{id}")
	public ResponseEntity<Cliente>retornaClientePorId(@PathVariable Long id){		
		Cliente cli = clienteService.getById(id);
		return ResponseEntity.ok().body(cli);		
	}
	
	@PostMapping("/{id}/enderecos")//o id será do cliente 
	public ResponseEntity<Cliente> criarEndereco(@RequestBody Endereco endereco,@PathVariable Long id){
		Cliente cli = clienteService.createEnderecoInCliente(endereco, id);//buscar o cliente, e inserir os dados do endereço no cliente
		URI location = ServletUriComponentsBuilder
				.fromCurrentRequest()
				.path("/{id}")
				.buildAndExpand(cli.getId())
				.toUri();
		return ResponseEntity.created(location).build();
	}
}
