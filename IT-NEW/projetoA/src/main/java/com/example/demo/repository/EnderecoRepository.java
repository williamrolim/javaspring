package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.demo.enums.TipoStatus;
import com.example.demo.model.Endereco;

public interface EnderecoRepository extends JpaRepository<Endereco, Long> {
	@Query("SELECT COUNT(e) FROM Endereco e Where e.cliente.id=?1 and e.status=?2")
	Integer countClienteIdAndStatus(Long id, TipoStatus status);
}
