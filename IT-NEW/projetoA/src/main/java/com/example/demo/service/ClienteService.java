package com.example.demo.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.enums.TipoStatus;
import com.example.demo.model.Cliente;
import com.example.demo.model.Endereco;
import com.example.demo.repository.ClienteRepository;
import com.example.demo.repository.EnderecoRepository;

@Service
public class ClienteService {

	@Autowired
	private ClienteRepository clienteRepository;

	@Autowired
	private EnderecoRepository enderecoRepository;

	@Transactional
	public Cliente create(Cliente cliente) {
		Cliente cli = clienteRepository.save(cliente);
		return cli;
	}

	@Transactional(readOnly = true)
	public Page<Cliente> getAll(Pageable pageable) {
		Page<Cliente> listCliente = clienteRepository.findAll(pageable);
		return listCliente;
	}

	@Transactional(readOnly = true)
	public Cliente getById(Long id) {
		Optional<Cliente> cli = clienteRepository.findById(id);
		cli.orElseThrow(() -> new RuntimeException("Cliente não encontrado"));
		return cli.get();
	}
	
	//Buscando por nome e sobrenome
//	@Transactional(readOnly = true)
//	public Cliente findByNomeAndSobreNome(String nome, String sobreNome) {
//		Optional<Cliente> cli = clienteRepository.findByNomeSobrenome(nome, sobreNome);
//		cli.orElseThrow(() -> new RuntimeException("Cliente não encontrado"));
//		return cli.get();	
//	}


	@Transactional
	public Cliente createEnderecoInCliente(Endereco endereco, Long id) {
		List<Endereco> listEndereco = new ArrayList<>();

		Optional<Cliente> cli = clienteRepository.findById(id);//pegamos o cliente id, pelo que ele está no momento
		cli.orElseThrow(() -> new RuntimeException("Cliente não encontrado"));//caso não achar o cliente
		
//		Integer enderecoAtivo = enderecoRepository.countClienteIdAndStatus(id, TipoStatus.ATIVO);
//		if(enderecoAtivo > 1) {
//			throw new RuntimeException("Não pode existir mais de um endereço ativo");
//		}
		
		endereco.setCliente(cli.get());
		listEndereco.add(endereco);
		
		cli.get().setEnderecos(listEndereco);
		
		Cliente cliPersist  = clienteRepository.save(cli.get());
		
		return cliPersist;

	}

}
