  _____                       _                           ___       ___     ___   
 |_   _|    ___      ___     | |_      ___       o O O   /   \     | _ \   |_ _|  
   | |     / -_)    (_-<     |  _|    / -_)     o        | - |     |  _/    | |   
  _|_|_    \___|    /__/_    _\__|    \___|    TS__[O]   |_|_|    _|_|_    |___|  
_|"""""| _|"""""| _|"""""| _|"""""| _|"""""|  {======| _|"""""| _| """ | _|"""""| 
"`-0-0-' "`-0-0-' "`-0-0-' "`-0-0-' "`-0-0-' ./o--000' "`-0-0-' "`-0-0-' "`-0-0-' 
${application.title} ${application.version}
Powered by Spring Boot ${spring-boot.version}

https://devops.datenkollektiv.de/banner.txt/index.html