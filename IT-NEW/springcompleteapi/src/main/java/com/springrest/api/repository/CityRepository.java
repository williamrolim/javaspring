package com.springrest.api.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.springrest.api.model.City;

@Repository
public interface CityRepository extends CrudRepository<City, Long>{

}
