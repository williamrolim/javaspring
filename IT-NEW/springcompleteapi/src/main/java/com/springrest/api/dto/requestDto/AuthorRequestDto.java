package com.springrest.api.dto.requestDto;

import lombok.Data;

@Data
public class AuthorRequestDto {
	private String name;
	private Long zipcodeId;
	
}
