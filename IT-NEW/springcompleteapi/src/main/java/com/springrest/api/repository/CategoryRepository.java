package com.springrest.api.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.springrest.api.model.Category;
@Repository
public interface CategoryRepository extends CrudRepository<Category, Long> {

}
