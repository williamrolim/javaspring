package com.springrest.api.dto.requestDto;

import lombok.Data;

@Data
public class ZipcodeRequestDto {
	private String name;
	private Long cityId;
}
