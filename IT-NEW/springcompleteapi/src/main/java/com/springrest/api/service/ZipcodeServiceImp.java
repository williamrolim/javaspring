package com.springrest.api.service;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;

import com.springrest.api.dto.requestDto.ZipcodeRequestDto;
import com.springrest.api.model.City;
import com.springrest.api.model.Zipcode;
import com.springrest.api.repository.ZipcodeRepository;

public class ZipcodeServiceImp implements ZipcodeService{

	@Autowired
	ZipcodeRepository zipcodeRepository;
	
	@Autowired
	CityService cityService;
	
	//private final ZipcodeRepository zipcodeRepository;
//	  private final CityService  cityService;
//	
//	@Autowired
//	public ZipcodeServiceImp(ZipcodeRepository zipcodeRepository, CityService cityService) {
//		this.zipcodeRepository = zipcodeRepository;
//		this.cityService = cityService;
//	}
	
	@Transactional
	@Override
	public Zipcode addZipcode(ZipcodeRequestDto zipcodeRequestDto) {
		Zipcode zipcode = new Zipcode();
		zipcode.setName(zipcodeRequestDto.getName());//Verificando que no codigo postal(zipcode) tem uma cidade
		if (zipcodeRequestDto.getCityId() == null) {//se não tiver nenhum codigo postal ele grava , caso já existir devolve o objeto
			return zipcodeRepository.save(zipcode);
		}
		
		City city = cityService.getCity(zipcodeRequestDto.getCityId());//obtendo a cidade -- se isso não for valido gerará uma exessão
		zipcode.setCity(city);
		return zipcodeRepository.save(zipcode);
	}

	@Override
	public List<Zipcode> getZipcodes() {
		//isso retornara um fluxo 
		
		//Percorrera todos os codigos postais e os dividirá em partes individuais e retornara a lista e trazer os ceps
		return StreamSupport
        .stream(zipcodeRepository.findAll().spliterator(), false)
        .collect(Collectors.toList());

	}

    @Override
    public Zipcode getZipcode(Long zipcodeId) {
        return zipcodeRepository.findById(zipcodeId).orElseThrow(() ->
                new IllegalArgumentException(
                        "zipcode with id: " + zipcodeId + " could not be found"));
    }

    @Override
    public Zipcode deleteZipcode(Long zipcodeId) {
        Zipcode zipcode = getZipcode(zipcodeId);
        zipcodeRepository.delete(zipcode);
        return zipcode;
    }

    @Transactional
    @Override
    public Zipcode editZipcode(Long zipcodeId, ZipcodeRequestDto zipcodeRequestDto) {
        Zipcode zipcodeToEdit = getZipcode(zipcodeId);
        zipcodeToEdit.setName(zipcodeRequestDto.getName());
        if (zipcodeRequestDto.getCityId() != null) {
            return zipcodeToEdit;
        }
        City city = cityService.getCity(zipcodeRequestDto.getCityId());
        zipcodeToEdit.setCity(city);
        return zipcodeToEdit;
    }

    @Transactional //METODOS DE REVISÃO
    @Override
    public Zipcode addCityToZipcode(Long zipcodeId, Long cityId) {
        Zipcode zipcode = getZipcode(zipcodeId);
        City city = cityService.getCity(cityId);
        if (Objects.nonNull(zipcode.getCity())) {//VERIFICANDO QUE O OBJETO NÃO ESTÁ
            throw new IllegalArgumentException("zipcode already has a city");
        }
        zipcode.setCity(city);
        return zipcode;
    }

    @Transactional
    @Override
    public Zipcode removeCityFromZipcode(Long zipcodeId) {
        Zipcode zipcode = getZipcode(zipcodeId);
        if (!Objects.nonNull(zipcode.getCity())) {//Se o codigo postal não tiver em uma cidade eu não vou poder remove-la
            throw new IllegalArgumentException("zipcode does not have a city");
        }
        zipcode.setCity(null);
        return zipcode;
    }


}
