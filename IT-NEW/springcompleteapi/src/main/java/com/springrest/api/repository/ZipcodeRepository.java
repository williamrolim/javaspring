package com.springrest.api.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.springrest.api.model.Zipcode;
@Repository
public interface ZipcodeRepository extends CrudRepository<Zipcode, Long> {

}
