package com.springrest.api.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@Table(name="Author")
public class Author {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String name;
	
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "zipcode_id")
	private Zipcode zipcode;
	
	@ManyToMany(mappedBy = "categories" ,cascade = CascadeType.ALL,fetch = FetchType.LAZY)
	private List<Book> books = new ArrayList<>();

	public Author(String name, Zipcode zipcode, List<Book> books) {
		this.name = name;
		this.zipcode = zipcode;
		this.books = books;
	}
	
	
	public void addBook(Book book) {//ADICINAR LIVRO
		books.add(book);
	}
	
	public void removeBook(Book book) {//REMOVER LIVRO
		books.remove(book);
	}
	
	
}
