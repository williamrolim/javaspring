package com.springrest.api.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.springrest.api.dto.requestDto.CityRequestDto;
import com.springrest.api.model.City;

@Service
public interface CityService {
	public City addCity(CityRequestDto cityRequestDto);
	public List<City> getCities(); //retorna uma lista de cidades
	public City getCity(Long cityId);
	public City deleteCity (Long cityId);
	public City editCity (Long cityId,CityRequestDto cityRequestDto);
}
