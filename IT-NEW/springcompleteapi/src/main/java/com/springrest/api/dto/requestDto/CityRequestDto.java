package com.springrest.api.dto.requestDto;

import lombok.Data;

@Data
public class CityRequestDto {
	private String name;
}
