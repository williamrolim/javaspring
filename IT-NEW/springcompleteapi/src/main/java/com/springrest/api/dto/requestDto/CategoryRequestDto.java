package com.springrest.api.dto.requestDto;

import lombok.Data;

@Data
public class CategoryRequestDto {
	private String name;
}
