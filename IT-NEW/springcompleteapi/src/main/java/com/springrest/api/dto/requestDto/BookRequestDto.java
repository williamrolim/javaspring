package com.springrest.api.dto.requestDto;

import java.util.List;

import lombok.Data;

@Data
public class BookRequestDto {
	private String name;
	private List<Long> authorsIds;
	private Long categoryId;
}
