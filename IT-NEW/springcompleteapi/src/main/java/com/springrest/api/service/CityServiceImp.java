package com.springrest.api.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springrest.api.dto.requestDto.CityRequestDto;
import com.springrest.api.model.City;
import com.springrest.api.repository.CityRepository;

@Service
public class CityServiceImp implements CityService {

//	private final CityRepository cityRepository;
//	
//	@Autowired
//	public CityServiceImp(CityRepository cityRepository) {
//		this.cityRepository = cityRepository;
//	}

	@Autowired
	CityRepository cityRepository;

	@Override
	public City addCity(CityRequestDto cityRequestDto) {
		City city = new City(); // criando uma nova cidade
		city.setName(cityRequestDto.getName());

		return cityRepository.save(city);
	}

	@Override
	public List<City> getCities() {
		List<City> cities = new ArrayList<>();// Criando uma lista de cidades
		//irá percorrer todas as cidades encontrandoas
		//e adicionando na lista que retornar a lista populada de cidades
		cityRepository.findAll().forEach(cities::add);
		return cities;
	}

	@Override
	public City getCity(Long cityId) {
		return cityRepository.findById(cityId).orElseThrow(() ->
		new IllegalArgumentException("city with cityId " + cityId + " could not be find"));
	}

	@Override
	public City deleteCity(Long cityId) {
		City city = getCity(cityId); //Obtendo a cidade que vc quer deletar
		cityRepository.delete(city);
		return city;
	}

	@Transactional
	@Override
	public City editCity(Long cityId, CityRequestDto cityRequestDto) {
		// Primeiro vc precisa obter a cidade que quer alterar/editar
		City cityToEdit = getCity(cityId);
		cityToEdit.setName(cityRequestDto.getName());
		return cityRepository.save(cityToEdit);
	}

}
