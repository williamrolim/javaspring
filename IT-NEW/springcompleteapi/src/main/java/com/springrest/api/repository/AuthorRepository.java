package com.springrest.api.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.springrest.api.model.Author;

@Repository
public interface AuthorRepository extends CrudRepository<Author, Long> {

}
