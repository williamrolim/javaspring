package com.springrest.api.service;

import java.util.List;

import com.springrest.api.dto.requestDto.CategoryRequestDto;
import com.springrest.api.dto.responseDto.CategoryResponseDto;
import com.springrest.api.model.Category;

public interface CategoryService {
    public Category getCategory(Long categoryId);
    public CategoryResponseDto addCategory(CategoryRequestDto categoryRequestDto);
    public CategoryResponseDto getCategoryById(Long categoryId);
    public List<CategoryResponseDto> getCategories();
    public CategoryResponseDto deleteCategory(Long categoryId);
    public CategoryResponseDto editCategory(Long categoryId, CategoryRequestDto categoryRequestDto);
}
