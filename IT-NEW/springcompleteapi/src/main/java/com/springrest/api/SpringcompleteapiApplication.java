package com.springrest.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringcompleteapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringcompleteapiApplication.class, args);
	}

}
