package com.springrest.api.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springrest.api.dto.mapper;
import com.springrest.api.dto.requestDto.BookRequestDto;
import com.springrest.api.dto.responseDto.BookResponseDto;
import com.springrest.api.model.Author;
import com.springrest.api.model.Book;
import com.springrest.api.model.Category;
import com.springrest.api.repository.BookRepository;

@Service
public class BookServiceImp implements BookService {

//    private final BookRepository bookRepository;
//    private final AuthorService authorService;
//    private final CategoryService categoryService;
//
//    @Autowired
//    public BookServiceImpl(BookRepository bookRepository, AuthorService authorService, CategoryService categoryService) {
//        this.bookRepository = bookRepository;
//        this.authorService = authorService;
//        this.categoryService = categoryService;
//    }
	@Autowired
    BookRepository bookRepository;
	@Autowired
    AuthorService authorService;
	@Autowired
    CategoryService categoryService;


    @Transactional
    @Override
    public BookResponseDto addBook(BookRequestDto bookRequestDto) {
        Book book = new Book();
        book.setName(bookRequestDto.getName());// Populando o book
        if (bookRequestDto.getAuthorsIds().isEmpty()) {//verificando se bookdto tem autor
            throw new IllegalArgumentException("you need atleast on author");
        } else {
            List<Author> authors = new ArrayList();//um livro pode ter varios autores , então criamos uma lista
            for (Long authorId: bookRequestDto.getAuthorsIds()) {//se não tiver author lança exception
                Author author = authorService.getAuthor(authorId);//verificando os registros dos livros criados pelo autor
                authors.add(author);//adiciona os livros
            }
            book.setAuthors(authors);//definará os livros para os autores dessa lista
        }
        if (bookRequestDto.getCategoryId() == null) {// verificando se o livro tem uma categoria
            throw new IllegalArgumentException("book atleast on category");//caso não tenha o livro precisa de uma categoria
        }
        Category category = categoryService.getCategory(bookRequestDto.getCategoryId());//Pegando a categoria do livro
        book.setCategory(category);//adicionando a categoria no livro

        Book book1 = bookRepository.save(book); // agora salvando o livro
        return mapper.bookToBookResponseDto(book1);
    }

    @Override
    public BookResponseDto getBookById(Long bookId) {
        Book book = getBook(bookId);
        return mapper.bookToBookResponseDto(book);
    }

    @Override
    public Book getBook(Long bookId) {
        Book book = bookRepository.findById(bookId).orElseThrow(() ->
                new IllegalArgumentException("cannot find book with id: " + bookId));
        return book;
    }

    @Override
    public List<BookResponseDto> getBooks() {
        List<Book> books = StreamSupport  //divida os resultados e não permita paralelo a falso
                .stream(bookRepository.findAll().spliterator(), false)////divida os resultados e não permita paralelo a falso
                .collect(Collectors.toList());// e colete a lista
        return mapper.booksToBookResponseDtos(books);
    }

    @Override
    public BookResponseDto deleteBook(Long bookId) {
        Book book = getBook(bookId);
        bookRepository.delete(book);
        return mapper.bookToBookResponseDto(book);
    }

    @Transactional
    @Override
    public BookResponseDto editBook(Long bookId, BookRequestDto bookRequestDto) {
        Book bookToEdit = getBook(bookId);
        bookToEdit.setName(bookRequestDto.getName());
        if (!bookRequestDto.getAuthorsIds().isEmpty()){//vamos ver que a lista dos authores NÃO esta vazia
            List<Author> authors = new ArrayList<>();
            for (Long authorId: bookRequestDto.getAuthorsIds()) {
                Author author = authorService.getAuthor(authorId);
                authors.add(author);
            }
            bookToEdit.setAuthors(authors);
        }
        if (bookRequestDto.getCategoryId() != null) {
            Category category = categoryService.getCategory(bookRequestDto.getCategoryId());
            bookToEdit.setCategory(category);
        }
        return mapper.bookToBookResponseDto(bookToEdit);
    }

    @Override
    public BookResponseDto addAuthorToBook(Long bookId, Long authorId) {
        Book book = getBook(bookId);//obtendo o livro
        Author author = authorService.getAuthor(authorId);//obtendo o autor
        if (author.getBooks().contains(author)) {//este autor já está atribuído a este livro
            throw new IllegalArgumentException("this author is already assigned to this book");
        }
        book.addAuthor(author); //relações bidirecionais, precisamos adicionar ambos (book e author
        author.addBook(book);
        return mapper.bookToBookResponseDto(book);
    }

    @Override
    public BookResponseDto deleteAuthorFromBook(Long bookId, Long authorId) {
        Book book = getBook(bookId);
        Author author = authorService.getAuthor(authorId);
        if (!(author.getBooks().contains(book))){
            throw new IllegalArgumentException("book does not have this author");
        }
        author.removeBook(book);
        book.removeAuthor(author);
        return mapper.bookToBookResponseDto(book);
    }

    @Override
    public BookResponseDto addCategoryToBook(Long bookId, Long categoryId) {
        Book book = getBook(bookId);
        Category category = categoryService.getCategory(categoryId);
        if (Objects.nonNull(book.getCategory())){//Se o objeto não tiver um nivel nulo obtenha a categoria
            throw new IllegalArgumentException("book already has a catogory");
        }
        book.setCategory(category); //SE O LIVRO NÃO TIVER CATEGORIA PRECISAMOS COLOCAR ALGUMA
        category.addBook(book);
        return mapper.bookToBookResponseDto(book);
    }

    @Override
    public BookResponseDto removeCategoryFromBook(Long bookId, Long categoryId) {
        Book book = getBook(bookId);
        Category category = categoryService.getCategory(categoryId);
        if (!(Objects.nonNull(book.getCategory()))){//verificando que não está vazia a categoira do livro
            throw new IllegalArgumentException("book does not have a category to delete");
        }
        book.setCategory(null);
        category.removeBook(book);
        return mapper.bookToBookResponseDto(book);
    }
}
