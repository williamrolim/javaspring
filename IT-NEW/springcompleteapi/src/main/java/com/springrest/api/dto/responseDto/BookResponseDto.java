package com.springrest.api.dto.responseDto;

import java.util.List;

import lombok.Data;

@Data
public class BookResponseDto {
	private Long id;
	private String name;
	private List<String> authorNames;//retornaremos apenas o nome dos authorNames
	private String categorName;//retornaremos apenas os nomes das categorias
}
