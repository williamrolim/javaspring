package com.springrest.api.dto;

import java.util.ArrayList;
import java.util.List;

import com.springrest.api.dto.responseDto.AuthorResponseDto;
import com.springrest.api.dto.responseDto.BookResponseDto;
import com.springrest.api.dto.responseDto.CategoryResponseDto;
import com.springrest.api.model.Author;
import com.springrest.api.model.Book;
import com.springrest.api.model.Category;

//Mapas para as respostas dos DTOS
public class mapper {

	public static BookResponseDto bookToBookResponseDto(Book book) {
		BookResponseDto bookResponseDto = new BookResponseDto();
		bookResponseDto.setId(book.getId());
		bookResponseDto.setCategorName(book.getCategory().getName());
		List<String> names = new ArrayList<>();
		List<Author> authors = book.getAuthors();

		for (Author author : authors) {// preenchera a lista de nomes para que podemos adicionar ao bookDTO
			names.add(author.getName());
		}

		bookResponseDto.setAuthorNames(names);
		return bookResponseDto;
	}

	// Retornar uma lista de resposta Dto -- author (confirmar)
	public static List<BookResponseDto> booksToBookResponseDtos(List<Book> books) {

		List<BookResponseDto> booksResponseDtos = new ArrayList<>();
		for (Book book : books) {
			booksResponseDtos.add(bookToBookResponseDto(book));
		}
		return booksResponseDtos;
	}

	public static AuthorResponseDto authorToAuthorResponseDto(Author author) {
		AuthorResponseDto authorResponseDto = new AuthorResponseDto();
		authorResponseDto.setId(author.getId());
		authorResponseDto.setName(author.getName());
		List<String> names = new ArrayList<>();
		List<Book> books = author.getBooks();//Para livro pegamos pegar seus nomes e colocar em uma lista de nomes 
		for (Book book : books) {//para cada livro precisamos pegar os nomes dos livros
			names.add(book.getName());//colocando o nome da lista do nomes
		}
		//ASSIM que todos os nomes dos livros foram adicionados , adicionaremos esses nomes ao atorResponse
		authorResponseDto.setBookNames(names);// 
		return authorResponseDto;
}
	
	public static List<AuthorResponseDto> authorToAuthorResponseDtos(List<Author> authors) {

		List<AuthorResponseDto> authorResponseDtos = new ArrayList<>();
		for (Author author : authors) {
			authorResponseDtos.add(authorToAuthorResponseDto(author));
		}
		return authorResponseDtos;
	}
	
	public static CategoryResponseDto categoryToCategoryResponseDto(Category category) {
		CategoryResponseDto categoryResponseDto = new CategoryResponseDto();
		categoryResponseDto.setId(category.getId());
		categoryResponseDto.setName(category.getName());
		List<String> names = new ArrayList<String>();
		List<Book> books = category.getBooks();//temos que percorrer esses livros e adicionar a lista de nomes
		for(Book book : books) {
			names.add(book.getName());
		}
		//agora temos que definir esses nomes de livro para a categoria
		categoryResponseDto.setBookNames(names);
		return categoryResponseDto;
		
	}
	
	public static List<CategoryResponseDto> categoriesToCaregoryResponseDtos(List<Category> categories) {

		List<CategoryResponseDto> categoryResponseDto = new ArrayList<>();
		for (Category category : categories) {
			categoryResponseDto.add(categoryToCategoryResponseDto(category));
		}
		return categoryResponseDto;
	}


	
	
}