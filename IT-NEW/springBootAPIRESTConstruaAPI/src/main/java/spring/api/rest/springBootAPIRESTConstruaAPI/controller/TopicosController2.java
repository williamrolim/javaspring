package spring.api.rest.springBootAPIRESTConstruaAPI.controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import spring.api.rest.springBootAPIRESTConstruaAPI.controller.dto.TopicoDTO;
import spring.api.rest.springBootAPIRESTConstruaAPI.model.Curso;
import spring.api.rest.springBootAPIRESTConstruaAPI.model.Topico;

@RestController// JÁ ASSUME por padrão que todo metodo vai ter o response body
public class TopicosController2 {
	
	@RequestMapping("/topicos2")
	public List <TopicoDTO>lista(){
		//Spring usa uma biblioteca chamada Jackson , o jackson que faz a conversão de java para json. 
		//O spring usa o Jackson por baixo dos panos, 
		Topico topico = new Topico("Duvida", "Duvida com Spring", new Curso("Spring","programação"));
		//Converter de topico para DTO
		return  TopicoDTO.converter(Arrays.asList(topico,topico,topico));//varios objetos separados por virgula e ele tranforma em uma lista e devolve uma lista com todos os objetos 
	}
}
