package spring.api.rest.springBootAPIRESTConstruaAPI.model;

public enum StatusTopico {

	NAO_RESPONDIDO,
	NAO_SOLUCIONADO,
	SOLUCIONADO,
	FECHADO;
}
