package it.ws.io.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import it.ws.share.dto.UserDTO;

//Essa entidade é o objeto pai

@Entity(name="users")
public class UserEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue
	private long id;
	
	@Column(nullable = false)
	private String userId;
	
	@Column(nullable = false , length = 50)
	private String firstName;
	
	@Column(nullable = false , length = 50)
	private String lastName;
	
	@Column(nullable = false , length = 100)
	private String email;

	@Column(nullable = false)
	private String encryptedPassword;
	
	private String emailVerificationToken;
	
	@Column(nullable = true, columnDefinition = "boolean default false" )
	private Boolean emailVerificationStatus;
	
	/**
	 * Como a entidade usuario contém a lista de endereços podemos dizer que o usuario
	 * possui o endereço.
	 * OBS: (mappedBy = "userDetails") Se não oferecemos o nome correto do relacionamento
	 * bidirecional não funcionara.
	 * Se queremos que os endereços sejam persistidos quando o usuario for persistido
	 * temos que fornececer o cascade = CascadeType.ALL = se 
	 */
	@OneToMany(mappedBy = "userDetails", cascade = CascadeType.ALL) //userDetails que faz referencia UserDTO userDetails;    
	private List<AddressEntity> addresses;//Um usuario pode ter centenas de endereços
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEncryptedPassword() {
		return encryptedPassword;
	}

	public void setEncryptedPassword(String encryptedPassword) {
		this.encryptedPassword = encryptedPassword;
	}

	public String getEmailVerificationToken() {
		return emailVerificationToken;
	}

	public void setEmailVerificationToken(String emailVerificationToken) {
		this.emailVerificationToken = emailVerificationToken;
	}

	public Boolean getEmailVerificationStatus() {
		return emailVerificationStatus;
	}

	public void setEmailVerificationStatus(Boolean emailVerificationStatus) {
		this.emailVerificationStatus = emailVerificationStatus;
	}

	public List<AddressEntity> getAddresses() {
		return addresses;
	}

	public void setAddresses(List<AddressEntity> addresses) {
		this.addresses = addresses;
	}

}
