package it.ws.io.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import it.ws.share.dto.UserDTO;

@Entity(name="addresses")
public class AddressEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	private long id;
	
	@Column(length = 30, nullable = false)// nullable = false esse valor não pode ser vazio
	public String addressId;
	
	@Column(length = 15, nullable = false)
	private String city;
	
	@Column(length = 15, nullable = false)
	private String country;

	@Column(length = 60, nullable = false)
	private String streetName;
	
	@Column(length = 7, nullable = false)
	private String postalCode;
	
	@Column(length = 10, nullable = false)
	private String type;
	
	@ManyToOne //Muitos usuarios podem ter varios endereços
	@JoinColumn(name="users_id")//Foreign key == chave estrangeira user = nome da tabela id é o id primay key da tabela users
	private UserEntity userDetails;//estamos criando um relacionamento bidirecional

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getAddressId() {
		return addressId;
	}

	public void setAddressId(String addressId) {
		this.addressId = addressId;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getStreetName() {
		return streetName;
	}

	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public UserEntity getUserDetails() {
		return userDetails;
	}

	public void setUserDetails(UserEntity userDetails) {
		this.userDetails = userDetails;
	}

}
