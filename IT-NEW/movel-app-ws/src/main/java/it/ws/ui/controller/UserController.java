package it.ws.ui.controller;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import it.ws.exceptions.UserServiceException;
import it.ws.service.UserService;
import it.ws.share.dto.UserDTO;
import it.ws.ui.model.request.UserDetailsRequestModel;
import it.ws.ui.model.response.ErrorMessages;
import it.ws.ui.model.response.OperationStatusMode;
import it.ws.ui.model.response.RequestOperationName;
import it.ws.ui.model.response.RequestOperationStatus;
import it.ws.ui.model.response.UserRest;

@RestController
@RequestMapping("users")// http://localhost:8090/users/movel-app-ws, outro app só mudar o nome do contexto ex: http://localhost:8090/users/shoe-store
public class UserController {
	
	@Autowired
	UserService userService;
	
	@GetMapping(path ="/{id}",
			produces = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public UserRest getUser(@PathVariable String id) {//
		UserRest returnValue = new UserRest();
		
		UserDTO userDto = userService.getUserByUserId(id);
		BeanUtils.copyProperties(userDto, returnValue);//copia as propriedades  do userDto para returnValue
		return returnValue;//"get usuário foi chamado";  return returnValue;
	}
	
//	@PostMapping(consumes = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE },//pode consumir json & xml format
//				 produces = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })//pode responder json & xml format
//	public UserRest createUser(@RequestBody UserDetailsRequestModel userDetails) throws Exception {
//		UserRest returnValue = new UserRest();
//		
//		/*Pode alterar os comentarios para verificar o tratamento de excessão espefica
//		 * ou o outro metodo generalista (captura todas as excessoes)
//		 */
//		if (userDetails.getFirstName().isEmpty()) throw new UserServiceException(ErrorMessages.MISSING_REQUIRED_FIELD.getErrorMessage());
//		//if (userDetails.getFirstName().isEmpty()) throw new NullPointerException("this object is null");
//
//		UserDTO userDTO = new UserDTO();
//		/**
//		 *BeanUtils.copyProperties(userDetails, userDTO); 
//		 * copiando informações do userDetails para userDto			
//		 */
//		BeanUtils.copyProperties(userDetails, userDTO);
//		
//		UserDTO createUser = userService.createUser(userDTO);//retorna dados do usuario do objeto
//		BeanUtils.copyProperties(createUser, returnValue);//pega o objeto do createUser e copia para o retorno
//		return returnValue;
//	}
	
	
	@PostMapping(consumes = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE },//pode consumir json & xml format
			 produces = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })//pode responder json & xml format
	public UserRest createUser(@RequestBody UserDetailsRequestModel userDetails) throws Exception {
	UserRest returnValue = new UserRest();
	
	/*Pode alterar os comentarios para verificar o tratamento de excessão espefica
	 * ou o outro metodo generalista (captura todas as excessoes)
	 */
		if (userDetails.getFirstName().isEmpty()) throw new UserServiceException(ErrorMessages.MISSING_REQUIRED_FIELD.getErrorMessage());

		ModelMapper modelMapper = new ModelMapper();
		UserDTO userDTO = modelMapper.map(userDetails, UserDTO.class);
		
		UserDTO createUser = userService.createUser(userDTO);
		returnValue = modelMapper.map(createUser, UserRest.class);//source, destinationSource
		return returnValue;
	}
	
	@PutMapping(path ="/{id}",
			 consumes = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE },//pode consumir json & xml format
			 produces = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })//pode responder json & xml format
	public UserRest updateUser(@PathVariable String id,@RequestBody UserDetailsRequestModel userDetails) {
		
		UserRest returnValue = new UserRest();
		
		/*Pode alterar os comentarios para verificar o tratamento de excessão espefica
		 * ou o outro metodo generalista (captura todas as excessoes)
		 */
		if (userDetails.getFirstName().isEmpty()) throw new UserServiceException(ErrorMessages.MISSING_REQUIRED_FIELD.getErrorMessage());
		//if (userDetails.getFirstName().isEmpty()) throw new NullPointerException("this object is null");

		UserDTO userDTO = new UserDTO();

		BeanUtils.copyProperties(userDetails, userDTO);
		
		UserDTO updateUser = userService.updateUser(id, userDTO);
		BeanUtils.copyProperties(updateUser, returnValue);//pega o objeto do createUser e copia para o retorno
		return returnValue; //return "update usuário foi chamado";
		
	}
	
	@DeleteMapping(path ="/{id}",
			produces = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })//pode responder json & xml format)
	public OperationStatusMode deleteUser(@PathVariable String id) {
		OperationStatusMode returnValue = new OperationStatusMode();
		returnValue.setOperationName(RequestOperationName.DELETE.name());
		
		userService.deleteUser(id);
		
		returnValue.setOperationResult(RequestOperationStatus.SUCESS.name());
		return returnValue; //		return "delete usuário foi chamado";
	}
	
	/**
	 * Metodo de controle de paginação.
	 * Quando um valor de limite não for fornecido o valor será 25(padrão), se o 
	 * valor for 40 , 50 etc o valor default não será utilizado
	 * Obs: valor padrão começa com 0 e não com 1 (caso um msg de erro)
	 * @param page
	 * @param limit
	 * @return
	 */
	@GetMapping(produces = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })                                                  //valor padrão
	public List<UserRest> getUsers(@RequestParam(value = "page", defaultValue = "0") int page,
			@RequestParam(value = "limit", defaultValue = "2")int limit){
		List<UserRest> returnValue = new ArrayList<>();
		
		 List<UserDTO> users =  userService.getUsers(page,limit);
		 
		 //Looping na lista para cada objeto usuario 
		 for (UserDTO userDto : users) {
			 UserRest userModel = new UserRest();
			 BeanUtils.copyProperties(userDto, userModel);
			 returnValue.add(userModel);
		 }
		return returnValue;
	}
	
}
