package it.ws.ui.model.response;

import java.util.List;

/**
 * Informações do usuário que estão sendo enviadas de volta como uma confirmação
 * que seus dados foram armazenados
 * 
 * @author Administrador
 *
 */
public class UserRest {
	/**
	 * Certifique-se que não usará nenhuma informação sensivel que possa comprometer
	 * o sistema
	 */
	private String userId;
	private String firstName;
	private String lastName;
	private String email;
	private List<AddressesRest> addresses;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<AddressesRest> getAddresses() {
		return addresses;
	}

	public void setAddresses(List<AddressesRest> addresses) {
		this.addresses = addresses;
	}

}
