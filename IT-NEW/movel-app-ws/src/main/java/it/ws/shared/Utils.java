package it.ws.shared;

import java.security.SecureRandom;
import java.util.Random;

import org.springframework.stereotype.Component;

@Component
public class Utils {
	/**
	 * Podemos gerar um Id de usuario com 10 caracteres ou 15 caracteres ou 30 caracteres ou 50
	 * depende de vc
	 * ALPHABET = Como fonte de caracteres
	 */
	private final Random RAMDOM = new SecureRandom();
	private final String ALPHABET = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
//	private final int ITERATIONS = 1000;
//	private final int KEY_LENGTH = 256;
	
	public String generateUserId(int lenght) {
		return generateRandomString(lenght);
	}
	
	public String generateAddressId(int lenght) {
		return generateRandomString(lenght);
	}
	
	/**
	 * Função que gera variedade de caracteres aleatorios
	 * @param lenght
	 * @return
	 */
	private String generateRandomString(int lenght) {
		StringBuilder returnValue = new StringBuilder(lenght);
		
		for (int i = 0; i < lenght; i++) {
			returnValue.append(ALPHABET.charAt(RAMDOM.nextInt(ALPHABET.length())));
		}
		return new String (returnValue);
	}
	
}
