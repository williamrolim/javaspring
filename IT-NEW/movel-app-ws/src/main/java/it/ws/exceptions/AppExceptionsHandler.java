package it.ws.exceptions;

import java.util.Date;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import it.ws.ui.model.response.ErrorMessage;

@ControllerAdvice
public class AppExceptionsHandler {
	/**
	 * Podemos retornar uma mensagem em json apresentado (error)
	 * ou podemos costumizar esse erro e deixar a msg personalizada
	 * Medodo capaz de lidar com excessões especificas do serviço do
	 * usuario (User) . Tentams retornar ma msg da excessão 	
	 * @param ex
	 * @param request
	 * @return
	 */
	@ExceptionHandler(value = {UserServiceException.class}) //podemos fornecer uma ou mais excessões que esse metodo pode manipular
	public ResponseEntity<Object> handleUserServiceException(UserServiceException ex, WebRequest request)
	{
		/*Retornarei minha classe customizada (ErroMessage) que será utilizada e convertida
		 * em json (ou representação similiar)
		 */		
		ErrorMessage errorMessage = new ErrorMessage(new Date(),ex.getMessage());
			
		//return new ResponseEntity<>(ex.getMessage(), new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
		return new ResponseEntity<>(errorMessage, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);

	}
	
	/**
	 * Metodo que irá lidar com todas excessões (cuidado - pesquisar antes de utilizar)
	 * Se vc quiser que um manipulador de excessões trate de multiplas excessões , digamos que
	 * vc quer 2 excessões para manipular o mesmo metodo
	 * @ExceptionHandler(value = {UserServiceException.class, PODE ADICIONAR MAIS UMA VIRGULA AQUI
	 * E COLOCAR A NOVA EXESSÃO QUE QUER LANÇAR}) mas tenho que chamar A mae de todas
	 * as excessoes para lidar com varias ao mesmo tempo ex :
	 * public ResponseEntity<Object> handleUserServiceException(Exception ex...
	 * 
	 */
	
	/**
	 * Metodo que captura todas as excessões 
	 * adequado para excessões que pensei que não poderia acontecer
	 * e que não podem ser tratados com tratamento de excessao
	 * util para excessões que não podem ser capturadas
	 * lidando com todas ecessões atravez desse metodo (CUIDADO = PESQUISAR ANTES DE UTILIZAR)
	 * @param ex
	 * @param request
	 * @return
	 */
	@ExceptionHandler(value = {Exception.class}) //podemos fornecer uma ou mais excessões que esse metodo pode manipular
	public ResponseEntity<Object> handleOtherException(Exception ex, WebRequest request)
	{
		ErrorMessage errorMessage = new ErrorMessage(new Date(),ex.getMessage());
			
		return new ResponseEntity<>(errorMessage, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);

	}
	
}
