package it.ws.security;

import it.ws.SpringApplicationContext;

public class SecurityConstants {
	
	public static final long EXPIRATION_TIME = 864000000; // 10 DIAS (tempo de expiração do token (contem os millesegundos que será igual a 10 dias)
	public static final String TOKEN_PREFIX = "Bearer ";//(O portador) prefixo de token que será passado junto com o cabeçalho
	public static final String HEADER_STRING = "Authorization";//Cabeçaçho de autorização
	public static final String SIGN_UP_URL = "/users";// Acesso publico para cadastrar sua rota
	//public static final String TOKEN_SECRET = "jf9i4jgu83nfl0jfu57ejf7";//sequencia alfanumerica de caracteres vc pode alterar com outros caracteres
	
	
	public static String getTokenSecret()
	{												//SpringApplicationContext (classe it.ws)
		AppProperties appProperties = (AppProperties) SpringApplicationContext.getBean("AppProperties");
		return appProperties.getTokenSecret();
	}
}
