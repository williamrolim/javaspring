package it.ws.security;

import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import it.ws.service.UserService;

@EnableWebSecurity
public class WebSecurity extends WebSecurityConfigurerAdapter{

	private final UserService userDetailService;
	private final BCryptPasswordEncoder bCryptPasswordEncoder;
	
	public WebSecurity(UserService userDetailsService, BCryptPasswordEncoder bCryptPasswordEncoder) {
		this.userDetailService = userDetailsService;
		this.bCryptPasswordEncoder = bCryptPasswordEncoder;
	}
	
	@Override
	protected void configure(HttpSecurity http)throws Exception {
		http
		.csrf()
		.disable()
		.authorizeRequests()
		.antMatchers(HttpMethod.POST,SecurityConstants.SIGN_UP_URL) //.antMatchers(HttpMethod.POST,"/users")
		.permitAll()//terá permissão total na aplicação
		.anyRequest()
		/*.authenticated().and().addFilter(new AuthenticationFilter(authenticationManager()));//adicionar um filtro para autenticar o usuario */
		.authenticated().and()
		.addFilter(getAuthenticationFilter())
		.addFilter(new AuthorizationFilter(authenticationManager()))
		.sessionManagement() //Não deixar que a sessão seja armazenada nos Cookies
		.sessionCreationPolicy(SessionCreationPolicy.STATELESS);// impedira que sejá armazenada em cache 
	}                                               //STATELESS É A MAIS RIGOROSA
	/**
	 * gerenciador de autenticação constroi a interface de detalhes do usuario
	 * e tbem estamos criptografando para proteger a senha do usuario
	 */
	@Override
	public void configure (AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailService).passwordEncoder(bCryptPasswordEncoder);
	}
	
	public AuthenticationFilter getAuthenticationFilter() throws Exception {
		final AuthenticationFilter filter = new AuthenticationFilter(authenticationManager());
		filter.setFilterProcessesUrl("/users/login");//caminho de indicação de envio do browser alterando (padrão login agora http://localhost:8090/login)
		return filter;       //http://localhost:8090/users/login
	}
	
}
