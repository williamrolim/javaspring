package it.ws.security;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import it.ws.SpringApplicationContext;
import it.ws.io.entity.UserEntity;
import it.ws.service.UserService;
import it.ws.share.dto.UserDTO;
import it.ws.ui.model.request.UserLoginRequestModel;

/**
 * Spring será utilizado para autenticar o usuario (nome de usuario e senha) que foram fornecidos
 * pelo usuario
 * @author Administrador
 *
 */
public class AuthenticationFilter extends UsernamePasswordAuthenticationFilter {

	private final AuthenticationManager authenticationManager;
	
	public AuthenticationFilter(AuthenticationManager authenticationManager) {
		this.authenticationManager = authenticationManager;
	}
	
	/**
	 * Contem o nome do usuario e a senha convertida quando a solicitação
	 * do usuario é enviada
	 */
	@Override
	public Authentication attemptAuthentication(HttpServletRequest req,HttpServletResponse res) throws AuthenticationException {
		try {
			UserLoginRequestModel creds = new ObjectMapper() 
					.readValue(req.getInputStream(), UserLoginRequestModel.class);
			
			return authenticationManager.authenticate(   // autenticando o usuario
					new UsernamePasswordAuthenticationToken(
							creds.getEmail(),
							creds.getPassword(),
							new ArrayList<>())
					);
		}catch (IOException e) {
			throw new RuntimeException(e);
					
		}
	}

	/**
	 * O nome do usuario será lido do objeto de autenticação
	 * gerara o json web token e incluira no cabecalho do cliente (header)
	 * ler sobre JsonWebToken
	 */
	@Override
	protected void successfulAuthentication(HttpServletRequest req,
										   HttpServletResponse res,
										   FilterChain chain,
										   Authentication auth) throws IOException, ServletException {
		
	String userName = ((User) auth.getPrincipal()).getUsername();
	//String tokenSecret = new SecurityConstants().getTokenSecret();
	
	String token  = Jwts.builder()
			        .setSubject(userName)    // estamos obtendo os millsegundos atuais e em seguida adicioando 10 dias a ele
			        .setExpiration(new Date(System.currentTimeMillis() + SecurityConstants.EXPIRATION_TIME))
			        //.signWith(SignatureAlgorithm.HS512, SecurityConstants.TOKEN_SECRET)
			        .signWith(SignatureAlgorithm.HS512, SecurityConstants.getTokenSecret())
			        .compact();//SignatureAlgorithm.HS512 algoritimo de assinatura
	
	UserService userService = (UserService) SpringApplicationContext.getBean("userServiceImp");
	
    UserDTO userDTO = userService.getUser(userName);

	res.addHeader(SecurityConstants.HEADER_STRING, SecurityConstants.TOKEN_PREFIX + token);
	//geramos o token	
	res.addHeader("UserID", userDTO.getUserId());
	}
}
