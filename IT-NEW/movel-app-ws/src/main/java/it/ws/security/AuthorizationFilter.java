package it.ws.security;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import io.jsonwebtoken.Jwts;

public class AuthorizationFilter extends BasicAuthenticationFilter {
	
	public AuthorizationFilter(AuthenticationManager authManager) {
		super(authManager);
	}
	
	@Override
	protected void doFilterInternal(HttpServletRequest req,
										   HttpServletResponse res,
										   FilterChain chain) throws IOException, ServletException {
		//podemos acessar o cabeçalho da solicitaçao e o nme do header 
		String header = req.getHeader(SecurityConstants.HEADER_STRING);
		
		// se o cabecelho for == null e o header começa com Bearer "
	    if (header == null || !header.startsWith (SecurityConstants.TOKEN_PREFIX)){
	    	chain.doFilter(req, res);
	    	return;
	    }
	    
	    UsernamePasswordAuthenticationToken authentication = getAuthentication(req);
	    SecurityContextHolder.getContext().setAuthentication(authentication);
	    chain.doFilter(req, res);
		
	}
	
	/**
	 * Seu trabalho é retornar a autenticação de senhasToken
	 * @param request
	 * @return
	 */
	private UsernamePasswordAuthenticationToken getAuthentication(HttpServletRequest request) {
		String token = request.getHeader(SecurityConstants.HEADER_STRING);//estamos lendo esse token
		
		if (token != null) {
			token = token.replace(SecurityConstants.TOKEN_PREFIX, "");//Nos livramos do prefixo Bear (não precisamos dele, precismas do json)
			
			String user = Jwts.parser()
					     //.setSigningKey(SecurityConstants.TOKEN_SECRET)//precisamos fornecer um token secret correto
						 .setSigningKey(SecurityConstants.getTokenSecret())//precisamos fornecer um token secret correto
					     .parseClaimsJws(token)//essa função analisa declarações
					     .getBody()
					     .getSubject();
			
			if (user != null) {
				return new UsernamePasswordAuthenticationToken(user, null, new ArrayList<>());
			}
			return null;
		}
		return null;
	}
}
