package it.ws.service;

import java.util.List;

import org.springframework.security.core.userdetails.UserDetailsService;

import it.ws.share.dto.UserDTO;


public interface UserService extends UserDetailsService{
	
	UserDTO createUser(UserDTO user);

	UserDTO getUserByUserId(String userId);
	
	UserDTO getUser(String email);
	
	UserDTO updateUser(String userId, UserDTO user);
	
	void deleteUser(String userId);

	List<UserDTO> getUsers(int page, int limit);
}
