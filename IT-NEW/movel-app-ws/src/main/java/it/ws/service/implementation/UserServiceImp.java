package it.ws.service.implementation;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.ws.exceptions.UserServiceException;
import it.ws.io.entity.UserEntity;
import it.ws.io.repositories.UserRepository;
import it.ws.service.UserService;
import it.ws.share.dto.AddressDTO;
import it.ws.share.dto.UserDTO;
import it.ws.shared.Utils;
import it.ws.ui.model.response.ErrorMessage;
import it.ws.ui.model.response.ErrorMessages;

/**
 * Classe que implenta a regra de negocios
 * @author Administrador
 *
 */
@Service
public class UserServiceImp implements UserService{
	
	@Autowired
	Utils  utils;
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	BCryptPasswordEncoder bCryptPasswordEncoder;
	
//	@Override
//	public UserDTO createUser(UserDTO user) {
//		
//
//		if(userRepository.findByEmail(user.getEmail())!= null) 
//			throw new RuntimeException("Record already exists");
//		
//		UserEntity userEntity = new UserEntity();
//		BeanUtils.copyProperties(user, userEntity);//Copiar a propriedade de user para userEntity
//		
//
//		String publicUserId = utils.generateUserId(30);//Gerar id de usuarios seguros (com 30 caracteres)
////		userEntity.setUserId("testUserid");
//		userEntity.setUserId(publicUserId);
//		userEntity.setEncryptedPassword(bCryptPasswordEncoder.encode(user.getPassword()));
//		
//		//userEntity.setEncryptedPassword("test");		
//		UserEntity storedUserDetails = userRepository.save(userEntity);
//		
//		UserDTO returnValue = new UserDTO();
//		BeanUtils.copyProperties(storedUserDetails, returnValue);
//		
//		return returnValue;
//	}
	
	@Override
	public UserDTO createUser(UserDTO user) {
		

		if(userRepository.findByEmail(user.getEmail())!= null) 
			throw new RuntimeException("Record already exists");
		
		for (int i =0;i<user.getAddresses().size(); i++) {
			AddressDTO address = user.getAddresses().get(i);
			address.setUserDetails(user);
			address.setAddressId(utils.generateAddressId(30));
			user.getAddresses().set(i,address);
		}
		
		ModelMapper modelMapper = new ModelMapper();
		UserEntity userEntity = modelMapper.map(user, UserEntity.class);//BeanUtils.copyProperties(user, userEntity);

		String publicUserId = utils.generateUserId(30);//Gerar id de usuarios seguros (com 30 caracteres)
		userEntity.setUserId(publicUserId);
		userEntity.setEncryptedPassword(bCryptPasswordEncoder.encode(user.getPassword()));
		
		UserEntity storedUserDetails = userRepository.save(userEntity);
		
		UserDTO returnValue = modelMapper.map(storedUserDetails, UserDTO.class);
		
		return returnValue;
	}
	

	@Override
	public UserDTO getUser(String email) {
        UserEntity userEntity = userRepository.findByEmail(email);
		if (userEntity == null) throw new UsernameNotFoundException(email);
		UserDTO returnValue = new UserDTO();
		BeanUtils.copyProperties(userEntity, returnValue);
		return returnValue;
	}
	/**
	 * Implementando metodos da UserService que possui a interface UserDetailsService
	 * Esse metodo será usado no processo de login do usuario em nosso aplicativo movel
	 * 
	 * obs:rever User (entender melhor essa classe)
	 */
	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		UserEntity userEntity = userRepository.findByEmail(email);
		if (userEntity == null) throw new UsernameNotFoundException(email);
		return new User(userEntity.getEmail(), userEntity.getEncryptedPassword(), new ArrayList<>());
	}
	
	@Override
	public UserDTO getUserByUserId(String userId) {
		UserDTO returnValue = new UserDTO();
		
		UserEntity userEntity = userRepository.findByUserId(userId);
		//if (userEntity == null) throw new UsernameNotFoundException(userId);
		if (userEntity == null)
			throw new UsernameNotFoundException("User with ID .. : " + userId + " not found");
		BeanUtils.copyProperties(userEntity,returnValue);
		return returnValue;
	}

	
	@Override
	public UserDTO updateUser(String userId, UserDTO user) {
		UserDTO returnValue = new UserDTO();		
		UserEntity userEntity = userRepository.findByUserId(userId);
		 
		//vc pode usar UsernameNotFoundException q vem do spring  ou UserServiceException que criamos (fica a gosto)
		if (userEntity == null) throw new UserServiceException(ErrorMessages.NO_RECORD_FOUND.getErrorMessage());
		
		//Só vou pegar os campos ao qual eu quero atualizar
		//A uma logia de negocios aqui inteiramente de acordo com as necessidades do 
		//aplicativo .Pode verificar todos os campos, se o last name não é null ,vazio, etc
		//como tbém não pode ser obrigatorio etc
		userEntity.setFirstName(user.getFirstName());
		userEntity.setLastName(user.getLastName());
		
		UserEntity updateUserDetails = userRepository.save(userEntity);
		//BeanUtils.copyProperties(REFERENCIA  COM OS DADO,REFERENCIA P/ TRANFERENCIA,);
		BeanUtils.copyProperties(updateUserDetails, returnValue );

		return returnValue;
	}
	
	@Transactional
	@Override
	public void deleteUser(String userId) {
		UserEntity userEntity = userRepository.findByUserId(userId);
		if (userEntity == null)
			throw new UserServiceException(ErrorMessages.NO_RECORD_FOUND.getErrorMessage());
		
		userRepository.delete(userEntity);		
	}

	@Override
	public List<UserDTO> getUsers(int page, int limit) {
		List<UserDTO> returnValue = new ArrayList<>();
		
		if (page > 0) page = page -1;
		//import org.springframework.data.domain.Pageable;
		Pageable pageableRequest = PageRequest.of(page, limit);// criar a tabela de paginas
	//PageRequest.of(page, limit) = especicficar o tamanho da pagina (inicio e fim);
		Page<UserEntity> usersPage = userRepository.findAll(pageableRequest);// retorna uma pagina de usuarios
		List<UserEntity> users = usersPage.getContent();
		
		//examinando a lista de usuarios e para cada usuario da entidade
		//criaremos um novoDto
		for (UserEntity userEntity : users) {
			UserDTO userDto = new UserDTO();
			BeanUtils.copyProperties(userEntity, userDto);
			returnValue.add(userDto);
		}
		return returnValue;
	}


}
