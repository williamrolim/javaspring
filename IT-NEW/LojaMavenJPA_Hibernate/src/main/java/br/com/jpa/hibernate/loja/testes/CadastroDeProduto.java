package br.com.jpa.hibernate.loja.testes;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import br.com.jpa.hibernate.loja.modelo.Categoria;
import br.com.jpa.hibernate.loja.modelo.Produto;

public class CadastroDeProduto {
	public static void main(String[] args) {
		Categoria celulares = new Categoria("CELULARES");

		//tela do usuario para ele preencher
		Produto celular = new Produto("Xiomi Redmi", "Muito Legal", new BigDecimal("900"),celulares );
		
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("loja");
		
		EntityManager em = factory.createEntityManager();
		em.getTransaction().begin();
		em.persist(celular);	
		em.getTransaction().commit();
		em.close();
	}
}
