package br.com.banco.jpa.teste;

import java.util.List;

import javax.persistence.EntityManager;

import br.com.banco.jpa.DAO.AccountDAO;
import br.com.banco.jpa.DAO.CardDAO;
import br.com.banco.jpa.DAO.CardEnum;
import br.com.banco.jpa.DAO.Type_CardDAO;
import br.com.banco.jpa.model.Account;
import br.com.banco.jpa.model.Card;
import br.com.banco.jpa.model.Type_Card;
import br.com.banco.jpa.util.JPAUtil;

public class AccountBeggin {

	public static void main(String[] args) {
		cardApplication();
//		searchAccountCard();
//		accountApplication();
//		searchAccount();
	}
	

	
	static void accountApplication() {
		EntityManager em = JPAUtil.getEntityManagerFactory();

//		Account account0 = new Account("William Rolim", "1234", "92939297","199","111.111.222-22");
//		Account account1 = new Account("Marilia Pera", "4321", "12343457","991","222.333.444-11");

		AccountDAO accountDao  = new AccountDAO(em);
		
		em.getTransaction().begin();
		
//		accountDao.cadastrar(account0);
//		accountDao.cadastrar(account1);
//		
		em.getTransaction().commit();
		em.close();	
	}
	
	static void searchAccountCard() {	
		EntityManager em = JPAUtil.getEntityManagerFactory();

		AccountDAO accountDao  = new AccountDAO(em);
		CardDAO cardDAO = new CardDAO(em);
		
		System.out.println("Buscando Todas as contas \n");
		accountDao.buscarTodos().forEach(System.out::println);
		
		System.out.println("Buscando Por nome \n");
		accountDao.searchForName("Marilia Pera").forEach(System.out::println);
		
		
		
		List<Object[]> relatorio = cardDAO.listaAllCard();
		for (Object[] objects : relatorio) {//pra cada object dentro do relatorio
			System.out.println(objects[0]);
			System.out.println(objects[1]);
			System.out.println(objects[2]);

		}
		//cardDAO.listaAllCard().forEach(System.out::print);
		
		em.close();

	}
	
	static void cardApplication() {
		EntityManager em = JPAUtil.getEntityManagerFactory();
		
		
		Account account0 = new Account("William Rolim", "1234", "92939297","199","111.111.222-22");//id1
		Account account1 = new Account("Marilia Pera", "4321", "12343457","991","222.333.444-11");//id2

		em.getTransaction().begin();
		
		AccountDAO accountDAO = new AccountDAO(em);
		AccountDAO accountDAO2 = new AccountDAO(em);


		accountDAO.register(account0);
		accountDAO2.register(account1);

	
		Type_Card typeCard1 = new Type_Card("Gold Card");
		Type_Card typeCard2 = new Type_Card("Silver Card");
		
		Type_CardDAO tcDao1 = new Type_CardDAO(em);
		Type_CardDAO tcDao2 = new Type_CardDAO(em);
		Type_CardDAO tcDao3 = new Type_CardDAO(em);

		tcDao1.register(typeCard1);
		tcDao2.register(typeCard2);
		tcDao3.register(typeCard2);
		
		Card card = new Card("William R Santana", CardEnum.Mastercard,"2444-4557-5456-4543","523",9000.00);//id3
		Card card1 = new Card("William R Santana", CardEnum.Elo,"1444-4557-5456-1643","123",6000.00);//id3
		Card card2 = new Card("Maria Maria", CardEnum.Elo,"1434-4557-5456-4643","223",8000.00);//id3
					
		CardDAO cardDAO = new CardDAO(em);
		CardDAO cardDAO1 = new CardDAO(em);
		CardDAO cardDAO2 = new CardDAO(em);
				

		cardDAO.register(card);
		cardDAO1.register(card1);
		cardDAO2.register(card2);
		
//		   List<Card> emplist = new ArrayList<Card>();
//		      emplist.add(card);
//		      emplist.add(card1);
//		      emplist.add(card2);
//		
		List<Object[]> relatorio = cardDAO.listaAllCard();
		for (Object[] objects : relatorio) {//pra cada object dentro do relatorio
			System.out.println(objects[0]);
			System.out.println(objects[1]);
			System.out.println(objects[2]);

		}

		
		em.getTransaction().commit();
		em.close();
		
	
	}
	
	
}