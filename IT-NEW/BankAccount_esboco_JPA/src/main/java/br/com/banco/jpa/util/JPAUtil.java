package br.com.banco.jpa.util;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JPAUtil {

	private static final EntityManagerFactory FACTORY = Persistence
			.createEntityManagerFactory("banco");
	
	public static EntityManager getEntityManagerFactory() {
		return FACTORY.createEntityManager();
	}
	
}
