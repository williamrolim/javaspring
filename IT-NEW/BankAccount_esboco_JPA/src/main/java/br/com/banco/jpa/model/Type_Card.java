package br.com.banco.jpa.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Type_Card {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int type_card_id;
	private String name;
	
	public Type_Card() {
	}

	public Type_Card(String name) {
		this.name = name;
	}

	public int getId() {
		return type_card_id;
	}

	public void setId(int id) {
		this.type_card_id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Type_Card [id=" + type_card_id + ", name=" + name + "]";
	}


	
	
}
