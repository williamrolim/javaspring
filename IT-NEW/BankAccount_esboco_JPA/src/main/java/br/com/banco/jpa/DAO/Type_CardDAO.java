package br.com.banco.jpa.DAO;

import javax.persistence.EntityManager;

import br.com.banco.jpa.model.Type_Card;

public class Type_CardDAO {
	private EntityManager em;

	public Type_CardDAO(EntityManager em) {
		this.em = em;
	}
	
	public void register(Type_Card typecard) {
		this.em.persist(typecard);
		
	}
}
