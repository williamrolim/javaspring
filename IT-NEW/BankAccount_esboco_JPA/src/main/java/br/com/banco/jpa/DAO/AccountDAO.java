package br.com.banco.jpa.DAO;

import java.util.List;

import javax.persistence.EntityManager;

import br.com.banco.jpa.model.Account;

public class AccountDAO {
	private EntityManager em;
	
	public AccountDAO(EntityManager em) {
		this.em = em;
	}
	
	public void register(Account account) {
		this.em.persist(account);
	}
	
	public void atualizar(Account account) {
		this.em.merge(account);
	}
	
	public void deletar(Account account) {
		this.em.remove(account);
	}
	
	public Account findoToId(Long id) {
		return this.em.find(Account.class, id);
	}
	
	public List<Account>buscarTodos(){
		String jpql = "SELECT a FROM Account AS a";
		return em.createQuery(jpql, Account.class).getResultList();
	}
	
	public List<Account> searchForName(String name_owner){
		String jpql = "SELECT a FROM Account a WHERE a.name_owner = :name_owner ";
		return em.createQuery(jpql, Account.class)
				.setParameter("name_owner", name_owner)
				.getResultList();	
	}

	
	
	
}
