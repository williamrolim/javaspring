package br.com.banco.jpa.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name= "accounts")
public class Account {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int account_id;
	@Basic
	@Column(nullable = false, length = 50)
	private String name_owner;
	@Basic
	@Column(nullable = false, length = 4)
	private String agency_code;
	@Basic
	@Column(nullable = false, length = 8)	
	private String account_code;
	@Basic
	@Column(nullable = false, length = 3)
	private String digit_verification;
	@Basic
	@Column(nullable = false, length = 14,unique=true)
	private String register_id;//rEGISTRO ID = CPF
	//( targetEntity=Card.class )
	@OneToMany
	private List<Card> card = new ArrayList<Card>();
	
	public Account() {
		
	}

	public Account(String name_owner, String agency_code, String account_code, String digit_verification,
			String register_id) {
		this.name_owner = name_owner;
		this.agency_code = agency_code;
		this.account_code = account_code;
		this.digit_verification = digit_verification;
		this.register_id = register_id;
	}

	public String getName_owner() {
		return name_owner;
	}

	public void setName_owner(String name_owner) {
		this.name_owner = name_owner;
	}

	public String getAgency_code() {
		return agency_code;
	}

	public void setAgency_code(String agency_code) {
		this.agency_code = agency_code;
	}

	public String getAccount_code() {
		return account_code;
	}

	public void setAccount_code(String account_code) {
		this.account_code = account_code;
	}

	public String getDigit_verification() {
		return digit_verification;
	}

	public void setDigit_verification(String digit_verification) {
		this.digit_verification = digit_verification;
	}


	public String getRegister_id() {
		return register_id;
	}

	public void setRegister_id(String register_id) {
		this.register_id = register_id;
	}

	

	public int getAccount_id() {
		return account_id;
	}

	public void setAccount_id(int account_id) {
		this.account_id = account_id;
	}

	public List<Card> getCard() {
		return card;
	}

	public void setCard(List<Card> card) {
		this.card = card;
	}

	@Override
	public String toString() {
		return "Account [account_id=" + account_id + ", name_owner=" + name_owner + ", agency_code=" + agency_code
				+ ", account_code=" + account_code + ", digit_verification=" + digit_verification + ", register_id="
				+ register_id + ", card=" + card + "]";
	}


	

}
