package br.com.banco.jpa.model;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.PrimaryKeyJoinColumns;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;

import br.com.banco.jpa.DAO.CardEnum;

@Entity
@Table(name= "cards")
public class Card {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Basic
	@Column(nullable = false, length = 50)
	private String name;
	@Basic
	@Column(nullable = false, length = 45)
	@Enumerated(EnumType.STRING)
	private CardEnum flag;
	@Basic
	@Column(nullable = false, length = 45)
	private String number;
	@Basic
	@Column(nullable = false, length = 5)
	private String digit_code;
	@Basic
	@Column(nullable = false, columnDefinition="Double(14.2)")
	private double limit_balance;
	
	@ManyToOne (fetch = FetchType.LAZY,cascade = CascadeType.ALL)
	@JoinColumn(name="account_id")//mapeamos a chave estrangeira
	private Account account_id;

	@OneToOne
	@JoinColumn(name="type_card_id")
	private Type_Card type_card_id;
	
	private int idAccount;
	public void pegarIdConta(Account account_id) {
		this.setIdAccount(account_id.getAccount_id());
	}
	public Card() {
		
	}
	
	public Card(String name, CardEnum flag, String number, String digit_code, double limit_balance) {
	this.name = name;
	this.flag = flag;
	this.number = number;
	this.digit_code = digit_code;
	this.limit_balance = limit_balance;
}
	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public CardEnum getFlag() {
		return flag;
	}

	public void setFlag(CardEnum flag) {
		this.flag = flag;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getDigit_code() {
		return digit_code;
	}

	public void setDigit_code(String digit_code) {
		this.digit_code = digit_code;
	}

	public double getLimit_balance() {
		return limit_balance;
	}

	public void setLimit_balance(double limit_balance) {
		this.limit_balance = limit_balance;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Account getAccount_id() {
		return account_id;
	}

	public void setAccount_id(Account account_id) {
		this.account_id = account_id;
	}

	public Type_Card getType_card_id() {
		return type_card_id;
	}

	public void setType_card_id(Type_Card type_card_id) {
		this.type_card_id = type_card_id;
	}

	@Override
	public String toString() {
		return "Card [id=" + id + ", name=" + name + ", flag=" + flag + ", number=" + number + ", digit_code="
				+ digit_code + ", limit_balance=" + limit_balance + ", account_id=" + account_id + ", type_card_id="
				+ type_card_id + "]";
	}
	public int getIdAccount() {
		return idAccount;
	}
	public void setIdAccount(int idAccount) {
		this.idAccount = idAccount;
	}




	
	

}
