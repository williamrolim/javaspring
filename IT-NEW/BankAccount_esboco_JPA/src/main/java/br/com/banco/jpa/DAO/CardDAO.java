package br.com.banco.jpa.DAO;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import br.com.banco.jpa.model.Card;

public class CardDAO {

	private EntityManager em;

	public CardDAO(EntityManager em) {
		this.em = em;
	}
	
	public void register(Card card) {
		this.em.persist(card);
		
	}
	
	/*	String jpql = "SELECT new br.com.jpa.hibernate.loja.vo.RelatorioDeVendasVo ("//criando uma estancia de relatorioVendas, e os 3 atributos abaixo passa para o construtor
				+ "produto.nome, "
				+ "SUM(item.quantidade), "//ITEM QUANTIDADE [[[[PEDIDO]]]]
				+ "MAX(pedido.dataCadastro))" //dataCadastro mesmo nome que est� na entidade 
				+ "FROM Pedido pedido "//[[[[ITEM_PEDIDO]]]]
				+ "JOIN pedido.itens item "//private List<ItemPedido> itens = new ArrayList<ItemPedido>() [[[[PEDIDO]]]]
				+ "JOIN item.produto produto " //private Produto produto; [[[[ITEM_PEDIDO]]]]
				+ "GROUP BY  produto.nome "//fun��o de agrega�ao deve-se usar o group by
				+ "ORDER BY item.quantidade DESC";
		return em.createQuery(jpql,RelatorioDeVendasVo.class).getResultList();*/
	public List<Object[]> listaAllCard(){
		String jpql = "SELECT a,c,t FROM Account AS a " +
				"JOIN Card AS c " +
				"ON c.id= a.account_id " +
				"JOIN Type_Card AS t " +
				"ON c.id = t.id";

		return em.createQuery(jpql,Object[].class).getResultList();
		
	
	}
	

//	public List<Object[]>relatorioVendas() {
//	String jpql = "SELECT produto.nome, "
//			+ "SUM(item.quantidade), "//ITEM QUANTIDADE [[[[PEDIDO]]]]
//			+ "MAX(pedido.dataCadastro) " //dataCadastro mesmo nome que est� na entidade 
//			+ "FROM Pedido pedido "//[[[[ITEM_PEDIDO]]]]
//			+ "JOIN pedido.itens item "//private List<ItemPedido> itens = new ArrayList<ItemPedido>() [[[[PEDIDO]]]]
//			+ "JOIN item.produto produto " //private Produto produto; [[[[ITEM_PEDIDO]]]]
//			+ "GROUP BY  produto.nome "//fun��o de agrega�ao deve-se usar o group by
//			+ "ORDER BY item.quantidade DESC";
//	return em.createQuery(jpql,Object[].class).getResultList();
//}
	
}
