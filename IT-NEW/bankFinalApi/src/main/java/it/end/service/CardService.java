package it.end.service;

import it.end.io.entity.CardEntity;

public interface CardService {
 
	CardEntity createCardForAccount(CardEntity cardEntity, Integer idAccount);
}
