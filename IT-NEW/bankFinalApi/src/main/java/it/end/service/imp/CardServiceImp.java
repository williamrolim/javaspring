package it.end.service.imp;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.end.io.entity.AccountEntity;
import it.end.io.entity.CardEntity;
import it.end.io.entity.TypeCardEntity;
import it.end.io.entity.enums.TypeCardsEnums;
import it.end.io.repository.AccountRepository;
import it.end.io.repository.CardRepository;
import it.end.io.repository.TypeCardRepository;
import it.end.service.CardService;

@Service
public class CardServiceImp implements CardService{
	
	@Autowired
	AccountRepository accountRepository;
	
	@Autowired
	CardRepository cardRepository;
	
	@Autowired
	TypeCardRepository typeCardRepository;

	
//	@Override
//	public CardEntity createCardForAccount(CardEntity cardEntity) {
//		Optional<AccountEntity> account = accountRepository.findById(null);
//		
//		return null;
//	}


	@Override
	public CardEntity createCardForAccount(CardEntity cardEntity, Integer idAccount) {
		
//    public Cliente createDocumentoInCliente(Documento documento, Long id) {
//        //List<Documento> listDocumento = new ArrayList<>();
//        Cliente cliPersist;
//        Optional<Cliente> cli = clienteRepository.findById(id);
//        cli.orElseThrow(() -> new RuntimeException("Cliente não encontrado"));
//
//        documento.setCliente(cli.get());
//        cli.get().getDocumentos().add(documento);
//        //cli.get().setDocumentos(listDocumento);
//        cliPersist = clienteRepository.save(cli.get());
//
//        return cliPersist;
		
		Optional<AccountEntity> account = accountRepository.findById(idAccount);
		account.orElseThrow(() -> new RuntimeException("Account dont find"));//caso não achar o cliente
		//typeCardEntity.orElseThrow(() -> new RuntimeException("Type Card dont find"));
		
		cardEntity.setAccount(account.get());
		TypeCardEntity tpc = new TypeCardEntity();
		tpc.setName(TypeCardsEnums.CITIBANK);
		typeCardRepository.save(tpc);
		CardEntity card = cardRepository.save(cardEntity);
		
		return card;
	}
	


}
