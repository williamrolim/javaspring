package it.end.service.imp;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.end.exceptions.AccountServiceException;
import it.end.io.entity.AccountEntity;
import it.end.io.entity.CardEntity;
import it.end.io.entity.TypeCardEntity;
import it.end.io.repository.AccountRepository;
import it.end.model.ui.response.ErrorMessages;
import it.end.service.AccountService;

@Service
public class AccountServiceImp implements AccountService{

	@Autowired
	AccountRepository accountRepository;
	
	@Override
	public AccountEntity createAccount(AccountEntity account) {

	    Optional<AccountEntity> accountEntity = accountRepository.findByRegisterId(account.getRegisterId());
	    if (accountEntity == null) 
			throw new RuntimeException("Account already exists");
	    
//		for (int i =0;i<account.getCard().size(); i++) {
//			CardEntity card = account.getCard().get(i);
//			card.setAccount(account);
//			card.setId(account.getId());
//			account.getCard().set(i,card);
//		}
//		
	    AccountEntity accountSaved = accountRepository.save(account);
	 
		return accountSaved;		
	}

	@Override
	public Optional<AccountEntity> getAccountEntity(String registerId) {
	    Optional<AccountEntity> accountEntity = accountRepository.findByRegisterId(registerId);
	    if (accountEntity == null) 			
	    	throw new RuntimeException("Account already exists");
		return accountEntity;
	}

	@Override
	public AccountEntity updateAccountEntity(String registerId,AccountEntity account) {
	    Optional<AccountEntity> accountEntity = accountRepository.findByRegisterId(registerId);
	    if (accountEntity == null) 			
	    	 throw new AccountServiceException(ErrorMessages.NO_RECORD_FOUND.getErrorMessage());
		AccountEntity accountUpdate = accountRepository.save(account);
		return accountUpdate;
	}

}
