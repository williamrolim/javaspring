package it.end.service;

import java.util.Optional;

import it.end.io.entity.AccountEntity;
import it.end.shared.dto.AccountDTO;

public interface AccountService {
  
	AccountEntity createAccount(AccountEntity account);

	Optional<AccountEntity> getAccountEntity(String registerId);

	AccountEntity updateAccountEntity(String registerId, AccountEntity account);

}
