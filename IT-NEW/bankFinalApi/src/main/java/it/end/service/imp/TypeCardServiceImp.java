package it.end.service.imp;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.end.io.entity.AccountEntity;
import it.end.io.entity.CardEntity;
import it.end.io.entity.TypeCardEntity;
import it.end.io.repository.CardRepository;
import it.end.io.repository.TypeCardRepository;
import it.end.service.TypeCardService;

@Service
public class TypeCardServiceImp implements TypeCardService {
	
	@Autowired
	TypeCardRepository typeCardRepository;
	

//	@Override
//	public TypeCardEntity createTypeCard(TypeCardEntity typeCardEntity) {
//	    Optional<TypeCardEntity> tcEntity = typeCardRepository.findById(typeCardEntity.getId());
//	    if (tcEntity == null) 
//			throw new RuntimeException("Type Card already exists");
//		return typeCardRepository.save(typeCardEntity);
//	}
	
	
	@Override
	public TypeCardEntity createTypeCardManager(TypeCardEntity typeCardEntity) {

		Optional<TypeCardEntity> cards= typeCardRepository.findById(typeCardEntity.getId());
	    if (cards == null) 
			throw new RuntimeException("Card already exists");
	    
		for (int i =0;i< typeCardEntity.getCard().size(); i++) {
			CardEntity card = typeCardEntity.getCard().get(i);
			card.setTypeCard(typeCardEntity);
			card.setId(typeCardEntity.getId());
			typeCardEntity.getCard().set(i,card);
		}
//		
	    TypeCardEntity  typeCardSaved = typeCardRepository.save(typeCardEntity);
	 
		return typeCardSaved;		
	}
	


}
