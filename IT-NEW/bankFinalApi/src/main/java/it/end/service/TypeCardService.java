package it.end.service;

import it.end.io.entity.TypeCardEntity;

public interface TypeCardService {
	
	TypeCardEntity createTypeCardManager(TypeCardEntity typeCardEntity);

}
