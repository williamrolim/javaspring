package it.end.shared.dto;

import java.util.List;
import java.util.stream.Collectors;

import it.end.io.entity.AccountEntity;

public class AccountDTO {
	private String nameOwner;
	private String agencyCode;

	private String accountCode;

	private String digitVerification;

	public AccountDTO() {
	}

	public AccountDTO(AccountEntity accountEntity) {
		this.nameOwner = accountEntity.getNameOwner();
		this.agencyCode = accountEntity.getAgencyCode();
		this.accountCode = accountEntity.getAccountCode();
		this.digitVerification = accountEntity.getDigitVerification();
	}

	public String getNameOwner() {
		return nameOwner;
	}

	public void setNameOwner(String nameOwner) {
		this.nameOwner = nameOwner;
	}

	public String getAgencyCode() {
		return agencyCode;
	}

	public void setAgencyCode(String agencyCode) {
		this.agencyCode = agencyCode;
	}

	public String getAccountCode() {
		return accountCode;
	}

	public void setAccountCode(String accountCode) {
		this.accountCode = accountCode;
	}

	public String getDigitVerification() {
		return digitVerification;
	}

	public void setDigitVerification(String digitVerification) {
		this.digitVerification = digitVerification;
	}

	public static List<AccountDTO> converter(List<AccountEntity> account) {
		return account.stream().map(AccountDTO::new).collect(Collectors.toList());
	}

}
