package it.end.exceptions;

import java.util.Date;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import it.end.model.ui.response.ErrorMessage;

@ControllerAdvice
public class AccountExceptionHandler {
	
	@ExceptionHandler(value = {AccountServiceException.class}) //podemos fornecer uma ou mais excessões que esse metodo pode manipular
	public ResponseEntity<Object> handleUserServiceException(AccountServiceException ex, WebRequest request)
	{
	
		ErrorMessage errorMessage = new ErrorMessage(new Date(),ex.getMessage());
			
		//return new ResponseEntity<>(ex.getMessage(), new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
		return new ResponseEntity<>(errorMessage, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);

	}

	@ExceptionHandler(value = {Exception.class}) 
	public ResponseEntity<Object> handleOtherException(Exception ex, WebRequest request)
	{
		ErrorMessage errorMessage = new ErrorMessage(new Date(),ex.getMessage());
			
		return new ResponseEntity<>(errorMessage, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);

	}
}
