package it.end.exceptions;

public class AccountServiceException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	public AccountServiceException(String message) {
		super(message);
	}
	
	
}