package it.end.ui.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import it.end.io.entity.CardEntity;
import it.end.service.CardService;

@RestController
@RequestMapping("/api/v1/accounts/")
public class CardController {

	@Autowired
	CardService cardService;

	@PostMapping({ "cards/{idAccount}" })
	public CardEntity createCard(@RequestBody CardEntity cardEntity, @PathVariable Integer idAccount, UriComponentsBuilder uriBuilder) {
		// if (cardEntity.getDigit_code().isEmpty()) throw new
		// AccountServiceException(ErrorMessages.MISSING_REQUIRED_FIELD.getErrorMessage());
		CardEntity card = cardService.createCardForAccount(cardEntity, idAccount);

		return card;
	}

}
