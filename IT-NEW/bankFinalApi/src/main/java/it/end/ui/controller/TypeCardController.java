package it.end.ui.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.end.io.entity.TypeCardEntity;
import it.end.service.TypeCardService;

@RestController
@RequestMapping("/api/v1/accounts/cards/typecards")
public class TypeCardController  {
	
	@Autowired
	TypeCardService typeCardService;
	
	@PostMapping
	public TypeCardEntity cadastrar(@RequestBody TypeCardEntity typeCardEntity) {

		TypeCardEntity tc = typeCardService.createTypeCardManager(typeCardEntity);
		
		return tc;
	}
	
}
