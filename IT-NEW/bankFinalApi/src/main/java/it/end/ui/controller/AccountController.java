package it.end.ui.controller;

import java.net.URI;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import it.end.exceptions.AccountServiceException;
import it.end.io.entity.AccountEntity;
import it.end.model.ui.response.ErrorMessages;
import it.end.service.AccountService;
import it.end.service.TypeCardService;
import it.end.shared.dto.AccountDTO;

@RestController
@RequestMapping("/api/v1/accounts")
public class AccountController {

	@Autowired
	AccountService accountService;
	
	@Autowired
	TypeCardService typeCardService;

	@PostMapping
	public ResponseEntity<AccountDTO> createAccount(@RequestBody AccountEntity accountEntity,UriComponentsBuilder uriBuilder) {
		if (accountEntity.getRegisterId().isEmpty()) throw new AccountServiceException(ErrorMessages.MISSING_REQUIRED_FIELD.getErrorMessage());
		accountService.createAccount(accountEntity);

		URI uri = uriBuilder.path("/{id}").buildAndExpand(accountEntity.getId()).toUri();
		return ResponseEntity.created(uri).body(new AccountDTO(accountEntity));
	}
	


	@GetMapping(path = "{registerId}")//nunca deixar espaço entre o get e o parenteses
	public Optional<AccountEntity> getAccountEntity(@PathVariable String registerId) {
		Optional<AccountEntity> account = accountService.getAccountEntity(registerId);
		return account;
	}
	
	@PutMapping(path = "{registerId}")
	public AccountEntity updateAccount(@PathVariable String registerId, @RequestBody AccountEntity accountEntity) {
		if (accountEntity.getRegisterId().isEmpty()) throw new AccountServiceException(ErrorMessages.MISSING_REQUIRED_FIELD.getErrorMessage());

		AccountEntity account = accountService.updateAccountEntity(registerId,accountEntity);
		return account;
	}
}
