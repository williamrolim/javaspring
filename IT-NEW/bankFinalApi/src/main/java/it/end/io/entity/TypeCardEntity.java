package it.end.io.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import it.end.io.entity.enums.TypeCardsEnums;
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity(name = "type_card")
public class TypeCardEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	
	@Enumerated(EnumType.STRING)
	private TypeCardsEnums name;	//	@JsonProperty(namespace = "typeCard")

	@OneToMany(mappedBy = "typeCard" , cascade = CascadeType.ALL)
	private List<CardEntity> card;
	
	
	
	public TypeCardEntity(Integer id, TypeCardsEnums name) {
		this.id = id;
		this.name = name;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	public TypeCardsEnums getName() {
		return name;
	}

	public void setName(TypeCardsEnums name) {
		this.name = name;
	}

	public List<CardEntity> getCard() {
		return card;
	}

	public void setCard(List<CardEntity> card) {
		this.card = card;
	}

	
}