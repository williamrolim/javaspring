package it.end.io.entity.enums;

public enum CardFlagEnums {
	CITIBANK, 
	VISA, 
	MASTERCARD, 
	ELO, 
	AMERICANEXPRESS, 
	DISCOVERNETWORD;
}
