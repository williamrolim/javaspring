package it.end.io.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.end.io.entity.CardEntity;

@Repository
public interface CardRepository extends JpaRepository<CardEntity, Long> {
//	@Query(value = "INSERT INTO Documento (tipodoc,numero) VALUES (:tipoDoc,:numero)", nativeQuery = true)
//	@Transactional
//	Documento inserirDocumento(@Param("tipoDoc") TipoDoc tipoDoc, @Param("numero") String numero);

	Optional<CardEntity> findById(Integer id);
//	@Query(value = "INSERT INTO Card (Account_id,Type_card_id) VALUES (:tipoDoc,:numero)", nativeQuery = true)


}
