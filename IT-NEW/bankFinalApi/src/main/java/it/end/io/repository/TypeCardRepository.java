package it.end.io.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.end.io.entity.TypeCardEntity;

@Repository
public interface TypeCardRepository extends JpaRepository<TypeCardEntity, Integer> {

	TypeCardEntity save(String string);


	
	
}
