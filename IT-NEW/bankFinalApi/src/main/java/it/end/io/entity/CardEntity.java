package it.end.io.entity;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonProperty;

import it.end.io.entity.enums.CardFlagEnums;

@Entity(name = "card")
public class CardEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Basic
	@Column(nullable = false, length = 50)
	private String name;

	@Basic
	@Column(nullable = false, length = 45)
	@Enumerated(EnumType.STRING)
	private CardFlagEnums flag;

	@Basic
	@Column(nullable = false, length = 45)
	private String number;

	@Basic
	@Column(nullable = false, length = 5)
	private String digit_code;

	@Basic
	@Column(nullable = false, columnDefinition = "Double(14.2)")
	private Double limit_balance;
	
	@ManyToOne
	@JoinColumn(name = "account_id")
	@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
	private AccountEntity account;
	
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "type_card_id")
	@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
	private TypeCardEntity typeCard;
	
	public CardEntity() {
		// TODO Auto-generated constructor stub
	}
	
	public CardEntity(Integer id, String name, CardFlagEnums flag, String number, String digit_code,
			Double limit_balance, AccountEntity account, TypeCardEntity typeCard) {
		this.id = id;
		this.name = name;
		this.flag = flag;
		this.number = number;
		this.digit_code = digit_code;
		this.limit_balance = limit_balance;
		this.account = account;
		this.typeCard = typeCard;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public CardFlagEnums getFlag() {
		return flag;
	}

	public void setFlag(CardFlagEnums flag) {
		this.flag = flag;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getDigit_code() {
		return digit_code;
	}

	public void setDigit_code(String digit_code) {
		this.digit_code = digit_code;
	}

	public Double getLimit_balance() {
		return limit_balance;
	}

	public void setLimit_balance(Double limit_balance) {
		this.limit_balance = limit_balance;
	}

	public AccountEntity getAccount() {
		return account;
	}

	public void setAccount(AccountEntity account) {
		this.account = account;
	}

	public TypeCardEntity getTypeCard() {
		return typeCard;
	}

	public void setTypeCard(TypeCardEntity typeCard) {
		this.typeCard = typeCard;
	}
	
	
}
