package it.end.io.entity.enums;

public enum TypeCardsEnums {
	CITIBANK("city bank"),
	DEBIT_CARD("Debit Card"), 
	CREDIT_CARD("credicard card"), 
	MEAL_CARD("meal card"), 
	GIFT_CARD("gift card");
	
	private String typeCards;
	
	TypeCardsEnums(String typeCards) {
		this.typeCards = typeCards;
	}

	 public String getTypeCards() {
		 return typeCards;
	 }
	 

	 public void setTypeCards(String typeCards) {
		 this.typeCards = typeCards;
	 }
}
