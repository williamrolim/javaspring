package it.end;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BankFinalApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(BankFinalApiApplication.class, args);
	}

}
