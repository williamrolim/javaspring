package it.controller;

import java.net.URI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import it.model.Card;
import it.model.TypeCard;
import it.service.CardService;
import it.service.TypeCardService;

@RestController
@RequestMapping({"/api/v1/type_cards"})
public class TypeCardController {
  @Autowired
  CardService cardService;
  
  @Autowired
  TypeCardService typeCardService;
  
  @PostMapping
  public ResponseEntity<TypeCard> createAccount(@RequestBody TypeCard type_card) {
    TypeCard typcardCadaster = this.typeCardService.createdTypeCard(type_card);
    URI locationURL = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
      .buildAndExpand(new Object[] { typcardCadaster.getId() }).toUri();
    return ResponseEntity.created(locationURL).build();
  }
  
  @PostMapping({"/{id}/type_cards"})
  public ResponseEntity<Card> createdTypeCardAssocietedIdTypeCard(@RequestBody TypeCard type_card, @PathVariable Integer id) {
    Card tc = this.cardService.createTypeCardInCardAssocietedId(type_card, id);
    URI location = 
      ServletUriComponentsBuilder.fromCurrentRequest()
      .path("/{id}")
      .buildAndExpand(new Object[] { tc.getId() }).toUri();
    return ResponseEntity.created(location).build();
  }
}