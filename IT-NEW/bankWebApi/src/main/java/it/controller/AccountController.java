package it.controller;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.util.UriComponentsBuilder;

import it.controller.form.AccountAllForm;
import it.controller.form.AccountForm;
import it.model.Account;
import it.model.Card;
import it.model.dto.AccountDTO;
import it.repository.AccountRepository;
import it.repository.CardRepository;
import it.service.AccountService;
import it.service.CardService;

@RestController
@RequestMapping({ "/api/v1/accounts" })
public class AccountController {
	@Autowired
	private AccountService accountService;

	@Autowired
	private CardService cardService;
	
	@Autowired
	private AccountRepository accountRepository;
	
	@Autowired
	private CardRepository cardRepository;
	
	
	@PostMapping
	@Test
	public ResponseEntity<Account> createAccount(@RequestBody Account account) {
		Account accounts = this.accountService.saved(account);
		URI locationURL = ServletUriComponentsBuilder
				.fromCurrentRequest().path("/{id}")
				.buildAndExpand(accounts.getId())
				.toUri();
		return ResponseEntity.created(locationURL).build();
	}

	@PostMapping({ "/saveAccountDidatico" })
	public ResponseEntity<Account> createAccount(@RequestBody @Valid Account account, UriComponentsBuilder uriBuilder) {
		Account accounts = this.accountService.saved(account);
		URI uri = uriBuilder.path("/accountsid/{id}")
				.buildAndExpand(accounts.getId())
				.toUri();
		return ResponseEntity.created(uri).body(accounts);
	}

	@PostMapping({ "/saveAccountDTOPersonalizada" })
	public void cadastrar(@RequestBody AccountForm form) {
		Account account = form.coverter();
		this.accountRepository.save(account);
	}

	@RequestMapping({ "/accounts" })
	public List<AccountDTO> list() {
		List<Account> account = this.accountRepository.findAll();
		return AccountDTO.convert(account);
	}

	@RequestMapping({ "/findByCardsName" })
	public List<AccountDTO> list(String nameCard) {
		Optional<Account> account = this.accountRepository.findByCards_Name(nameCard);
		return AccountDTO.convert(account);
	}
	
//	@GetMapping( "{register_id}" )
//	public ResponseEntity<AccountAllForm> searchAccountForId(@PathVariable String register_id) {
//		AccountAllForm doc = this.accountService.getByRegisterIdAccount(register_id);
//		return ResponseEntity.ok().body(doc);
//	}

	@GetMapping( "{register_id}" )
	public ResponseEntity<Account> searchAccountForId(@PathVariable String register_id) {
		Account doc = this.accountService.getByRegisterIdAccount(register_id);
		return ResponseEntity.ok().body(doc);
	}
	
//	@GetMapping( "{registerId}" )
//	public ResponseEntity<Optional<Account>> searchAccountForIds(@PathVariable String registerId) {
//		Optional<Account> doc = accountRepository.procurarPeloNome(registerId);
//		System.out.println("vejaaaa ===== " + doc.toString());
//		return ResponseEntity.ok().body(doc);
//	}

	@GetMapping
	public ResponseEntity<Page<Account>> getAllAccounts(Pageable pageable) {
		Page<Account> all = this.accountService.getAllAccounts(pageable);
		return ResponseEntity.ok().body(all);
	}

	@PostMapping({ "{idCard}/cards/{idTypeCard}"})
	public ResponseEntity<Card> createdCardAssocieteIdAccount(@RequestBody Card cards, @PathVariable Integer idCard, @PathVariable Integer idTypeCard){
		Account account = this.accountService.createCardInAccount(cards, idCard,idTypeCard);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("{idCard}")
				.buildAndExpand(account.getId()).toUri();
		return ResponseEntity.created(location).build();
	
	}
	


	
//	@PostMapping({ "/saveCardDTOPersonalizada" })
//	public void cadastrar(@RequestBody CardForm form) {
//		Card card = form.convert(form);
//		this.accountRepository.save(account);
//	}
}
