package it.controller.form;

import it.model.Account;

public class AccountForm {
  private String name_owner;
  
  private String registerId;
  
  public String getName_owner() {
    return this.name_owner;
  }
  
  public void setName_owner(String name_owner) {
    this.name_owner = name_owner;
  }
  
  public String getRegisterId() {
    return this.registerId;
  }
  
  public void setRegisterId(String registerId) {
    this.registerId = registerId;
  }
  
  public Account coverter() {
    return new Account(this.name_owner, this.registerId);
  }
}
