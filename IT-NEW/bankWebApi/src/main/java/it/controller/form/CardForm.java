package it.controller.form;

import java.util.Optional;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import it.model.Account;
import it.model.Card;
import it.model.TypeCard;
import it.model.enums.CardEnum;
import it.repository.TypeCardRepository;

public class CardForm {

	@Autowired
	TypeCardRepository typeCardRepository;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Basic
	@Column(nullable = false, length = 50)
	private String name;

	@Basic
	@Column(nullable = false, length = 45)
	@Enumerated(EnumType.STRING)
	private CardEnum flag;

	@Basic
	@Column(nullable = false, length = 45)
	private String number;

	@Basic
	@Column(nullable = false, length = 5)
	private String digit_code;

	@Basic
	@Column(nullable = false, columnDefinition = "Double(14.2)")
	private Double limit_balance;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "account_id", nullable = false)
	@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
	private Account account;

	@OneToOne(cascade = { CascadeType.ALL })
	@JoinColumn(name = "type_card_id")
	@JsonIgnore
	private TypeCard type_card;

	public CardForm() {
	}

	public CardForm(Integer id, String name, CardEnum flag, String number, String digit_code, Double limit_balance,
			Account account, TypeCard type_card) {
		this.id = id;
		this.name = name;
		this.flag = flag;
		this.number = number;
		this.digit_code = digit_code;
		this.limit_balance = limit_balance;
		this.account = account;
		this.type_card = type_card;
	}

	public Integer getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public CardEnum getFlag() {
		return flag;
	}

	public String getNumber() {
		return number;
	}

	public String getDigit_code() {
		return digit_code;
	}

	public Double getLimit_balance() {
		return limit_balance;
	}

	public Account getAccount() {
		return account;
	}

	public TypeCard getType_card() {
		return type_card;
	}

	public Card convert(CardForm form, TypeCardRepository typeCardRepository) {

		return null;
	}
	
	
	
	

}
