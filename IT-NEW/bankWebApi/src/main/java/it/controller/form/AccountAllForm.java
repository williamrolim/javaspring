package it.controller.form;

import org.springframework.beans.factory.annotation.Autowired;

import it.model.Account;
import it.model.Card;
import it.model.TypeCard;
import it.model.enums.CardEnum;
import it.model.enums.TypeCards;
import it.repository.CardRepository;

public class AccountAllForm {

	@Autowired
	CardRepository cardRepository;
	
	

	private String name_owner;

	private String agency_code;

	private String account_code;

	private String digit_verification;

	private String registerId;


	private String name;

	private CardEnum flag;

	private String number;

	private String digit_code;

	private Double limit_balance;
	

	private TypeCards nameType;

	public AccountAllForm(Account account, Card card, TypeCard typeCard) {
		this.name_owner = account.getName_owner();
		this.agency_code = account.getAgency_code();
		this.account_code = account.getAccount_code();
		this.digit_verification = account.getDigit_verification();
		this.registerId = account.getRegisterId();
		this.name = card.getName();
		this.flag = card.getFlag();
		this.number = card.getNumber();
		this.digit_code = card.getDigit_code();
		this.limit_balance = card.getLimit_balance();
		this.nameType = typeCard.getName();
	}


	public String getName_owner() {
		return name_owner;
	}

	public String getAgency_code() {
		return agency_code;
	}

	public String getAccount_code() {
		return account_code;
	}

	public String getDigit_verification() {
		return digit_verification;
	}

	public String getRegisterId() {
		return registerId;
	}



	public String getName() {
		return name;
	}

	public CardEnum getFlag() {
		return flag;
	}

	public String getNumber() {
		return number;
	}

	public String getDigit_code() {
		return digit_code;
	}

	public Double getLimit_balance() {
		return limit_balance;
	}


	public TypeCards getNameType() {
		return nameType;
	}



	public void setName_owner(String name_owner) {
		this.name_owner = name_owner;
	}

	public void setAgency_code(String agency_code) {
		this.agency_code = agency_code;
	}

	public void setAccount_code(String account_code) {
		this.account_code = account_code;
	}

	public void setDigit_verification(String digit_verification) {
		this.digit_verification = digit_verification;
	}

	public void setRegisterId(String registerId) {
		this.registerId = registerId;
	}



	public void setName(String name) {
		this.name = name;
	}

	public void setFlag(CardEnum flag) {
		this.flag = flag;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public void setDigit_code(String digit_code) {
		this.digit_code = digit_code;
	}

	public void setLimit_balance(Double limit_balance) {
		this.limit_balance = limit_balance;
	}


	public void setNameType(TypeCards nameType) {
		this.nameType = nameType;
	}
	

}
