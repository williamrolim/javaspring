package it.model.dto;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import it.model.Account;

public class AccountDTO {
	  private Integer id;
	  
	  private String name_owner;
	  
	  private String agency_code;
	  
	  private String account_code;
	  
	  private String digit_verification;
	  
	  private String registerId;
	  
	  public AccountDTO(Account account) {
	    this.id = account.getId();
	    this.name_owner = account.getName_owner();
	    this.agency_code = account.getAgency_code();
	    this.account_code = account.getAccount_code();
	    this.digit_verification = account.getDigit_verification();
	    this.registerId = account.getRegisterId();
	  }
	  
	  public Integer getId() {
	    return this.id;
	  }
	  
	  public String getName_owner() {
	    return this.name_owner;
	  }
	  
	  public String getAgency_code() {
	    return this.agency_code;
	  }
	  
	  public String getAccount_code() {
	    return this.account_code;
	  }
	  
	  public String getDigit_verification() {
	    return this.digit_verification;
	  }
	  
	  public String getRegisterId() {
	    return this.registerId;
	  }
	  
	  public static List<AccountDTO> convert(List<Account> accounts) {
	    return (List<AccountDTO>)accounts.stream().map(AccountDTO::new).collect(Collectors.toList());
	  }
	  
	  public static List<AccountDTO> convert(Optional<Account> account) {
	    return (List<AccountDTO>)account.stream().map(AccountDTO::new).collect(Collectors.toList());
	  }
	}
