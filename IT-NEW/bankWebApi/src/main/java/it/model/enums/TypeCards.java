package it.model.enums;

public enum TypeCards {
	CITIBANK,DEBIT_CARD, CREDIT_CARD, MEAL_CARD, GIFT_CARD;
}