package it.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

import it.model.enums.TypeCards;

@Entity(name = "type_card")
public class TypeCard implements Serializable {
  private static final long serialVersionUID = 1L;
  
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;
  
  @Enumerated(EnumType.STRING)
  private TypeCards name;
  
  
  public TypeCard() {}
  
  public TypeCard(Integer id, TypeCards name) {
    this.id = id;
    this.name = name;
  }
  
  public Integer getId() {
    return this.id;
  }
  
  public void setId(Integer id) {
    this.id = id;
  }
  
  public TypeCards getName() {
    return this.name;
  }
  
  public void setName(TypeCards name) {
    this.name = name;
  }
  

}
