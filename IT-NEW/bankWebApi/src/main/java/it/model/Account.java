package it.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;

@Entity
public class Account implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Basic
	@Column(nullable = false, length = 50)
	@NotEmpty
	@NotNull
	@Length(max = 50)
	private String name_owner;

	@Basic
	@Column(nullable = false, length = 4)
	private String agency_code;

	@Basic
	@Column(nullable = false, length = 8)
	private String account_code;

	@Basic
	@Column(nullable = false, length = 3)
	private String digit_verification;

	@Basic
	@Column(nullable = false, length = 18, unique = true)
	@Pattern(regexp = "^\\d{3}.\\d{3}.\\d{3}-\\d{2}$|^\\d{2}\\.\\d{3}\\.\\d{3}\\/\\d{4}\\-\\d{2}$")
	private String registerId;

	@OneToMany(mappedBy = "account", cascade = CascadeType.ALL,fetch = FetchType.LAZY)
	private List<Card> cards = new ArrayList<>();

	
	public Account() {
	}

	public Account(String name_owner, String registerId) {
		this.name_owner = name_owner;
		this.registerId = registerId;
	}

	public Account(Integer id, String name_owner, String agency_code, String account_code, String digit_verification,
			String registerId, List<Card> cards) {
		this.id = id;
		this.name_owner = name_owner;
		this.agency_code = agency_code;
		this.account_code = account_code;
		this.digit_verification = digit_verification;
		this.registerId = registerId;
		this.cards = cards;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName_owner() {
		return this.name_owner;
	}

	public void setName_owner(String name_owner) {
		this.name_owner = name_owner;
	}

	public String getAgency_code() {
		return this.agency_code;
	}

	public void setAgency_code(String agency_code) {
		this.agency_code = agency_code;
	}

	public String getAccount_code() {
		return this.account_code;
	}

	public void setAccount_code(String account_code) {
		this.account_code = account_code;
	}

	public String getDigit_verification() {
		return this.digit_verification;
	}

	public void setDigit_verification(String digit_verification) {
		this.digit_verification = digit_verification;
	}

	public String getRegisterId() {
		return this.registerId;
	}

	public void setRegisterId(String registerId) {
		this.registerId = registerId;
	}

	public List<Card> getCards() {
		return this.cards;
	}

	public void setCards(List<Card> cards) {
		this.cards = cards;
	}
}
