package it.model;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import it.model.enums.CardEnum;

@Entity
public class Card implements Serializable {
  private static final long serialVersionUID = 1L;
  
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;
  
  @Basic
  @Column(nullable = false, length = 50)
  private String name;
  
  @Basic
  @Column(nullable = false, length = 45)
  @Enumerated(EnumType.STRING)
  private CardEnum flag;
  
  @Basic
  @Column(nullable = false, length = 45)
  private String number;
  
  @Basic
  @Column(nullable = false, length = 5)
  private String digit_code;
  
  @Basic
  @Column(nullable = false, columnDefinition = "Double(14.2)")
  private Double limit_balance;
  
  @ManyToOne
  @JoinColumn(name = "account_id")
  @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)//Para não chamar o objeto Account dentro de card
  private Account account;
  
  @OneToOne 
  @JoinColumn(name = "type_card_id",referencedColumnName = "id" )
  private TypeCard type_card;
  
  public Card() {}
  
  public Card(Integer id, String name, CardEnum flag, String number, String digit_code, Double limit_balance, Account account, TypeCard type_card) {
    this.id = id;
    this.name = name;
    this.flag = flag;
    this.number = number;
    this.digit_code = digit_code;
    this.limit_balance = limit_balance;
    this.account = account;
    this.type_card = type_card;
  }
  
  public Integer getId() {
    return this.id;
  }
  
  public void setId(Integer id) {
    this.id = id;
  }
  
  public String getName() {
    return this.name;
  }
  
  public void setName(String name) {
    this.name = name;
  }
  
  public CardEnum getFlag() {
    return this.flag;
  }
  
  public void setFlag(CardEnum flag) {
    this.flag = flag;
  }
  
  public String getNumber() {
    return this.number;
  }
  
  public void setNumber(String number) {
    this.number = number;
  }
  
  public String getDigit_code() {
    return this.digit_code;
  }
  
  public void setDigit_code(String digit_code) {
    this.digit_code = digit_code;
  }
  
  public Double getLimit_balance() {
    return this.limit_balance;
  }
  
  public void setLimit_balance(Double limit_balance) {
    this.limit_balance = limit_balance;
  }
  
  public Account getAccount() {
    return this.account;
  }
  
  public void setAccount(Account account) {
    this.account = account;
  }
  
  public TypeCard getType_card() {
    return this.type_card;
  }
  
  public void setType_card(TypeCard type_card) {
    this.type_card = type_card;
  }
}