package it.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import it.model.Card;

@Repository
public interface CardRepository extends JpaRepository<Card, Integer> {}