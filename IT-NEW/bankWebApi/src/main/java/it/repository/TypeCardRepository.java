package it.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.model.TypeCard;

@Repository
public interface TypeCardRepository extends JpaRepository<TypeCard, Integer> {

	String findByName(TypeCard name);
}