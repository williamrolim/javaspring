package it.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import it.model.Account;
import it.model.Card;

@Repository
public interface AccountRepository extends JpaRepository<Account, Integer> {
	
	Optional<Account> findByRegisterId(String paramString);
  
  Optional<Account> findByCards_Name(String paramString);
  
//	@Query(value = "INSERT INTO Documento (tipodoc,numero) VALUES (:tipoDoc,:numero)", nativeQuery = true)
//	@Transactional
//	Documento inserirDocumento(@Param("tipoDoc") TipoDoc tipoDoc, @Param("numero") String numero);
 
  
  
  @Query(value = "SELECT * "
			 + "FROM Account a "
			  + "INNER JOIN Card c "
			  + "ON  a.id = c.account_id "
			  + "INNER JOIN Type_Card tc "
			  + "ON c.type_card_id = tc.id "
			  + "WHERE a.register_id = :registerid" , nativeQuery = true)
  	@Transactional
	  Optional<Account> findByRegisterIds(@Param("registerid") String registerid);
  
 
  @Query(value = "SELECT * "
			 + "FROM Account a "
		     + "WHERE a.register_id = :registerid", nativeQuery = true)
  @Transactional
  Optional<Account> procurarPeloNome(@Param("registerid") String registerid);

   
}
