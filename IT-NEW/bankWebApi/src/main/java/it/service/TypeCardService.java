package it.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.model.TypeCard;
import it.repository.TypeCardRepository;

@Service
public class TypeCardService {
  @Autowired
  TypeCardRepository typeCardRepository;
  
  public TypeCard createdTypeCard(TypeCard type_card) {
    return (TypeCard)this.typeCardRepository.save(type_card);
  }
}
