package it.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.controller.form.AccountAllForm;
import it.model.Account;
import it.model.Card;
import it.model.TypeCard;
import it.repository.AccountRepository;
import it.repository.TypeCardRepository;

@Service
public class AccountService {
  @Autowired
  private AccountRepository accountRepository;
  
  @Autowired
  private TypeCardRepository typeCardRepository;
  
  @Transactional
  public Account saved(Account account) {
    return this.accountRepository.save(account);
  }
  
//@Transactional
//public AccountAllForm getByRegisterIdAccount(String register_id) {
//  Optional<AccountAllForm> findByRegisterId = this.accountRepository.findByRegisterIds(register_id);
//  System.out.println("VEJAAAAAAAAAAAAAAAA " + register_id);
//  System.out.println(findByRegisterId.toString());
//  findByRegisterId.orElseThrow(() -> new RuntimeException("Account dont find"));
//  return findByRegisterId.get();
//}
 

  
  @Transactional
  public Account getByRegisterIdAccount(String register_id) {
    Optional<Account> findByRegisterId = this.accountRepository.findByRegisterIds(register_id);
    System.out.println("VEJAAAAAAAAAAAAAAAA " + register_id);
    System.out.println(findByRegisterId.toString());
    findByRegisterId.orElseThrow(() -> new RuntimeException("Account dont find"));
    return findByRegisterId.get();
  }
  
  public Page<Account> getAllAccounts(Pageable pageable) {
    Page<Account> allAccounts = this.accountRepository.findAll(pageable);
    return allAccounts;
  }
  
  @Transactional
  public Account createCardInAccount(Card cards, Integer idCard,Integer idtyperCard) {
    List<Card> listCards = new ArrayList<>();
    Optional<Account> accounts = this.accountRepository.findById(idCard);
    accounts.orElseThrow(() -> new RuntimeException("Account dont find"));
    Optional<TypeCard> type_cards = this.typeCardRepository.findById(idtyperCard);
    type_cards.orElseThrow(() -> new RuntimeException("Type Card dont find"));
    cards.setAccount(accounts.get());
    listCards.add(cards);
    ((Account)accounts.get()).setCards(listCards);
    cards.setType_card(type_cards.get());
    Account accountPersist = this.accountRepository.save(accounts.get());
    //this.typeCardRepository.save(type_cards.get());
    return accountPersist;
  }
}