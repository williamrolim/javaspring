package it.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.model.Card;
import it.model.TypeCard;
import it.repository.CardRepository;
import it.repository.TypeCardRepository;

@Service
public class CardService {
  @Autowired
  CardRepository cardRepository;
  
  @Autowired
  TypeCardRepository typeCardRepository;
  
  public Card createTypeCardInCardAssocietedId(TypeCard type_card, Integer id) {
   // List<Type_Card> listTypeCards = new ArrayList<>();
   Optional<Card> cards = this.cardRepository.findById(id);
    Optional<TypeCard> typeCards = this.typeCardRepository.findById(id);
    typeCards.orElseThrow(() -> new RuntimeException("Card dont find"));
    //type_card.setCards(cards.get());
    //listTypeCards.add(type_card);
    Card cardPersist = (Card)this.cardRepository.save(cards.get());
    return cardPersist;
  }
}