package java8.novosrecursos;

	import java.util.ArrayList;
	import java.util.Comparator;
	import java.util.List;
	import java.util.function.Consumer;

	public class OrdenaStrings03 {

		public static void main(String[] args) {
			List<String> palavras = new ArrayList<String>();
			palavras.add("alura");
			palavras.add("editora casa do codigo");
			palavras.add("caelum");

			Comparator<String> comparador = new ComparadorPorTamanho();

			palavras.sort(comparador);
			System.out.println(palavras);

			/*Consumer forma nova de interar em todos os elementos de uma cole��o*/
			Consumer<String> consumidor = new ImprimeNaLinha();
			palavras.forEach(consumidor);

		}

		static class ImprimeNaLinha implements Consumer<String> {

			@Override
			public void accept(String s) {
				System.out.println(s);
			}

		}

		static class ComparadorPorTamanho implements Comparator<String> {

			@Override
			public int compare(String s1, String s2) {
				if (s1.length() < s2.length()) {
					return -1;
				}
				if (s1.length() > s2.length()) {
					return 1;
				}
				return 0;
			}
		}
}
