package java8.novosrecursos;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;

public class OrdenaStrings05 {

	public static <T> void main(String[] args) {
		List<String> palavras = new ArrayList<String>();
		palavras.add("alura");
		palavras.add("editora casa do codigo");
		palavras.add("caelum");
		
		//Verificando qual � a palavra maior
		
		//0FormaDeFazer ambas com o mesmo resultado
		palavras.sort(Comparator.comparing(String::length));

		//1FormaDeFazer ambas com o mesmo resultado
		palavras.sort(Comparator.comparing(s -> s.length()));
		
		System.out.println(palavras);
		
		//2Forma de fazer ambas com o mesmo resultado
		Function<String, Integer> funcao = s -> s.length();
		Comparator<String> comparador = Comparator.comparing(funcao);
		palavras.sort(comparador);
		
		System.out.println(palavras);
		
		//3Forma  de fazer ambas com o mesmo resultado
		Function<String, Integer> funcao2 = new Function<String, Integer>() {

			@Override
			public Integer apply(String s) {
				return s.length();
			}
		};
		Comparator<String> comparador2 = Comparator.comparing(funcao2);
		palavras.sort(comparador2);
		
		System.out.println(palavras);
		
		
		Consumer<String>impressor =  System.out::println;
		palavras.forEach(impressor);
		
		palavras.forEach(System.out::println);
		
		palavras.sort(Comparator.comparing(s -> s.length()));
		//ou
		palavras.sort(Comparator.comparing(String::length));
		//ou
		palavras.sort(String.CASE_INSENSITIVE_ORDER);
		
		palavras.forEach(System.out::println);
		

	}


}
