package java8.novosrecursos;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Consumer;

public class OrdenaStrings02 {

	public static void main(String[] args) {
		List<String> palavras = new ArrayList<String>();
		palavras.add("alura");
		palavras.add("editora casa do codigo");
		palavras.add("caelum");

		Comparator<String> comparador = new ComparadorPorTamanho();

		palavras.sort(comparador);
		System.out.println(palavras);

		// para cada strings dentro de palavra fa�a "system out println
		for (String p : palavras) {
			System.out.println(p);
		}

		Consumer<String> consumidor = null;
		palavras.forEach(consumidor);

	}


	static class ComparadorPorTamanho implements Comparator<String> {

		@Override
		public int compare(String s1, String s2) {
			if (s1.length() < s2.length()) {
				return -1;
			}
			if (s1.length() > s2.length()) {
				return 1;
			}
			return 0;
		}
	}
}
