package java8.novosrecursos;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class Curso02 {
	private String nome;
	private int alunos;

	public Curso02(String nome, int alunos) {

		this.nome = nome;
		this.alunos = alunos;
	}

	public String getNome() {
		return nome;
	}

	public int getAlunos() {
		return alunos;
	}

//		@Override
//		public String toString() {
//			return "Curso [nome=" + nome + ", alunos=" + alunos + "]";
//		}

}

public class AlunosStreams02 {

	public static void main(String[] args) {
		List<Curso> cursos = new ArrayList<Curso>();
		cursos.add(new Curso("Python", 45));
		cursos.add(new Curso("Javascript",150));
		cursos.add(new Curso("Java8",113));
		cursos.add(new Curso("C", 55));
		
		/*Optional permite que trabalhamos com referencias
		 * sem ter que trabalhar com varios ifs
		 * ifs = null, ifs = null
		 * Optional = pode ser que ele existe e pode ser que ele n�o exista
		 */

		Optional<Curso> optionalCurso = cursos.stream()
		.filter(c -> c.getAlunos() >= 100)
		.findAny();
		
		Curso curso = optionalCurso.orElse(null);
		System.out.println(curso.getNome());
		
		//Se existir esse curso com mais de 100 imprima se n�o, n�o faz nada
		optionalCurso.ifPresent(c -> System.out.println(c.getNome()));
		
		System.out.println("\n");
		//ou
		cursos.stream()
		.filter(c -> c.getAlunos() >= 100)
		.findAny()
		.ifPresent(c -> System.out.println(c.getNome()));
		
		OptionalDouble media = cursos.stream()
		.filter(c -> c.getAlunos() >= 0)
		.mapToInt(Curso::getAlunos)
		.average();
		
		System.out.println("Media total dos cursos..: " + media);
		
		//atrav�s de uma Collector guardo o resultado em uma lista
		List<Curso> resultado = cursos.stream()
				.filter(c -> c.getAlunos() >= 100)
				.collect(Collectors.toList());
		
		cursos.stream()
		.forEach(c -> System.out.println(c.getNome()));
		
		Map<String, Integer> mapa = cursos.stream()
		.filter(c -> c.getAlunos() >= 100)//map, chave e valor , leitura: dado curso quero guardar o nome de alunos
		.collect(Collectors.toMap(c -> c.getNome(), c -> c.getAlunos())); // TO MAP TEM QUE ESPECIFICAR QUAL � A CHAVE E QUAL � O VALOR
        // to map recebe 2 chaves, dado curso qual vai ser a chave, dado curso qual vai ser o valor
		
		System.out.println(mapa);
		
		//ou
		
		cursos.stream()
		.filter(c -> c.getAlunos() >= 100)
		.collect(Collectors.toMap(
				c -> c.getNome(), c -> c.getAlunos()))
		.forEach((nome,alunos) -> System.out.println(nome  + " tem " + alunos + " alunos"));
       
		//ou
		cursos.parallelStream()//parallelStream = roda em paralelo, ele usou um pull de threads de acordo com o tamanho, a quantidade de p
		//processadores que tem no compudador, fez, juntou, fez um map reduce, se tiver um bilh�o de elementos, tirar proveito desse recurso
		.filter(c -> c.getAlunos() >= 100)
		.collect(Collectors.toMap(
				c -> c.getNome(), c -> c.getAlunos()))
		.forEach((nome,alunos) -> System.out.println(nome  + " tem " + alunos + " alunos"));
		
		/*Na aula anterior fizemos um filtro para pegar todos os cursos com mais de 50 alunos:

			cursos.stream()
			   .filter(c -> c.getAlunos() > 50)COPIAR C�DIGO
			Como podemos fazer pra pegar o primeiro elemento desse Stream filtrado?*/

				cursos.stream()
			   .filter(c -> c.getAlunos() > 50).findFirst()
			   .ifPresent(c -> System.out.println(c.getNome()));
				
				/*Calcule a quantidade m�dia de alunos de todos os seus cursos utilizando a API de Stream.*/
				OptionalDouble mediaCursos =  cursos.stream()
			    .mapToInt(c -> c.getAlunos())
			    .average();
				
				System.out.println(mediaCursos);
				
				/*Depois de filtrar todos os cursos com mais de 50 alunos, temos um Stream<Curso> como resultado:

				Stream<Curso> stream = cursos.stream()
   				.filter(c -> c.getAlunos() > 50);COPIAR C�DIGO
				Como podemos transformar esse Stream<Curso> filtrado em uma List<Curso>?*/
				
				List<Curso> cursosFiltrados = cursos.stream()
						   .filter(c -> c.getAlunos() > 50)
						   .collect(Collectors.toList());
				
	}
}
