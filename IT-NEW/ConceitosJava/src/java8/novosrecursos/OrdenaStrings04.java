package java8.novosrecursos;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Consumer;

public class OrdenaStrings04 {

	public static <T> void main(String[] args) {
		List<String> palavras = new ArrayList<String>();
		palavras.add("alura");
		palavras.add("editora casa do codigo");
		palavras.add("caelum");

//		palavras.sort((s1, s2) -> {
//			return Integer.compare(s1.length(), s2.length());
//		});
		palavras.sort((s1, s2) -> Integer.compare(s1.length(), s2.length()));
	
		System.out.println(palavras);
		
		Consumer<String>impressor = s -> System.out.println(s);
		palavras.forEach(impressor);
		
		palavras.sort((s1, s2) -> s1.length() - s2.length());
		
		System.out.println(palavras);
		
		new Thread(new Runnable() {

		    @Override
		    public void run() {
		        System.out.println("Executando um Runnable");
		    }

		}).start();
		
		new Thread(() -> System.out.println("Executando um Runnable")).start();
		
		//O codigo abaixo n�o funciona pois object n�o � uma interface funcional 
//		Object ob = s -> System.out.println(s);

		/* Consumer forma nova de interar em todos os elementos de uma cole��o */
		/*
		 * Consumer n�o se poder dar new em uma interface, pois falta informa��o s� pode
		 * dar new quando fornecer os metodos de implementa��o
		 */
//		Consumer<String> consumidor = new Consumer<String>() {
//			public void accept(String s) {
//				System.out.println(s);
//				}
//			};
//			
//		palavras.forEach(consumidor);

//		palavras.forEach((String s) -> {
//			System.out.println(s);
//		});

//		palavras.forEach((s) -> { //N�o precisa inferir o tipo, pois ele j� compreende
//			System.out.println(s);
//		});

		// para cada palavra s system out println s
	}


}