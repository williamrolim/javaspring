package java8.novosrecursos;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Stream;

class Curso {
	private String nome;
	private int alunos;
	
	public Curso(String nome, int alunos) {
	
		this.nome = nome;
		this.alunos = alunos;
	}

	public String getNome() {
		return nome;
	}

	public int getAlunos() {
		return alunos;
	}

//	@Override
//	public String toString() {
//		return "Curso [nome=" + nome + ", alunos=" + alunos + "]";
//	}

	

}

public class AlunosStreams {

	public static void main(String[] args) {
		List<Curso> cursos = new ArrayList<Curso>();
		cursos.add(new Curso("Python", 45));
		cursos.add(new Curso("Javascript", 150));
		cursos.add(new Curso("Java8", 113));
		cursos.add(new Curso("C", 55));

		// cursos.sort(Comparator.comparing(c -> c.getAlunos()));
		cursos.sort(Comparator.comparing(Curso::getAlunos));

		cursos.forEach(c -> System.out.println(c.getNome()));
		System.out.println("Curso com valor de mais 100");
		cursos.stream()
		.filter(c -> c.getAlunos() >= 100)
		.forEach(c -> System.out.println(c.getNome()));// stream :um
																// fluxo uma
																	// objetos -
																// STREAM � UMA
															// INFERAFACE
		// evitam problema de concorrencia, trabalhar em paralelo com ele

		// Quero uma sequencia de n�meros inteiros que simbolizam o tal de alunos nesse
		// curso

		/* quais s�o os maiores que 100 */
		System.out.println("Maiores cursos maiores que 100");
		cursos.stream().filter(c -> c.getAlunos() >= 100).map(c -> c.getAlunos())
				.forEach(total -> System.out.println(total));

		// ou
		System.out.println("Maiores cursos maiores que 100");

		cursos.stream().filter(c -> c.getAlunos() >= 100).map(c -> c.getAlunos()).forEach(System.out::println);

		// ou
		cursos.stream().filter(c -> c.getAlunos() >= 100).map(Curso::getAlunos).forEach(System.out::println);

		// Somando Todos
		cursos.stream().filter(c -> c.getAlunos() >= 100).mapToInt(Curso::getAlunos).sum();

		// Como voc� faria pra ordenar essa lista pela quantidade de alunos?
		System.out.println("Ordenando pela quantidade de alunos");
		cursos.sort(Comparator.comparingInt(Curso::getAlunos));
		cursos.forEach(c -> System.out.println(c.getAlunos() ));
		
		/*Utilizando a API de Stream, crie um filtro para todos os cursos que tenham mais de 50 alunos.

		Depois disso fa�a um forEach no resultado. Qual foi a sa�da do seu c�digo?*/
		
		System.out.println("Cursos que tenham mais de 50 alunos.");
		cursos.stream().filter(c -> c.getAlunos() > 50)
		.map(Curso::getNome)
		.forEach(System.out::println);
		
		/*Como transformar o nosso Stream<Curso> em um Stream<String>
		 *  contendo apenas os nomes dos cursos?*/
		Stream<String> nomes = cursos.stream().map(Curso::getNome);
			System.out.println(nomes);

	}
}
