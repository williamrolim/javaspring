package java8.novosrecursos;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.Period;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;

public class Datas {
public static void main(String[] args) {
	LocalDate hoje = LocalDate.now();
	System.out.println(hoje);
	
	LocalDate copaDoMundo = LocalDate.of(2022, Month.NOVEMBER, 21);
	
	//Quanto tempo falta para copaDo mundo 
	
	Period periodo = Period.between(hoje, copaDoMundo);
	System.out.println(periodo.getMonths());
	
	LocalDate proximaCopaDoMundo = copaDoMundo.plusYears(4);
	System.out.println(proximaCopaDoMundo);
	
	DateTimeFormatter formatadorDatas = DateTimeFormatter.ofPattern("dd/MM/yyyy");
	
	//pegar a compa do mundo e formatar usando o formatador de datas que acabamos de criar
	String valorFormatado = proximaCopaDoMundo.format(formatadorDatas);
	
	System.out.println(valorFormatado);
	
	DateTimeFormatter formatadorDatasComHoras = DateTimeFormatter.ofPattern("dd/MM/yyyy hh:mm");
	
	LocalDateTime agora = LocalDateTime.now();
	System.out.println(agora.format(formatadorDatasComHoras));
	
	LocalTime intervalo = LocalTime.of(8, 15);
	System.out.println(intervalo);
	
	LocalDate agora2 = LocalDate.now();
	LocalDate dataFutura = LocalDate.of(2099, Month.JANUARY, 25);

	Period periodo2 = Period.between(agora2, dataFutura);
	//intervalo.isAfter(intervalo)
	//YearMonth
	//lOCALtIME
	}
}
