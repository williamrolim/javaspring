package collections;

public class TestaCursoComAluno {
	public static void main(String[] args) {
		Curso2 javaColecoes = new Curso2("Dominando as colecoes do Java", "William Rolim");

		javaColecoes.adiciona(new Aula("Trabalhando com ArrayList", 21));
		javaColecoes.adiciona(new Aula("Criando uma Aula", 20));
		javaColecoes.adiciona(new Aula("Modelando com colecoes", 24));

		Aluno a1 = new Aluno("Rodrigo Turini", 34672);
		Aluno a2 = new Aluno("William Rolim", 32123);
		Aluno a3 = new Aluno("Maria Barbosa", 43531);

		javaColecoes.matricula(a1);
		javaColecoes.matricula(a2);
		javaColecoes.matricula(a3);

		System.out.println("Todos os alunos matriculados");

		javaColecoes.getAlunos().forEach(a -> {
			System.out.println(a);
		});
		
		System.out.println("O aluno " + a1 + " esta matriculado?");
		System.out.println(javaColecoes.estaMatriculado(a1));
		
		Aluno william = new Aluno("William Rolim" ,32123);
		System.out.println(javaColecoes.estaMatriculado(william));
		
		System.out.println(a2.equals(william));

		
	}
}
