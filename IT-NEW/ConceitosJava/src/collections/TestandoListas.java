package collections;

import java.util.ArrayList;
import java.util.Collections;

public class TestandoListas {
 public static void main(String[] args) {
	String aula1  = "Conhecendo mais de listas";
	String aula2 = "Modelando a classe Aula";
	String aula3 = "Trabalhando com cursos";
	
	//ARRAY LIST TIPO GENERICO
	ArrayList<String> aulas = new ArrayList<>();
	
	aulas.add(aula1);
	aulas.add(aula2);
	aulas.add(aula3);

	System.out.println(aulas);
	
	aulas.remove(0);
	
	System.out.println(aulas);
	
	//para cada string aula dentro aulas fa�a o seguinte
	for (String aula : aulas) {
		System.out.println("Aula :" + aula);
	}
	
	String primeiraAula = aulas.get(0);
	System.out.println("A primeira aula � ..: " + primeiraAula);
	
	//cole��es sempre size
	
	for (int i = 0; i < aulas.size(); i++) {
		System.out.println("aula " + aulas.get(i));
	}
	
	//bounds limites
	System.out.println(aulas.size());
	
	aulas.forEach(aula -> 
	{System.out.println("percorrendo : " + aula);});
	
	
	aulas.add("Aumentando nosso conhecimento");
	
	System.out.println(aulas + "\n");
	
	System.out.println("\nDepois de ordenada");
	//alterar de acordo com a string lexografica na ordem alfabetica
	Collections.sort(aulas);
	System.out.println(aulas);

}
}
