package collections;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.TreeSet;

public class Curso2 {
	
	
	private String nome;
	private String instrutor;
	private List<Aula>aulas = new LinkedList<Aula>();
	private Set<Aluno> alunos = new HashSet<>(); //HashSet n�o guarda a ordem do conjunto de elementos inseridos
	//private Set<Aluno> alunos = new LinkedHashSet<>();//LinkedHashSet guarda a ordem dos elementos
	//private Set<Aluno> alunos = new TreeSet<>();

	private Map<Integer, Aluno> matriculaParaAluno = new HashMap<Integer, Aluno>();
	
	public Curso2(String nome, String instrutor) {
		this.nome = nome;
		this.instrutor = instrutor;
	}

	public String getNome() {
		return nome;
	}
	
	public String getInstrutor() {
		return instrutor;
	}
	
	public List<Aula>getAulas(){
		return Collections.unmodifiableList(aulas);
	}
	public void adiciona (Aula aula) {
		this.aulas.add(aula);
	}
	
//	public int getTempoTotal() {
//		int tempoTotal = 0;
//		for (Aula aula : aulas) {
//			tempoTotal += aula.getTempo();
//		}
//		return tempoTotal;
//	}
	
	public int getTempoTotal() {
		//stream
		return this.aulas.stream().mapToInt(Aula::getTempo).sum();
	}


	public void matricula(Aluno aluno) {
		this.alunos.add(aluno);
		this.matriculaParaAluno.put(aluno.getNumeroMatricula(), aluno);
	}
	
	public Set<Aluno> getAlunos() {
		return Collections.unmodifiableSet(alunos);
	}

	public boolean estaMatriculado(Aluno a1) {
		return this.alunos.contains(a1);
	}
	
	public Aluno buscaMatriculado(int matricula) {
		return matriculaParaAluno.get(matricula);
		
	}
	
	@Override
	public String toString() {
		return "[Curso: =" + nome + ", tempo total =" + this.getTempoTotal() + " aulas : " + this.aulas + "]";
	}

//	public Aluno buscaMatriculado(int matricula) {
//		for (Aluno aluno : alunos) {
//			if (aluno.getNumeroMatricula() == matricula) {
//				return aluno;
//			}
//		}
//		throw new NoSuchElementException("Aluno n�o encontrado" + matricula);
//		
//	}
	
}
