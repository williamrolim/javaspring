package collections;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class TestaAlunos {
	public static void main(String[] args) {
		Set<String> alunos = new HashSet<String>();
		
		alunos.add("William Rolim");
		alunos.add("Rodrigo Rolim");
		alunos.add("Jacqueline Santiago");
		alunos.add("Ivanira Rolim");
		alunos.add("Tancredo Neves");
		alunos.add("Macavilson Sousa");
		alunos.add("William Rolim");
		
		System.out.println(alunos.size());
		
		boolean ivaniraEstaMatriculada = alunos.contains("Ivanira Rolim");
		
		alunos.remove("Tancredo Neves");
		
		System.out.println(ivaniraEstaMatriculada);
		
		for (String aluno : alunos) {
			System.out.println(aluno);
		}
		
		alunos.forEach( aluno -> {
			System.out.println(alunos);
		});
		
		System.out.println(alunos.toString());
		
		List<String>alunoEmlista = new ArrayList<>(alunos);
		
		
	}
}
