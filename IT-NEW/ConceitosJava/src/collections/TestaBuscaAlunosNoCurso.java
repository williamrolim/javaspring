package collections;

public class TestaBuscaAlunosNoCurso {
	public static void main(String[] args) {
		Curso2 javaColecoes = new Curso2("Dominando as colecoes do Java", "William Rolim");

		javaColecoes.adiciona(new Aula("Trabalhando com ArrayList", 21));
		javaColecoes.adiciona(new Aula("Criando uma Aula", 20));
		javaColecoes.adiciona(new Aula("Modelando com colecoes", 24));

		Aluno a1 = new Aluno("Rodrigo Turini", 34672);
		Aluno a2 = new Aluno("William Rolim", 32123);
		Aluno a3 = new Aluno("Maria Barbosa", 43531);

		javaColecoes.matricula(a1);
		javaColecoes.matricula(a2);
		javaColecoes.matricula(a3);
		
		System.out.println("Quem � o aluno matricula 32123 ?");
		Aluno aluno = javaColecoes.buscaMatriculado(32123);
		System.out.println("Aluno..: " + aluno);
	}
}
