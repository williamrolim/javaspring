package collections;

import java.util.LinkedHashMap;
import java.util.Map;

public class MapTest {
public static void main(String[] args) {
	Map<String, String> map = new LinkedHashMap<>();
	
	map.put("teckado", "teclado");
	map.put("mesa", "mesona");
	map.put("teste", "teste2");
	map.put("quadro", "monalisa");
	
	
	for(Map.Entry<String, String> entry : map.entrySet()){
		System.out.println(entry.getKey() + " " + entry.getValue());
	}
}
}
