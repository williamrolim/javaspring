package collections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class TestaListaDeAula {
	public static void main(String[] args) {
	
		Aula a1 = new Aula("Revistando as  ArrayLists", 21);
		Aula a2 = new Aula("Listas de objetos", 15);
		Aula a3 = new Aula("Relacionamento de listas e objetos", 15);
		
		ArrayList<Aula> aulas = new ArrayList<>();
		
		aulas.add(a1);
		aulas.add(a2);
		aulas.add(a3);
		
		System.out.println(aulas.toString());
		
		String s1 = "William";
		String s2 = "Mailliw";
		String s3 = "Wagner";
		System.out.println(s1.compareTo(s2));//quem � maior - se o s1 for menor ddevolve um n�mero negativo , se o s2 for maior positivo
	
		Collections.sort(aulas);
		
		//n�o estou invocado metodo estou pegando aquele metodo e comparando 
		Collections.sort(aulas, Comparator.comparing(Aula::getTempo));
		
		System.out.println(aulas);
		
	    aulas.sort(Comparator.comparing(Aula::getTempo));
		System.out.println(aulas);

	}
}
