package collections;

import java.util.Iterator;
import java.util.Set;

public class TestaCursoComAluno2 {
public static void main(String[] args) {
	Curso2 javaColecoes = new Curso2("Dominando as colecoes do Java", "William Rolim");

	javaColecoes.adiciona(new Aula("Trabalhando com ArrayList", 21));
	javaColecoes.adiciona(new Aula("Criando uma Aula", 20));
	javaColecoes.adiciona(new Aula("Modelando com colecoes", 24));

	Aluno a1 = new Aluno("Rodrigo Turini", 34672);
	Aluno a2 = new Aluno("William Rolim", 32123);
	Aluno a3 = new Aluno("Maria Barbosa", 43531);

	javaColecoes.matricula(a1);
	javaColecoes.matricula(a2);
	javaColecoes.matricula(a3);

	System.out.println("Todos os alunos matriculados");

	Set<Aluno> alunos = javaColecoes.getAlunos();
	Iterator<Aluno> iterator = alunos.iterator();
	
	//iterator.hasNext(); tem um proximo elemento para retirar da sacola.hasNext();
	//iterator.next(); se tem me devolve o proximo next();
	
	while(iterator.hasNext()) {
		Aluno proximo = iterator.next();
		System.out.println(proximo);
	}
	
	// iterator.next()
	System.out.println("depois do ultimo");
	alunos.iterator();
	Aluno depoisDoUltimo = iterator.next();
	System.out.println(depoisDoUltimo);
}
}
