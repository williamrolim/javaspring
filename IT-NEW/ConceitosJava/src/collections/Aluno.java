package collections;

public class Aluno {

	private String nome;
	private int numeroMatricula;
	
	
	public Aluno(String nome, int numeroMatricula) {
		if (nome == null) {
			throw new NullPointerException("Nome n�o pode ser null");
		}
		this.nome = nome;
		this.numeroMatricula = numeroMatricula;
	}
	
	public String getNome() {
		return nome;
	}

	public int getNumeroMatricula() {
		return numeroMatricula;
	}

	@Override
	public boolean equals(Object obj) {
		Aluno outro = (Aluno) obj;
		return this.nome.equals(outro.nome);
	}
	// Se fosse reescreve o metodo equals precisa reescrever o hashcode
	@Override
	public int hashCode() {//haschode por default identificador unico do objeto
						// n�mero magico que guarda o identificador do objeto
		return this.nome.hashCode();//se tenho dois objetos identicos eles precisam ter o mesmo codigo de espalhamento
	}
	@Override
	public String toString() {
		return "Aluno [nome=" + nome + ", numeroMatricula=" + numeroMatricula + "]";
	}
	
	
	
	
}
