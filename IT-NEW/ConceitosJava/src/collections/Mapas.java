package collections;

import java.util.HashMap;

public class Mapas {
	public static void main(String[] args) {
		HashMap<String, Integer> empIds = new HashMap<>();
		
		empIds.put("William", 1111);
		empIds.put("Amanda", 2222);
		empIds.put("Creuza", 3333);
		
		System.out.println(empIds);
		
		System.out.println(empIds.get("William"));
		
		System.out.println(empIds.containsKey("Amanda"));
		
		System.out.println(empIds.containsValue(3333));
		
		empIds.put("Joao", 4444);
		
		System.out.println(empIds);
		
		empIds.replace("William", 0000);
		
		System.out.println(empIds);
		
		empIds.putIfAbsent("Steve", 1111);
		System.out.println(empIds);


		empIds.put("ROLA", 69696);

		System.out.println(empIds);
		
		empIds.put("BUCETA", 69696);
		System.out.println(empIds);
		
		

	}
}
