package javafile.input.output;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;

public class JavaFileIO {

	public static void main(String[] args) {
		String[] nomes = {"William", "Ana", "Maria", "Tancredo"};
		try {
			BufferedWriter escrever = new BufferedWriter(new FileWriter("C:\\Users\\Administrador\\Downloads\\CITI-EXPRESS\\JAVAIO\\nomes.txt"));
			escrever.write("Escrevendo no arquivo");
			escrever.write("\nEscrevendo em outra linha");
			
			for (String nome : nomes) {
				escrever.write("\n" + nome);
			}
			escrever.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try {
			BufferedReader leitura = new BufferedReader(new InputStreamReader(new FileInputStream("C:\\Users\\Administrador\\Downloads\\\\CITI-EXPRESS\\JAVAIO\\nomes.txt"), "UTF-8"));
			String linha;
			
			while ((linha = leitura.readLine())!= null) {
				System.out.println(linha);
			}
			leitura.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
