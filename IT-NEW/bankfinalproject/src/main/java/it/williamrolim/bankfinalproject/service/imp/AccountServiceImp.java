package it.williamrolim.bankfinalproject.service.imp;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import it.williamrolim.bankfinalproject.exceptions.ErrorHandling;
import it.williamrolim.bankfinalproject.model.Account;
import it.williamrolim.bankfinalproject.model.requestDTO.AccountRequestDTO;
import it.williamrolim.bankfinalproject.repository.AccontRepository;
import it.williamrolim.bankfinalproject.repository.CardRepository;
import it.williamrolim.bankfinalproject.service.AccountService;
import it.williamrolim.bankfinalproject.service.CardService;

@Service
public class AccountServiceImp implements AccountService{

	@Autowired
	AccontRepository accontRepository;

	@Autowired
	CardService cardService;
	
	@Autowired
	CardRepository cardRepository;
	
	@Transactional
	@Override
	public Account insertAccount(AccountRequestDTO accountRequestDTO) {
		Account account = new Account();
		account.setNameOwner(accountRequestDTO.getNameOwner());
		account.setAgencyCode(accountRequestDTO.getAgencyCode());
		account.setAccountCode(accountRequestDTO.getAccountCode());
		account.setDigitVerification(accountRequestDTO.getDigitVerification());
		account.setRegisterId(accountRequestDTO.getRegisterId());
		
//		List<Card> cards = account.getCard();
//		
//		for (Card card: cards) {
//			cardRepository.save(card);
//		}
		
		try {
			if (account != null) 
				accontRepository.save(account);
		} catch (Exception e) {
			throw new IllegalArgumentException("Error: Verifique os campos");
		}
		return account;
	}

	@Override
	public Page<Account> getAllAccounts(Pageable page) {
        Page<Account> AllAccounts = accontRepository.findAll(page);
        return AllAccounts;

	}

	@Override
	public Account getAccountById(Integer accountId) {
		
		try {
			if (accontRepository.findById(accountId) == null)
				 accontRepository.deleteById(accountId);
		} catch (Exception e) {
			throw new ErrorHandling("Cannot delete account with linked cards");
		}
		Account account = accontRepository.findById(accountId).orElseThrow(() ->
        new IllegalArgumentException(
                "account with id: " + accountId + " could not be found"));
		
        return account;
}
	

	@Override
	public Account getAccountByRegisterId(String registerId) {
		   return accontRepository.findByRegisterId(registerId).orElseThrow(() ->
	        new IllegalArgumentException(
	                "account with id: " + registerId + " could not be found"));
		   
	}

	@Override
	public Account deleteAccount(Integer accountId) {
		Account account = getAccountById(accountId);
		

        return  account;
	}

	@Override
	public Account updateAccount(Integer accountId, AccountRequestDTO accountRequestDTO) {

		Account accountUpdate = getAccountById(accountId);
		accountUpdate.setNameOwner(accountRequestDTO.getNameOwner());
		accountUpdate.setAgencyCode(accountRequestDTO.getAgencyCode());
		accountUpdate.setAccountCode(accountRequestDTO.getAccountCode());
		accountUpdate.setDigitVerification(accountRequestDTO.getDigitVerification());
        if (accountId != null || accountRequestDTO.getRegisterId() != null) {
            return accountUpdate;
        }
        return accountUpdate;
	}

	@Override
	public Account addCardToAccount(Long accountId, Long cardId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Account removeCardFromAccount(Long cardId) {
		// TODO Auto-generated method stub
		return null;
	}

}
