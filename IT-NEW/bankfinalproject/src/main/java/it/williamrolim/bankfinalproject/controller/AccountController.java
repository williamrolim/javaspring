package it.williamrolim.bankfinalproject.controller;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import it.williamrolim.bankfinalproject.exceptions.ErrorHandling;
import it.williamrolim.bankfinalproject.model.Account;
import it.williamrolim.bankfinalproject.model.Card;
import it.williamrolim.bankfinalproject.model.requestDTO.AccountRequestDTO;
import it.williamrolim.bankfinalproject.service.AccountService;

@RestController
@RequestMapping("/account")
public class AccountController {
	
	@Autowired
	AccountService accountService;
	
    @PostMapping
    public ResponseEntity<Account> insertAccount(@RequestBody final AccountRequestDTO accountRequestDTO) {
    	Account account = accountService.insertAccount(accountRequestDTO);
        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(account.getId())
                .toUri();
        return ResponseEntity.created(location).build();
    }
    
    @GetMapping("/{accountId}")
    public ResponseEntity<Account> getAccountById(@PathVariable final Integer accountId) {
    	Account acccount = accountService.getAccountById(accountId);
        return new ResponseEntity<>(acccount, HttpStatus.OK);
    }

    @GetMapping("/getAll")
    public ResponseEntity<Page<Account>> getAllAccounts(Pageable pageable) {
    	Page<Account> acccount = accountService.getAllAccounts(pageable);
        return new ResponseEntity<>(acccount, HttpStatus.OK);
    }

    @DeleteMapping("/delete/{accountId}")
    public ResponseEntity<Account> deleteAccount(@PathVariable final Integer accountId) {
    	Account account = accountService.deleteAccount(accountId);
     return new ResponseEntity<>(account, HttpStatus.OK);
    }

    @PostMapping("/update/{accountId}")
    public ResponseEntity<Account> updateAccount(@RequestBody final AccountRequestDTO accountRequestDTO,
                                               @PathVariable final Integer accountId) {
    	Account account = accountService.updateAccount(accountId, accountRequestDTO);
        return new ResponseEntity<>(account, HttpStatus.OK);
    }
//
//    @PostMapping("/addCity/{cityId}/toZipcode/{zipcodeId}")
//    public ResponseEntity<Zipcode> addCity(@PathVariable final Long cityId,
//                                           @PathVariable final Long zipcodeId) {
//        Zipcode zipcode = zipcodeService.addCityToZipcode(zipcodeId, cityId);
//        return new ResponseEntity<>(zipcode, HttpStatus.OK);
//    }
//
//    @PostMapping("/deleteCity/{zipcodeId}")
//    public ResponseEntity<Zipcode> deleteCity(@PathVariable final Long zipcodeId) {
//        Zipcode zipcode = zipcodeService.removeCityFromZipcode(zipcodeId);
//        return new ResponseEntity<>(zipcode, HttpStatus.OK);
//    }

}
