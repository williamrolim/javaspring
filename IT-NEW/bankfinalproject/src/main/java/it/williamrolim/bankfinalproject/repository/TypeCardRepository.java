package it.williamrolim.bankfinalproject.repository;

import org.springframework.data.repository.CrudRepository;

import it.williamrolim.bankfinalproject.model.TypeCard;

public interface TypeCardRepository extends CrudRepository<TypeCard, Integer> {
	
}
