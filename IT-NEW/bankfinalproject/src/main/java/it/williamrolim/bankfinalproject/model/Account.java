package it.williamrolim.bankfinalproject.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;

@Entity(name = "account")
public class Account implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Basic
	@Column(nullable = false, length = 50)
	@NotEmpty
	@NotNull
	@Length(max = 50)
	private String nameOwner;

	@Basic
	@Column(nullable = false, length = 4)
	private String agencyCode;

	@Basic
	@Column(nullable = false, length = 8)
	private String accountCode;

	@Basic
	@Column(nullable = false, length = 3)
	private String digitVerification;

	@Basic
	@Column(nullable = false, length = 18, unique = true)
	@Pattern(regexp = "^\\d{3}.\\d{3}.\\d{3}-\\d{2}$|^\\d{2}\\.\\d{3}\\.\\d{3}\\/\\d{4}\\-\\d{2}$")
	private String registerId;

	 @OneToMany(mappedBy = "account", cascade = CascadeType.ALL)
	private List<Card> card;
	
	//fetch = FetchType.LAZYnão tinha
	public Account() {
		
	}

	public Account(Integer id, @NotEmpty @NotNull @Length(max = 50) String nameOwner, String agencyCode,
			String accountCode, String digitVerification,
			@Pattern(regexp = "^\\d{3}.\\d{3}.\\d{3}-\\d{2}$|^\\d{2}\\.\\d{3}\\.\\d{3}\\/\\d{4}\\-\\d{2}$") String registerId,
			List<Card> card) {
		this.id = id;
		this.nameOwner = nameOwner;
		this.agencyCode = agencyCode;
		this.accountCode = accountCode;
		this.digitVerification = digitVerification;
		this.registerId = registerId;
		this.card = card;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNameOwner() {
		return nameOwner;
	}

	public void setNameOwner(String nameOwner) {
		this.nameOwner = nameOwner;
	}

	public String getAgencyCode() {
		return agencyCode;
	}

	public void setAgencyCode(String agencyCode) {
		this.agencyCode = agencyCode;
	}

	public String getAccountCode() {
		return accountCode;
	}

	public void setAccountCode(String accountCode) {
		this.accountCode = accountCode;
	}

	public String getDigitVerification() {
		return digitVerification;
	}

	public void setDigitVerification(String digitVerification) {
		this.digitVerification = digitVerification;
	}

	public String getRegisterId() {
		return registerId;
	}

	public void setRegisterId(String registerId) {
		this.registerId = registerId;
	}

	public List<Card> getCard() {
		return card;
	}

	public void setCard(List<Card> card) {
		this.card = card;
	}
	
	
	



}
