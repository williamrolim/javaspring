package it.williamrolim.bankfinalproject.repository;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import it.williamrolim.bankfinalproject.model.Account;

@Repository
public interface AccontRepository extends CrudRepository<Account, Integer> {

	Optional<Account> findByRegisterId(String registerId);

	Page<Account> findAll(Pageable page);



}
