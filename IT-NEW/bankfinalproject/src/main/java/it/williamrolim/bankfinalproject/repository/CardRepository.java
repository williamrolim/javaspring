package it.williamrolim.bankfinalproject.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import it.williamrolim.bankfinalproject.model.Card;

@Repository
public interface CardRepository extends CrudRepository<Card, Integer> {

}
