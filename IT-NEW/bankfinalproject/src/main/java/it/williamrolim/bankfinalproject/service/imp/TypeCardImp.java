package it.williamrolim.bankfinalproject.service.imp;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import it.williamrolim.bankfinalproject.model.Account;
import it.williamrolim.bankfinalproject.model.TypeCard;
import it.williamrolim.bankfinalproject.model.requestDTO.AccountRequestDTO;
import it.williamrolim.bankfinalproject.model.requestDTO.TypeCardRequestDTO;
import it.williamrolim.bankfinalproject.repository.TypeCardRepository;
import it.williamrolim.bankfinalproject.service.TypeCardService;
@Service
public class TypeCardImp implements TypeCardService {

	@Autowired
	TypeCardRepository typeCardRepository;
	
	@Override
	public TypeCard insertTypeCard(TypeCardRequestDTO typeCardRequestDTO) {
		TypeCard typeCard = new TypeCard();
		typeCard.setId(typeCardRequestDTO.getId());
		typeCard.setName(typeCardRequestDTO.getName());
		
		return typeCardRepository.save(typeCard);
	}

	@Override
	public List<TypeCard> getAllTypeCards() {
		return StreamSupport
		        .stream(typeCardRepository.findAll().spliterator(), false)
		        .collect(Collectors.toList());
	}

	@Override
	public TypeCard getTypeCard(Integer accountId) {
	       return typeCardRepository.findById(accountId).orElseThrow(() ->
	        new IllegalArgumentException(
	                "account with id: " + accountId + " could not be found"));
	}

	@Override
	public TypeCard deleteTypeCard(Integer typeCardId) {
		return typeCardRepository.findById(typeCardId).orElseThrow(() ->
        new IllegalArgumentException(
                "account with id: " + typeCardId + " could not be found"));
	}

	@Override
	public TypeCard updateTypeCard(Integer accountId, TypeCardRequestDTO typeCardRequestDTO) {
		// TODO Auto-generated method stub
		return null;
	}

}
