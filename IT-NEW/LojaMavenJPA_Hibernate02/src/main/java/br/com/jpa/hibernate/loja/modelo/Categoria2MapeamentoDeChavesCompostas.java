package br.com.jpa.hibernate.loja.modelo;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "categorias")
public class Categoria2MapeamentoDeChavesCompostas {
	
	@EmbeddedId //O JPA sabe que aqui dentro tem os atributos para chave primaria
	private CategoriaId id;
	//JPA precisa que tem um construtor padr�o
	public Categoria2MapeamentoDeChavesCompostas() {
		
	}

	public Categoria2MapeamentoDeChavesCompostas(String nome, String tipo) {
		this.id = new CategoriaId(nome, "xpto");
		
	}

	//Fazendo um Delegate
	public String getNome() {
		return this.id.getNome();
	}

}
