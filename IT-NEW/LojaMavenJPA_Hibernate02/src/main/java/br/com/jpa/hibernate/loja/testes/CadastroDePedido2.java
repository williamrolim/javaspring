package br.com.jpa.hibernate.loja.testes;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.EntityManager;

import br.com.jpa.hibernate.loja.dao.CategoriaDao;
import br.com.jpa.hibernate.loja.dao.ClienteDao;
import br.com.jpa.hibernate.loja.dao.PedidoDao2;
import br.com.jpa.hibernate.loja.dao.ProdutoDao;
import br.com.jpa.hibernate.loja.dao.ProdutoDao2;
import br.com.jpa.hibernate.loja.modelo.Categoria;
import br.com.jpa.hibernate.loja.modelo.Cliente;
import br.com.jpa.hibernate.loja.modelo.ItemPedido2;
import br.com.jpa.hibernate.loja.modelo.Pedido2;
import br.com.jpa.hibernate.loja.modelo.Produto;
import br.com.jpa.hibernate.loja.modelo.Produto2;
import br.com.jpa.hibernate.loja.util.JPAUtil;
import br.com.jpa.hibernate.loja.vo.RelatorioDeVendasVo;

public class CadastroDePedido2 {
//para cadastrar um pedido, preciso de um produto
//para cadastrar um produto preciso de uma categoria
	public static void main(String[] args) {
		popularBancoDeDados();
		
		EntityManager em = JPAUtil.getEntitiyManager();
		ProdutoDao2 produtoDao = new ProdutoDao2(em);
		ClienteDao clienteDao = new ClienteDao(em);
		
		Produto2 produto = produtoDao.buscaPorID(1l);
		Cliente cliente = clienteDao.buscaPorID(1l);
		
		Produto2 produto2 = produtoDao.buscaPorID(2l);
		Cliente cliente2 = clienteDao.buscaPorID(2l);
		
		Produto2 produto3 = produtoDao.buscaPorID(3l);


		em.getTransaction().begin();

		Pedido2 pedido = new  Pedido2(cliente);//para criar um pedido preciso de um cliente
		pedido.adicionarItem(new ItemPedido2(10,pedido,produto));
		
		Pedido2 pedido2 = new  Pedido2(cliente2);//para criar um pedido preciso de um cliente
		pedido.adicionarItem(new ItemPedido2(20,pedido2,produto2));
		
		pedido.adicionarItem(new ItemPedido2(6,pedido2,produto3));
		
		PedidoDao2 PedidoDao2 = new PedidoDao2(em);
		PedidoDao2.cadastrar(pedido);
		PedidoDao2.cadastrar(pedido2);

		
		em.getTransaction().commit();
		
		BigDecimal totalVendido =PedidoDao2.valorTotalVendido();
		System.out.println("Valor Total..: " + totalVendido);
		
		/*	List<Object[]> relatorio = pedidoDao.relatorioVendas();

		for (Object[] objects : relatorio) {//pra cada object dentro do relatorio
			System.out.println(objects[0]);
			System.out.println(objects[1]);
			System.out.println(objects[2]);
		}
		*/
		
		List<RelatorioDeVendasVo> relatorio = PedidoDao2.relatorioVendas();
		relatorio.forEach(System.out::println);
		em.close();
		

	}
	
	private static void popularBancoDeDados() {
		Categoria celulares = new Categoria("CELULARES");
		Categoria celulares2 = new Categoria("CELULARES");
		
		Categoria videogames = new Categoria("VIDEOGAMES");
		Categoria informatica = new Categoria("INFORMATICA");

		//tela do usuario para ele preencher
		Produto celular = new Produto("Xiomi Redmi", "Muito Legal", new BigDecimal("900"),celulares );
		Produto celular2 = new Produto("Iphone", "New Generation", new BigDecimal("13000"),celulares );
		
		Produto videogame = new Produto ("PS5","Playstation 5",new BigDecimal("5000"),celulares );
		
		Produto macbook = new Produto ("MacBook", "MacBook Pro retina",new BigDecimal("1200"),celulares );

		
		Cliente cliente = new Cliente("William R", "123.343.123-22");
		Cliente cliente2 = new Cliente("Maria Jose", "223.353.466-13");


		EntityManager em = JPAUtil.getEntitiyManager();
			
		ProdutoDao produtoDao = new ProdutoDao(em);
		CategoriaDao categoriaDao = new CategoriaDao(em);
		ClienteDao clienteDao = new ClienteDao(em);
		
		em.getTransaction().begin();
		
		categoriaDao.cadastrar(celulares);
		produtoDao.cadastrar(celular);
		
		categoriaDao.cadastrar(celulares2);
		produtoDao.cadastrar(celular2);
		clienteDao.cadastrar(cliente);
		
		categoriaDao.cadastrar(celulares2);
		produtoDao.cadastrar(celular2);
		
		categoriaDao.cadastrar(videogames);
		produtoDao.cadastrar(videogame);
		
		categoriaDao.cadastrar(informatica);
		produtoDao.cadastrar(macbook);
		clienteDao.cadastrar(cliente2);
		
		em.getTransaction().commit();
		em.close();
	}

}
