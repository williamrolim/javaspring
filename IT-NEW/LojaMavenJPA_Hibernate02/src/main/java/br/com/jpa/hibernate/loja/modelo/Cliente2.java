package br.com.jpa.hibernate.loja.modelo;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "clientes")
public class Cliente2 {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Embedded //Embuta para mim os atributos dessa classe 
	private DadosPessoais dadosPessoais;
	
	public Cliente2() {
	}
	
	public Cliente2(String nome, String cpf) {
		this.dadosPessoais = new DadosPessoais(nome, cpf);
	}

	public DadosPessoais getDadosPessoais() {
		return dadosPessoais;
	}
	
	//metodo delegate
	public String getNome() {
		return this.dadosPessoais.getNome();
	}
	
	public String getCpf() {
		return this.dadosPessoais.getCpf();
	}
	
	




}
