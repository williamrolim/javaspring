package br.com.jpa.hibernate.loja.dao;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.EntityManager;

import br.com.jpa.hibernate.loja.modelo.Produto;

public class ProdutoDao {
	private EntityManager em;

	//inje��o de dependencias
	public ProdutoDao(EntityManager em) {
		this.em = em;
	}
	
	public void cadastrar (Produto produto) {
		this.em.persist(produto);
	}
	
	public void atualizar(Produto produto) {
		this.em.merge(produto); // for�o a categoria para ela estar em um estado managed
	}
	
	public void remove(Produto produto) {//essa categoria precisa estar em estado MANAGED (REMOVED)
		this.em.remove(produto); 
	}
	
	public Produto buscaPorID(Long id) {
		return em.find(Produto.class, id); //find() 2 parametros: quem � a entidade, 
	}
	
	public List<Produto>buscarTodos(){
		//a melhor maneira de fazer uma busca � utilizando JPQL java persistence query language
		//n�o passar o nome da tabela e sim o nome da ENTIDADE
		String jpql = "SELECT p FROM Produto AS p";//carregue o objeto inteiro P a entidade com todos os atributos	
		return em.createQuery(jpql, Produto.class).getResultList();
		//  infere que vai devolter uam lista de produto
	}
	
	public List<Produto>buscarPorNome(String nome){
		String jpql = "SELECT p FROM Produto AS p WHERE p.nome = :nome";//Produto nome da entidade --- nome do atributo
//		String jpql = "SELECT p FROM Produto AS p WHERE p.nome = ?1; //?parametro por ordem

		return em.createQuery(jpql, Produto.class)
				.setParameter("nome", nome)
				.getResultList();
	}
	
	//Big decimal pois o pre�o est� como bigdecimal
	public BigDecimal buscarPrecoDoProdutoPorNome(String nome){
		String jpql = "SELECT p.preco FROM Produto AS p WHERE p.nome = :nome";//Produto nome da entidade --- nome do atributo
//		String jpql = "SELECT p FROM Produto AS p WHERE p.nome = ?1; //?parametro por ordem

		return em.createQuery(jpql, BigDecimal.class)
				.setParameter("nome", nome)
				.getSingleResult();
	}
	
	public List<Produto>buscarPorNomeDaCategoria(String nome){//filtrar por um atributo dentro de um relacionamento p.categoria.nome = :nome (j� realiza o join automatico
	String jpql = "SELECT p FROM Produto p  WHERE p.categoria.nome = :nome " ;//categoria que � um relacionamento, categoria � uma atributo da classe Produto s� que � um relacionamento, filtrar por um atributo dentro do relacioanamento e j� faz o o join manual
	return em.createQuery(jpql, Produto.class)
			.setParameter("nome", nome)
			.getResultList();
}
	
//	public List<Produto>buscarPorNomeDaCategoria(String nome){
//		String jpql = "SELECT p FROM Categoria AS c  " +
//				"INNER JOIN Produto As p " +
//				" ON c.id = p.id " + 
//				" WHERE c.nome = :nome";//nome do atributo
//		return em.createQuery(jpql, Produto.class)
//				.setParameter("nome", nome)
//				.getResultList();
//	}
}
