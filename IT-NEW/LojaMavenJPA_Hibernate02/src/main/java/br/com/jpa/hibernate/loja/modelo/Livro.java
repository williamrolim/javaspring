package br.com.jpa.hibernate.loja.modelo;

import javax.persistence.Entity;

@Entity //assume que  entidade apos o @Inheritance
public class Livro extends Produto3 {
	private String autor;
	private Integer numeroDePaginas;

	public Livro() {
	}

	public Livro(String autor, Integer numeroDePaginas) {
		this.autor = autor;
		this.numeroDePaginas = numeroDePaginas;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public Integer getNumeroDePaginas() {
		return numeroDePaginas;
	}
	public void setNumeroDePaginas(Integer numeroDePaginas) {
		this.numeroDePaginas = numeroDePaginas;
	}
	
	@Override
	public String toString() {
		return "Livro [autor=" + autor + ", numeroDePaginas=" + numeroDePaginas + "]";
	}


}
