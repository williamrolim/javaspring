package br.com.jpa.hibernate.loja.testes;

import java.math.BigDecimal;

import javax.persistence.EntityManager;

import br.com.jpa.hibernate.loja.dao.CategoriaDao;
import br.com.jpa.hibernate.loja.dao.ProdutoDao;
import br.com.jpa.hibernate.loja.modelo.Categoria;
import br.com.jpa.hibernate.loja.modelo.Produto;
import br.com.jpa.hibernate.loja.util.JPAUtil;

public class CadastroDeProduto02 {

	public static void main(String[] args) {
		cadastrarProduto();
		
		//est� tudo em detached, n�o tem mais gerenciado, agora posso fazer consultas no banco de dados
		
		EntityManager em = JPAUtil.getEntitiyManager();
		
		ProdutoDao produtoDao = new ProdutoDao(em);
		
		Produto p = produtoDao.buscaPorID(1l);
		
		System.out.println("Busca pelo pre�o..: " 	+ p.getPreco());
		
	     produtoDao.buscarTodos().forEach(System.out::println);
	     // produtoDao.buscarTodos().forEach(p2 -> System.out.println(p2.getNome()));
	     
	     System.out.println("\nBusca Por Nome");

	     produtoDao.buscarPorNome("Iphone").forEach(System.out::println);
	     
	     System.out.println("\nBusca Por Categoria");
	     produtoDao.buscarPorNomeDaCategoria("CELULARES").forEach(System.out::println);
	     
	     System.out.println("\nBusca Por nome o PRE�O");
	     BigDecimal precoProduo = produtoDao.buscarPrecoDoProdutoPorNome("Iphone");
	     System.out.println(precoProduo);


	}

	private static void cadastrarProduto() {
		Categoria celulares = new Categoria("CELULARES");
		Categoria celulares2 = new Categoria("CELULARES");

		//tela do usuario para ele preencher
		Produto celular = new Produto("Xiomi Redmi", "Muito Legal", new BigDecimal("900"),celulares );
		Produto celular2 = new Produto("Iphone", "New Generation", new BigDecimal("13000"),celulares );

		EntityManager em = JPAUtil.getEntitiyManager();
			
		ProdutoDao produtoDao = new ProdutoDao(em);
		CategoriaDao categoriaDao = new CategoriaDao(em);

		em.getTransaction().begin();
		
		categoriaDao.cadastrar(celulares);
		produtoDao.cadastrar(celular);
		
		categoriaDao.cadastrar(celulares2);
		produtoDao.cadastrar(celular2);
		em.getTransaction().commit();
		em.close();
	}
}

