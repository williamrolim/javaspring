package br.com.jpa.hibernate.loja.testes;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.EntityManager;

import br.com.jpa.hibernate.loja.dao.CategoriaDao;
import br.com.jpa.hibernate.loja.dao.ClienteDao2;
import br.com.jpa.hibernate.loja.dao.PedidoDao3;
import br.com.jpa.hibernate.loja.dao.ProdutoDao2;
import br.com.jpa.hibernate.loja.dao.ProdutoDao3;
import br.com.jpa.hibernate.loja.modelo.Categoria;
import br.com.jpa.hibernate.loja.modelo.Cliente2;
import br.com.jpa.hibernate.loja.modelo.ItemPedido3;
import br.com.jpa.hibernate.loja.modelo.Pedido3;
import br.com.jpa.hibernate.loja.modelo.Produto2;
import br.com.jpa.hibernate.loja.modelo.Produto3;
import br.com.jpa.hibernate.loja.util.JPAUtil;

public class TesteCriteria {

	public static void main(String[] args) {
		popularBancoDeDados();		
		EntityManager em = JPAUtil.getEntitiyManager();

		ProdutoDao2 produtoDao = new ProdutoDao2(em);
		List <Produto2> produto =produtoDao.buscarParametrosDinamicaComCriteria("PS5", null, null);
		System.out.println(produto);
		
	}
	
	private static void popularBancoDeDados() {
		Categoria celulares = new Categoria("CELULARES");
		Categoria celulares2 = new Categoria("CELULARES");
		
		Categoria videogames = new Categoria("VIDEOGAMES");
		Categoria informatica = new Categoria("INFORMATICA");

		//tela do usuario para ele preencher
		Produto3 celular = new Produto3("Xiomi Redmi", "Muito Legal", new BigDecimal("900"),celulares );
		Produto3 celular2 = new Produto3("Iphone", "New Generation", new BigDecimal("13000"),celulares );
		
		Produto3 videogame = new Produto3 ("PS5","Playstation 5",new BigDecimal("5000"),celulares );
		
		Produto3 macbook = new Produto3 ("MacBook", "MacBook Pro retina",new BigDecimal("1200"),celulares );

		
		Cliente2 cliente = new Cliente2("William R", "123.343.123-22");
		Cliente2 cliente2 = new Cliente2("Maria Jose", "223.353.466-13");


		EntityManager em = JPAUtil.getEntitiyManager();
		
		Pedido3 pedido = new  Pedido3(cliente);//para criar um pedido preciso de um cliente
		pedido.adicionarItem(new ItemPedido3(10, pedido,celular));
		
		Pedido3 pedido2 = new  Pedido3(cliente2);//para criar um pedido preciso de um cliente
		pedido.adicionarItem(new ItemPedido3(20,pedido2,videogame));
		
		pedido.adicionarItem(new ItemPedido3(6,pedido2,macbook));
		
		PedidoDao3 PedidoDao2 = new PedidoDao3(em);
		PedidoDao2.cadastrar(pedido);
		PedidoDao2.cadastrar(pedido2);

			
		ProdutoDao3 produtoDao = new ProdutoDao3(em);
		CategoriaDao categoriaDao = new CategoriaDao(em);
		ClienteDao2 clienteDao = new ClienteDao2(em);
		
		em.getTransaction().begin();
		
		categoriaDao.cadastrar(celulares);
		produtoDao.cadastrar(celular);
		
		categoriaDao.cadastrar(celulares2);
		produtoDao.cadastrar(celular2);
		clienteDao.cadastrar(cliente);
		
		categoriaDao.cadastrar(celulares2);
		produtoDao.cadastrar(celular2);
		
		categoriaDao.cadastrar(videogames);
		produtoDao.cadastrar(videogame);
		
		categoriaDao.cadastrar(informatica);
		produtoDao.cadastrar(macbook);
		clienteDao.cadastrar(cliente2);
		
		em.getTransaction().commit();
		em.close();
	}

}
