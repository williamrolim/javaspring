package br.com.jpa.hibernate.loja.modelo;

import javax.persistence.Embeddable;

@Embeddable //@Embeddable classe embutivel, consigo embutila dentro de uma entidade, indicando para jpa
public class DadosPessoais {
	private String nome;
	private String cpf;
	
	public DadosPessoais() {
		
	}
	
	public DadosPessoais(String nome, String cpf) {
		this.nome = nome;
		this.cpf = cpf;
	}

	public String getNome() {
		return nome;
	}

	public String getCpf() {
		return cpf;
	}

}
