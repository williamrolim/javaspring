package br.com.jpa.hibernate.loja.testes;

import java.math.BigDecimal;

import javax.persistence.EntityManager;

import br.com.jpa.hibernate.loja.dao.CategoriaDao;
import br.com.jpa.hibernate.loja.dao.ClienteDao;
import br.com.jpa.hibernate.loja.dao.PedidoDao2;
import br.com.jpa.hibernate.loja.dao.ProdutoDao2;
import br.com.jpa.hibernate.loja.modelo.Categoria;
import br.com.jpa.hibernate.loja.modelo.Cliente;
import br.com.jpa.hibernate.loja.modelo.ItemPedido2;
import br.com.jpa.hibernate.loja.modelo.Pedido;
import br.com.jpa.hibernate.loja.modelo.Pedido2;
import br.com.jpa.hibernate.loja.modelo.Produto2;
import br.com.jpa.hibernate.loja.util.JPAUtil;

public class PerfomanceConsultas {

	public static void main(String[] args) {
		popularBancoDeDados();		
		EntityManager em = JPAUtil.getEntitiyManager();
		
		Pedido pedido = em.find(Pedido.class, 1l);//ERROR: Consulta fazendo join com a tabela cliente
		Pedido pedido2 = em.find(Pedido.class, 2l);//ERROR: Consulta fazendo join com a tabela cliente
		
//		System.out.println("Pegando a data do pedido ..: \n" + pedido.getDataCadastro());
//		
//		System.out.println("Pegando o tamanho lista dos pedido ..: \n" +pedido.getItens().size());
		
		//SIMULANDO O ERRO LazyInitializationException (Corrigido na classe PerfomanceConsultas2
		em.close();
		System.out.println(pedido.getCliente().getNome());
	}
	
	private static void popularBancoDeDados() {
		Categoria celulares = new Categoria("CELULARES");
		Categoria celulares2 = new Categoria("CELULARES");
		
		Categoria videogames = new Categoria("VIDEOGAMES");
		Categoria informatica = new Categoria("INFORMATICA");

		//tela do usuario para ele preencher
		Produto2 celular = new Produto2("Xiomi Redmi", "Muito Legal", new BigDecimal("900"),celulares );
		Produto2 celular2 = new Produto2("Iphone", "New Generation", new BigDecimal("13000"),celulares );
		
		Produto2 videogame = new Produto2 ("PS5","Playstation 5",new BigDecimal("5000"),celulares );
		
		Produto2 macbook = new Produto2 ("MacBook", "MacBook Pro retina",new BigDecimal("1200"),celulares );

		
		Cliente cliente = new Cliente("William R", "123.343.123-22");
		Cliente cliente2 = new Cliente("Maria Jose", "223.353.466-13");


		EntityManager em = JPAUtil.getEntitiyManager();
		
		Pedido2 pedido = new  Pedido2(cliente);//para criar um pedido preciso de um cliente
		pedido.adicionarItem(new ItemPedido2(10,pedido,celular));
		
		Pedido2 pedido2 = new  Pedido2(cliente2);//para criar um pedido preciso de um cliente
		pedido.adicionarItem(new ItemPedido2(20,pedido2,videogame));
		
		pedido.adicionarItem(new ItemPedido2(6,pedido2,macbook));
		
		PedidoDao2 pedidoDao2 = new PedidoDao2(em);
		pedidoDao2.cadastrar(pedido);
		pedidoDao2.cadastrar(pedido2);

			
		ProdutoDao2 produtoDao = new ProdutoDao2(em);
		CategoriaDao categoriaDao = new CategoriaDao(em);
		ClienteDao clienteDao = new ClienteDao(em);
		
		em.getTransaction().begin();
		
		categoriaDao.cadastrar(celulares);
		produtoDao.cadastrar(celular);
		
		categoriaDao.cadastrar(celulares2);
		produtoDao.cadastrar(celular2);
		clienteDao.cadastrar(cliente);
		
		categoriaDao.cadastrar(celulares2);
		produtoDao.cadastrar(celular2);
		
		categoriaDao.cadastrar(videogames);
		produtoDao.cadastrar(videogame);
		
		categoriaDao.cadastrar(informatica);
		produtoDao.cadastrar(macbook);
		clienteDao.cadastrar(cliente2);
		
		em.getTransaction().commit();
		em.close();
	}

}
