package br.com.jpa.hibernate.loja.modelo;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "pedidos")
public class Pedido3 {

	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY) //quem vai gerar n�o � a aplica��o e sim o banco de dados
	private Long id;
	@Column (name = "valor_total")
	private BigDecimal valorTotal = BigDecimal.ZERO;//atributos que n�o tem relacionamento, nome composto, coloca o mesmo nome do atributo Cammelcase
	private LocalDate dataCadastro = LocalDate.now();
	
	//@ManyToOne empre vai fazer um select, vai incluir um join para carregar esse registro sempre que voc� carregar a entidade principal.
	@ManyToOne (fetch = FetchType.LAZY) //Todo cliente pertence a um pedido - um cliente pode estar vinculado a mais de um pedido
	private Cliente2 cliente;
	//cascade = tudo que acontecer no pedido fa�a tbem no itens pedido								//Relacionamento bidirecionais tem que colocar o mammp
	@OneToMany(mappedBy = "pedido", cascade = CascadeType.ALL)//para n�o gerar uma nova tabela, temos que indicar o relacionamento que est� mapeado
	private List<ItemPedido3> itens = new ArrayList<ItemPedido3>();//inicializar a lista como a lista vazia, para evitar ver a lista � nula atravez de uma checagem
	//n�o preciso verificar se alista est� nula, sempre estara estanciada e com uma cole��o vazia
	//CascadeType.ALL tudo que fizer no pedido, fa�a no pedido, vale para excls�o, se matei um pedido n�o ter um item pedido sem o pedido (um n�o existe sem o outro)
	public Pedido3() {
		
	}
	
	public Pedido3(Cliente2 cliente) {
		this.cliente = cliente;
	}
	
	public void adicionarItem(ItemPedido3 item) {
		item.setPedido(this);//setando o pedido como this, o proprio pedido a classe atual
		this.itens.add(item);//seto os 2 itens do valor biderecional
		this.valorTotal = this.valorTotal.add(item.getValor());
	}//Estou vinculando os 2 lados (pedido & itemPedido) o item conhece o pedido, e o pedido conhece o item

	public BigDecimal getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(BigDecimal valorTotal) {
		this.valorTotal = valorTotal;
	}

	public LocalDate getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(LocalDate dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Cliente2 getCliente() {
		return cliente;
	}

	public void setCliente(Cliente2 cliente) {
		this.cliente = cliente;
	}
	
	public List<ItemPedido3> getItens() {
		return itens;
	}

	public void setItens(List<ItemPedido3> itens) {
		this.itens = itens;
	}

	@Override
	public String toString() {
		return "Pedido [valorTotal=" + valorTotal + ", dataCadastro=" + dataCadastro + ", cliente=" + cliente + "]";
	}
	
}
