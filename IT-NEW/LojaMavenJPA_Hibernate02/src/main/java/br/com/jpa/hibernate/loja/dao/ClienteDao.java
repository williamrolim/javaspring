package br.com.jpa.hibernate.loja.dao;

import javax.persistence.EntityManager;

import br.com.jpa.hibernate.loja.modelo.Cliente;

public class ClienteDao {

	private EntityManager em;

	public ClienteDao(EntityManager em) {
		this.em = em;
	}

	public void cadastrar(Cliente cliente) {
		this.em.persist(cliente);
	}

	public Cliente buscaPorID(Long id) {
		return this.em.find(Cliente.class, id);
	}

}
