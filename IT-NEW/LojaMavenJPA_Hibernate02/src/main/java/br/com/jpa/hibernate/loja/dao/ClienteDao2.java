package br.com.jpa.hibernate.loja.dao;

import javax.persistence.EntityManager;

import br.com.jpa.hibernate.loja.modelo.Cliente2;

public class ClienteDao2 {

	private EntityManager em;

	public ClienteDao2(EntityManager em) {
		this.em = em;
	}

	public void cadastrar(Cliente2 cliente) {
		this.em.persist(cliente);
	}

	public Cliente2 buscaPorID(Long id) {
		return this.em.find(Cliente2.class, id);
	}

}
