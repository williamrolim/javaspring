package br.com.jpa.hibernate.loja.modelo;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
//ITEM PEDIDO � DEPENDENTE DO PEDIDO
@Entity
@Table (name = "itens_pedido")
public class ItemPedido2 {
@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
private Long id;

@Column(name = "preco_unitario")
private BigDecimal precoUnitario;

private int quantidade;

@ManyToOne(fetch = FetchType.LAZY)//s� vou carregar se eu fazer o acesso
private Pedido2 pedido;
@ManyToOne(fetch = FetchType.LAZY)
private Produto2 produto;

public ItemPedido2() {
	
}
//n�o vou passar o pre�o, pois eu pego do Produto
public ItemPedido2(int quantidade, Pedido2 pedido, Produto2 produto) {
	this.quantidade = quantidade;
	this.pedido = pedido;
	this.precoUnitario = produto.getPreco();// j� passo o pre�o unitatirio do produto
	this.produto = produto;
}

public Long getId() {
	return id;
}

public void setId(Long id) {
	this.id = id;
}

public BigDecimal getPrecoUnitario() {
	return precoUnitario;
}

public void setPrecoUnitario(BigDecimal precoUnitario) {
	this.precoUnitario = precoUnitario;
}

public int getQuantidade() {
	return quantidade;
}

public void setQuantidade(int quantidade) {
	this.quantidade = quantidade;
}

public Pedido2 getPedido() {
	return pedido;
}

public void setPedido(Pedido2 pedido) {
	this.pedido = pedido;
}

public Produto2 getProduto2() {
	return produto;
}

public void setProduto(Produto2 produto) {
	this.produto = produto;
}


public BigDecimal getValor() {
	// TODO Auto-generated method stub
	return precoUnitario.multiply(new BigDecimal(quantidade));//convertendo quantidade para bigdecima
}
@Override
public String toString() {
	return "ItemPedido [precoUnitario=" + precoUnitario + ", quantidade=" + quantidade + ", pedido=" + pedido
			+ ", produto=" + produto + "]";
}


//na teoria quando recebo do contrutor n�o preciso dos setters


}
