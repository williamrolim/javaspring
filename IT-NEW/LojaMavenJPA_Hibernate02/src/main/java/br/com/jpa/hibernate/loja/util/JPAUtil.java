package br.com.jpa.hibernate.loja.util;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JPAUtil {
 private static final EntityManagerFactory FACTORY = Persistence
		 .createEntityManagerFactory("loja");//mesmo nome do persistence
 
 public static EntityManager getEntitiyManager() {
	 return FACTORY.createEntityManager();
 }
}
