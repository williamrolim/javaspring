package br.com.jpa.hibernate.loja.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import br.com.jpa.hibernate.loja.modelo.ItemPedido;
import br.com.jpa.hibernate.loja.modelo.Pedido;
import br.com.jpa.hibernate.loja.modelo.Produto;
import br.com.jpa.hibernate.loja.vo.RelatorioDeVendasVo;

public class PedidoDao {

	private EntityManager em;

	public PedidoDao(EntityManager em) {
		this.em = em;
	}

	public void cadastrar(Pedido pedido) {
		this.em.persist(pedido);
	}
	
	//Valor Total de todos os pedidos e imprimir na tela
	
	public BigDecimal valorTotalVendido() {
		String jpql = "SELECT SUM(p.valorTotal) From Pedido p";
		return em.createQuery(jpql,BigDecimal.class)
				.getSingleResult();//.singleResult() trazer um unico resultado
	}
	
	//3 Entidades relacionadas com a consulta
	//Como cada um pode ser de um tipo vou utilizar um Object , n�o � um unico objeto ent�o � um array de objeto
	public List<Object[]>relatorioVendas() {
		String jpql = "SELECT produto.nome, "
				+ "SUM(item.quantidade), "//ITEM QUANTIDADE [[[[PEDIDO]]]]
				+ "MAX(pedido.dataCadastro) " //dataCadastro mesmo nome que est� na entidade 
				+ "FROM Pedido pedido "//[[[[ITEM_PEDIDO]]]]
				+ "JOIN pedido.itens item "//private List<ItemPedido> itens = new ArrayList<ItemPedido>() [[[[PEDIDO]]]]
				+ "JOIN item.produto produto " //private Produto produto; [[[[ITEM_PEDIDO]]]]
				+ "GROUP BY  produto.nome "//fun��o de agrega�ao deve-se usar o group by
				+ "ORDER BY item.quantidade DESC";
		return em.createQuery(jpql,Object[].class).getResultList();
	}
	

}
