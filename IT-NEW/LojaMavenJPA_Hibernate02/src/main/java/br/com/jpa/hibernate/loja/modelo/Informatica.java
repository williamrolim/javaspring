package br.com.jpa.hibernate.loja.modelo;

import javax.persistence.Entity;

@Entity //assume que  entidade apos o @Inheritance
public class Informatica extends Produto3 {
	private String marca;
	private Integer modelo;

	public Informatica() {
	}

	public Informatica(String marca, Integer modelo) {
		this.marca = marca;
		this.modelo = modelo;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public Integer getModelo() {
		return modelo;
	}

	public void setModelo(Integer modelo) {
		this.modelo = modelo;
	}

}
