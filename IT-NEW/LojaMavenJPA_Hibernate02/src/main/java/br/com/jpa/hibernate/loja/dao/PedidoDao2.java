package br.com.jpa.hibernate.loja.dao;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.EntityManager;

import br.com.jpa.hibernate.loja.modelo.Pedido;
import br.com.jpa.hibernate.loja.modelo.Pedido2;
import br.com.jpa.hibernate.loja.vo.RelatorioDeVendasVo;

public class PedidoDao2 {

	private EntityManager em;

	public PedidoDao2(EntityManager em) {
		this.em = em;
	}

	public void cadastrar(Pedido2 pedido) {
		this.em.persist(pedido);
	}
	
	//Valor Total de todos os pedidos e imprimir na tela
	
	public BigDecimal valorTotalVendido() {
		String jpql = "SELECT SUM(p.valorTotal) From Pedido p";
		return em.createQuery(jpql,BigDecimal.class)
				.getSingleResult();//.singleResult() trazer um unico resultado
	}
	
	//3 Entidades relacionadas com a consulta
	//Como cada um pode ser de um tipo vou utilizar um Object , n�o � um unico objeto ent�o � um array de objeto
//	public List<Object[]>relatorioVendas() {
//		String jpql = "SELECT produto.nome, "
//				+ "SUM(item.quantidade), "//ITEM QUANTIDADE [[[[PEDIDO]]]]
//				+ "MAX(pedido.dataCadastro) " //dataCadastro mesmo nome que est� na entidade 
//				+ "FROM Pedido pedido "//[[[[ITEM_PEDIDO]]]]
//				+ "JOIN pedido.itens item "//private List<ItemPedido> itens = new ArrayList<ItemPedido>() [[[[PEDIDO]]]]
//				+ "JOIN item.produto produto " //private Produto produto; [[[[ITEM_PEDIDO]]]]
//				+ "GROUP BY  produto.nome "//fun��o de agrega�ao deve-se usar o group by
//				+ "ORDER BY item.quantidade DESC";
//		return em.createQuery(jpql,Object[].class).getResultList();
//	}
	
	//Refatorando o metodo acima
	//Criar uma classe que tranforme esse array de objeto em um relatorio - classe que representa isso
	/*
	 * Quando voc� quer fazer um relat�rio, voc� quer fazer uma consulta, um 
	 * select de um relat�rio e n�o quer devolver um array de object, tem um 
	 * recurso, o select new. Ent�o voc� pode fazer String jpql = "SELECT new ". 
	 * � como se eu estivesse dando new em uma classe. Qual classe? RelatorioDeVendasVo.
	 */
	public List<RelatorioDeVendasVo>relatorioVendas() {//Vo -> value objets,n�o tem nenhum comportamento s� atributos
		String jpql = "SELECT new br.com.jpa.hibernate.loja.vo.RelatorioDeVendasVo ("//criando uma estancia de relatorioVendas, e os 3 atributos abaixo passa para o construtor
				+ "produto.nome, "
				+ "SUM(item.quantidade), "//ITEM QUANTIDADE [[[[PEDIDO]]]]
				+ "MAX(pedido.dataCadastro))" //dataCadastro mesmo nome que est� na entidade 
				+ "FROM Pedido pedido "//[[[[ITEM_PEDIDO]]]]
				+ "JOIN pedido.itens item "//private List<ItemPedido> itens = new ArrayList<ItemPedido>() [[[[PEDIDO]]]]
				+ "JOIN item.produto produto " //private Produto produto; [[[[ITEM_PEDIDO]]]]
				+ "GROUP BY  produto.nome "//fun��o de agrega�ao deve-se usar o group by
				+ "ORDER BY item.quantidade DESC";
		return em.createQuery(jpql,RelatorioDeVendasVo.class).getResultList();
	}
	
	//Resolvend o "LazyInitializationException"
	//Criando a query planejada = carrego tudo o que precisar, evitar tomar o LazyInitializationException
	//Se o EntitiyManager tiver fechado
	
	public Pedido buscarPedidoComCliente(Long id) {//Nessa consulta j� carregue determinado relacionamento junto
		return em.createQuery("SELECT p FROM Pedido p JOIN FETCH p.cliente WHERE p.id = :id ", Pedido.class)
				.setParameter("id", id)
				.getSingleResult();
		
	}
}
