package br.com.jpa.hibernate.loja.vo;

import java.time.LocalDate;
//Classe que representa o relatorio

public class RelatorioDeVendasVo {//Value object � um objeto de valor, s� tem atributos, getters e construtor
	private String nomeDoProduto;
	private Long quantidadeVendida;
	private LocalDate dataUltimaVenda;
	
	public RelatorioDeVendasVo(String nomeDoProduto, Long quantidadeVendida, LocalDate dataUltimaVenda) {
		this.nomeDoProduto = nomeDoProduto;
		this.quantidadeVendida = quantidadeVendida;
		this.dataUltimaVenda = dataUltimaVenda;
	}

	public String getNomeDoProduto() {
		return nomeDoProduto;
	}

	public Long getQuantidadeVendida() {
		return quantidadeVendida;
	}

	public LocalDate getDataUltimaVenda() {
		return dataUltimaVenda;
	}

	@Override
	public String toString() {
		return "RelatorioDeVendasVo [nomeDoProduto=" + nomeDoProduto + ", quantidadeVendida=" + quantidadeVendida
				+ ", dataUltimaVenda=" + dataUltimaVenda + "]";
	}	
	
}
