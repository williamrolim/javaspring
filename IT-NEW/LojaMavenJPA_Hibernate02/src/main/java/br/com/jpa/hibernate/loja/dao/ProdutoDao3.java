package br.com.jpa.hibernate.loja.dao;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import br.com.jpa.hibernate.loja.modelo.Produto2;
import br.com.jpa.hibernate.loja.modelo.Produto3;

public class ProdutoDao3 {
	private EntityManager em;

	//inje��o de dependencias
	public ProdutoDao3(EntityManager em) {
		this.em = em;
	}
	
	public void cadastrar (Produto3 produto) {
		this.em.persist(produto);
	}
	
	public void atualizar(Produto3 produto) {
		this.em.merge(produto); // for�o a categoria para ela estar em um estado managed
	}
	
	public void remove(Produto3 produto) {//essa categoria precisa estar em estado MANAGED (REMOVED)
		this.em.remove(produto); 
	}
	
	public Produto3 buscaPorID(Long id) {
		return em.find(Produto3.class, id); //find() 2 parametros: quem � a entidade, 
	}
	
	public List<Produto3>buscarTodos(){
		//a melhor maneira de fazer uma busca � utilizando JPQL java persistence query language
		//n�o passar o nome da tabela e sim o nome da ENTIDADE
		String jpql = "SELECT p FROM Produto3 AS p";//carregue o objeto inteiro P a entidade com todos os atributos	
		return em.createQuery(jpql, Produto3.class).getResultList();
		//  infere que vai devolter uam lista de Produto2
	}
	
	public List<Produto3>buscarPorNome(String nome){
		String jpql = "SELECT p FROM Produto3 AS p WHERE p.nome = :nome";//Produto2 nome da entidade --- nome do atributo
//		String jpql = "SELECT p FROM Produto2 AS p WHERE p.nome = ?1; //?parametro por ordem

		return em.createQuery(jpql, Produto3.class)
				.setParameter("nome", nome)
				.getResultList();
	}
	
	//Big decimal pois o pre�o est� como bigdecimal
	public BigDecimal buscarPrecoDoProdutoPorNome(String nome){
		String jpql = "SELECT p.preco FROM Produto3 AS p WHERE p.nome = :nome";//Produto2 nome da entidade --- nome do atributo
//		String jpql = "SELECT p FROM Produto2 AS p WHERE p.nome = ?1; //?parametro por ordem

		return em.createQuery(jpql, BigDecimal.class)
				.setParameter("nome", nome)
				.getSingleResult();
	}
	
//	public List<Produto2>buscarPorNomeDaCategoria(String nome){//filtrar por um atributo dentro de um relacionamento p.categoria.nome = :nome (j� realiza o join automatico
//	String jpql = "SELECT p FROM Produto2 p  WHERE p.categoria.nome = :nome " ;//categoria que � um relacionamento, categoria � uma atributo da classe Produto2 s� que � um relacionamento, filtrar por um atributo dentro do relacioanamento e j� faz o o join manual
//	return em.createQuery(jpql, Produto2.class)
//			.setParameter("nome", nome)
//			.getResultList();
//}
	
	public List<Produto3>buscarPorNomeDaCategoria(String nome){//filtrar por um atributo dentro de um relacionamento p.categoria.nome = :nome (j� realiza o join automatico
	return em.createNamedQuery("produtosPorCategoria", Produto3.class)
			.setParameter("nome", nome)
			.getResultList();
}	
	
//	public List<Produto2>buscarPorNomeDaCategoria(String nome){
//		String jpql = "SELECT p FROM Categoria AS c  " +
//				"INNER JOIN Produto2 As p " +
//				" ON c.id = p.id " + 
//				" WHERE c.nome = :nome";//nome do atributo
//		return em.createQuery(jpql, Produto2.class)
//				.setParameter("nome", nome)
//				.getResultList();
//	}
	
	
	//Metodo que posso buscar por nome, preco, ou dataCadastrado de forma dinamica
	//Busca por parametros opcionais dinamicos
	public  List<Produto3> buscarParametrosDinamica(String nome, BigDecimal preco, LocalDate dataCadastro){
		
		String jpql = "SELECT p FROM Produto3 p WHERE 1=1 ";//1=1 VERDADEIRO (GAMBIARRA)
		if(nome != null && !nome.trim().isEmpty()) {
			jpql = " AND p.nome = :nome ";
		}
		if (preco != null) {
			jpql = " AND p.preco = :preco";
		}
		if (dataCadastro != null) {
			jpql = " AND p.dataCadastro = :dataCadastro";
		}
		TypedQuery <Produto3> query = em.createQuery(jpql, Produto3.class);
		if(nome != null && nome.trim().isEmpty()) {
			query.setParameter("nome", nome);
		}
		if (preco != null) {
			query.setParameter("preco", preco);
		}
		if (dataCadastro != null) {
			query.setParameter("dataCadastro", dataCadastro);
		}
		
		return query.getResultList();
		
	}
	
	public  List<Produto3> buscarParametrosDinamicaComCriteria(String nome, BigDecimal preco, LocalDate dataCadastro){
		
		CriteriaBuilder builder = em.getCriteriaBuilder();//Builder que sabe criar o objeto criteria
		//N�o � criado pelo em EntityManager e sim pelo CriteriaBuilder
	    CriteriaQuery<Produto3> query = builder.createQuery(Produto3.class); //Agora � criteria query
	    //se nosso select � igual ao from, (select de produto) trabalhando com a mesma entidade, nao precisa chamar o select
	    Root<Produto3> from = query.from(Produto3.class); //precisamos falar para ele aonde ele vai disparar o from
	    
	    Predicate filtros = builder.and();//O objeto que vai fazer os ands
	    if(nome != null && !nome.trim().isEmpty()) {
	    	filtros = builder.and(filtros,builder.equal(from.get("nome"), nome));//(campo,filtro)
	    }
		if (preco != null) {
	    	filtros = builder.and(filtros,builder.equal(from.get("preco"), preco));
		}
		if (dataCadastro != null) {
	    	filtros = builder.and(filtros,builder.equal(from.get("dataCadastro"), dataCadastro));//(campo,filtro)
		}
		
		 query.where(filtros);
		 return em.createQuery(query).getResultList();
	    
	
	}
}
