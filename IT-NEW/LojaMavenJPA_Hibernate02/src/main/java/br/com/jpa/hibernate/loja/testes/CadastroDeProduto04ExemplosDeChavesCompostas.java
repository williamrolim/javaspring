package br.com.jpa.hibernate.loja.testes;

import java.math.BigDecimal;

import javax.persistence.EntityManager;

import br.com.jpa.hibernate.loja.dao.CategoriaDao;
import br.com.jpa.hibernate.loja.dao.ProdutoDao2;
import br.com.jpa.hibernate.loja.modelo.Categoria;
import br.com.jpa.hibernate.loja.modelo.Categoria2MapeamentoDeChavesCompostas;
import br.com.jpa.hibernate.loja.modelo.CategoriaId;
import br.com.jpa.hibernate.loja.modelo.Produto2;
import br.com.jpa.hibernate.loja.util.JPAUtil;

public class CadastroDeProduto04ExemplosDeChavesCompostas {

	public static void main(String[] args) {
		cadastrarProduto();
		
		//est� tudo em detached, n�o tem mais gerenciado, agora posso fazer consultas no banco de dados
		
		EntityManager em = JPAUtil.getEntitiyManager();
		
		ProdutoDao2 produtoDao = new ProdutoDao2(em);
		
		em.find(Categoria2MapeamentoDeChavesCompostas.class, new CategoriaId("CELULARES", "xpto"));
		
		em.close();
	}

	private static void cadastrarProduto() {
		Categoria celulares = new Categoria("CELULARES");
		Categoria celulares2 = new Categoria("CELULARES");

		//tela do usuario para ele preencher
		Produto2 celular = new Produto2("Xiomi Redmi", "Muito Legal", new BigDecimal("900"),celulares );
		Produto2 celular2 = new Produto2("Iphone", "New Generation", new BigDecimal("13000"),celulares );

		EntityManager em = JPAUtil.getEntitiyManager();
			
		ProdutoDao2 produtoDao = new ProdutoDao2(em);
		CategoriaDao categoriaDao = new CategoriaDao(em);

		em.getTransaction().begin();
		
		categoriaDao.cadastrar(celulares);
		produtoDao.cadastrar(celular);
		
		categoriaDao.cadastrar(celulares2);
		produtoDao.cadastrar(celular2);
		em.getTransaction().commit();
		em.close();
	}
}

