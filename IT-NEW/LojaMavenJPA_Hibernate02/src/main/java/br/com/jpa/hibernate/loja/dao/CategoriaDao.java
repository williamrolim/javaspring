package br.com.jpa.hibernate.loja.dao;

import javax.persistence.EntityManager;

import br.com.jpa.hibernate.loja.modelo.Categoria;

public class CategoriaDao {
	private EntityManager em;

	//inje��o de dependencias
	public CategoriaDao(EntityManager em) {
		this.em = em;
	}
	
	public void cadastrar (Categoria categoria) {
		this.em.persist(categoria);
	}
	
	public void atualizar(Categoria categoria) {
		this.em.merge(categoria); // for�o a categoria para ela estar em um estado managed
	}
	
	public void remove(Categoria categoria) {//essa categoria precisa estar em estado MANAGED (REMOVED)
		categoria = em.merge(categoria);//merge pegadinha, tenho que retribuir o objeto
		this.em.remove(categoria); 
	}
	
}
