package br.com.jpa.hibernate.loja.modelo;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

//mapeamento de uma entidade
@Entity
@Table(name = "produtos")
@NamedQuery(name = "Produto2.produtosPorCategoria", 
query = "SELECT p FROM Produto2 p  WHERE p.categoria.nome = :nome")
public class Produto2 {
	
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY) //quem vai gerar n�o � a aplica��o e sim o banco de dados
	private Long id;
	private String nome;	
	//@Column(name = "desc")
	private String descricao;
	private BigDecimal preco;
	//Localdate o hibernate j� sabe que vai gerar uma coluna data automatica no banco de dados
	private LocalDate dataCadastro = LocalDate.now();
	//verificar cardinalidade desse relacionamento
	@ManyToOne (fetch = FetchType.LAZY) //Muitos para um (muitos produtos vinculados com uma categira, uma categoria pode ter varios produtos)
	private Categoria categoria;
	
	//JPA precisa que tem um construtor padr�o
	public Produto2() {
		
	}
	
	public Produto2(String nome, String descricao, BigDecimal preco, Categoria categoria) {
		this.nome = nome;
		this.descricao = descricao;
		this.preco = preco;
		this.categoria = categoria;
	}

	public String getNome() {
		return nome;
	}


	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}


	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}


	public BigDecimal getPreco() {
		return preco;
	}


	public void setPreco(BigDecimal preco) {
		this.preco = preco;
	}


	public LocalDate getDataCadastro() {
		return dataCadastro;
	}


	public void setDataCadastro(LocalDate dataCadastro) {
		this.dataCadastro = dataCadastro;
	}


	public Categoria getCategoria() {
		return categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}
	

	@Override
	public String toString() {
		return "Produto [id=" + id + ", nome=" + nome + ", descricao=" + descricao + ", preco=" + preco
				+ ", dataCadastro=" + dataCadastro + ", categoria=" + this.categoria + "]";
	}
	
	
	
}
