package com.ex.projeto.api.controller;

import java.net.URI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.ex.projeto.api.model.Cliente;
import com.ex.projeto.api.model.Documento;
import com.ex.projeto.api.model.Endereco;
import com.ex.projeto.api.service.ClienteService;
import com.ex.projeto.api.service.DocumentoService;
import com.ex.projeto.api.service.EnderecoService;
@RestController
@RequestMapping("/api/v1/clientes")
public class ClienteController {

	@Autowired
	private ClienteService clienteService;
	
	@Autowired
	private EnderecoService enderecoService;
	
	@Autowired
	private DocumentoService documentoService;
	
	@PostMapping
	public ResponseEntity<Cliente>createCliente(@RequestBody Cliente cliente){
		Cliente clientes = clienteService.saved(cliente);
		URI locationURL = ServletUriComponentsBuilder
				.fromCurrentRequest()
				.path("/{id}")
				.buildAndExpand(clientes.getId())
				.toUri();
		return ResponseEntity.created(locationURL).build();
	}
	
	@GetMapping
	public ResponseEntity<Page<Cliente>>getAll(Pageable pageable){
		Page<Cliente> all = clienteService.getAll(pageable);
		return ResponseEntity.ok().body(all);
	}
	
	@GetMapping("{id}")
	public ResponseEntity<Cliente>getIdCliente(@PathVariable Long id){		
		Cliente cliente = clienteService.getById(id);
		return ResponseEntity.ok().body(cliente);		
	}
	
	@PostMapping("/{id}/enderecos")//o id será do cliente 
	public ResponseEntity<Cliente> createdAddressInCliente(@RequestBody Endereco enderecos, @PathVariable Long id){
		Cliente cli = clienteService.createAddresInClient(enderecos,id);//buscar o cliente, e inserir os dados do endereço no cliente
		URI location = ServletUriComponentsBuilder
				.fromCurrentRequest()
				.path("/{id}")
				.buildAndExpand(cli.getId())
				.toUri();
				
		return ResponseEntity.created(location).build();
	}
	
	   
		@GetMapping("/{idCliente}/enderecos")
		public ResponseEntity<Page<Endereco>>getAll2(Pageable pageable){
			Page<Endereco> all = enderecoService.getAll(pageable);
			return ResponseEntity.ok().body(all);
		}
	   

	    @GetMapping("/{idCliente}/enderecos/{idEndereco}")
	    public ResponseEntity<Endereco> allClientesAndAdress(@PathVariable Long idCliente, @PathVariable Long idEndereco) {

	        Endereco endereco = enderecoService.getAllClientsAndAdressWhithIds(idCliente, idEndereco);

	        return ResponseEntity.ok().body(endereco);
	    }
	    
	    
		@PostMapping("/{id}/documentos")//o id será do cliente 
		public ResponseEntity<Cliente> createdDocumentInCliente(@RequestBody Documento documentos, @PathVariable Long id){
			Cliente cli = clienteService.createDocumentInClient(documentos,id);
			URI location = ServletUriComponentsBuilder
					.fromCurrentRequest()
					.path("/{id}")
					.buildAndExpand(cli.getId())
					.toUri();
					
			return ResponseEntity.created(location).build();
		}
		
		@GetMapping("/{idCliente}/documentos")
		public ResponseEntity<Page<Documento>>getAll3(Pageable pageable){
			Page<Documento> all = documentoService.getAll(pageable);
			return ResponseEntity.ok().body(all);
		}
		
	    @GetMapping("/{idCliente}/documentos/{idDocumento}")
	    public ResponseEntity<Documento> allClientesAndDocuments(@PathVariable Long idCliente, @PathVariable Long idDocumento) {

	        Documento documents = documentoService.getAllClientsAndDocumentsWhithIds(idCliente, idDocumento);

	        return ResponseEntity.ok().body(documents);
	    }
	   
	    

}
