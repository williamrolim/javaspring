package com.ex.projeto.api.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.ex.projeto.api.model.enums.tipo_logradouro;
import com.ex.projeto.api.model.enums.TipoStatus;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
@Entity
//@JsonIdentityInfo(
//        generator = ObjectIdGenerators.PropertyGenerator.class,
//        property = "id")
public class Endereco implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String bairro;
	
	@Enumerated(EnumType.STRING)
	private tipo_logradouro tipo_logradouro;
	
	private String nome_rua;
	private Integer numero;

	@Enumerated(EnumType.STRING)
	private TipoStatus status;
	
	private String cidade;
	private String estado;

    @JsonBackReference
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cliente_id", nullable = false)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Cliente cliente;
	
	public Endereco() {
	}

	public Endereco(Long id, String bairro, tipo_logradouro tipo_logradouro, String nome_rua, Integer numero,
			TipoStatus status, String cidade, String estado, Cliente cliente) {
		this.id = id;
		this.bairro = bairro;
		this.tipo_logradouro = tipo_logradouro;
		this.nome_rua = nome_rua;
		this.numero = numero;
		this.status = status;
		this.cidade = cidade;
		this.estado = estado;
		this.cliente = cliente;
	}



	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public tipo_logradouro getTipo_logradouro() {
		return tipo_logradouro;
	}

	public void setTipo_logradouro(tipo_logradouro tipo_logradouro) {
		this.tipo_logradouro = tipo_logradouro;
	}

	public String getNome_rua() {
		return nome_rua;
	}

	public void setNome_rua(String nome_rua) {
		this.nome_rua = nome_rua;
	}

	public Integer getNumero() {
		return numero;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}


	public TipoStatus getTipoStatus() {
		return status;
	}

	public void setTipoStatus(TipoStatus status) {
		this.status = status;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public TipoStatus getStatus() {
		return status;
	}

	public void setStatus(TipoStatus status) {
		this.status = status;
	}

	
	
}
