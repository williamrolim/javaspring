﻿using System;
using System.Windows.Forms;

namespace Acompanhamento_Pessoal
{
    public partial class frmTaskList : Form
    {
        public frmTaskList()
        {
            InitializeComponent();
        }



        private void frmTaskList_Load(object sender, EventArgs e)
        {
            pnlForAdmin.Hide(); //Esconder o Painel admistrador
        }

        private void txtUserNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = General.isNumber(e);
        }

        private void txtDayAmount_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = General.isNumber(e);
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            frmTask frm = new frmTask();
            frm.Hide();
            frm.ShowDialog();
            this.Visible = true;
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            frmTask frm = new frmTask();
            frm.Hide();
            frm.ShowDialog();
            this.Visible = true;
        }
    }
}
