﻿using System.Windows.Forms;

namespace Acompanhamento_Pessoal
{
    public class General
    {
        public static bool isNumber(KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                return true;
            }
            else
                return false;
        }
    }
}
