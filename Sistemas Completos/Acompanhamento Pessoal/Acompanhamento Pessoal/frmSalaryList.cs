﻿using System;
using System.Windows.Forms;

namespace Acompanhamento_Pessoal
{
    public partial class frmSalaryList : Form
    {
        public frmSalaryList()
        {
            InitializeComponent();
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            frmSalary frm = new frmSalary();
            frm.Hide();
            frm.ShowDialog();
            this.Visible = true;
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            frmSalary frm = new frmSalary();
            frm.Hide();
            frm.ShowDialog();
            this.Visible = true;
        }
    }
}
