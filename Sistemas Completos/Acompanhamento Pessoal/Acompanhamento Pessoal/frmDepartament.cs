﻿using System;
using System.Windows.Forms;
using DAL;
using BLL;

namespace Acompanhamento_Pessoal
{
    public partial class frmDepartament : Form
    {
        public frmDepartament()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            DEPARTMENT department = new DEPARTMENT();
            department.DepartmentName = txtDepartment.Text;
            BLL.DepartmentBLL.AddDepartment(department);



        }
    }
}
