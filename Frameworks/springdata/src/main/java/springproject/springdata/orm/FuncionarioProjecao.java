package springproject.springdata.orm;

public interface FuncionarioProjecao {
	Integer getId();
	String getNome();
	Double getSalario();
}
