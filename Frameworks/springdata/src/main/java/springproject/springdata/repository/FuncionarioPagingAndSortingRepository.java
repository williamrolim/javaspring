package springproject.springdata.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import springproject.springdata.orm.Funcionario;
import springproject.springdata.orm.FuncionarioProjecao;

//Paginação
@Repository
public interface FuncionarioPagingAndSortingRepository
extends PagingAndSortingRepository<Funcionario, Integer> ,
JpaSpecificationExecutor<Funcionario>{
	
	@Query(value = "SELECT f.id, f.nome, f.salario FROM funcionarios f" , nativeQuery = true )
	List<FuncionarioProjecao>findFuncionarioSalario(); 
}
