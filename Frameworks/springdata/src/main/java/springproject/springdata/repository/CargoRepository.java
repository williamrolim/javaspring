package springproject.springdata.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import springproject.springdata.orm.Cargo;

@Repository                                      
public interface CargoRepository extends CrudRepository<Cargo, Integer> {

}
