package springproject.springdata.service;


import java.util.Scanner;

import org.springframework.stereotype.Service;

import springproject.springdata.orm.Cargo;
import springproject.springdata.repository.CargoRepository;

@Service
public class CrudCargoService {
	private final CargoRepository cargoRepository;
	private Boolean system = true;
	
	 public static Scanner scanner = new Scanner(System.in);

	public CrudCargoService(CargoRepository cargoRepository) {
		this.cargoRepository = cargoRepository;
	}

	public void inicial() {
		while (system) {
			System.out.println("Qual ação de cargo deseja executar ? ");
			System.out.println("0 - Sair");
			System.out.println("1 - Salvar");
			System.out.println("2 - Atualizar");
			System.out.println("3 - Excluir");
			System.out.println("4 - Visualizar Todos");
			int action = scanner.nextInt();			
			switch (action) {
			case 1:
				System.out.println("1 - SALVAR");
				salvar();
				break;
			case 2:
				System.out.println("2 - ATUALIZAR");
				atualizar();
				break;
			case 3:
				System.out.println("3 - EXCLUIR");
				excluir();
				break;
			case 4:
				System.out.println("4 - VISUALIZAR TODOS");
				visualizarTodos();
				break;
			case 5:
				System.out.println("0 - SAIR");
				system = false;
			default:
				system = false;
				break;
			}
		}
	}
	
	private void visualizarTodos() {
		System.out.println("\n" + cargoRepository.findAll());
		System.out.println("VISUALIZAÇÃO DE TODOS");
	}

	private void salvar() {
		scanner.nextLine();
		System.out.println("Descrição do cargo..:  ");
		String descricao = scanner.nextLine();
		Cargo cargo = new Cargo();
		cargo.setDescricao(descricao);
		cargoRepository.save(cargo);
		System.out.println("Cargo salvo com Sucesso!!!");
	}

	private void atualizar() {
		System.out.println("Digite o Codigo do cargo (ID)..: ");
		int id = scanner.nextInt();
		scanner.next();
		System.out.println("Descrição do cargo..:  ");
		String descricao = scanner.nextLine();
		Cargo cargo = new Cargo();
		cargo.setId(id);
		cargo.setDescricao(descricao);
		cargoRepository.save(cargo);
		System.out.println("Cargo Atualizado com Sucesso!!!");
	}

	private void excluir() {
		System.out.println("Digite o Codigo do cargo (ID)..: ");
		int id = scanner.nextInt();
		cargoRepository.deleteById(id);
		System.out.println("Cargo Exluido com Sucesso!!!");

	}
}
