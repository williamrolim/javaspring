package springproject.springdata.service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Scanner;

import org.springframework.stereotype.Service;

import springproject.springdata.orm.Funcionario;
import springproject.springdata.orm.FuncionarioProjecao;
import springproject.springdata.repository.FuncionarioPagingAndSortingRepository;
import springproject.springdata.repository.FuncionarioRepository;

@Service
public class RelatorioService {
	private Boolean system = true;

	public static Scanner scanner = new Scanner(System.in);
	private final FuncionarioRepository funcionarioRepository;
	private final FuncionarioPagingAndSortingRepository funcionarioParsing;
	private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

	public RelatorioService(FuncionarioRepository funcionarioRepository,FuncionarioPagingAndSortingRepository funcionarioParsing) {
		this.funcionarioRepository = funcionarioRepository;
		this.funcionarioParsing = funcionarioParsing;
	}

	public void inicial() {
		while (system) {
			System.out.println("Qual ação de cargo deseja executar ? ");
			System.out.println("0 - Sair");
			System.out.println("1 - Busca Funcionario Nome");
			System.out.println("2 - Busca Funcionario com o Salario Maior");
			System.out.println("3- Busca data de contratacao");
			System.out.println("4 - Pesquisa funcionario Salario");
			int action = scanner.nextInt();
			switch (action) {
			case 1:
				System.out.println("1 - Busca Funcionario Nome");
				buscaFuncionarioNome();
				break;
			case 2:
				System.out.println("2 - Funcionario Com o salario Maior são : ");
				buscarFuncionarioNomeSalarioMaiorData();
				break;
			case 3:
				System.out.println("3 - Busca pela data de contratacao : ");
				buscaFuncionarioDataContratacao();
				break;
								
			case 4:
				System.out.println("4 - Pesquisa funcionario Salario: ");
				pesquisaFuncionarioSalario();
				break;
			default:
				system = false;
				break;
			}
		}
	}

	public void buscaFuncionarioNome() {
		scanner.nextLine();
		System.out.println("Digite o nome para Consulta..: ");
		String nome = scanner.nextLine();
		List<Funcionario> list = funcionarioRepository.findByNome(nome);
		list.forEach(System.out::println);
	}

	public void buscarFuncionarioNomeSalarioMaiorData() {
		scanner.nextLine();
		System.out.println("Digite o nome para Pesquisar..: ");
		String nome = scanner.nextLine();

		System.out.println("Digite a data para Pesquisar..: ");
		String data = scanner.next();
		LocalDate localData = LocalDate.parse(data,formatter);

		System.out.println("Qual o salario deseja Pesquisar..: ");
		Double salario = scanner.nextDouble();

		List<Funcionario> list =
				funcionarioRepository.findNomeSalarioMaiorDataContratacao(nome, salario, localData);

		list.forEach(System.out::println);
	}

	private void buscaFuncionarioDataContratacao() {
		scanner.nextLine();
		System.out.println("Digite a data para Pesquisar..: ");
		String data = scanner.next();
		LocalDate localData = LocalDate.parse(data,formatter);

		List<Funcionario> list =
				funcionarioRepository.findDataContratoMaior(localData);

		list.forEach(System.out::println);
	}

	private void pesquisaFuncionarioSalario() {
		scanner.nextLine();
		List<FuncionarioProjecao> list =  funcionarioParsing.findFuncionarioSalario();
		list.forEach(fu -> System.out.println("Funcionario id: " + fu.getId() + " nome " + fu.getNome() + " Salario " + fu.getSalario()));
		}

}