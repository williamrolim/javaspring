package springproject.springdata.service;

import java.time.format.DateTimeFormatter;
import java.util.Scanner;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import springproject.springdata.orm.Funcionario;
import springproject.springdata.orm.UnidadeTrabalho;
import springproject.springdata.repository.CargoRepository;
import springproject.springdata.repository.FuncionarioRepository;
import springproject.springdata.repository.UnidadeTrabalhoRepository;

@Service
public class CrudUnidadeTrabalhoService {
	
	private final UnidadeTrabalhoRepository unidadeTrabalhoRepository;

	private Boolean system = true;
	
	public CrudUnidadeTrabalhoService(UnidadeTrabalhoRepository unidadeTrabalhoRepository) {
		this.unidadeTrabalhoRepository = unidadeTrabalhoRepository;
	}

	Scanner scanner = new Scanner(System.in);
	
	public void inicial() {
		while (system) {
			System.out.println("Qual ação de cargo deseja executar ? ");
			System.out.println("0 - Sair");
			System.out.println("1 - Salvar");
			System.out.println("2 - Atualizar");
			System.out.println("3 - Excluir");
			System.out.println("4 - Visualizar Todos");
			int action = scanner.nextInt();
			switch (action) {
			case 1:
				System.out.println("1 - SALVAR");
				salvar();
				break;
			case 2:
				System.out.println("2 - ATUALIZAR");
				atualizar();
				break;
			case 3:
				System.out.println("3 - EXCLUIR");
				excluir();
				break;
			case 4:
				System.out.println("4 - VISUALIZAR TODOS");
				visualizar();
				break;
			case 5:
				System.out.println("0 - SAIR");
				system = false;
			default:
				system = false;
				break;
			}
		}
	}
	
	private void salvar() {
		System.out.println("Digite o nome da unidade");
        String nome = scanner.next();

        System.out.println("Digite o endereco");
        String endereco = scanner.next();

        UnidadeTrabalho unidadeTrabalho = new UnidadeTrabalho();
        unidadeTrabalho.setDescricao(nome);
        unidadeTrabalho.setEndereco(endereco);

        unidadeTrabalhoRepository.save(unidadeTrabalho);
        System.out.println("Salvo");
	}
	
	private void atualizar() {
		System.out.println("Digite o id");
        Integer id = scanner.nextInt();

        System.out.println("Digite o nome da unidade");
        String nome = scanner.next();

        System.out.println("Digite o endereco");
        String endereco = scanner.next();

        UnidadeTrabalho unidadeTrabalho = new UnidadeTrabalho();
        unidadeTrabalho.setId(id);
        unidadeTrabalho.setDescricao(nome);
        unidadeTrabalho.setEndereco(endereco);

        unidadeTrabalhoRepository.save(unidadeTrabalho);
        System.out.println("Alterado");
	}
	
	private void visualizar() {
		Iterable<UnidadeTrabalho> unidades = unidadeTrabalhoRepository.findAll();
		unidades.forEach(unidade -> System.out.println(unidade));
	}
	
	private void excluir() {
		System.out.println("Id");
		int id = scanner.nextInt();
		unidadeTrabalhoRepository.deleteById(id);
		System.out.println("Deletado");
	}
	

	
}
