package springproject.springdata.service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import springproject.springdata.orm.Cargo;
import springproject.springdata.orm.Funcionario;
import springproject.springdata.orm.UnidadeTrabalho;
import springproject.springdata.repository.CargoRepository;
import springproject.springdata.repository.FuncionarioPagingAndSortingRepository;
import springproject.springdata.repository.FuncionarioRepository;
import springproject.springdata.repository.UnidadeTrabalhoRepository;

@Service
public class CrudFuncionarioService {
	private final FuncionarioRepository funcionarioRepository;
	private final CargoRepository cargoRepository;
	private final UnidadeTrabalhoRepository unidadeTrabalhoRepository;
	private final FuncionarioPagingAndSortingRepository funcionarioPaging;

	private Boolean system = true;
	private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

	Scanner scanner = new Scanner(System.in);

	public CrudFuncionarioService(FuncionarioRepository funcionarioRepository, CargoRepository cargoRepository,
			UnidadeTrabalhoRepository unidadeTrabalhoRepository,FuncionarioPagingAndSortingRepository funcionarioPaging) {
		this.funcionarioRepository = funcionarioRepository;
		this.cargoRepository = cargoRepository;
		this.unidadeTrabalhoRepository = unidadeTrabalhoRepository;
		this.funcionarioPaging = funcionarioPaging;
	}

	public void inicial() {
		while (system) {
			System.out.println("Qual ação de cargo deseja executar ? ");
			System.out.println("0 - Sair");
			System.out.println("1 - Salvar");
			System.out.println("2 - Atualizar");
			System.out.println("3 - Excluir");
			System.out.println("4 - Visualizar Todos");
			System.out.println("5 - Paginação Banco dados");
			System.out.println("6 - Paginação Banco Ordenando Por nomes");

			int action = scanner.nextInt();
			switch (action) {
			case 1:
				System.out.println("1 - SALVAR");
				salvar();
				break;
			case 2:
				System.out.println("2 - ATUALIZAR");
				//atualizar();
				break;
			case 3:
				System.out.println("3 - EXCLUIR");
				//excluir();
				break;
			case 4:
				System.out.println("4 - VISUALIZAR TODOS");
				visualizarTodos();
				break;
			case 5:
				System.out.println("5 - Paginacao Banco dados Funcionarios");
				paginacaoBancoDados();		
				break;
			case 6:
				System.out.println("6 - Ordenar por nomes");
				paginacaoBancoDadosOrdenacaoNomes();	
				break;				
				
			case 7:
				System.out.println("0 - SAIR");
				system = false;
				break;
			default:
				system = false;
				break;
			}
		}
	}


	public void visualizarTodos() {
		System.out.println(funcionarioRepository.findAll());
	}

	public void salvar() {
		scanner.nextLine();
		System.out.println("Digite o nome do Funcionario..: ");
		String nome = scanner.nextLine();
		System.out.println("Digite o cpf do Funcionario..: ");
		String cpf = scanner.nextLine();
		System.out.println("Digite o salario do Funcionario..: ");
		double salario = scanner.nextDouble();
		scanner.nextLine();
		System.out.println("Digite a data de contratacao Funcionario..: ");
		String dataContratacao = scanner.nextLine();
		System.out.println("Digite o codigo do cargo (id)..: ");
		Integer cargoId = scanner.nextInt();
		
		 List<UnidadeTrabalho> unidades = unidade();

	        Funcionario funcionario = new Funcionario();
	        funcionario.setNome(nome);
	        funcionario.setCpf(cpf);
	        funcionario.setSalario(salario);
	        funcionario.setDataContratacao(LocalDate.parse(dataContratacao, formatter));
	        Optional<Cargo> cargo = cargoRepository.findById(cargoId);
	        funcionario.setCargo(cargo.get());
	        funcionario.setUnidadeTrabalhos(unidades);

	        funcionarioRepository.save(funcionario);
	        System.out.println("Salvo");

	}

	private List<UnidadeTrabalho> unidade() {
		Boolean isTrue = true;
		List<UnidadeTrabalho> unidades = new ArrayList<>();

		while (isTrue) {
			System.out.println("Digite o unidadeId (Para sair digite 0)");
			Integer unidadeId = scanner.nextInt();

			if (unidadeId != 0) {
				Optional<UnidadeTrabalho> unidade = unidadeTrabalhoRepository.findById(unidadeId);
				unidades.add(unidade.get());
			} else {
				isTrue = false;
			}
		}

		return unidades;
	}
	
	public void paginacaoBancoDados() {
		System.out.println("Digite a pagina que você quer visualizar");
		Integer page = scanner.nextInt();
		                                     // unsorted Ordenação Default ordenação por id
		Pageable pageable = PageRequest.of(page, 5, Sort.unsorted());
		Page<Funcionario> funcionarios = funcionarioPaging.findAll(pageable);
		
		System.out.println(funcionarios);
		System.out.println("Pagina atual ..: " + funcionarios.getNumber());
		System.out.println("Total elemento..: " + funcionarios.getTotalElements());
		funcionarios.forEach(funcionario -> System.out.println(funcionario));		
	}
	
	public void paginacaoBancoDadosOrdenacaoNomes() {
		System.out.println("Digite a pagina que você quer visualizar");
		Integer page = scanner.nextInt();
		                                     // unsorted Ordenação Default ordenação por id
		Pageable pageable = PageRequest.of(page, 5, Sort.by	(Sort.Direction.ASC, "nome"));
		Page<Funcionario> funcionarios = funcionarioPaging.findAll(pageable);
		
		System.out.println(funcionarios);
		System.out.println("Pagina atual ..: " + funcionarios.getNumber());
		System.out.println("Total elemento..: " + funcionarios.getTotalElements());
		funcionarios.forEach(funcionario -> System.out.println(funcionario));		
	}
	

}
