package springproject.springdata;

import java.util.Scanner;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import springproject.springdata.service.CrudCargoService;
import springproject.springdata.service.CrudFuncionarioService;
import springproject.springdata.service.CrudUnidadeTrabalhoService;
import springproject.springdata.service.RelatorioFuncionarioDinamico;
import springproject.springdata.service.RelatorioService;

@SpringBootApplication
public class SpringdataApplication implements CommandLineRunner {

	private final CrudCargoService cargoService;
	private final CrudFuncionarioService funcionarioService;
	private final CrudUnidadeTrabalhoService unidadeTrabalhoService;

	private final RelatorioService relatorioService;
	private final RelatorioFuncionarioDinamico relatorioFuncionarioDinamico;
	private Boolean system = true;

	Scanner scanner = new Scanner(System.in);

	public SpringdataApplication(CrudCargoService cargoService, CrudFuncionarioService funcionarioService,CrudUnidadeTrabalhoService unidadeTrabalhoService,
			RelatorioService relatorioService,RelatorioFuncionarioDinamico relatorioFuncionarioDinamico) {
		this.cargoService = cargoService;
		this.funcionarioService = funcionarioService;
		this.unidadeTrabalhoService = unidadeTrabalhoService;
		this.relatorioService = relatorioService;
		this.relatorioFuncionarioDinamico = relatorioFuncionarioDinamico;
	}

	public static void main(String[] args) {
		// Ela faz com que o framework do Spring seja inicializado junto a nossa
		// aplicação.
		SpringApplication.run(SpringdataApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		while (system) {
			System.out.println("Qual função deseja executar?");
			System.out.println("0 - Sair");
			System.out.println("1 - Funcionario");
			System.out.println("2 - Cargo");
			System.out.println("3 - Unidade");
			System.out.println("4 - Relatorios" );
			System.out.println("5 - Relatorio Funcionario dinamico");

			Integer function = scanner.nextInt();

			switch (function) {
			case 1:
				funcionarioService.inicial();
				break;
			case 2:
				cargoService.inicial();
				break;
			case 3:
				unidadeTrabalhoService.inicial();
				break;
			case 4:
				relatorioService.inicial();
				break;
			case 5:
				relatorioFuncionarioDinamico.inicial(scanner);
				break;
			default:
				System.out.println("Finalizando");
				system = false;
				break;
			}
		}

	}
}
